<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pay_slip_temp".
 *
 * @property int $id
 * @property string $member_no
 * @property string $name
 * @property string $department
 * @property string $bank_no
 * @property double $salary
 * @property double $diligent_allowance
 * @property double $phone_allowance
 * @property double $other_allowance
 * @property double $total_salary
 * @property double $social_security
 * @property double $deduction
 * @property double $net_income
 * @property double $tax_income
 * @property double $net_total
 * @property double $funds_income
 * @property int $total_days
 * @property string $pay_date
 * @property string $month_expend
 * @property string $created_date
 * @property string $updated_date
 * @property string $file_name
 */
class PaySlipTemp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pay_slip_temp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['salary', 'diligent_allowance', 'phone_allowance', 'other_allowance', 'total_salary', 'social_security', 'deduction', 'net_income', 'tax_income', 'net_total', 'funds_income','cumulative_social_security'], 'number'],
            [['total_days'], 'integer'],
            [['pay_date', 'created_date', 'updated_date'], 'safe'],
            [['member_no'], 'string', 'max' => 100],
            [['name', 'department', 'month_expend', 'file_name'], 'string', 'max' => 255],
            [['bank_no'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_no' => 'Member No',
            'name' => 'Name',
            'department' => 'Department',
            'bank_no' => 'Bank No',
            'salary' => 'Salary',
            'diligent_allowance' => 'Diligent Allowance',
            'phone_allowance' => 'Phone Allowance',
            'other_allowance' => 'Other Allowance',
            'total_salary' => 'Total Salary',
            'social_security' => 'Social Security',
            'deduction' => 'Deduction',
            'net_income' => 'Net Income',
            'tax_income' => 'Tax Income',
            'net_total' => 'Net Total',
            'funds_income' => 'Funds Income',
            'total_days' => 'Total Days',
            'pay_date' => 'Pay Date',
            'month_expend' => 'Month Expend',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
            'file_name' => 'File Name',
            'cumulative_social_security' => 'cumulative_social_security'
        ];
    }
}
