<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member_account".
 *
 * @property int $id
 * @property string $account
 */
class MemberAccount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'member_account';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['account'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account' => 'Account',
        ];
    }

    public function getArea()
    {
        return $this->hasMany(Area::className(), ['account' => 'account']);
    }

}
