<?php

namespace app\models;

use app\models\Member;
use Yii;

/**
 * This is the model class for table "member_leave".
 *
 * @property int $id
 * @property int $type_id
 * @property int $member_id
 * @property string $start_date
 * @property string $end_date
 * @property string $img_url
 * @property string $remark
 * @property string $isActive
 * @property string $isDelete
 * @property string $created_date
 * @property string $updated_date
 */
class MemberLeave extends \yii\db\ActiveRecord
{

    public $member_no;
    public $name_th;
    public $surname_th;
    public $full_name;
    public $gender_name;
    public $department_name;
    public $position_name;
    public $type_name;
    public $subtype_name;
    public $memberundder_name;
    public $img;
    public $leave_type_id;
    public $leave_type;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'member_leave';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_id', 'member_id', 'start_date', 'end_date', 'remark'], 'required'],
            [['type_id', 'member_id'], 'integer'],
            [['start_date', 'end_date', 'created_date', 'updated_date'], 'safe'],
            [['remark'], 'string'],
            [['img_url'], 'string', 'max' => 255],
            [['isActive', 'isDelete'], 'string', 'max' => 1],
        ];
    }

/**
 * {@inheritdoc}
 */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'member_id' => 'Member ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'img_url' => 'Img Url',
            'remark' => 'Remark',
            'isActive' => 'Is Active',
            'isDelete' => 'Is Delete',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }

    public function getMember()
    {
        // \app\components\Helpers::Debig($this->hasOne(Member::className(), ['id' => 'member_id']));

        return $this->hasOne(Member::className(), ['id' => 'member_id']);
    }

    public function getLeaveType()
    {
        // \app\components\Helpers::Debig($this->hasOne(Member::className(), ['id' => 'member_id']));

        return $this->hasOne(LeaveType::className(), ['id' => 'type_id']);
    }


    public function afterFind()
    {

        $this->member_no = $this->member->member_no;
        $this->name_th = $this->member->name_th;
        $this->surname_th = $this->member->surname_th;
        $this->full_name = $this->member->name_th . ' ' . $this->member->surname_th;
        $this->gender_name = $this->member->gender_name;
        $this->department_name = $this->member->department_name;
        $this->position_name = $this->member->position_name;
        $this->type_name = $this->member->type_name;
        $this->subtype_name = $this->member->subtype_name;
        $this->memberundder_name = $this->member->memberundder_name;
        $this->img = $this->member->img;

        $this->leave_type_id = $this->leaveType->id;
        $this->leave_type = $this->leaveType->name;

    }

}
