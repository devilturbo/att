<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property int $id
 * @property int $notification_id
 * @property int $member_id
 * @property string $isRead
 * @property string $isDelete
 * @property string $create_date
 * @property string $isNotification
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notification_id', 'member_id'], 'integer'],
            [['create_date'], 'safe'],
            [['isRead', 'isDelete', 'isNotification'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notification_id' => 'Notification ID',
            'member_id' => 'Member ID',
            'isRead' => 'Is Read',
            'isDelete' => 'Is Delete',
            'create_date' => 'Create Date',
            'isNotification' => 'Is Notification',
        ];
    }
}
