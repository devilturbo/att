<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $img_url
 * @property string $name_th
 * @property string $name_en
 * @property string $address
 * @property string $tel
 * @property string $email
 * @property string $tax_id
 * @property double $hr_work
 * @property string $time_attendance
 * @property int $probation
 * @property int $errand_leave
 * @property int $errand_cut_leave
 * @property int $sick_leave
 * @property int $sick_cut_leave
 * @property int $hoilday_leave
 * @property string $maternity_leave
 * @property int $ordained_leave
 * @property string $created_date
 * @property string $updated_date
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'string'],
            [['hr_work'], 'number'],
            [['time_attendance', 'created_date', 'updated_date'], 'safe'],
            [['probation', 'errand_leave', 'errand_cut_leave', 'sick_leave', 'sick_cut_leave', 'hoilday_leave', 'ordained_leave'], 'integer'],
            [['img_url', 'name_th', 'name_en'], 'string', 'max' => 255],
            [['tel', 'email'], 'string', 'max' => 150],
            [['tax_id'], 'string', 'max' => 100],
            [['maternity_leave'], 'string', 'max' => 4],
            [['name_th'],'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img_url' => 'โลโก้บริษัท',
            'name_th' => 'ชื่อภาษาไทย',
            'name_en' => 'ชื่อภาษาอังกฤษ',
            'address' => 'ที่อยู่',
            'tel' => 'โทรศัพท์',
            'email' => 'อีเมล',
            'tax_id' => 'เลขประจำตังผู้เสียภาษี',
            'hr_work' => 'ชั่วโมงการทำงาน',
            'time_attendance' => 'เวลาเข้างานมตรฐาน',
            'probation' => 'ช่วงเวลาทดลองงาน',
            'errand_leave' => 'ลากิจ คิดเงิน(วัน)',
            'errand_cut_leave' => ' ลากิจ ไม่คิดเงิน(วัน)',
            'sick_leave' => 'ลาป่วย ไม่คิดเงิน(วัน)',
            'sick_cut_leave' => 'ลาป่วย คิดเงิน(วัน)',
            'hoilday_leave' => 'ลาพักร้อน(วัน)',
            'maternity_leave' => 'ลาคลอด(วัน)',
            'ordained_leave' => 'ลาบวช(วัน)',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }
}
