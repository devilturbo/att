<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member_punch_clock".
 *
 * @property int $id
 * @property int $member_id
 * @property int $area_id
 * @property string $punch_datetime
 * @property string $img
 * @property string $punch_type
 */
class MemberPunchClock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'member_punch_clock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'area_id','group_id'], 'integer'],
            [['punch_datetime'], 'safe'],
            [['img', 'punch_type', 'latitude', 'longitude'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'area_id' => 'Area ID',
            'punch_datetime' => 'Punch Datetime',
            'img' => 'Img',
            'punch_type' => 'Punch Type',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            // 'group_id' => 'group_id',
        ];
    }

        
    public function getMember()
    {
        return $this->hasOne(Member::className(), ['id' => 'member_id']);
    }

    public function getArea()
    {
        return $this->hasOne(Area::className(), ['id' => 'area_id']);
    }

    // public function getAreaGroup()
    // {
    //     return $this->hasOne(Area::className(), ['id' => 'group_id']);
    // }
    public $area_name;
    public $area_lat;
    public $area_long;
    public function afterFind()
    {
        $this->area_name  = $this->area->area_name;
        $this->area_lat  = $this->area->latitude;
        $this->area_long  = $this->area->longitude;
    }
}
