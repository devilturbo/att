<?php

namespace app\models;

use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;

use app\components\Helpers;
use app\components\Utilities;

class User extends \yii\db\ActiveRecord  implements IdentityInterface
{
    static $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $auth_company;
    public $auth_member;
    public $auth_add_member;
    public $auth_import_member;
    public $auth_group_member;
    public $auth_report_ba;
    public $auth_approve_order;
    public $auth_record_time;
    public $auth_record_leave;
    public $auth_update_time;
    public $auth_holidays;
    public $auth_location;
    public $auth_vacancy;
    public $auth_record_ot;
    public $auth_news;
    public $auth_export;
    public $auth_member_role;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['username', 'password'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'username' => 'Username',
            'password' => 'Password'
        ];
    }  
    /**
    * @inheritdoc
    */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
          return static::findOne(['access_token' => $token]);
    }
 
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static  function findByUsername($username)
    {
        // $users = self::getUser();

        // $auth  = Member::find()->where(['email'=>$username])->One();
        // $position = Position::find()->where(['id'=>$auth->position_id])->One();
        // $authMenu = Utilities::getListAuth($position);
        // $users[100]['username']  = $username;
        // $users[100]['password']  = $auth->phone_number;
        // $users = $users + $authMenu;
        // foreach ($users as $user) {
        //     if (strcasecmp($user['username'], $username) === 0) {
        //         return new static($users);
        //     }
        // }

        return static::findOne(['email' => $username]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->phone_number === $password;
    }

    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['id' => 'position_id']);
    }

    public function afterFind()
    {
        $this->auth_company = !empty($this->position->auth_company)?$this->position->auth_company:null;
        $this->auth_member = !empty($this->position->auth_member)?$this->position->auth_member:null;
        $this->auth_add_member = !empty($this->position->auth_add_member)?$this->position->auth_add_member:null;
        $this->auth_import_member = !empty($this->position->auth_import_member)?$this->position->auth_import_member:null;
        $this->auth_group_member = !empty($this->position->auth_group_member)?$this->position->auth_group_member:null;
        $this->auth_report_ba = !empty($this->position->auth_report_ba)?$this->position->auth_report_ba:null;
        $this->auth_approve_order = !empty($this->position->auth_approve_order)?$this->position->auth_approve_order:null;
        $this->auth_record_time   = !empty($this->position->auth_record_time)?$this->position->auth_record_time:null;
        $this->auth_record_leave  = !empty($this->position->auth_record_leave)?$this->position->auth_record_leave:null;
        $this->auth_update_time   = !empty($this->position->auth_update_time)?$this->position->auth_update_time:null;
        $this->auth_holidays      = !empty($this->position->auth_holidays)?$this->position->auth_holidays:null;
        $this->auth_location      = !empty($this->position->auth_location)?$this->position->auth_location:null;
        $this->auth_vacancy       = !empty($this->position->auth_vacancy)?$this->position->auth_vacancy:null;
        $this->auth_record_ot     = !empty($this->position->auth_record_ot)?$this->position->auth_record_ot:null;
        $this->auth_news   = !empty($this->position->auth_news)?$this->position->auth_news:null;
        $this->auth_export     = !empty($this->position->auth_export)?$this->position->auth_export:null;
        $this->auth_member_role   = !empty($this->position->auth_member_role)?$this->position->auth_member_role:null;
    }

    // public static function getUser()
    // {
    //     $users = [
    //         '100' => [
    //             'id' => "100",
    //             'username' => 'atadmin@gmail.com',
    //             'password' => 'Usx434psdf',
    //             'authKey' => 'test101key',
    //             'accessToken' => '101-token'
    //         ]
    //     ];
    //     return $users;
    // }
}