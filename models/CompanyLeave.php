<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_leave".
 *
 * @property int $id
 * @property int $company_id
 * @property string $days_leave
 * @property string $subject
 * @property string $remark
 * @property string $created_date
 * @property string $updated_date
 */
class CompanyLeave extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_leave';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id'], 'integer'],
            [['days_leave', 'created_date', 'updated_date'], 'safe'],
            [['remark'], 'string'],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'days_leave' => 'Days Leave',
            'subject' => 'Subject',
            'remark' => 'Remark',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }

    public function afterFind()
    {
        $this->days_leave = empty($this->days_leave)? null : date('d/m/Y',strtotime($this->days_leave));
    }
}
