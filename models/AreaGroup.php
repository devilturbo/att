<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "area_group".
 *
 * @property int $id
 * @property string $group_name
 * @property string $keeper
 * @property string $created_date
 */
class AreaGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $owner;
    public static function tableName()
    {
        return 'area_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['created_date'], 'safe'],
            [['group_name', 'keeper'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_name' => 'Group Name',
            'keeper' => 'Keeper',
            'created_date' => 'Created Date',
            [['isDelete'], 'string', 'max' => 1],

        ];
    }

    public function getAreaList()
    {
        return $this->hasMany(AreaList::className(), ['group_id' => 'id'])
        ->onCondition(['isDelete' => "N"]);
    }

    public function getMember()
    {
        return $this->hasOne(Member::className(), ['member_no' => 'keeper']);
    }

    public function afterFind()
    {
        //$this->brach_no.' '.
        $this->owner = $this->member->fullname;
    }

}
