<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "position".
 *
 * @property int $id
 * @property string $position_name
 * @property string $remark
 * @property int $life_cycle
 * @property int $auth_company
 * @property int $auth_member
 * @property int $auth_add_member
 * @property int $auth_import_member
 * @property int $auth_group_member
 * @property int $auth_report_ba
 * @property int $auth_approve_order
 * @property int $auth_record_time
 * @property int $auth_record_leave
 * @property int $auth_update_time
 * @property int $auth_holidays
 * @property int $auth_location
 * @property int $auth_vacancy
 * @property string $created_date
 * @property string $updated_date
 */
class Position extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'position';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['life_cycle', 'auth_company', 'auth_member', 'auth_add_member', 'auth_import_member', 'auth_group_member', 'auth_report_ba', 'auth_approve_order', 'auth_record_time', 'auth_record_leave', 'auth_update_time', 'auth_holidays', 'auth_location', 'auth_vacancy','auth_record_ot','auth_news','auth_export','auth_member_role','auth_backend_role'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['position_name', 'remark'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position_name' => 'ชื่อตำแหน่ง',
            'remark' => 'หมายเหตุ',
            'life_cycle' => 'กำหนดระยะเวลาสูงสุดในการคงอยู่ในระบบ',
            'auth_company' => 'Auth Company',
            'auth_member' => 'Auth Member',
            'auth_add_member' => 'Auth Add Member',
            'auth_import_member' => 'Auth Import Member',
            'auth_group_member' => 'Auth Group Member',
            'auth_report_ba' => 'Auth Report Ba',
            'auth_approve_order' => 'Auth Approve Order',
            'auth_record_time' => 'Auth Record Time',
            'auth_record_leave' => 'Auth Record Leave',
            'auth_update_time' => 'Auth Update Time',
            'auth_holidays' => 'Auth Holidays',
            'auth_record_ot' => 'auth_record_ot',
            'auth_location' => 'Auth Location',
            'auth_vacancy' => 'Auth Vacancy',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }
}
