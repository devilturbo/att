<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pay_slip".
 *
 * @property int $id
 * @property string $name
 * @property string $department
 * @property string $bank_no
 * @property double $salary
 * @property double $diligent_allowance
 * @property double $phone_allowance
 * @property double $other_allowance
 * @property double $total_salary
 * @property double $social_security
 * @property double $deduction
 * @property double $net_income
 * @property double $funds_income
 * @property int $total_days
 * @property string $pay_date
 * @property string $month_expend
 * @property string $created_date
 * @property string $updated_date
 * @property double $tax_income
 */
class PaySlip extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public  $file;
    public static function tableName()
    {
        return 'pay_slip';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['salary', 'diligent_allowance', 'phone_allowance', 'other_allowance', 'total_salary', 'social_security', 'deduction', 'net_income', 'funds_income', 'tax_income','net_total','cumulative_social_security'], 'number'],
            [['total_days'], 'integer'],
            [['pay_date', 'created_date', 'updated_date'], 'safe'],
            [['name', 'department', 'month_expend','file_name'], 'string', 'max' => 255],
            [['bank_no','member_no'], 'string', 'max' => 50],
            [['file'], 'file', 'extensions' => 'xls,xlsx', 'skipOnEmpty' => true]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'member_no' => 'member_no',
            'department' => 'Department',
            'bank_no' => 'Bank No',
            'salary' => 'Salary',
            'diligent_allowance' => 'Diligent Allowance',
            'phone_allowance' => 'Phone Allowance',
            'other_allowance' => 'Other Allowance',
            'total_salary' => 'Total Salary',
            'social_security' => 'Social Security',
            'deduction' => 'Deduction',
            'net_income' => 'Net Income',
            'funds_income' => 'Funds Income',
            'total_days' => 'Total Days',
            'pay_date' => 'Pay Date',
            'month_expend' => 'Month Expend',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
            'tax_income' => 'Tax Income',
            'file' => 'ระบุไฟล์ xls,xlsx',
            'file_name' => 'file_name',
            'net_total' => 'net_total',
            'cumulative_social_security' => 'cumulative_social_security'

        ];
    }

    public function afterFind(){
        $this->month_expend = empty($this->month_expend) ? null : date('m/Y',strtotime($this->month_expend));
        //$this->pay_date = empty($this->pay_date) ? null : date('d/m/Y',strtotime($this->pay_date));
    }
}
