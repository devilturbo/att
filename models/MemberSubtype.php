<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member_subtype".
 *
 * @property int $id
 * @property int $position_id
 * @property int $member_type_id
 * @property string $remark
 * @property string $time_attendance
 * @property string $attendance_type
 * @property string $ba
 * @property string $leave_codition
 * @property string $leave_type
 * @property string $leave_year
 * @property string $monday
 * @property string $thesday
 * @property string $wednesday
 * @property string $thursday
 * @property string $friday
 * @property string $saturday
 * @property string $sunday
 * @property string $substitution_holiday
 * @property string $location_type
 * @property string $created_date
 * @property string $updated_date
 */
class MemberSubtype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $attendance_type_name;
    public $leave_codition_name;
    public $leave_type_name;
    public $leave_year_name;
    public $substitution_holiday_name;
    public $normal_holiday;
    public $location_type_name;
    public $type_name;
    public static function tableName()
    {
        return 'member_subtype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['position_id', 'member_type_id','type_id'], 'integer'],
            [['remark','subtype_name'], 'string'],
            [['time_attendance', 'created_date', 'updated_date'], 'safe'],
            [['attendance_type', 'ba', 'leave_codition', 'leave_type', 'leave_year', 'monday', 'thesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'substitution_holiday', 'location_type','isLeave'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position_id' => 'Position ID',
            'member_type_id' => 'Member Type ID',
            'remark' => 'Remark',
            'time_attendance' => 'Time Attendance',
            'attendance_type' => 'Attendance Type',
            'ba' => 'Ba',
            'leave_codition' => 'Leave Codition',
            'leave_type' => 'Leave Type',
            'leave_year' => 'Leave Year',
            'monday' => 'Monday',
            'thesday' => 'Thesday',
            'wednesday' => 'Wednesday',
            'thursday' => 'Thursday',
            'friday' => 'Friday',
            'saturday' => 'Saturday',
            'sunday' => 'Sunday',
            'substitution_holiday' => 'Substitution Holiday',
            'location_type' => 'Location Type',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
            'isLeave' => 'isLeave',
        ];
    }

    public function getType()
    {
    return $this->hasOne(MemberType::className(), ['id' => 'type_id']);
    }

     public function init() {
        parent::init();
        if ($this->isNewRecord) {
            $this->time_attendance = '08:00';
            $this->attendance_type = 'Y';
            $this->leave_codition = 'N';
            $this->leave_type = 'N';
            $this->leave_year = 'N';
            $this->saturday = 'Y';
            $this->sunday = 'Y';
            $this->substitution_holiday = 'Y';
            $this->location_type = 'Y';
        }
    }

    public function afterFind(){
        $this->time_attendance = empty($this->time_attendance) ? null : date('H:i',strtotime($this->time_attendance));
        $this->attendance_type_name = $this->attendance_type == 'Y' ? 'เข้าทำงานปกติตามเวลาที่กำหนด' : 'เข้าออกงานไม่เป็นเวลา';
        $this->leave_codition_name = $this->leave_codition == 'Y'? 'อนุญาตให้ลาเป็นชั่วโมงได้' : 'อนุญาตให้ลาเป็นวันเท่านั้น';
        $this->leave_type_name = $this->leave_type == 'Y'? 'วันแรกที่เริ่มทำงาน' : 'วันแรกที่บรรจุเป็นพนักงานประจำ';
        $this->leave_year_name = $this->leave_codition == 'Y'? 'ใช่' : 'ไม่ใช่';
        $this->substitution_holiday_name = $this->substitution_holiday == 'Y' ? 'บังคับหยุดในวันทำงานถัดไป' : 'สะสมเป็นวันหยุดชดเชย';
        $this->location_type_name = $this->location_type == 'Y' ? 'บังคับ': 'ไม่บังคับ';
        $this->type_name = empty($this->type->type_name) ? null : $this->type->type_name;
        $normal_holiday = '';
        if($this->monday=='Y')
             $normal_holiday.= 'จันทร์,';
         if($this->thesday=='Y')
             $normal_holiday.= 'อังคาร,';
         if($this->wednesday=='Y')
             $normal_holiday.= 'พุธ,';
         if($this->thursday=='Y')
             $normal_holiday.= 'พฤหัส,';
         if($this->friday=='Y')
             $normal_holiday.= 'ศุกร์,';
         if($this->saturday=='Y')
             $normal_holiday.= 'เสาร์,';
         if($this->sunday=='Y')
             $normal_holiday.= 'อาทิตย์,';

         $this->normal_holiday = rtrim($normal_holiday, ", ");

    }
}
