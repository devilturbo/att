<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "title".
 *
 * @property int $id
 * @property string $title_th
 * @property string $short_title_th
 * @property string $short_title_en
 */
class Title extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'title';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_code', 'title_th', 'title_en'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_th' => 'Title Th',

        ];
    }
}
