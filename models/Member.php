<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member".
 *
 * @property int $id
 * @property int $member_no
 * @property int $title_th_id
 * @property int $title_en_id
 * @property int $gender_id
 * @property string $name_th
 * @property string $surname_th
 * @property string $name_en
 * @property string $surname_en
 * @property string $citizen_id
 * @property string $bookbank_no
 * @property string $bookbank_img
 * @property string $nickname
 * @property string $phone_number
 * @property string $line_id
 * @property string $email
 * @property string $relation_type
 * @property string $relation_name
 * @property string $relation_phone_number
 * @property string $start_work_date
 * @property string $full_time_work_date
 * @property string $address
 * @property int $status
 * @property string $img
 * @property int $position_id
 * @property int $member_under_id
 * @property int $member_type_id
 * @property int $agency_id
 */
class Member extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $fullname;
    public $fullname_en;
    public $agency_name;
    public $title_th;
    public $title_en;
    public $gender_name;
    public $department_name;
    public $position_name;
    public $type_name;
    public $subtype_name;
    public $memberundder_name;
    public $isLeave;
    public $zone_name;
    public $account_name;
    public $area_name;
    public $channel_name;
    public $status_name;
    public $start_date;
    public $end_date;
    public $updated_index;
    public $txtnormal_holiday;
    public $time_att;
    public $isBa = 'N';
    public $member_location_name;
    public $start_date_cal;
    public $isCheckLeave = 'Y';
    public static function tableName()
    {
        return 'member';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_no', 'title_th_id', 'title_en_id', 'gender_id', 'position_id', 'member_under_id', 'member_type_id', 'agency_id', 'member_subtype_id', 'department_id', 'area_id', 'channel_id', 'zone_id', 'account_id'], 'integer'],
            [['start_work_date', 'full_time_work_date', 'status', 'inActive_date', 'updated_date'], 'safe'],
            [['address'], 'string'],
            [['name_th', 'surname_th', 'name_en', 'surname_en', 'bookbank_no', 'bookbank_img', 'line_id', 'email', 'relation_type', 'relation_name', 'relation_phone_number', 'img'], 'string', 'max' => 255],
            [['citizen_id', 'phone_number'], 'string', 'max' => 20],
            [['nickname'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_no' => 'Member No',
            'title_th_id' => 'Title Th ID',
            'title_en_id' => 'Title En ID',
            'gender_id' => 'Gender ID',
            'name_th' => 'Name Th',
            'surname_th' => 'Surname Th',
            'name_en' => 'Name En',
            'surname_en' => 'Surname En',
            'citizen_id' => 'Citizen ID',
            'bookbank_no' => 'Bookbank No',
            'bookbank_img' => 'Bookbank Img',
            'nickname' => 'Nickname',
            'phone_number' => 'Phone Number',
            'line_id' => 'Line ID',
            'email' => 'Email',
            'relation_type' => 'Relation Type',
            'relation_name' => 'Relation Name',
            'relation_phone_number' => 'Relation Phone Number',
            'start_work_date' => 'Start Work Date',
            'full_time_work_date' => 'Full Time Work Date',
            'address' => 'Address',
            'status' => 'Status',
            'img' => 'Img',
            'position_id' => 'Position ID',
            'member_under_id' => 'Member Under ID',
            'member_type_id' => 'Member Type ID',
            'agency_id' => 'Agency ID',
            'member_subtype_id' => 'Member Subtype ID',
            'department_id' => 'Department ID',
        ];
    }

    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency_id']);
    }
    public function getTitleth()
    {
        return $this->hasOne(Title::className(), ['id' => 'title_th_id']);
    }
    public function getTitleen()
    {
        return $this->hasOne(Title::className(), ['id' => 'title_en_id']);
    }

    public function getGenders()
    {
        return $this->hasOne(Gender::className(), ['id' => 'gender_id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['id' => 'position_id']);
    }

    public function getMembertype()
    {
        return $this->hasOne(MemberType::className(), ['id' => 'member_type_id']);
    }

    public function getMembersubtype()
    {
        return $this->hasOne(MemberSubtype::className(), ['id' => 'member_subtype_id']);
    }

    public function getAreas()
    {
        return $this->hasOne(MemberArea::className(), ['id' => 'area_id']);
    }

    public function getZones()
    {
        return $this->hasOne(MemberZone::className(), ['id' => 'zone_id']);
    }

    public function getAccounts()
    {
        return $this->hasOne(MemberAccount::className(), ['id' => 'account_id']);
    }

    public function getChannels()
    {
        return $this->hasOne(MemberChannel::className(), ['id' => 'channel_id']);
    }

    public function getLocation()
    {
        return $this->hasOne(LocationWork::className(), ['member_id' => 'id']);
    }


    public function afterFind()
    {
        $company = $this->Company();
        $this->agency_name = empty($this->agency->agent_name) ? null : $this->agency->agent_name;
        $this->title_th = empty($this->titleth->title_th) ? null : $this->titleth->title_th;
        $this->title_en = empty($this->titleen->title_en) ? null : $this->titleen->title_en;
        $this->gender_name = empty($this->genders->gender_th) ? null : $this->genders->gender_th;
        $this->department_name = empty($this->department->department_name) ? null : $this->department->department_name;
        $this->position_name = empty($this->position->position_name) ? null : $this->position->position_name;
        $this->type_name = empty($this->membertype->type_name) ? null : $this->membertype->type_name;
        $this->subtype_name = empty($this->membersubtype->subtype_name) ? null : $this->membersubtype->subtype_name;
        $this->memberundder_name = empty($this->member_under_id) ? null : $this->getSup($this->member_under_id);
        $this->fullname = $this->name_th . ' ' . $this->surname_th;
        $this->fullname_en = $this->name_en . ' ' . $this->surname_en;
        $this->zone_name = empty($this->zones->zone) ? null : $this->zones->zone;
        $this->area_name = empty($this->areas->area) ? null : $this->areas->area;
        $this->channel_name = empty($this->channels->channel) ? null : $this->channels->channel;
        $this->account_name = empty($this->accounts->account) ? null : $this->accounts->account;
        $this->status_name = $this->status == 'Y' ? 'ใช้งาน' : 'ไม่ใช้งาน';
        $this->inActive_date = $this->status == 'N' ? $this->inActive_date : null;
        $this->start_date = empty($this->start_work_date) ? null : date('d/m/Y', strtotime($this->start_work_date . " +" . $company->probation . " days"));
        $this->start_date_cal = empty($this->start_work_date) ? null : date('Y-m-d', strtotime($this->start_work_date . " +" . $company->probation . " days"));
        $this->end_date = $this->inActive_date ? date('d/m/Y', strtotime($this->inActive_date)) : null;
        $this->updated_index = empty($this->updated_date) ? null : date('d/m/Y', strtotime($this->updated_date)) . ' - ' . date('H:i:s', strtotime($this->updated_date));
        $this->isBa = empty($this->membertype->ba) ? null : $this->membertype->ba;
        $this->member_location_name = empty($this->location->location_name) ? null : $this->location->location_name;
        if (!empty($this->membersubtype->ba)) {
            $this->isBa = $this->membersubtype->ba;
        }

        if(!empty($this->membertype->leave_type)){
            $this->isLeave = $this->membertype->leave_type;
        }


        if(!empty($this->membersubtype->leave_type)){
            $this->isLeave = $this->membersubtype->leave_type;
        }

        if(!empty($this->membertype->isLeave)){
            $this->isCheckLeave = $this->membertype->isLeave;
        }
        if(!empty($this->membersubtype->isLeave)){
            $this->isCheckLeave = $this->membersubtype->isLeave;
        }

        $this->txtnormal_holiday = null;
        if (!empty($this->membertype->normal_holiday)) {
            $this->txtnormal_holiday = $this->membertype->normal_holiday;
        }
        if (!empty($this->membersubtype->normal_holiday)) {
            $this->txtnormal_holiday = $this->membersubtype->normal_holiday;
        }


        $this->time_att = null;
        if (!empty($this->membertype->time_attendance)) {
            $this->time_att = $this->membertype->time_attendance;
        }
        if (!empty($this->membersubtype->normal_holiday)) {
            $this->time_att = $this->membersubtype->time_attendance;
        }

    }

    public function getSup($id)
    {
        $sql = 'select (select title_th from title where title.id = member.title_th_id limit 1) as title , name_th,surname_th from member where id =' . $id;
        $res = Yii::$app->db->createCommand($sql)->queryAll();
        return $res[0]['title'] . ' ' . $res[0]['name_th'] . ' ' . $res[0]['surname_th'];

    }

    public function Company()
    {
        $model = Company::find()->One();
        return $model;
    }

}
