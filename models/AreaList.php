<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "area_list".
 *
 * @property int $id
 * @property int $area_id
 * @property string $created_date
 * @property int $group_id
 */
class AreaList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'area_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['area_id', 'group_id'], 'integer'],
            [['created_date'], 'safe'],
            [['isDelete'], 'string', 'max' => 1],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area_id' => 'Area ID',
            'created_date' => 'Created Date',
            'group_id' => 'Group ID',
        ];
    }
    
    public function getArea()
    {
        return $this->hasOne(Area::className(), ['id' => 'area_id'])
            ->onCondition(['isDelete' => "N"]);
    }
}
