<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member_temp_import".
 *
 * @property int $id
 * @property string $title
 * @property string $gender
 * @property string $name_th
 * @property string $surname_th
 * @property string $name_en
 * @property string $surname_en
 * @property string $citizen_id
 * @property string $nickname
 * @property string $phone_number
 * @property string $email
 * @property string $agency
 * @property string $department
 * @property string $position
 * @property string $mebertype
 * @property string $merber_subtype
 * @property string $contact
 * @property string $under
 * @property string $area
 * @property string $distinct_area
 * @property string $chanel
 * @property string $work_station
 * @property string $branch_station
 * @property string $start_date
 * @property string $end_date
 * @property int $work_life
 * @property string $status
 * @property string $created_date
 * @property string $updated_date
 */
class MemberTempImport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public  $file;
    public static function tableName()
    {
        return 'member_temp_import';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['start_date', 'end_date', 'created_date', 'updated_date','address','bookbank_no'], 'safe'],
            [['work_life'], 'integer'],
            [['title', 'gender'], 'string', 'max' => 11],
            [['name_th', 'surname_th', 'name_en', 'surname_en', 'email', 'agency', 'department', 'position', 'mebertype', 'merber_subtype', 'contact', 'under', 'area', 'distinct_area', 'chanel', 'work_station', 'branch_station'], 'string', 'max' => 255],
            [['citizen_id', 'phone_number'], 'string', 'max' => 20],
            [['nickname'], 'string', 'max' => 150],
            [['status'], 'string', 'max' => 1],
            [['file'], 'file', 'extensions' => 'xls,xlsx', 'skipOnEmpty' => true]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'gender' => 'Gender',
            'name_th' => 'Name Th',
            'surname_th' => 'Surname Th',
            'name_en' => 'Name En',
            'surname_en' => 'Surname En',
            'citizen_id' => 'Citizen ID',
            'nickname' => 'Nickname',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'agency' => 'Agency',
            'department' => 'Department',
            'position' => 'Position',
            'mebertype' => 'Mebertype',
            'merber_subtype' => 'Merber Subtype',
            'contact' => 'Contact',
            'under' => 'Under',
            'area' => 'Area',
            'distinct_area' => 'Distinct Area',
            'chanel' => 'Chanel',
            'work_station' => 'Work Station',
            'branch_station' => 'Branch Station',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'work_life' => 'Work Life',
            'status' => 'Status',
            'address' => 'ที่อยู่ตามบัตร ปชช',
            'bookbank_no' => 'บัญชี ธนาคาร',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }
}
