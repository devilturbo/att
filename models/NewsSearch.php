<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\News;

/**
 * NewsSearch represents the model behind the search form of `app\models\News`.
 */
class NewsSearch extends News
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id', 'position_id', 'member_type_id', 'member_subtype_id', 'member_area_id', 'member_zone_id', 'member_chanel_id', 'member_account_id'], 'integer'],
            [['topic', 'topic_img', 'details', 'news_date', 'note', 'status', 'createDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'news_id' => $this->news_id,
            'news_date' => $this->news_date,
            'position_id' => $this->position_id,
            'member_type_id' => $this->member_type_id,
            'member_subtype_id' => $this->member_subtype_id,
            'member_area_id' => $this->member_area_id,
            'member_zone_id' => $this->member_zone_id,
            'member_chanel_id' => $this->member_chanel_id,
            'member_account_id' => $this->member_account_id,
            'createDate' => $this->createDate,
        ]);

        $query->andFilterWhere(['like', 'topic', $this->topic])
            ->andFilterWhere(['like', 'topic_img', $this->topic_img])
            ->andFilterWhere(['like', 'details', $this->details])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
