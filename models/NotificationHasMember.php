<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notification_has_member".
 *
 * @property int $id
 * @property string $detail
 * @property int $module_id
 * @property string $module
 * @property int $member_id
 * @property int $position_id
 * @property int $member_type_id
 * @property int $member_subtype_id
 * @property int $member_area_id
 * @property int $member_zone_id
 * @property int $member_chanel_id
 * @property int $member_account_id
 * @property string $create_date
 */
class NotificationHasMember extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification_has_member';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['module_id', 'member_id', 'position_id', 'member_type_id', 'member_subtype_id', 'member_area_id', 'member_zone_id', 'member_chanel_id', 'member_account_id'], 'integer'],
            [['create_date'], 'safe'],
            [['detail', 'module'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'detail' => 'Detail', // หัวข้อ
            'module_id' => 'Module ID', // id news
            'module' => 'Module', // news, leave
            'member_id' => 'Member ID', 
            'position_id' => 'Position ID',
            'member_type_id' => 'Member Type ID',
            'member_subtype_id' => 'Member Subtype ID',
            'member_area_id' => 'Member Area ID',
            'member_zone_id' => 'Member Zone ID',
            'member_chanel_id' => 'Member Chanel ID',
            'member_account_id' => 'Member Account ID',
            'create_date' => 'Create Date',
        ];
    }
}
