<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Member;

/**
 * MemberSearch represents the model behind the search form of `app\models\Member`.
 */
class MemberSearch extends Member
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'gender_id', 'status'], 'integer'],
            [['name_th', 'surname_th', 'name_en', 'surname_en', 'citizen_id', 'bookbank_no', 'bookbank_img', 'nickname', 'phone_number', 'line_id', 'email', 'relation_type', 'relation_name', 'relation_phone_number', 'start_work_date', 'full_time_work_date', 'address', 'img'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Member::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
          
            'gender_id' => $this->gender_id,
            'start_work_date' => $this->start_work_date,
            'full_time_work_date' => $this->full_time_work_date,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name_th', $this->name_th])
            ->andFilterWhere(['like', 'surname_th', $this->surname_th])
            ->andFilterWhere(['like', 'name_en', $this->name_en])
            ->andFilterWhere(['like', 'surname_en', $this->surname_en])
            ->andFilterWhere(['like', 'citizen_id', $this->citizen_id])
            ->andFilterWhere(['like', 'bookbank_no', $this->bookbank_no])
            ->andFilterWhere(['like', 'bookbank_img', $this->bookbank_img])
            ->andFilterWhere(['like', 'nickname', $this->nickname])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'line_id', $this->line_id])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'relation_type', $this->relation_type])
            ->andFilterWhere(['like', 'relation_name', $this->relation_name])
            ->andFilterWhere(['like', 'relation_phone_number', $this->relation_phone_number])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'img', $this->img]);

        return $dataProvider;
    }
}
