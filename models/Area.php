<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "area".
 *
 * @property int $id
 * @property string $brach_no
 * @property string $account
 * @property string $local_name
 * @property string $longitude
 * @property string $latitude
 * @property double $distance
 * @property string $area
 * @property string $zone
 * @property string $channel
 * @property string $created_by
 * @property string $created_date
 */
class Area extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $area_name;
    public $display;
    public static function tableName()
    {
        return 'area';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['distance'], 'number'],
            [['created_date'], 'safe'],
            [['brach_no'], 'string', 'max' => 100],
            [['isDelete'], 'string', 'max' => 1],
            [['account', 'local_name', 'longitude', 'latitude', 'area', 'zone', 'channel', 'created_by','img','type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brach_no' => 'Brach No',
            'account' => 'Account',
            'local_name' => 'Local Name',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'distance' => 'Distance',
            'area' => 'Area',
            'zone' => 'Zone',
            'channel' => 'Channel',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'img' => "image",
            'type'=>'type'
        ];
    }

    public function afterFind()
    {
            //$this->brach_no.' '.
            $this->area_name = $this->brach_no.' '.$this->account.' '.$this->local_name;
            $this->display = $this->brach_no.' / '.$this->account.' / '.$this->local_name;
        }
}
