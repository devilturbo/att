<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agency".
 *
 * @property int $id
 * @property string $agent_name
 * @property string $detail
 * @property string $remark
 * @property string $created_date
 * @property string $updated_date
 */
class Agency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['detail', 'remark'], 'string'],
            [['created_date', 'updated_date'], 'safe'],
            [['agent_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agent_name' => 'Agent Name',
            'detail' => 'Detail',
            'remark' => 'Remark',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }
}
