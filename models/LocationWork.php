<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "location_work".
 *
 * @property int $id
 * @property int $member_id
 * @property int $area_id
 * @property string $created_date
 */
class LocationWork extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $location_name;
    public static function tableName()
    {
        return 'location_work';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id'], 'integer'],
            [['created_date', 'area_id'], 'safe'],
        ];
    }

    public function getArea()
    {
        return $this->hasOne(Area::className(), ['id' => 'area_id'])
            ->onCondition(['isDelete' => "N"]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'area_id' => 'Area ID',
            'created_date' => 'Created Date',
        ];
    }

    public function getAreas()
    {
        return $this->hasOne(Area::className(), ['id' => 'area_id'])
            ->onCondition(['isDelete' => "N"]);
    }

    public function getAreasDetail()
    {
        return $this->hasOne(Area::className(), ['brach_no' => 'area_id'])
            ->onCondition(['isDelete' => "N"]);
    }

    public $type;
    public $brach_no;
    public $account;
    public $img;
    public $latitude;
    public $longitude;
    public $local_name;
    public function afterFind()
    {

        $this->location_name = empty($this->areas->area_name) ? null : $this->areas->area_name;
        if (empty($this->location_name)) {
            $this->location_name = empty($this->areasDetail) ? null : $this->areasDetail->area_name;
        }

        $this->type = empty($this->areasDetail) ? null : $this->areasDetail->type;
        $this->local_name = empty($this->areasDetail) ? null : $this->areasDetail->local_name;
        $this->brach_no = empty($this->areasDetail) ? null : $this->areasDetail->brach_no;
        $this->account = empty($this->areasDetail) ? null : $this->areasDetail->account;
        $this->img = empty($this->areasDetail) ? null : $this->areasDetail->img;
        $this->latitude = empty($this->areasDetail) ? null : $this->areasDetail->latitude;
        $this->longitude = empty($this->areasDetail) ? null : $this->areasDetail->longitude;

    }

}
