<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $news_id
 * @property string $topic
 * @property string $topic_img
 * @property string $details
 * @property string $news_date
 * @property int $position_id
 * @property int $member_type_id
 * @property int $member_subtype_id
 * @property int $member_area_id
 * @property int $member_zone_id
 * @property int $member_chanel_id
 * @property int $member_account_id
 * @property string $note
 * @property string $status
 * @property string $createDate
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $position_name;
    public $type_name;
    public $subtype_name;
    public $area_name;
    public $zone_name;
    public $channel_name;
    public $account_name;

    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['topic', 'topic_img', 'details', 'news_date', 'position_id', 'member_type_id', 'member_subtype_id', 'member_area_id', 'member_zone_id', 'member_chanel_id', 'member_account_id', 'note', 'createDate'], 'required'],
            [['details', 'note'], 'string'],
            [['news_date', 'createDate'], 'safe'],
            [['position_id', 'member_type_id', 'member_subtype_id', 'member_area_id', 'member_zone_id', 'member_chanel_id', 'member_account_id'], 'integer'],
            [['topic', 'topic_img', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'news_id' => 'News ID',
            'topic' => 'Topic',
            'topic_img' => 'Topic Img',
            'details' => 'Details',
            'news_date' => 'News Date',
            'position_id' => 'Position ID',
            'member_type_id' => 'Member Type ID',
            'member_subtype_id' => 'Member Subtype ID',
            'member_area_id' => 'Member Area ID',
            'member_zone_id' => 'Member Zone ID',
            'member_chanel_id' => 'Member Chanel ID',
            'member_account_id' => 'Member Account ID',
            'note' => 'Note',
            'status' => 'Status',
            'createDate' => 'Create Date',
        ];
    }

    // public function getPosition()
    // {
    //   return $this->hasOne(Position::className(), ['id' => 'position_id']);
    // }
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['id' => 'position_id']);
    }

    public function getMembertype()
    {
        return $this->hasOne(MemberType::className(), ['id' => 'member_type_id']);
    }

    public function getMemberSubtype()
    {
        return $this->hasOne(MemberSubtype::className(), ['id' => 'member_subtype_id']);
    }

    public function getAreas()
    {
        return $this->hasOne(MemberArea::className(), ['id' => 'member_area_id']);
    }

    public function getZones()
    {
        return $this->hasOne(MemberZone::className(), ['id' => 'member_zone_id']);
    }

    public function getChannels()
    {
        return $this->hasOne(MemberChannel::className(), ['id' => 'member_chanel_id']);
    }

    public function getAccounts()
    {
        return $this->hasOne(MemberAccount::className(), ['id' => 'member_account_id']);
    }

    public function afterFind()
    {
        $this->position_name = empty($this->position->position_name) ? null : $this->position->position_name;
        $this->type_name = empty($this->membertype->type_name) ? null : $this->membertype->type_name;
        $this->subtype_name = empty($this->memberSubtype->subtype_name) ? null : $this->memberSubtype->subtype_name;
        $this->zone_name = empty($this->zones->zone) ? null : $this->zones->zone;
        $this->area_name = empty($this->areas->area) ? null : $this->areas->area;
        $this->channel_name = empty($this->channels->channel) ? null : $this->channels->channel;
        $this->account_name = empty($this->accounts->account) ? null : $this->accounts->account;
    }
}
