<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@att';
    public $sourcePath = '@att';
    public $css = [
        'vendors/mdi/css/materialdesignicons.min.css',
        'vendors/css/vendor.bundle.base.css',

        'vendors/jqvmap/jqvmap.min.css',
        'vendors/flag-icon-css/css/flag-icon.min.css',
        'https://fonts.googleapis.com/css?family=Kanit:200,300&display=swap',
        'vendors/datatables.net-bs4/dataTables.bootstrap4.css',
        'css/dataTables.bootstrap4.min.css',
        'css/datepicker.css',
        'css/fixedHeader.dataTables.min.css',
        'css/fixedColumns.dataTables.min.css',
        'css/datepicker.css?v=1001',
        'css/select.css',
        'css/prism.css',
        'css/chosen.css',
        'css/mapbox-gl.css',

        'css/themify-icons/themify-icons.css',
        'css/styles.css',
   
        'vendors/select2/select2.min.css',
        'vendors/select2-bootstrap-theme/select2-bootstrap.min.css',
        // 'css/vertical-layout-light/style.css',
        'css/style.css',
        'css/multi.min.css',
        'vendors/summernote/dist/summernote-bs4.css',
        'vendors/quill/quill.snow.css',
        'vendors/simplemde/simplemde.min.css',
        'vendors/bootstrap-datepicker/bootstrap-datepicker.min.css',
        'vendors/jquery-tags-input/jquery.tagsinput.min.css',
        'vendors/jquery-file-upload/uploadfile.css',
        'vendors/dropify/dropify.min.css',

//
        //        'vendors/datatables.net-bs4/dataTables.bootstrap4.css',
        //        'vendors/flag-icon-css/css/flag-icon.min.css',
        //        'vendors/jqvmap/jqvmap.min.css',

    ];
    public $js = [

        'vendors/js/vendor.bundle.base.js',
        'vendors/jquery.flot/jquery.flot.js',
        'vendors/jquery.flot/jquery.flot.pie.js',
        'vendors/jquery.flot/jquery.flot.resize.js',
        'vendors/jqvmap/jquery.vmap.min.js',
        'vendors/jqvmap/maps/jquery.vmap.world.js',
        'vendors/jqvmap/maps/jquery.vmap.usa.js',
        'vendors/peity/jquery.peity.min.js',
        'js/chosen.jquery.js',
        'js/prism.js',
        'js/init.js',
        'vendors/datatables.net/jquery.dataTables.js',
        'vendors/datatables.net-bs4/dataTables.bootstrap4.js',
        'vendors/typeahead.js/typeahead.bundle.min.js',
        'vendors/select2/select2.min.js',
        'js/off-canvas.js',
        'js/dataTables.fixedHeader.min.js',
        'js/moment.min.js',
        'js/bootstrap-datepicker.min.js',
        'js/dataTables.fixedColumns.min.js',
        'js/datepicker.js',
        'js/typeahead.js',
        'js/select2.js',
        'js/lazyload.js',
        'js/dashboard.js',
        'js/mapbox-gl.js',
        'js/gmap.js',
        'js/prettify.js',
        'js/no-ui-slider.js',
        'js/ion-range-slider.js',
        'vendors/ion-rangeslider/js/ion.rangeSlider.min.js',
        'vendors/nouislider/nouislider.min.js',
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyC9rdiZU4nruvkkL6qSI6zNtCUSv-neWpg',
        // 'js/scripts.js',
        // 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js',

        'vendors/chart.js/Chart.min.js',
        'js/multi.min.js',
        'https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js',
        'https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js',
        'vendors/summernote/dist/summernote-bs4.min.js',
        'vendors/tinymce/tinymce.min.js',
        'vendors/quill/quill.min.js',
        'vendors/simplemde/simplemde.min.js',
        'vendors/bootstrap-datepicker/bootstrap-datepicker.min.js',
        'js/formpickers.js',
        'js/x-editable.js',
        // 'js/form-repeater.js',
        'js/editorDemo.js',
    ];
    public $depends = [

        'yii\web\YiiAsset',

        //'yii\bootstrap\BootstrapAsset',

    ];
}
