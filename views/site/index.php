  <?php
use app\components\Utilities;
$this->title = "Attendant";
// Helpers::Debig(Yii::$app->user->identity);
// date_default_timezone_set("Asia/Jakarta");
date_default_timezone_set("Asia/Bangkok");
// $datetime = date("Y-m-d H:i:s");
// $timestamp = strtotime($datetime);
// $time = $timestamp - (7 * 60 * 60);
// $datetime = date("Y-m-d H:i:s", $time);
// $date = new DateTime();
// $timeZone = $date->getTimezone();
// echo $timeZone->getName() .  " " . $datetime;
?>


  <div class="dashboard-header d-flex flex-column grid-margin">
      <!-- header -->
      <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-1">
          <div class="d-flex align-items-center">
              <h4 class="mb-0 font-weight-bold mr-2">Attendant</h4>
              <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">Home</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">Dashboard</p>
              </div>
          </div>



          <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">

              <div class="btn-group mt-3 mt-md-0" role="group" aria-label="Button group">
                  <a href="<?=Yii::$app->request->baseUrl?>/">
                      <button type="button" class="btn btn-outline-secondary border">
                          <i class="mdi mdi-reload text-body"></i>
                      </button>
                  </a>
              </div>
          </div>
      </div>
  </div><!-- end header -->
  <!-- Main content --->
  <div class="row mb-2">
      <p class=" ml-2 mt-2 text-muted">Last Sync <?=$modelSync?></p>
      <span class="ml-auto mr-3" style="width: 350px;">

          <form action="<?=Yii::$app->request->baseUrl?>/">
              <div class="row">
                  <div class="col-9">
                      <span>ค้นหาตามวันที่</span>
                      <div id="datepicker-popup" class="input-group date datepicker">
                          <input type="text" id="date" name="date"
                              value="<?=empty($_GET['date']) ? Yii::$app->Utilities->formatDate('m/d/Y', date('Y-m-d')) : Yii::$app->Utilities->formatDate('m/d/Y', $_GET['date'])?>"
                              class="form-control" data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น">
                          <span class="input-group-addon input-group-append border-left">
                              <span class="mdi mdi-calendar input-group-text"></span>
                          </span>
                      </div>
                  </div>
                  <div class="col-2">
                      <span>&nbsp;</span>
                      <button type="submit" class="btn btn-outline-primary btn-sm">ค้นหา</button>
                  </div>
              </div>
          </form>



      </span>
  </div>

  <div class="row mb-4">
      <div class="col-sm-12 col-md-4 col-lg-3 mb-2">
          <div class="card shadow-sm">
              <div class="card-body">
                  <div class="row">
                      <p class="card-title ml-3 mt-1 mb-3 text-muted ">พนักงานทั้งหมด</p>

                  </div>
                  <div class="row">
                      <span class="ml-3"><span class="h2 mr-2 text-blue" id="memberCount_1">
                              <span class="spinner-border" role="status">
                                  <span class="sr-only">Loading...</span>
                              </span>
                          </span>คน</span>

                  </div>
              </div>
          </div>
      </div>

      <div class="col-sm-12 col-md-4 col-lg-3 mb-2">
          <div class="card shadow-sm">
              <div class="card-body">
                  <div class="row">
                      <p class="card-title ml-3 mt-1 mb-3 text-muted ">พนักงานที่ใช้งานวันนี้</p>

                  </div>
                  <div class="row">
                      <span class="ml-3">
                          <span class="h2 mr-2 text-success" id="total">
                              <?='' //$total?>
                              <span class="spinner-border" role="status">
                                  <span class="sr-only">Loading...</span>
                              </span>
                          </span>คน
                      </span>
                  </div>
              </div>
          </div>
      </div>

      <div class="col-sm-12 col-md-4 col-lg-3 mb-2">
          <div class="card shadow-sm">
              <div class="card-body">
                  <div class="row">
                      <p class="card-title ml-3 mt-1 mb-3 text-muted ">พนักงานที่หยุด</p>
                  </div>
                  <div class="row">
                      <span class="ml-3">
                          <span class="h2 mr-2 text-red" id="dayOff">
                              <?='' //$dayOffToday?>
                              <span class="spinner-border" role="status">
                                  <span class="sr-only">Loading...</span>
                              </span>
                          </span>คน</span>

                  </div>
              </div>
          </div>
      </div>
      <div class="col-sm-12 col-md-4 col-lg-3 mb-2">
          <div class="card shadow-sm">
              <div class="card-body">
                  <div class="row">
                      <p class="card-title ml-3 mt-1 mb-3 text-muted ">พนักงานที่ลา</p>
                  </div>
                  <div class="row">
                      <span class="ml-3"><span class="h2 mr-2 text-warning" id="leaveToday">
                              <?='' //$leaveToday?>
                              <span class="spinner-border" role="status">
                                  <span class="sr-only">Loading...</span>
                              </span>
                          </span>คน</span>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="row mb-4 ">
      <div class="col-12 ">
          <table class="table table-bordered" style="text-align: center;" cellspacing="0">
              <thead>
                  <tr>
                      <th>Type</th>
                      <th>Total</th>
                      <th>Sup</th>
                      <th>PI</th>
                      <th>Total CVS</th>
                      <th>Total MT</th>
                      <th>Total BA</th>
                      <th>BA HBA Skin</th>
                      <th>BA Hair</th>
                      <th>BA Fab Sol</th>
                      <th>BA Watsons</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td>Headcount</td>
                      <td>
                          <span class='all' id="memberCount_2">
                              <span class="spinner-border spinner-border-sm" role="status">
                                  <span class="sr-only">Loading...</span>
                              </span>
                          </span>
                      </td>
                      <td>
                          <span class='Sup'>
                              <span class="spinner-border spinner-border-sm" role="status">
                                  <span class="sr-only">Loading...</span>
                              </span>
                          </span>
                      </td>
                      <td>
                          <span class='PI'>
                              <span class="spinner-border spinner-border-sm" role="status">
                                  <span class="sr-only">Loading...</span>
                              </span>
                          </span>
                      </td>
                      <td>
                          <span class='CVS'>
                              <span class="spinner-border spinner-border-sm" role="status">
                                  <span class="sr-only">Loading...</span>
                              </span>
                          </span>
                      </td>
                      <td>
                          <span class='MT'>
                              <span class="spinner-border spinner-border-sm" role="status">
                                  <span class="sr-only">Loading...</span>
                              </span>
                          </span>
                      </td>
                      <td>
                          <span class='BA'>
                              <span class="spinner-border spinner-border-sm" role="status">
                                  <span class="sr-only">Loading...</span>
                              </span>
                          </span>
                      </td>
                      <td>
                          <span class='BASkin'>
                              <span class="spinner-border spinner-border-sm" role="status">
                                  <span class="sr-only">Loading...</span>
                              </span>
                          </span>
                      </td>
                      <td>
                          <span class='BAHair'>
                              <span class="spinner-border spinner-border-sm" role="status">
                                  <span class="sr-only">Loading...</span>
                              </span>
                          </span>
                      </td>
                      <td>
                          <span class='BAFab'>
                              <span class="spinner-border spinner-border-sm" role="status">
                                  <span class="sr-only">Loading...</span>
                              </span>
                          </span>
                      </td>
                      <td>
                          <span class='BAFab'>
                              <span class="spinner-border spinner-border-sm" role="status">
                                  <span class="sr-only">Loading...</span>
                              </span>
                          </span>
                      </td>
                  </tr>
              </tbody>
          </table>
      </div>
  </div>

  <div class="row mb-4">
      <div class="col-12">
          <div class="card">

              <div class="card-header">
                  <div class="row">
                      <div class="col-sm-12 col-md-6 col-lg-4 pt-2">
                          <span class="h5">พนักงานที่ใช้งานวันนี้ :
                              <?=empty($_GET['date']) ? date("Y-m-d") : Yii::$app->Utilities->formatDate('Y-m-d', $_GET['date'])?></span>
                      </div>
                      <div class="col-auto ml-auto">
                          <a href="<?=Yii::$app->request->baseUrl?>/work/user-working-map" target="blank"
                              class="ml-auto">
                              <button class="btn btn-outline-secondary btn-sm  border">แผนที่</button>
                          </a>
                          <a href="<?=Yii::$app->request->baseUrl?>/work/user-working-today" target="blank">
                              <button class="btn btn-outline-secondary btn-sm border ml-2 ">ดูทั้งหมด</button>
                          </a>
                      </div>
                  </div>
              </div>

              <div class="card-body">

                  <div class="chart-container">
                      <canvas id="canvas-bar"></canvas>
                      <div id="animationProgress" class="spinner-grow"
                          style="width: 200px; height: 200px;margin-left:auto;margin-right:auto;display:block;"
                          role="status" max="1" value="0">
                          <span class="sr-only">Loading...</span>
                      </div>

                  </div>

              </div>
          </div>
      </div>

  </div>
  <input type="hidden" id="phpBaseUrl" value="<?=Yii::$app->request->baseUrl?>">
  <input type="hidden" id="date"
      value="<?=empty($_GET['date']) ? date("Y-m-d") : Yii::$app->Utilities->formatDate('Y-m-d', $_GET['date'])?>">

  <style type="text/css">
.chart-container {
    position: relative;
    margin: auto;
    height: 60vh;
    width: 80vw;

}

.text-red {
    color: #e73447;
}

.text-blue {
    color: #1097d1;
}
  </style>

  <!-- End Main content --->


  <?php
$script = <<< JS
const url_base = $("#phpBaseUrl").val()
const date = $("#date").val()
var progress = document.getElementById('animationProgress');


async function GetRows (){
$('#animationProgress').value

var data = await $.get(url_base + '/work/get-rows')
// console.log('rows',data);
$(".Sup").text(data.result.Sup)
$(".PI").text(data.result.PI)
$(".CVS").text(data.result.CVS)
$(".MT").text(data.result.MT)
$(".BA").text(data.result.BA)
$(".BASkin").text(data.result.BASkin)
$(".BAHair").text(data.result.BAHair)
$(".BAFab").text(data.result.BAFab)
$(".BAWatsons").text(data.result.BAWatsons)

}



async function GetDaily (){
var data = await $.get(url_base + '/work/get-daily')
// console.log('daily',data);
$("#memberCount_1").text(data.result.MemberCount)
$("#memberCount_2").text(data.result.MemberCount)
$("#total").text(data.result.WorkTotal)
$("#leaveToday").text(data.result.leaveToday)

}
async function GetDayOff (){
    // console.log('GetDayOff');
    // var data = await $.get(url_base + '/work/get-rows')
    try {
        var data = await $.get(url_base + '/work/get-dayoff')
            // console.log('dayoff',data);
            $("#dayOff").text(data.result.dayOffToday)

}
catch(err) {
    // console.log('err ',err);
    $("#dayOff").text(err.responseJSON.result.dayOffToday)

}
// console.log('dayoff',data);


}


async function ChartInit (){


    var data = await $.get(url_base + '/work/current-working',{date:date})
    // console.log('current',data.result);
    $("#animationProgress").remove()

    var res = data.result;

        var ctx = document.getElementById("canvas-bar").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
        labels: ['6:00','8:00','10:00','12:00', '14:00', '16:00', '18:00', '20:00', '22:00', '24:00'],
            datasets: [{
            label: 'พนักงานที่บันทึกเข้า',
            backgroundColor: "#0098e1",
            borderColor: "#0098e1",
            borderWidth: 1,
            stack: 'Stack 0',
            data: [
            res.time6_8, res.time8_10, res.time10_12, res.time12_14, res.time14_16, res.time16_18, res.time18_20, res.time20_22, res.time22_24
            ]
        },{
            label: 'พนักงานที่สาย',
            backgroundColor: "#f4b13b",
            borderColor: "#f4b13b",
            borderWidth: 1,
            stack: 'Stack 0',
            data: [
                res.late6_8, res.late8_10, res.late10_12, res.late12_14, res.late14_16, res.late16_18, res.late18_20, res.late20_22, res.late22_24
            ]
        },{
            label: 'พนักงานที่บันทึกออก',
            backgroundColor: "#e03945",
            borderColor: "#e03945",
            borderWidth: 1,
            stack: 'Stack 1',
            data: [
                res.out6_8, res.out8_10, res.out10_12, res.out12_14, res.out14_16, res.out16_18, res.out18_20, res.out20_22, res.out22_24
            ]
        }]
        },
        options: {
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
            stacked: true,
            gridLines: {
            display: true,
            color: "rgba(226,226,226,0.8)"
            }
        }],
        xAxes: [{
            gridLines: {
            display: false
        }
        }]
    },
    animation: {
                        duration: 0,
                        onProgress: function(animation) {
                        progress.value = animation.currentStep / animation.numSteps;

                        },
                        onComplete: function(animation) {

                            progress.value = 0;

                        }
                    }
        }
    });

}
GetDaily ()
GetRows()
ChartInit()
GetDayOff()

// var res = test()
// console.log(res);
// async function `{await $.get('http://localhost:8888/work/current-working')}
// console.log(arr);



JS;

$this->registerJs($script);
?>