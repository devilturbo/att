<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
.bg-bg {
    /* The image used */
    background-image: url("<?=Yii::$app->request->baseUrl?>/images/bg-login-1.jpg");

    /* Full height */
    height: 100%;

    /* Center and scale the image nicely */
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
</style>

<div class="login-area login-s2- bg-bg">
    <div class="container">
        <div class="login-box ptb--100">
            <form>
                <div class="login-form-head" style="background-color: #1566ff;">
                    <h4>Sign In</h4>
                    <p>Hello there, Sign in and start managing your Admin Template</p>
                </div>
                <div class="login-form-body">
                    <div class="form-gp">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="email" id="exampleInputEmail1">
                        <i class="ti-email"></i>
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="password" id="exampleInputPassword1">
                        <i class="ti-lock"></i>
                    </div>
                    <div class="row mb-4 rmber-area">
                        <div class="col-6">
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                <label class="custom-control-label" for="customControlAutosizing">
                                    Remember
                                    Me</label>
                            </div>
                        </div>
                        <div class="col-6 text-right">
                            <a href="#">Forgot Password?</a>
                        </div>
                    </div>

                    <div class="submit-btn-area">
                        <button class="btn btn-primary " type="submit">บันทึกข้อมูล</button>
                    </div>

                </div>

                <div class="form-footer text-center mt-5">
                    <p class="text-muted">Don't have an account? <a href="#">Sign up</a></p>
                </div>
        </div>
        </form>
    </div>
</div>