<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row" >
      <div class="text-center navbar-brand-wrapper d-flex align-items-center" style="background-color: #0096e5;">
        <a class="navbar-brand brand-logo" href="/index.php"><img src="<?=Yii::$app->homeUrl?>/images/logo.svg" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="/index.html"><img src="<?=Yii::$app->homeUrl?>/images/logo-mini.svg" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end"style="background-color: #0096e5;" >
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="mdi mdi-menu"></span>
        </button>
      
        <ul class="navbar-nav navbar-nav-right">

          <li class="nav-item nav-profile dropdown">
            <a class="nav-link" href="#" data-toggle="dropdown" id="profileDropdown">
              <img src="https://via.placeholder.com/30x30" alt="profile"/>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
              <a class="dropdown-item">
                <i class="mdi mdi-settings "></i>
                Settings
              </a>
              <a class="dropdown-item">
                <i class="mdi mdi-logout"></i>
                Logout
              </a>
            </div>
          </li>

        </ul>
      </div>
</nav>