

<?php
use app\components\Utilities;
?>
<div class="con-12" >
    
        <select multiple="multiple" name="location[]" id="location_select" >
          <?php if(!empty($modelArea)):?>

            <?php $optionGroupOld = null; $first = 'Y';  ?>
            <?php foreach ($modelArea as $key => $item):?>
            	<?php

            	
                    $isShow = 'N';   
                    if($first == 'Y'){
                        $optionGroupOld = $item->account;
                    }
            	
                    if($optionGroupOld != $item->account || $first == 'Y'){
                        $isShow = 'Y';
                        $first = 'N';
                    }

                    $optionGroupOld = $item->account;
                   

            		
            	?>
                <?php if($isShow =='Y'):?>
                <optgroup label="<?=$item->account?>">
              
                    <?php echo Utilities::getAreaByAccount(trim($item->account),$modelLocationWork);?>

                 </optgroup>
            <?php endif;?>
           <?php endforeach;?>
         <?php endif;?>
        </select>
    
</div>
