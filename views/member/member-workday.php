<?php
use yii\web\View;
$this->title = 'ประกาศข่าว';

// echo "<pre style='background: #202020; color: #fff; overflow: auto;'>";
// print_r($data);
// // htmlspecialchars
// echo "<pre>";
// die;
?>
<div class="dashboard-header d-flex flex-column grid-margin">
    <!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">ประวัติบันทึกเวลา</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">ประวัติบันทึกเวลา</p>
            </div>
        </div>
        <!-- <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
            <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button>
        </div> -->

        <button type="submit" id="btn_export" class="btn btn-outline-primary btn-sm ml-2 ">Data Export</button> 
        <!-- <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0" id='btn_export'> -->

    </div>
</div>
<div class="row">
    <div class="col-12">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/member">พนักงานทั้งหมด</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/member/profile">เพิ่มพนักงาน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member/import">Import ข้อมูลพนักงาน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="<?=Yii::$app->request->baseUrl?>/member/workday">ประวัติบันทึกเวลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/member/leave">ประวัติการลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"
                    href="<?=Yii::$app->request->baseUrl?>/member/change-time">ประวัติการปรับปรุงเวลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member/ot">ประวัติการทำงานล่วงเวลา</a>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-12 mb-4">
        <div class="row ">
            <div class="col-2 ml-auto"></div>
            <div class="col-2"></div>
        </div>

        <form>
            <div class="form-row align-items-center mb-2">
                <div class="col-sm-3 my-1-">
                    <span>ค้นหา</span>
                    <input type="text" class="form-control global_filter" id="global_filter" placeholder="คำที่ค้นหา">
                </div>
                <div class="col-3">
                    <div class="row">
                        <div class="col"><span id="ตำแหน่ง"></span></div>
                        <div class="col"><span id="ประเภท"></span></div>
                        <div class="col-auto">
                            <button type="button" class="btn btn-outline-primary btn-sm ml-auto bt-filter"
                                style="margin-top: 20px;" data-toggle="modal" data-target="#ModalFilter">Filter</button>
                        </div>
                    </div>
                </div>


                <div class="col-auto my-1 ml-auto" style="width: 350px;">
                    <span>ค้นหาจากช่วงเวลา</span>
                    <div class="input-group input-daterange">
                        <input type="text" id="min-date" class="form-control date-range-filter"
                            data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น" autocomplete="off"
                            >
                        <div class="input-group-addon mr-2 ml-2"> ถึง </div>
                        <input type="text" id="max-date" class="form-control date-range-filter"
                            data-date-format="yyyy-mm-dd" placeholder="สิ้นสุด" autocomplete="off"
                            >
                        <!-- <button id="search" class="btn btn-primary btn-sm ml-2">ค้นหา</button> -->
                    </div>
                </div>

                <!-- <div class="col-3 my-1 ml-auto">
                    <div class="row">
                        <div class="col-12">
                            <span>ค้นหาตามวันที่</span>
                            <div id="datepicker-popup" class="input-group date datepicker">
                                <input type="text" id="date" name="date" class="form-control"
                                    data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น" autocomplete="off">
                                <span class="input-group-addon input-group-append border-left">
                                    <span class="mdi mdi-calendar input-group-text"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div> -->


            </div>



            <!-- Modal -->
            <div id="ModalFilter" class="modal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <span>ค้นหาโดยละเอียด</span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="col-12 p-4">
                            <div class="row">
                                <!-- <div class="col-6 mb-2"><span id="ตำแหน่ง"></span></div>
                                <div class="col-6 mb-2"><span id="ประเภท"></span></div> -->
                                <div class="col-6 mb-2"><span id="แผนก"></span></div>
                                <!-- <div class="col-6 mb-2"><span id="ตำแหน่ง"></span></div> -->
                                <!-- <div class="col-6 mb-2"><span id="ประเภท"></span></div> -->
                                <div class="col-6 mb-2"><span id="ประเภทย่อย"></span></div>
                                <div class="col-6 mb-2"><span id="พื้นที่"></span></div>
                                <div class="col-6 mb-2"><span id="เขตพื้นที่"></span></div>
                                <div class="col-6 mb-2"><span id="ช่องทาง"></span></div>
                                <div class="col-6 mb-2"><span id="ห้างร้าน"></span></div>
                                <!-- <span class="col-6 mb-2"><span id="สถานะ"></span></div> -->
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </form>

    </div>
</div>
<?php


function thai_date($time){
    $thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");
    $thai_month_arr=array(
     "0"=>"",
     "1"=>"มกราคม",
     "2"=>"กุมภาพันธ์",
     "3"=>"มีนาคม",
     "4"=>"เมษายน",
     "5"=>"พฤษภาคม",
     "6"=>"มิถุนายน", 
     "7"=>"กรกฎาคม",
     "8"=>"สิงหาคม",
     "9"=>"กันยายน",
     "10"=>"ตุลาคม",
     "11"=>"พฤศจิกายน",
     "12"=>"ธันวาคม"     
    );
 $thai_date_return="วัน".$thai_day_arr[date("w",strtotime($time))];
 $thai_date_return.= "ที่ ".date("j",strtotime($time));
 $thai_date_return.=" ".$thai_month_arr[date("n",strtotime($time))];
 $thai_date_return.= " ".(date("Y",strtotime($time))+543);
 //$thai_date_return.= "  ".date("H:i",$time)." น.";
 return $thai_date_return;
}


?>
<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-body">


                <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                        <tr>
                            <th>รูป</th>
                            <th>รหัสพนักงาน</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>เวลา</th>
                            <th>สถานนะ</th>
                            <th>สถานที่</th>

                            <th>แผนก</th>
                            <th>ตำแหน่ง</th>
                            <th>ประเภท</th>
                            <th>ประเภทย่อย</th>
                            <th>วันที่</th>
                            <th>ผู้ดูแล</th>
                            <th>พื้นที่</th>
                            <th>เขตพื้นที่</th>
                            <th>ช่องทาง</th>
                            <th>ห้างร้าน</th>
                            <th>ดำเนินการ</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                    foreach ($data as $key => $e) {
    // \app\components\Helpers::Debig(($e->member));

    $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
    if (!empty($e->img)) {
        // $img = Yii::$app->request->baseUrl . '/' . $e->img;
        $img ='https://api.thebestpromote.com/web/' . $e->img;
    }
$location = $e->area->account . "/" . $e->area->brach_no . "/" . $e->area->local_name ; 
                    ?>
                        <tr class="dt-edit">
                            <td class="employee-col">
                                <img class="thumb mr-2 lazy" src="<?=$img?>" align="left">
                            </td>
                            <td><?=$e->member->member_no?></td>
                            <td><?=$e->member->fullname?></td>
                            <td><?=Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime)?></td>
                            <td><?=$e->punch_type== "in" ? "บันทึกเข้า" : "บันทึกออก"?></td>
                            <td><?=$location?></td>
                            <td><?=$e->member->department_name?></td>
                            <td><?=$e->member->position_name?></td>
                            <td><?=$e->member->type_name?></td>
                            <td><?=$e->member->subtype_name?></td>
                            <td><?=Yii::$app->Utilities->formatDate('d/m/yy', $e->punch_datetime)?></td>
                            <td><?=$e->member->memberundder_name?></td>
                            <td><?=$e->member->area_name?></td>
                            <td><?=$e->member->zone_name?></td>
                            <td><?=$e->member->channel_name?></td>
                            <td><?=$e->member->account_name?></td>
                            <td>
                                <a
                                    href="<?=Yii::$app->request->baseUrl?>/member/profile-workday?id=<?=$e->member->id?>">
                                    <button
                                        class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                                </a>
                            </td>
                        </tr>
                        <?php
                    }
?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<input type="hidden" id="phpBaseUrl" value="<?=Yii::$app->request->baseUrl?>">

<div id="myModal" class="modal modaldetail" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>รายละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>
            <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div-->
        </div>

    </div>
</div>



<table id="dataTable_export" class="table " cellspacing="0" style="visibility:hidden">
    <!-- <table id="dataTable_export" class="table " cellspacing="0"> -->
    <thead>
        <tr>
            <th>รหัสพนักงาน</th>
            <th>ชื่อ-นามสกุล</th>
            <th>เวลา</th>
            <th>สถานนะ</th>
            <th>สถานที่</th>

            <th>แผนก</th>
            <th>ตำแหน่ง</th>
            <th>ประเภท</th>
            <th>ประเภทย่อย</th>
            <th>วันที่</th>
            <th>ผู้ดูแล</th>
            <th>พื้นที่</th>
            <th>เขตพื้นที่</th>
            <th>ช่องทาง</th>
            <th>ห้างร้าน</th>
        </tr>
    </thead>
    <tbody id="tbody_export">

    </tbody>
</table>


<div id="myModal_export" class="modal modaldetail" role="dialog">
    <div class="modal-dialog " style="max-width: 100;">
        <div class="modal-content">
            <div class="modal-header">
                <span>Download</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div style="margin-left:15px" class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0"
                        id='btn_export_modal'>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<?php
$script = <<< JS
    const url_base = $("#phpBaseUrl").val()

 
table = $('#dataTable').DataTable({
    processing: true,
    serverSide: true,
    "bLengthChange" : false,
    // "pageLength": 2,
    ajax: {
        "url": url_base+ "/member/workday-dt",
        "type": "POST",
        "data" :{ _csrf:yii.getCsrfToken()},
       dataFilter: function(reps) {
            callDT();
            return reps;
        },
        error:function(err){
              console.log(err.responseText);
        }
         
                     
    },
    searching: true,
    paging:   true,
    ordering: false,
    info:     false,
    scrollY: 720,
    scrollX:        true,
    scrollCollapse: false,
    autoWidth:      false,
    
   createdRow: function( row, data, dataIndex ) {
        $(row).addClass( 'dt-edit' );
        
    },
    fixedColumns:   {
      leftColumns: 1
    },
    columnDefs: [
      { "width": "50px", "targets": [ 0 ] },
    ],
    responsive: false,
    fixedHeader: true

});


$('#dataTable_filter').hide()

function callDT() {
setTimeout(function(){  dt();}, 500);
}
function dt() {

    var colum = ["รูป", "รหัสพนักงาน" , "ชื่อ-นามสกุล", "เวลา", "สถานะ", "สถานที่",
        "แผนก", "ตำแหน่ง", "ประเภท", "ประเภทย่อย", "วันที่", "ผู้ดูแล", "พื้นที่", "เขตพื้นที่", "ช่องทาง",
        "ห้างร้าน", "ดำเนินการ"
    ];
//Edit row buttons
$('.dt-edit').each(function() {
    $(this).on('click', function(evt) {
        \$this = $(this);
        var dtRow = \$this;
        $('div.modal-body').innerHTML = '';
        $('div.modal-body').append('<div class="row mb-4 ml-1 img-thumbmail">' + dtRow[0]
            .cells[0].innerHTML + '</div>');
        for (var i = 1; i < dtRow[0].cells.length; i++) {
            $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">' +colum[i] + '</span><span class="col-8">' + dtRow[0].cells[i].innerHTML +'</span><div/>');
            // console.log(i);
        }
        $('#myModal').modal('show');
    });
});
$('#myModal').on('hidden.bs.modal', function(evt) {
    $('.modal .modal-body').empty();
});
}


async function downloadDropDown(){
    var columnsToSearch = {
        6: 'แผนก',
        7: 'ตำแหน่ง',
        8: 'ประเภท',
        9: 'ประเภทย่อย',
        12: 'พื้นที่',
        13: 'เขตพื้นที่',
        14: 'ช่องทาง',
        15: 'ห้างร้าน',
        // 4: 'สถานะ'
    };
for (var i in columnsToSearch) {

var _id = "";

if (columnsToSearch[i] == "แผนก"){
    _id = "department-search"
}
 if (columnsToSearch[i] == "ตำแหน่ง"){
    _id = "position-search"
}
if (columnsToSearch[i] == "ประเภท"){
    _id = "type-search"
}
if (columnsToSearch[i] == "ประเภทย่อย"){
    _id = "subtype-search"
}
if (columnsToSearch[i] == "พื้นที่"){
    _id = "area-search"
}
if (columnsToSearch[i] == "เขตพื้นที่"){
    _id = "zone-search"
}
if (columnsToSearch[i] == "ช่องทาง"){
    _id = "channel-search"
}
if (columnsToSearch[i] == "ห้างร้าน"){
    _id = "account-search"
}


var select = $('<select id='+_id+' class="form-control"><option value="">Select</option></select>')
    .appendTo($('#' + columnsToSearch[i]).empty().text(columnsToSearch[i] + ' '))

// get list from api 
    var arr  = await $.get(url_base+ '/member/getlist', {list: columnsToSearch[i]})
    var arrJson = JSON.parse(arr)

    if (columnsToSearch[i] == "สถานะ"){
                    select.append('<option value="wait">รออนุมัติ</option>');
                    select.append('<option value="accept">อนุมัติแล้ว</option>');
                    select.append('<option value="decline">ไม่อนุมัติ</option>');
                }
    Object.keys(arrJson).forEach(function(key) {
        select.append('<option value="' + key + '">' + arrJson[key] + '</option>');

    })
}


$('#global_filter,#subtype-search,#department-search,#position-search,#type-search,#area-search,#zone-search,#channel-search,#account-search,#date,#min-date,#max-date').on( 'change', function () {
    let global_filter = $('#global_filter').val() 
    let departmentSearch = $('#department-search').val()
    let positionSearch = $('#position-search').val()
    let typeSearch = $('#type-search').val()
    let subtypeSearch = $('#subtype-search').val()
    let areaSearch = $('#area-search').val()
    let zoneSearch = $('#zone-search').val()
    let channelSearch = $('#channel-search').val()
    let accountSearch = $('#account-search').val()
    let date = $('#date').val()
    let start = $('#min-date').val()
    let end = $('#max-date').val()

     let strSearch = global_filter +'&'+departmentSearch+'&'+positionSearch+'&'+typeSearch +'&'+subtypeSearch +'&'+date +'&'+areaSearch +'&'+zoneSearch +'&'+channelSearch +'&'+accountSearch+ '&' +start + '&'+end 
     
     table.search( strSearch ).draw();
} );

}

downloadDropDown()

$("#min-date").datepicker().datepicker("setDate", new Date());

$('#my-table_filter').hide();

$('input.column_filter').on('keyup click', function() {
    filterColumn($(this).parents('tr').attr('data-column'));
});




async function getData(){
    let start = $('#min-date').val()
    let end = $('#max-date').val()
    var arr  = await $.get(url_base+ '/work/getdata?start='+start+'&end='+end)
    // console.log('arr,',start,'end',end);
    return arr.result
}

$("#btn_export").on('click', async function(evt){

    // $('#dataTable_export').clear().draw()
    var table = $('#dataTable_export').DataTable();

    table.destroy();

    // var table = $("#dataTable_export").DataTable(); // Valid initialized DataTable
    // if (table instanceof $.fn.dataTable.Api) {
    //     $('#dataTable_export').clear().draw()
    // }

    var tbody = $("#dataTable_export tbody");

// if (tbody.children().length == 0) {
    $("#btn_export").append(' <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');

    $('#loadingGrow').css("visibility","visible");
    $("#tbody_export").empty();

    // console.log('ok');
   var data = await getData()
    //    console.log(data);
   data.map((value)=>{          
    var newRowContent = "<tr class='dt-edit'>"
    newRowContent += "<td>"+value.member_no+"</td>"
    newRowContent += "<td>"+value.fullname+"</td>"
    newRowContent += "<td>"+value.punch_time+"</td>"
    newRowContent += "<td>"+value.punch_type+"</td>"
    newRowContent += "<td>"+value.location+"</td>"
    newRowContent += "<td>"+value.department_name+"</td>"
    newRowContent += "<td>"+value.position_name+"</td>"
    newRowContent += "<td>"+value.type_name+"</td>"
    newRowContent += "<td>"+value.subtype_name+"</td>"
    newRowContent += "<td>"+value.punch_date+"</td>"
    newRowContent += "<td>"+value.memberundder_name+"</td>"
    newRowContent += "<td>"+value.area+"</td>"
    newRowContent += "<td>"+value.zone+"</td>"
    newRowContent += "<td>"+value.channel+"</td>"
    newRowContent += "<td>"+value.account+"</td>"
    newRowContent += "</tr>"


    $("#dataTable_export tbody").append(newRowContent);

   })

   table_export = $('#dataTable_export').DataTable({
      dom: 'it',
        searching: true,
        paging:   false,
        ordering: false,
        info:     false,
        scrollY: 300,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: true,
        fixedHeader: true,

    });


    var buttons = new $.fn.dataTable.Buttons(table_export, {
       buttons: [
    {
        extend: 'excel',
        text: 'Download',
        charset: 'utf-8',
        extension: '.xlsx',
        bom: true,
        className: 'btn btn-outline-primary btn-sm  d-none d-md-block testtest',
    }]
}).container().appendTo($('#btn_export_modal'));

   $('#myModal_export').modal('show');

   $("#btn_export").find('span:first').remove();

// }else{
//     $('#myModal_export').modal('show');

// }
})

// var buttons = new $.fn.dataTable.Buttons(table, {
//        buttons: [
//     {
//         extend: 'excel',
//         text: 'Data Export',
//         charset: 'utf-8',
//         extension: '.xlsx',
//         bom: true,
//         className: 'btn btn-outline-primary btn-sm  d-none d-md-block',
        
//     }]
// }).container().appendTo($('#btn_export'));


JS;
$this->registerJs($script, View::POS_READY);

?>