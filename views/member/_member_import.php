<?php
use yii\bootstrap\ActiveForm;
use yii\web\View;
use app\components\Utilities;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Import ข้อมูลพนักงาน';
$this->params['breadcrumbs'][] = $this->title;


?>
<?php $form = ActiveForm::begin([

    'action' => ['import'],
    'id' => 'importSlip',
    'enableClientScript' => false,
    'method' => 'POST',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'options' =>  ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-4',
        ],
        'options' =>  ['class' => 'form-group row']
    ],
]); ?>
<div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">Import ข้อมูลพนักงาน</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">Import ข้อมูลพนักงาน</p>
            </div>
        </div>
    </div>
</div>

<div class="row mb-4">
    <div class="col-12">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
        <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/member">พนักงานทั้งหมด</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/member/profile">เพิ่มพนักงาน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="<?=Yii::$app->request->baseUrl?>/member/import">Import ข้อมูลพนักงาน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member/workday">ประวัติบันทึกเวลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/member/leave">ประวัติการลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link "
                    href="<?=Yii::$app->request->baseUrl?>/member/change-time">ประวัติการปรับปรุงเวลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/member/ot">ประวัติการทำงานล่วงเวลา</a>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="row ">
            <div class="col-2 ml-auto"></div>
            <div class="col-2"></div>
        </div>


            <div class="form-row align-items-center mb-4">
                <div class="col-sm-3 my-1-">
                    <span>ค้นหา</span>
                    <input type="text" class="form-control global_filter" id="global_filter" placeholder="คำที่ค้นหา">
                </div>

                <div class="col-3">
                    <div class="row">
                        <div class="col" ><span id="สถานนะ"></span></div>
                        <div class="col" ><span id="ประเภท"></span></div>
                        <div class="col-auto">
                            <button type="button" class="btn btn-outline-primary btn-sm ml-auto bt-filter" style="margin-top: 20px;" data-toggle="modal" data-target="#ModalFilter">Filter</button>
                        </div>
                    </div>
                </div>




                <div class="col-auto my-1 ml-auto" style="width: 350px;">
                    <span>ค้นหาจากช่วงเวลา</span>
                    <div class="input-group input-daterange">
                        <input type="text" id="min-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น" >
                        <div class="input-group-addon mr-2 ml-2"> ถึง </div>
                        <input type="text" id="max-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="สิ้นสุด" >
                        <button type="submit" id="search" class="btn btn-primary btn-sm ml-2">ค้นหา</button>
                    </div>
                </div>
            </div>




    </div>
</div>

<!-- Modal -->
<div id="ModalFilter" class="modal" role="dialog">
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>ค้นหาโดยละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="col-12 p-4">

                <div class="row mb-4">
                    <div class="col-6 mb-2"><span id="เพศ"></span></div>
                    <div class="col-6 mb-2 " ><span id="เอเจนซี่"></span></div>
                    <div class="col-6 mb-2 " ><span id="แผนก"></span></div>
                    <div class="col-6 mb-2" ><span id="ตำแหน่ง"></span></div>
                    <div class="col-6 mb-2" ><span id="ประเภท"></span></div>
                    <div class="col-6 mb-2" ><span id="ประเภทย่อย"></span></div>
                    <div class="col-6 mb-2"><span id="สัญญาจ้าง"></span></div>
                    <div class="col-6 mb-2"><span id="พื้นที่"></span></div>
                    <div class="col-6 mb-2"><span id="ช่องทาง"></span></div>
                    <div class="col-6 mb-2"><span id="ห้างร้าน"></span></div>
                    <div class="col-6 mb-2"><span id="สถานนะ"></span></div>
                </div>

            </div>
        </div>
    </div>
</div>
    <div class="row mb-4">
        <div class="col-12">
            <div class="card" style="display: <?=Yii::$app->user->identity->auth_import_member ==2?'none':''?>">
                <div class="card-body">
                    <span class="mb-4 h5">เลือกไฟล์ที่ต้องการ</span> l <a href="<?=Yii::$app->homeUrl?>/shared/member/template/template_member.xlsx" target="blank" class="text-sm">ดาวน์โหลดไฟล์ตัวอย่าง</a><br>

                    <div class="input-group mb-3">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="MemberTempImport[file]" id="inputGroupFile02" onchange="uploadFile()">
                            <input type="hidden" name="MemberTempImport[file]" value="">
                            <label class="custom-file-label" id="filename" for="inputGroupFile02">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <button class="input-group-text" id="btn-upload" onclick="importSlip()">Upload</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row pr-3 mb-2">
                    <span class="h5 ml-3 mt-2"><?=$rows?> รายการ</span>

                    <button style="display: <?=Yii::$app->user->identity->auth_import_member ==2?'none':''?>" class="btn btn-primary btn-sm mr-2 ml-auto" onclick="MemmberAll();return false;">บันทึกทั้งหมด</button>
                    <a style="display: <?=Yii::$app->user->identity->auth_import_member ==2?'none':''?>" onclick="memberDelAll()" class="btn btn-outline-primary btn-sm ">ยกเลิก</a>
                </div>




                <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                    <tr>


                        <th>บัตรประชาชน</th>
                        <th>คำนำหน้า</th>
                        <th>ชื่อ</th>
                        <th>นามสกุล</th>
                        <th>เพศ</th>
                        <th>เบอร์โทรศัพท์</th>
                        <th>อีเมล</th>
                        <th>เอเจนซี่</th>
                        <th>แผนก</th>
                        <th>ตำแหน่ง</th>
                        <th>ประเภท</th>
                        <th>ประเภทย่อย</th>
                        <th>สัญญาจ้าง</th>
                        <th>ผู้ดูแล</th>
                        <th>พื้นที่</th>
                        <th>เขตพื้นที่</th>
                        <th>ช่องทาง</th>
                        <th>ห้างร้าน</th>
                        <th>สาขาที่ทำงาน</th>
                        <th>วันที่เริ่มงาน</th>
                        <th>ที่อยู่ตามบัตร ปชช</th>
                        <th>บัญชี ธนาคาร</th>
                        <th>สถานนะ</th>
                        <th>ดำเนินการ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($model)):?>
                    <?php foreach ($model as $key => $item):?>
                    <tr class="dt-edit" id="tr_<?=$item->id?>">
                        <td><?=$item->citizen_id?></td>
                        <td><?=$item->title?></td>
                        <td><?=$item->name_th?></td>
                        <td><?=$item->surname_th?></td>
                        <td><?=$item->gender?></td>
                        <td><?=$item->phone_number?></td>
                        <td><?=$item->email?></td>
                        <td><?=$item->agency?></td>
                        <td><?=$item->department?></td>
                        <td> <?=$item->position?></td>
                        <td><?=$item->mebertype?></td>
                        <td><?=$item->merber_subtype?></td>
                        <td><?=$item->contact?></td>
                        <td><?=$item->under?></td>
                        <td><?=$item->area?></td>
                        <td><?=$item->distinct_area?></td>
                        <td><?=$item->chanel?></td>
                        <td> <?=$item->work_station?></td>
                        <td> <?=$item->branch_station?></td>
                        <td><?=\app\components\Helpers::convertDate($item->start_date)?></td>
                        <td><?=$item->address?></td>
                        <td><?=$item->bookbank_no?></td>
                        <td><?=$item->status?></td>
                        <td>
                            <a href="javascript:void(0)" class="btn btn-primary btn-sm mb-1"  onclick="Memmber(<?=$item->id?>,'add')">บันทึก</a>
                            <a href="javascript:void(0)" class="btn btn-outline-primary btn-sm mb-1" onclick="Memmber(<?=$item->id?>,'edit')">แก้ไข</a>
                            <a href="javascript:void(0)" class="btn btn-outline-primary btn-sm mb-1" onclick="Memmber(<?=$item->id?>,'del')" >ลบ</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                  <?php endif;?>


                    </tbody>
                </table>
                <div class="row pr-3">
                    <button class="btn btn-primary btn-sm mr-2 ml-auto" onclick="MemmberAll()">บันทึกทั้งหมด</button>
                     <a onclick="memberDelAll()" class="btn btn-outline-primary btn-sm ">ยกเลิก</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<!-- ลบข้ำมูล -->
<div id="myModaldelete" class="modal" role="dialog">
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>ลบข้อมูล</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="row p-4 text-center">
                <h2 class="text-danger">ยืนยันการลบข้อมูล</h2>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">ยืนยัน</button>
                <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">ยกเลิก</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>รายละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>
            <!--div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div-->
        </div>

    </div>
</div>
<?php
$script  = <<< JS
 function filterGlobal () {
    $('#dataTable').DataTable().search(
        $('#global_filter').val()

       ).draw();
    }
    var columnsToSearch = {
        9: 'แผนก',
        10: 'ตำแหน่ง',
        11: 'ประเภท',
        12: 'ประเภทย่อย',
        13: 'สัญญาจ้าง',
        15: 'พื้นที่',
        8: 'เอเจนซี่',
        16: 'เขตพื้นที่',
        17: 'ช่องทาง',
        18: 'ห้างร้าน',
        5: 'เพศ',
        23: 'สถานนะ'
    };

    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging:   false,
        ordering: false,
        info:     false,
        scrollY: 500,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: false,
        fixedHeader: true,
        initComplete: function () {
            for (var i in columnsToSearch) {
                var api = this.api();
                var select = $('<select class="form-control"><option value="">Select</option></select>')
                    .appendTo($('#' + columnsToSearch[i]).empty().text(columnsToSearch[i] + ' '))

                    .attr('data-col', i)
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        api.column($(this).attr('data-col'))
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                api.column(i).data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>');
                });
            }
        }

    });

var colum = [  "บัตรประชาชน","คำนำหน้า","ชื่อ-นามสกุล",  "เพศ", "เบอร์โทรศัพท์","อีเมล", "เอเจนซี่", "แผนก","ตำแหน่ง","ประเภท","ประเภทย่อย","สัญญาจ้าง","ผู้ดูแล","พื้นที่","เขตพื้นที่","ช่องทาง","ห้างร้าน","สาขา","วันที่เริ่มงาน","วันสิ้นสุดงาน","อายุงาน","สถานนะ","ดำนเนินการ"];
   //Edit row buttons
  $('.dt-edit').each(function () {
    $(this).on('click', function(evt){
      _this = $(this);
      var dtRow = _this;
      $('div.modal-body').innerHTML='';
      $('div.modal-body').append( '<div class="row mb-4 ml-1 img-thumbmail">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=1; i < dtRow[0].cells.length; i++){
         $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">'+colum[i]+'</span><span class="col-8">'+dtRow[0].cells[i].innerHTML+'</span><div/>');
            console.log(i);
      }
      $('#myModal').modal('show');
    });
  });
  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .modal-body').empty();
  });
    $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
       var min = $('#min-date').val();
       var max = $('#max-date').val();
       var createdAt = data[2] || 0; 
        if (
           (min == "" || max == "") ||
           (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
         ) {
            return true;
        }
            return false;
        }
    );

    $('.date-range-filter').change(function() {
      table.draw();
    });
    $('#my-table_filter').hide();

    $('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );

JS;
$script2 = <<< JS
function uploadFile()
{
      var filename =$('input[type=file]')[0].files[0].name;
      $('#filename').html(filename)
      $('#btn-upload').removeClass('input-group-text').addClass('btn btn-primary btn-sm');
      
     
}

function Memmber(id,ele) 
{
    let check_event = true
    if(ele == 'del'){
        if(confirm('ยืนยันการลบข้อมูล')){
            check_event = true;
        }else{
            check_event = false;
            return false;
        }
    }
    if(check_event === true){
        $.ajax({
            url: './set-member',
            type: 'post',
            data: {id:id,_csrf:yii.getCsrfToken(),ele:ele},
            success: function (res) {
          
            if(ele == 'edit'){
                let link = `./profile?id=\${res.id}`;
                window.location.replace(link);
            }else if(ele == 'del'){
                $('#tr_'+id).remove();
            }else{
                
                  window.location.replace("./import");
            }
           
             
            }
        });
    }
    
  
}

function MemmberAll() 
{
        $.ajax({
            url: './set-memberall',
            type: 'post',
            data: {_csrf:yii.getCsrfToken()},
            success: function (res) {
                if(res.message){
                    alert(res.message)
                   window.location.replace("./import");
                }
                
            }
        });
}

function memberDelAll() {
   $.ajax({
            url: './del-memberall',
            type: 'post',
            data: {_csrf:yii.getCsrfToken()},
            success: function (res) {
                if(res.message){
                    alert(res.message)
                   window.location.replace("./import");
                }
                
            }
        });
}

function importSlip() {
   $('#importSlip').submit()
}
JS;
$this->registerJs($script2, View::POS_HEAD);
$this->registerJs($script, View::POS_READY);
?>