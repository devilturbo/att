  <?php
  use app\components\Helpers;
  use yii\web\View;
  ?>
  <style>
table tr td {
    border-top: 0 !important;

}
  </style>
  <div class="dashboard-header d-flex flex-column grid-margin">
      <!-- dashboard header -->
      <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
          <div class="d-flex align-items-center">
              <h4 class="mb-0 font-weight-bold mr-2">ประวัติพนักงาน</h4>
              <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าหลัก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">ประวัติพนักงาน</p>
              </div>
          </div>
          <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
              <!--button class="btn btn-primary btn-sm mr-3 d-none d-md-block">Download Report</button>
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button-->
          </div>
      </div>
  </div>

  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-body">
                  <div class="row">
                      <?php
                        $img = Yii::$app->request->baseUrl.'/images/user/thumb/thumb.jpg';
                        if(!empty($model->img)){

                          $img = Yii::$app->request->baseUrl.'/'.$model->img;
                        }
                      ?>
                      <div class="col-1 img-thumbmail"><img class="mr-2 lazy" src="<?=$img?>" align="left"></div>
                      <div class="col-3">
                          <table class="table-sm card-table-one">
                              <tbody>

                                  <tr>
                                      <td class="px-0"><strong>รหัสพนักงาน</strong></td>
                                      <td class="text-muted"><?=$model->member_no?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>ชื่อ-นามสกุล</strong></td>
                                      <td class="text-muted"><?=$model->fullname?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>เอเจนซี่</strong></td>
                                      <td class="text-muted"><?=$model->agency_name?></td>
                                  </tr>

                              </tbody>
                          </table>
                      </div>
                      <div class="col-3">
                          <table class="table-sm card-table-one">
                              <tbody>
                                  <tr>
                                      <td class="px-0"><strong>ตำแหน่ง</strong></td>
                                      <td class="text-muted"><?=$model->department_name?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>ประเภท</strong></td>
                                      <td class="text-muted"><?=$model->type_name?></td>
                                  </tr>

                                  <tr>
                                      <td class="px-0"><strong>ประเภทย่อย</strong></td>
                                      <td class="text-muted"><?=$model->subtype_name?></td>
                                  </tr>

                              </tbody>
                          </table>
                      </div>

                      <div class="col-3">
                          <table class="table-sm card-table-one">
                              <tbody>
                                  <tr>
                                      <td class="px-0"><strong>ผู้ดูแล</strong></td>
                                      <td class="text-muted"><?=$model->memberundder_name?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>อีเมล</strong></td>
                                      <td class="text-muted"><?=$model->email?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>เบอร์โทร</strong></td>
                                      <td class="text-muted"><?=$model->phone_number?></td>
                                  </tr>

                              </tbody>
                          </table>
                      </div>

                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="row mt-3">
      <div class="col-12">
          <ul class="nav nav-pills" id="pills-tab" role="tablist">
              <li class="nav-item">
                  <a class="nav-link"
                      href="<?=Yii::$app->homeUrl?>member/profile-detail?id=<?=$model->id?>">ประวัติพนักงาน</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link "
                      href="<?=Yii::$app->homeUrl?>member/profile-workday?id=<?=$model->id?>">บันทึกเวลาทำงาน</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link "
                      href="<?=Yii::$app->homeUrl?>member/profile-leave?id=<?=$model->id?>">บันทึกการลา</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link "
                      href="<?=Yii::$app->homeUrl?>member/profile-change-time?id=<?=$model->id?>">ปรับปรุงเวลา</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link"
                      href="<?=Yii::$app->homeUrl?>member/profile-ot?id=<?=$model->id?>">ทำงานล่วงเวลา</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link active"
                      href="<?=Yii::$app->homeUrl?>member/profile-location?id=<?=$model->id?>">สถานที่ทำงาน</a>
              </li>

          </ul>
      </div>
  </div>

  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-body">
                  <div class="row">

                  </div>

                  <div class="row mb-2">
                      <div class="col mt-1">
                          <span class=" font-weight-bold mr-2">ทั้งหมด <?=$modelAreaRows?> สถานที่</span>
                      </div>

                  </div>

                  <table id="example" class="table  display" cellspacing="0" width="100%">
                      <thead style="background-color: white;">
                          <tr>
                              <th>ลำดับ</th>
                              <th>กลุ่มสถานที่</th>
                              <th>Account</th>
                              <th>รหัสสาขา</th>
                              <th>สาขา</th>
                              <!-- <th width="50px;">ดำเนินการ</th> -->
                          </tr>
                      </thead>
                      <tbody>
                          <?php 
                          
                          $member = Yii::$app->Utilities->getMemberById($modelArea[0]->member_id);
                          $group = Yii::$app->Utilities->getAreaGroup($member->member_no);
                        //   \app\components\Helpers::Debig(count($modelArea));

                          foreach($modelArea as $key => $item):

                            $area = Yii::$app->Utilities->getAreaByBranchNo($item->area_id);
                            $output = "ไม่ระบุ";
                            // $isbreak = false;
                            foreach($group as $k => $e){
                                // if ($isbreak) break;

                                foreach($e->areaList as $kk => $v){
                                    // \app\components\Helpers::Debig(($v));

                                    if( !empty($area)){


                                        if($v->area_id == $area->id){
                                            $output = $e->group_name;
                                            // $isbreak =true;
                                             break;
                                        }
                                        // else{

                                 }
                                }
                            }
                            // $isbreak = false;

                            ?>
                          <tr class="dt-edit">
                              <td><?=$key+1?></td>
                              <td><?=$output?></td>
                              <td><?=$item->areasDetail->account?></td>
                              <td><?=$item->areasDetail->brach_no?></td>
                              <td><?=$item->areasDetail->local_name?></td>
                              <!-- <td>
                                  <button class="btn btn-outline-primary btn-sm">รายละเอียด</button>
                              </td> -->
                          </tr>
                          <?php 

                        endforeach; ?>
                        
                      </tbody>
                  </table>



              </div>
          </div>
      </div>
  </div>
  <div id="myModalAddLocation" class="modal modaldetail" role="dialog">
      <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <span>เพิ่มสาขา</span>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="p-4 col-12">
                  <div class="form-group">
                      <label for="example-text-input" class="col-form-label">ใส่รหัสสาขา</label>
                      <textarea class="form-control" id="example-text-input" name="subject"
                          placeholder="1223,345,345,6456,456,468," style="height:50px"></textarea>
                  </div>

              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">บันทึก</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
              </div>
          </div>

      </div>
  </div>



  <!-- ลบข้ำมูล -->
  <div id="myModaldelete" class="modal" role="dialog">
      <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <span>ลบข้อมูล</span>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="row p-4 text-center">
                  <h2 class="text-danger">ยืนยันการลบข้อมูล</h2>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">ยืนยัน</button>
                  <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">ยกเลิก</button>
              </div>
          </div>
      </div>
  </div>

  <!-- Modal -->
  <div id="myModal" class="modal modaldetail" role="dialog">
      <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <span>รายละเอียด</span>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">

              </div>
              <div class="modal-footer">
                  <!--  <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">บันทึก</button>
        <button class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#myModaldelete" data-dismiss="modal">ลบ</button> -->
                  <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">ปิด</button>
              </div>
          </div>

      </div>
  </div>
  <?php
$script = <<< JS
 $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });


  var table = $('#example').DataTable({
        dom: 'it',
        ordering: false,
        info:     false,
        paging: false,
        scrollY: 800,
        columnDefs: [
            // { "orderDataType": "dom-text", "targets": 1 }
            // { targets: 1, "orderDataType": "dom-text", type: 'string' }

        ],
       
    } );

  var colum = ["ลำดับ", "กลุ่มสถานที่","Account", "รหัสสาขา","สาขา"];
   //Edit row buttons
   $('.dt-edit').each(function () {
    $(this).on('click', function(evt){
      \$this = $(this);
      var dtRow = \$this;
      $('div.modal-body').innerHTML='';
      //$('div.modal-body').append( '<input type="text" id="id" name="id" placeholder="'+dtRow[0].cells[0].innerHTML+'">');
      $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">'+colum[0]+'</span><span class="col-8"><input type="text" id="id" name="id" placeholder='+dtRow[0].cells[0].innerHTML+'></span><div/>');

      //$('div.modal-body').append( '<div class="row mb-4 ml-1 img-thumbmail lazy">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=1; i < dtRow[0].cells.length-1; i++){
         $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">'+colum[i]+'</span><span class="col-8">'+dtRow[0].cells[i].innerHTML+'</span><div/>');
            console.log(i);
      }
      $('#myModal').modal('show');
    });
  });
  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .modal-body').empty();
  });

JS;
$this->registerJs($script, View::POS_READY);
?>