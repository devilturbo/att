<?php
use yii\web\View;
$this->title = 'ประวัติการปรับปรุงเวลา';

// echo "<pre style='background: #202020; color: #fff; overflow: auto;'>";
// print_r($data);
// // htmlspecialchars
// echo "<pre>";
// die;
?>


<style>
.spinner-load {
    display: block;
    position: fixed;
    z-index: 1031;
    top: 50%;
    right: 45%;

    height: 100%;
    /* background-color: gray; */
}
</style>




<div class="dashboard-header d-flex flex-column grid-margin">
    <!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">ประวัติการปรับปรุงเวลา</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">ประวัติการปรับปรุงเวลา</p>
            </div>
        </div>
        <!-- <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
            <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0" id='btn_export'>

            </div>
        </div> -->
        <button type="submit" id="btn_export" class="btn btn-outline-primary btn-sm ml-2 ">Data Export</button>

    </div>
</div>
<div class="row">
    <div class="col-12">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/member">พนักงานทั้งหมด</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/member/profile">เพิ่มพนักงาน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member/import">Import ข้อมูลพนักงาน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member/workday">ประวัติบันทึกเวลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/member/leave">ประวัติการลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active"
                    href="<?=Yii::$app->request->baseUrl?>/member/change-time">ประวัติการปรับปรุงเวลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/member/ot">ประวัติการทำงานล่วงเวลา</a>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-12 mb-4">
        <div class="row ">
            <div class="col-2 ml-auto"></div>
            <div class="col-2"></div>
        </div>

        <form>
            <div class="form-row align-items-center mb-4">
                <div class="col-sm-3">
                    <span>ค้นหา</span>
                    <input type="text" class="form-control global_filter" id="global_filter" placeholder="คำที่ค้นหา">
                </div>
                <!--div class="col-4">
                        <button class="btn btn-outline-primary btn-sm  d-none d-md-block mt-3">Filter</button>
                      </div-->


                <div class="col-3 my-1 ml-auto">
                    <div class="row">
                        <div class="col-12">
                            <span>ค้นหาตามวันที่</span>
                            <div id="datepicker-popup" class="input-group date datepicker">
                                <input type="text" id="date" name="date" class="form-control"
                                    data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น" autocomplete="off">
                                <span class="input-group-addon input-group-append border-left">
                                    <span class="mdi mdi-calendar input-group-text"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col"><span id="เพศ"></span></div>
                <div class="col "><span id="แผนก"></span></div>
                <div class="col"><span id="ตำแหน่ง"></span></div>
                <div class="col"><span id="ประเภท"></span></div>
                <div class="col"><span id="สถานะ"></span></div>
            </div>
        </form>

    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                        <tr>
                            <th>รูป</th>
                            <th>รหัสพนักงาน</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>เพศ</th>
                            <th>แผนก</th>
                            <th>ตำแหน่ง</th>
                            <th>ประเภท</th>
                            <th>ประเภทย่อย</th>
                            <th>ผู้ดูแล</th>
                            <th>รายละเอียด</th>
                            <th>ตั้งวันที่/เวลา</th>
                            <th>ถึงวันที่/เวลา</th>
                            <th>สถานที่</th>
                            <th>หมายเหตุ</th>
                            <th>สถานะ</th>
                            <th>แนบเอกสาร</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php


foreach ($data as $key => $e) {

    // \app\components\Helpers::Debig(($e));

    $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
    if (!empty($e->img)) {

        $img = Yii::$app->request->baseUrl . '/' . $e->img;
        
    }

    $img_2 = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';

    if (!empty($e->img_url)) {

        // $img_2 = Yii::$app->request->baseUrl . '/' . $e->img_url;
        $img_2 ='https://api.thebestpromote.com/web/' . $e->img_url;

    }

    $displayActive = "";
    if ($e->isActive == "W") {
        $displayActive = 'รออนุมัติ';
    } else if ($e->isActive == "Y") {
        $displayActive = "อนุมัติแล้ว";
    }else if ($e->isActive == "N") {
        $displayActive = "ไม่อนุมัติ";
    }

    ?>

                        <tr class="dt-edit">
                            <td class="employee-col">
                                <img class="thumb mr-2" src="<?=$img?>" align="left">
                            </td>

                            <td><?=$e->member_no?></td>
                            <td><?=$e->name_th . ' ' . $e->surname_th?></td>
                            <td><?=$e->gender_name?></td>
                            <td><?=$e->department_name?></td>
                            <td><?=$e->position_name?></td>
                            <td><?=$e->type_name?></td>
                            <td><?=$e->subtype_name?></td>
                            <td><?=$e->memberundder_name?></td>
                            <td>ปรับปรุงเวลา</td>
                            <td>
                                <?=Yii::$app->Utilities->formatDate('d/m/Y - H:i', $e->start_date . " " . $e->start_time)?>
                            </td>
                            <td>
                                <?=Yii::$app->Utilities->formatDate('d/m/Y - H:i', $e->end_date . " " . $e->end_time)?>
                            </td>

                            <td><?=$e->member->member_location_name?></td>
                            <td><?=$e->remark?></td>
                            <td><?=$displayActive?></td>
                            <td>
                                <span class="img-thumbmail"><a href="<?=$img_2?>" target="blank"><img src="<?=$img_2?>"
                                            align="left"></a>
                                </span>

                            </td>

                        </tr>

                        <?php

}

?>

                    </tbody>
                </table>
                <input type="hidden" id="phpBaseUrl" value="<?=Yii::$app->request->baseUrl?>">

            </div>
        </div>
    </div>
</div>




<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>รายละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>
            <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div-->
        </div>

    </div>
</div>




<table id="dataTable_export" class="table " cellspacing="0" style="visibility:hidden">
    <!-- <table id="dataTable_export" class="table " cellspacing="0"> -->
    <thead>
        <tr>
            <th>รหัสพนักงาน</th>
            <th>ชื่อ-นามสกุล</th>
            <th>เพศ</th>
            <th>แผนก</th>
            <th>ตำแหน่ง</th>
            <th>ประเภท</th>
            <th>ประเภทย่อย</th>
            <th>ผู้ดูแล</th>
            <th>รายละเอียด</th>
            <th>ตั้งวันที่/เวลา</th>
            <th>ถึงวันที่/เวลา</th>
            <th>สถานที่</th>
            <th>หมายเหตุ</th>
            <th>สถานะ</th>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>


<div id="myModal_export" class="modal modaldetail" role="dialog">
    <div class="modal-dialog " style="max-width: 100;">
        <div class="modal-content">
            <div class="modal-header">
                <span>Download</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div style="margin-left:15px" class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0"
                        id='btn_export_modal'>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<?php
$script = <<< JS

const url_base = $("#phpBaseUrl").val()

    var columnsToSearch = {
        4: 'แผนก',
        6: 'ตำแหน่ง',
        7: 'ประเภท',
        8: 'ประเภทย่อย',
        3: 'เพศ',
        13: 'สถานะ'
    };

    table = $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        "bLengthChange" : false,
        // "pageLength": 2,
// get data by ajax
        ajax: {
            "url": url_base+"/member/change-dt",
            "type": "POST",
            "data" :{ _csrf:yii.getCsrfToken()},
           dataFilter: function(reps) {
                callDT();
                // console.log(reps);
                return reps;
            },
            error:function(err){
                  console.log(err);
            }
             
                         
        },
        searching: true,
        paging:   true,
        ordering: false,
        info:     false,
        scrollY: 720,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        
       createdRow: function( row, data, dataIndex ) {
            $(row).addClass( 'dt-edit' );
            
        },
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },

        ],
        responsive: false,
        fixedHeader: true

    });


    $('#dataTable_filter').hide()


// var buttons = new $.fn.dataTable.Buttons(table, {
//        buttons: [
//     {
//         extend: 'excel',
//         text: 'Data Export',
//         charset: 'utf-8',
//         extension: '.xlsx',
//         bom: true,
//         className: 'btn btn-outline-primary btn-sm  d-none d-md-block',
//     }]
// }).container().appendTo($('#btn_export'));



function callDT() {
    setTimeout(function(){  dt();}, 500);
  }
  function dt() {
    var colum = ["รูป", "รหัสพนักงาน", "ชื่อ-นามสกุล", "เพศ", "แผนก", "ตำแหน่ง", "ประเภท", "ประเภทย่อย",
        "ผู้ดูแล", "สาเหตุ", "วันที่เริ่มงาน", "วันสิ้นสุดงาน", "รายละเอียด", "สถานนะ", "เอกสารแนบ"
    ];
    //Edit row buttons
    $('.dt-edit').each(function() {
        $(this).on('click', function(evt) {
            \$this = $(this);
            var dtRow = \$this;
            $('div.modal-body').innerHTML = '';
            $('div.modal-body').append('<div class="row mb-4 ml-1 img-thumbmail">' + dtRow[0]
                .cells[0].innerHTML + '</div>');
            for (var i = 1; i < dtRow[0].cells.length; i++) {
                $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">' +colum[i] + '</span><span class="col-8">' + dtRow[0].cells[i].innerHTML +'</span><div/>');
                // console.log(i);
            }
            $('#myModal').modal('show');
        });
    });
    $('#myModal').on('hidden.bs.modal', function(evt) {
        $('.modal .modal-body').empty();
    });
}


async function downloadDropDown(){
    for (var i in columnsToSearch) {

    // console.log(columnsToSearch[i]);
    var _id = "";
    if (columnsToSearch[i] == "เพศ"){
        _id = "gender-search"
    } 
    if (columnsToSearch[i] == "แผนก"){
        _id = "department-search"
    }
     if (columnsToSearch[i] == "ตำแหน่ง"){
        _id = "position-search"
    }
    if (columnsToSearch[i] == "ประเภท"){
        _id = "type-search"
    }
    if (columnsToSearch[i] == "สถานะ"){
        _id = "status-search"
    }

    var select = $('<select id='+_id+' class="form-control"><option value="">Select</option></select>')
        .appendTo($('#' + columnsToSearch[i]).empty().text(columnsToSearch[i] + ' '))


    // get list from api 
    var arr  = await $.get(url_base+ '/member/getlist', {list: columnsToSearch[i]})
        var arrJson = JSON.parse(arr)

        if (columnsToSearch[i] == "สถานะ"){
                        select.append('<option value="wait">รออนุมัติ</option>');
                        select.append('<option value="accept">อนุมัติแล้ว</option>');
                        select.append('<option value="decline">ไม่อนุมัติ</option>');

                    }

        // console.log(arrJson)
        Object.keys(arrJson).forEach(function(key) {
            select.append('<option value="' + key + '">' + arrJson[key] + '</option>');

        })
}


$('#global_filter,#gender-search,#department-search,#position-search,#type-search,#status-search,#date').on( 'change', function () {
        let global_filter = $('#global_filter').val() 
        let genderSearch = $('#gender-search').val()
        let departmentSearch = $('#department-search').val()
        let positionSearch = $('#position-search').val()
        let typeSearch = $('#type-search').val()
        let statusSearch = $('#status-search').val()
        let date = $('#date').val()

         let strSearch = global_filter+'&'+genderSearch +'&'+departmentSearch+'&'+positionSearch+'&'+typeSearch +'&'+statusSearch +'&'+date 
         
        //  console.log(strSearch);
         table.search( strSearch ).draw();
    } );
    
  }


 downloadDropDown()

 $('#date').datepicker({
   format: "yyyy-mm-dd",
   orientation: "bottom",
   autoclose: true,
   todayHighlight: true,
});

    
    $('#my-table_filter').hide();

    $('input.column_filter').on('keyup click', function() {
        filterColumn($(this).parents('tr').attr('data-column'));
    });




    async function getData(){
    var arr  = await $.get(url_base+ '/member-change-time/getdata')
    // console.log('arr,',arr);
    return arr.result
}

$("#btn_export").on('click', async function(evt){


    var tbody = $("#dataTable_export tbody");

if (tbody.children().length == 0) {
    $("#btn_export").append(' <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');

    $('#loadingGrow').css("visibility","visible");
    // console.log('ok');
   var data = await getData()
//    console.log(data);

   data.map((value)=>{          
    var newRowContent = "<tr class='dt-edit'>"
    newRowContent += "<td>"+value.member_no+"</td>"
    newRowContent += "<td>"+value.fullname+"</td>"
    newRowContent += "<td>"+value.gender_name+"</td>"
    newRowContent += "<td>"+value.department_name+"</td>"
    newRowContent += "<td>"+value.position_name+"</td>"
    newRowContent += "<td>"+value.type_name+"</td>"
    newRowContent += "<td>"+value.subtype_name+"</td>"
    newRowContent += "<td>"+value.memberundder_name+"</td>"
    newRowContent += "<td>"+value.detail+"</td>"
    newRowContent += "<td>"+value.start_date+"</td>"
    newRowContent += "<td>"+value.end_date+"</td>"
    newRowContent += "<td>"+value.member_location_name+"</td>"
    newRowContent += "<td>"+value.remark+"</td>"
    newRowContent += "<td>"+value.displayActive+"</td>"
    newRowContent += "</tr>"


    $("#dataTable_export tbody").append(newRowContent);

   })

   table_export = $('#dataTable_export').DataTable({
      dom: 'it',
        searching: true,
        paging:   false,
        ordering: false,
        info:     false,
        scrollY: 300,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: true,
        fixedHeader: true,

    });


    var buttons = new $.fn.dataTable.Buttons(table_export, {
       buttons: [
    {
        extend: 'excel',
        text: 'Download',
        charset: 'utf-8',
        extension: '.xlsx',
        bom: true,
        className: 'btn btn-outline-primary btn-sm  d-none d-md-block testtest',
    }]
}).container().appendTo($('#btn_export_modal'));

   $('#myModal_export').modal('show');

   $("#btn_export").find('span:first').remove();

}else{
    $('#myModal_export').modal('show');

}

})


JS;
$this->registerJs($script, View::POS_READY);

?>