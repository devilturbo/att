<?php

use yii\web\View;

$this->title = 'บันทึกการลา';
?>
<style>
table tr td {
    border-top: 0 !important;

}
</style>
<div class="dashboard-header d-flex flex-column grid-margin">
    <!-- dashboard header -->
    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">ประวัติพนักงาน</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าหลัก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">บันทึกการลา</p>
            </div>
        </div>
        <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
            <!--button class="btn btn-primary btn-sm mr-3 d-none d-md-block">Download Report</button>
              <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button-->
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <?php
                    $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
                    if (!empty($model->img)) {

                        $img = Yii::$app->request->baseUrl . '/' . $model->img;
                    }
                    ?>
                    <div class="col-1 img-thumbmail"><img class="mr-2 lazy" src="<?= $img ?>" align="left"></div>
                    <div class="col-3">
                        <table class="table-sm card-table-one">
                            <tbody>

                                <tr>
                                    <td class="px-0"><strong>รหัสพนักงาน</strong></td>
                                    <td class="text-muted"><?= $model->member_no ?></td>
                                </tr>
                                <tr>
                                    <td class="px-0"><strong>ชื่อ-นามสกุล</strong></td>
                                    <td class="text-muted"><?= $model->fullname ?></td>
                                </tr>
                                <tr>
                                    <td class="px-0"><strong>เอเจนซี่</strong></td>
                                    <td class="text-muted"><?= $model->agency_name ?></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="col-3">
                        <table class="table-sm card-table-one">
                            <tbody>
                                <tr>
                                    <td class="px-0"><strong>ตำแหน่ง</strong></td>
                                    <td class="text-muted"><?= $model->department_name ?></td>
                                </tr>
                                <tr>
                                    <td class="px-0"><strong>ประเภท</strong></td>
                                    <td class="text-muted"><?= $model->type_name ?></td>
                                </tr>

                                <tr>
                                    <td class="px-0"><strong>ประเภทย่อย</strong></td>
                                    <td class="text-muted"><?= $model->subtype_name ?></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>

                    <div class="col-3">
                        <table class="table-sm card-table-one">
                            <tbody>
                                <tr>
                                    <td class="px-0"><strong>ผู้ดูแล</strong></td>
                                    <td class="text-muted"><?= $model->memberundder_name ?></td>
                                </tr>
                                <tr>
                                    <td class="px-0"><strong>อีเมล</strong></td>
                                    <td class="text-muted"><?= $model->email ?></td>
                                </tr>
                                <tr>
                                    <td class="px-0"><strong>เบอร์โทร</strong></td>
                                    <td class="text-muted"><?= $model->phone_number ?></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-12">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link"
                    href="<?= Yii::$app->homeUrl ?>member/profile-detail?id=<?= $model->id ?>">ประวัติพนักงาน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link "
                    href="<?=Yii::$app->homeUrl?>member/profile-workday?id=<?=$model->id?>">บันทึกเวลาทำงาน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active"
                    href="<?= Yii::$app->homeUrl ?>member/profile-leave?id=<?= $model->id ?>">บันทึกการลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link "
                    href="<?= Yii::$app->homeUrl ?>member/profile-change-time?id=<?= $model->id ?>">ปรับปรุงเวลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link "
                    href="<?= Yii::$app->homeUrl ?>member/profile-ot?id=<?= $model->id ?>">ทำงานล่วงเวลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link "
                    href="<?= Yii::$app->homeUrl ?>member/profile-location?id=<?= $model->id ?>">สถานที่ทำงาน</a>
            </li>
            <!--li class="nav-item ml-auto">
                <button class="btn btn-outline-primary btn-sm">ดูในรูปแผนที่</button>
              </li-->
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-2"><span class="h6 text-bold">สิทธิ์ในการลา</span><br></div>


                    <?php
                    $calcLeave = \app\components\Utilities::getNormalHoliday($model->start_work_date,$model->start_date,$model->isLeave);
                    $probationStatus = $calcLeave['probationStatus'];
                    if($model->membersubtype->leave_type == 'Y'){
                        $probationStatus = 1;
                    }

                    $holidayLeft = $calcLeave['hoilday_leave'];
                    $errandLeft = $calcLeave['errand_leave'];
                    $errandCutLeft = $modelCompany->errand_cut_leave;
                    $sickLeft = $modelCompany->sick_leave;
                    $sickCutLeft = $modelCompany->sick_cut_leave;
                    $maternityLeft = $modelCompany->maternity_leave;
                    $ordainedLeft = $modelCompany->ordained_leave;
                    // ordained_leave

                    foreach ($dataAll as $key => $e) {

                        $start = strtotime($e->start_date);
                        $end = strtotime($e->end_date);
                        $datediff = $end - $start;
                        $day_ =  round($datediff / (60 * 60 * 24));

                        if ($day_ == 0) {
                            $day_ = 1;
                        }

                        switch ($e->leave_type_id) {
                            case 1:
                                // ลาพักร้อน
                                $holidayLeft -= $day_;
                                break;
                            case 2:
                                // ลากิจไม่คิดเงิน
                                $errandCutLeft -= $day_;
                                break;
                            case 3:
                                // ลากิจคิดเงิน
                                $errandLeft -= $day_;
                                break;
                            case 4:
                                // ลาป่วยไม่คิดเงิน
                                $sickLeft -= $day_;
                                break;
                            case 5:
                                // ลาป่วยคิดเงิน
                                $sickCutLeft -= $day_;
                                break;
                            case 6:
                                // ลาคลอด
                                $ordainedLeft -= $day_;
                                break;
                            case 7:
                                // ลาบวช
                                $maternityLeft -= $day_;
                                break;

                        }

                    }
                    if (!empty($model->membersubtype)) {
                        if ($model->membersubtype->isLeave == "N") {
                            $holidayLeft = 0;
                            $errandLeft = 0;
                            $errandCutLeft = 0;
                            $sickLeft = 0;
                            $sickCutLeft = 0;
                            $maternityLeft = 0;
                            $ordainedLeft = 0;
                        }
                    }

                    if($holidayLeft < 0)
                        $holidayLeft = 0;
                    if($errandLeft < 0)
                        $errandLeft = 0;
                    if($errandCutLeft < 0)
                        $errandCutLeft = 0;
                    if($sickLeft < 0)
                        $sickLeft = 0;
                    if($sickCutLeft < 0)
                        $sickCutLeft = 0;
                    if($maternityLeft < 0)
                        $maternityLeft = 0;
                    if($ordainedLeft < 0)
                        $ordainedLeft = 0;

                    ?>


                    <?php
                    if ($model->isCheckLeave == "N") {
        ?>
                    <div class="col-3">
                        <span class="text-muted">
                            ลาพักร้อน ไม่ระบุ วัน - คงเหลือ ไม่ระบุ วัน<br>
                            วันลากิจ(คิดเงิน) ไม่ระบุ วัน - คงเหลือ ไม่ระบุ วัน<br>
                            วันลาป่วย(คิดเงิน) ไม่ระบุ วัน - คงเหลือ ไม่ระบุ วัน<br>

                        </span>

                    </div>
                    <div class="col-3">
                        <span class="text-muted">
                            วันลากิจ(ไม่คิดเงิน) ไม่ระบุ วัน - คงเหลือ ไม่ระบุ วัน<br>
                            วันลาป่วย(ไม่คิดเงิน) ไม่ระบุ วัน - คงเหลือ ไม่ระบุ วัน<br>
                            ลาคลอด ไม่ระบุ วัน - คงเหลือ ไม่ระบุ วัน<br>
                        </span>

                    </div>
                    <div class="col-3">

                        <span class="text-muted">
                            ลาบวช ไม่ระบุ วัน - คงเหลือ ไม่ระบุ วัน<br>

                        </span>
                    </div>

                    <?php

    } else {
        ?>

                    <div class="col-3">

                        <span class="text-muted">
                            ลาพักร้อน <?=$probationStatus == 0 ?0:$calcLeave['hoilday_leave'] ?> วัน -
                            คงเหลือ <?=$probationStatus == 0 ?0:$holidayLeft ?> วัน<br>
                            วันลากิจ(คิดเงิน)  <?=$probationStatus == 0 ?0:$calcLeave['errand_leave'] ?> วัน -
                            คงเหลือ <?=$probationStatus == 0 ?0:$errandLeft ?> วัน<br>
                            วันลาป่วย(คิดเงิน) <?=$probationStatus == 0 ?0: $modelCompany->sick_cut_leave ?> วัน -
                            คงเหลือ <?=$probationStatus == 0 ?0:$sickCutLeft ?> วัน<br>

                        </span>

                    </div>
                    <div class="col-3">
                        <span class="text-muted">
                            วันลากิจ(ไม่คิดเงิน)  <?=$probationStatus == 0 ?0:$modelCompany->errand_cut_leave ?> วัน -
                            คงเหลือ <?=$probationStatus == 0 ?0:$errandCutLeft ?> วัน<br>
                            วันลาป่วย(ไม่คิดเงิน) <?=$probationStatus == 0 ?0:$modelCompany->sick_leave ?> วัน -
                            คงเหลือ <?=$probationStatus == 0 ?0:$sickLeft ?> วัน<br>
                            ลาคลอด <?=$probationStatus == 0 ?0:$modelCompany->maternity_leave ?> วัน -
                            คงเหลือ <?=$probationStatus == 0 ?0:$maternityLeft ?> วัน<br>
                        </span>

                    </div>
                    <div class="col-3">

                        <span class="text-muted">
                            ลาบวช <?=$probationStatus == 0 ?0:$modelCompany->ordained_leave ?> วัน - คงเหลือ <?=$probationStatus == 0 ?0:$ordainedLeft ?> วัน<br>

                        </span>
                    </div>
                    <?php
}

?>


                </div>


                <?php


function thai_date($time){
    $thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");
    $thai_month_arr=array(
     "0"=>"",
     "1"=>"มกราคม",
     "2"=>"กุมภาพันธ์",
     "3"=>"มีนาคม",
     "4"=>"เมษายน",
     "5"=>"พฤษภาคม",
     "6"=>"มิถุนายน", 
     "7"=>"กรกฎาคม",
     "8"=>"สิงหาคม",
     "9"=>"กันยายน",
     "10"=>"ตุลาคม",
     "11"=>"พฤศจิกายน",
     "12"=>"ธันวาคม"     
    );
 $thai_date_return="วัน".$thai_day_arr[date("w",strtotime($time))];
 $thai_date_return.= "ที่ ".date("j",strtotime($time));
 $thai_date_return.=" ".$thai_month_arr[date("n",strtotime($time))];
 $thai_date_return.= " ".(date("Y",strtotime($time))+543);
 //$thai_date_return.= "  ".date("H:i",$time)." น.";
 return $thai_date_return;
}


?>

                <div class="row mb-3">
                    <h5 class="font-weight-bold my-1 mt-4 ml-2">
                        <?=$dataCount?> รายการ
                        - 
                        <?php 
                              if (empty($_GET['date'])){
                                echo  thai_date(date('Y-m-d H:i:s')); 
                              }else{
                                echo  thai_date($_GET['date']); 
                              }
                              ?>
                    </h5>

                    <div class="col-3 my-1 ml-auto">

                        <form action="<?=Yii::$app->homeUrl?>member/profile-leave">
                            <div class="row">
                                <div class="col-9">
                                    <input type="hidden" name="id" value="<?=$_GET['id']?>">
                                    <span>ค้นหาตามวันที่</span>
                                    <div id="datepicker-popup" class="input-group date datepicker">
                                        <input type="text" id="date" name="date"
                                            value="<?=empty($_GET['date'] ) ? Yii::$app->Utilities->formatDate('m/d/Y',  date('Y-m-d')): Yii::$app->Utilities->formatDate('m/d/Y',  $_GET['date'])?>"
                                            class="form-control" data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น">
                                        <span class="input-group-addon input-group-append border-left">
                                            <span class="mdi mdi-calendar input-group-text"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <span>&nbsp;</span>
                                    <button type="submit" class="btn btn-outline-primary btn-sm ml-2">ค้นหา</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


                <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                        <tr>

                            <th>รายละเอียด</th>
                            <th>ตั้งวันที่/เวลา</th>
                            <th>ถึงวันที่/เวลา</th>
                            <th>หมายเหตุ</th>
                            <!-- <th>ประเภทการลา</th> -->
                            <th>สถานะ</th>
                            <th>เอกสารแนบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        // \app\components\Helpers::Debig($data);

                    foreach ($data as $key => $e) {

                        $img_2 = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';

                            $detail ="";
                            switch ($e->type_id) {
                                case 1:
                                    $detail= "ลาพักร้อน";
                                    break;
                                case 2:
                                    $detail = "ลากิจ (ไม่คิดเงิน)";
                                    break;
                                case 3:
                                    $detail = "ลากิจ (คิดเงิน)";
                                    break;
                                case 4:
                                    $detail = "ลาป่วย (ไม่คิดเงิน)";
                                    break;
                                case 5:
                                    $detail = "ลาป่วย (คิดเงิน)";
                                    break;
                                case 6:
                                     $detail = "ลาบวช";
                                    break;
                                case 7:
                                    $detail =  "ลาคลอด";
                                    break;
    
                            }

                        if (!empty($e->img_url)) {

                            // $img_2 = Yii::$app->request->baseUrl . '/' . $e->img_url;
                            $img_2 = 'https://api.thebestpromote.com/web/' . $e->img_url;

                        }

                        $displayActive = "";
                        if ($e->isActive == "W") {
                            $displayActive = 'รออนุมัติ';
                        } else if ($e->isActive == "Y") {
                            $displayActive = "อนุมัติแล้ว";
                        } else if ($e->isActive == "N") {
                            $displayActive = "ไม่อนุมัติ";
                        }

                        ?>

                        <tr class="dt-edit">
                            <td><?=$detail?></td>
                            <td><?= Yii::$app->Utilities->formatDate('d/m/Y', $e->start_date) ?></td>
                            <td><?= Yii::$app->Utilities->formatDate('d/m/Y', $e->end_date) ?></td>
                            <td><?= $e->remark ?></td>
                            <!-- <td><?php // echo $e->leave_type; ?></td> -->
                            <td><?= $displayActive ?></td>
                            <td>
                                <span class="img-thumbmail"><a href="<?= $img_2 ?>" target="blank"><img
                                            src="<?= $img_2 ?>" align="left"></a>
                                </span>
                            </td>

                        </tr>

                        <?php

                    }

                    ?>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal modaldetail" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>รายละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>

        </div>

    </div>
</div>


<?php
$script = <<< JS


$('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging:   false,
        ordering: false,
        info:     false,
        scrollY: 500,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        responsive: false,
        fixedHeader: true


    });

var colum = ["รายละเอียด", "ตั้งวันที่/เวลา","ถึงวันที่/เวลา","หมายเหตุ","สถานะ","เอกสารแนบ"];
   //Edit row buttons
  $('.dt-edit').each(function () {
    $(this).on('click', function(evt){
      \$this = $(this);
      var dtRow = \$this;
      $('div.modal-body').innerHTML='';
      //$('div.modal-body').append( '<div class="row mb-4 ml-1 img-thumbmail">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=0; i < dtRow[0].cells.length; i++){
         $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">'+colum[i]+'</span><span class="col-8">'+dtRow[0].cells[i].innerHTML+'</span><div/>');
            console.log(i);
      }
      $('#myModal').modal('show');
    });
  });
  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .modal-body').empty();
  });


JS;
$this->registerJs($script, View::POS_READY);

?>