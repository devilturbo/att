  <?php
use yii\web\View;
$this->title = 'ทำงานล่วงเวลา';
?>
  <style>
table tr td {
    border-top: 0 !important;

}
  </style>
  <div class="dashboard-header d-flex flex-column grid-margin">
      <!-- dashboard header -->
      <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
          <div class="d-flex align-items-center">
              <h4 class="mb-0 font-weight-bold mr-2">ประวัติพนักงาน</h4>
              <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าหลัก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">ทำงานล่วงเวลา</p>
              </div>
          </div>
          <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
              <!--button class="btn btn-primary btn-sm mr-3 d-none d-md-block">Download Report</button>
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button-->
          </div>
      </div>
  </div>

  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-body">
                  <div class="row">
                      <?php
$img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
if (!empty($model->img)) {

    $img = Yii::$app->request->baseUrl . '/' . $model->img;
}
?>
                      <div class="col-1 img-thumbmail"><img class="mr-2 lazy" src="<?=$img?>" align="left"></div>
                      <div class="col-3">
                          <table class="table-sm card-table-one">
                              <tbody>

                                  <tr>
                                      <td class="px-0"><strong>รหัสพนักงาน</strong></td>
                                      <td class="text-muted"><?=$model->member_no?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>ชื่อ-นามสกุล</strong></td>
                                      <td class="text-muted"><?=$model->fullname?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>เอเจนซี่</strong></td>
                                      <td class="text-muted"><?=$model->agency_name?></td>
                                  </tr>

                              </tbody>
                          </table>
                      </div>
                      <div class="col-3">
                          <table class="table-sm card-table-one">
                              <tbody>
                                  <tr>
                                      <td class="px-0"><strong>ตำแหน่ง</strong></td>
                                      <td class="text-muted"><?=$model->department_name?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>ประเภท</strong></td>
                                      <td class="text-muted"><?=$model->type_name?></td>
                                  </tr>

                                  <tr>
                                      <td class="px-0"><strong>ประเภทย่อย</strong></td>
                                      <td class="text-muted"><?=$model->subtype_name?></td>
                                  </tr>

                              </tbody>
                          </table>
                      </div>

                      <div class="col-3">
                          <table class="table-sm card-table-one">
                              <tbody>
                                  <tr>
                                      <td class="px-0"><strong>ผู้ดูแล</strong></td>
                                      <td class="text-muted"><?=$model->memberundder_name?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>อีเมล</strong></td>
                                      <td class="text-muted"><?=$model->email?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>เบอร์โทร</strong></td>
                                      <td class="text-muted"><?=$model->phone_number?></td>
                                  </tr>

                              </tbody>
                          </table>
                      </div>

                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="row mt-3">
      <div class="col-12">
          <ul class="nav nav-pills" id="pills-tab" role="tablist">
              <li class="nav-item">
                  <a class="nav-link"
                      href="<?=Yii::$app->homeUrl?>member/profile-detail?id=<?=$model->id?>">ประวัติพนักงาน</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link " href="<?=Yii::$app->homeUrl?>member/profile-workday?id=<?=$model->id?>">บันทึกเวลาทำงาน</a>
                </li>
              <li class="nav-item">
                  <a class="nav-link "
                      href="<?=Yii::$app->homeUrl?>member/profile-leave?id=<?=$model->id?>">บันทึกการลา</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link "
                      href="<?=Yii::$app->homeUrl?>member/profile-change-time?id=<?=$model->id?>">ปรับปรุงเวลา</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link active"
                      href="<?=Yii::$app->homeUrl?>member/profile-ot?id=<?=$model->id?>">ทำงานล่วงเวลา</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link "
                      href="<?=Yii::$app->homeUrl?>member/profile-location?id=<?=$model->id?>">สถานที่ทำงาน</a>
              </li>

          </ul>
      </div>
  </div>


  <?php


function thai_date($time){
    $thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");
    $thai_month_arr=array(
     "0"=>"",
     "1"=>"มกราคม",
     "2"=>"กุมภาพันธ์",
     "3"=>"มีนาคม",
     "4"=>"เมษายน",
     "5"=>"พฤษภาคม",
     "6"=>"มิถุนายน", 
     "7"=>"กรกฎาคม",
     "8"=>"สิงหาคม",
     "9"=>"กันยายน",
     "10"=>"ตุลาคม",
     "11"=>"พฤศจิกายน",
     "12"=>"ธันวาคม"     
    );
 $thai_date_return="วัน".$thai_day_arr[date("w",strtotime($time))];
 $thai_date_return.= "ที่ ".date("j",strtotime($time));
 $thai_date_return.=" ".$thai_month_arr[date("n",strtotime($time))];
 $thai_date_return.= " ".(date("Y",strtotime($time))+543);
 //$thai_date_return.= "  ".date("H:i",$time)." น.";
 return $thai_date_return;
}


?>


  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-body">

              <div class="row mb-3">
                    <h5 class="font-weight-bold my-1 mt-4 ml-2">
                        <?=$dataCount?> รายการ
                        - 
                        <?php 
                              if (empty($_GET['date'])){
                                echo  thai_date(date('Y-m-d H:i:s')); 
                              }else{
                                echo  thai_date($_GET['date']); 
                              }
                              ?>
                    </h5>

                    <div class="col-3 my-1 ml-auto">

                        <form action="<?=Yii::$app->homeUrl?>member/profile-change-time">
                            <div class="row">
                                <div class="col-9">
                                    <input type="hidden" name="id" value="<?=$_GET['id']?>">
                                    <span>ค้นหาตามวันที่</span>
                                    <div id="datepicker-popup" class="input-group date datepicker">
                                        <input type="text" id="date" name="date"
                                            value="<?=empty($_GET['date'] ) ? Yii::$app->Utilities->formatDate('m/d/Y',  date('Y-m-d')): Yii::$app->Utilities->formatDate('m/d/Y',  $_GET['date'])?>"
                                            class="form-control" data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น">
                                        <span class="input-group-addon input-group-append border-left">
                                            <span class="mdi mdi-calendar input-group-text"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <span>&nbsp;</span>
                                    <button type="submit" class="btn btn-outline-primary btn-sm ml-2">ค้นหา</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                  <table id="dataTable" class="table " cellspacing="0">
                      <thead>
                          <tr>

                              <th>รายละเอียด</th>
                              <th>ตั้งวันที่/เวลา</th>
                              <th>ถึงวันที่/เวลา</th>
                              <th>สถานที่</th>
                              <th>หมายเหตุ</th>
                              <th>สถานะ</th>
                              <th>เอกสารแนบ</th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php


foreach ($data as $key => $e) {

    // \app\components\Helpers::Debig(($e->member->areas));

    $img_2 = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';

    if (!empty($e->img_url)) {

        // $img_2 = Yii::$app->request->baseUrl . '/' . $e->img_url;
        $img_2 ='https://api.thebestpromote.com/web/' . $e->img_url;

    }

    $displayActive = "";
    if ($e->isActive == "W") {
        $displayActive = 'รออนุมัติ';
    } else if ($e->isActive == "Y") {
        $displayActive = "อนุมัติแล้ว";
    }else if ($e->isActive == "N") {
        $displayActive = "ไม่อนุมัติ";
    }

    ?>

                          <tr class="dt-edit">
                              <td>ทำงานล่วงเวลา</td>
                              <td>
                                  <?=Yii::$app->Utilities->formatDate('d/m/Y - H:i', $e->ot_date . " " . $e->start_time)?>
                              </td>
                              <td>
                                  <?=Yii::$app->Utilities->formatDate('d/m/Y - H:i', $e->ot_date . " " . $e->end_time)?>
                              </td>
                              <td><?=$e->member->member_location_name?></td>
                              <td><?=$e->remark?></td>
                              <td><?=$displayActive?></td>
                              <td>
                                  <span class="img-thumbmail"><a href="<?=$img_2?>" target="blank"><img
                                              src="<?=$img_2?>" align="left"></a>
                                  </span>
                              </td>

                          </tr>

                          <?php

}

?>

                      </tbody>
                  </table>


              </div>
          </div>
      </div>
  </div>

  <div id="myModal" class="modal modaldetail" role="dialog">
      <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <span>รายละเอียด</span>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">

              </div>

          </div>

      </div>
  </div>


  <?php
$script = <<< JS


$('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging:   false,
        ordering: false,
        info:     false,
        scrollY: 500,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        responsive: false,
        fixedHeader: true


    });

var colum = ["รายละเอียด", "ตั้งวันที่/เวลา","ถึงวันที่/เวลา","สถานที่","หมายเหตุ","สถานะ","เอกสารแนบ"];
   //Edit row buttons
  $('.dt-edit').each(function () {
    $(this).on('click', function(evt){
      \$this = $(this);
      var dtRow = \$this;
      $('div.modal-body').innerHTML='';
      //$('div.modal-body').append( '<div class="row mb-4 ml-1 img-thumbmail">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=0; i < dtRow[0].cells.length; i++){
         $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">'+colum[i]+'</span><span class="col-8">'+dtRow[0].cells[i].innerHTML+'</span><div/>');
            console.log(i);
      }
      $('#myModal').modal('show');
    });
  });
  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .modal-body').empty();
  });


JS;
$this->registerJs($script, View::POS_READY);

?>