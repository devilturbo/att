<?php

use yii\web\View;
use app\components\Utilities;
use app\components\Helpers;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'member';
$this->params['breadcrumbs'][] = $this->title;

?>
    <style>
        table{
            margin: 0 auto;
            width: 1198px !important;
            clear: both;
            border-collapse: collapse;
            table-layout: fixed;
        }
    </style>

<div class="dashboard-header d-flex flex-column grid-margin">
    <!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">พนักงานทั้งหมด(<?=$modelRows?>)</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงานทั้งหมด</p>
            </div>
        </div>
        <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
            <a style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>" href="<?=Yii::$app->homeUrl?>member/profile"><button
                        class="btn btn-primary btn-sm mr-2 d-none d-md-block">เพิ่มพนักงานใหม่</button></a>
            <span style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>" id='btn_export'></span>
        </div>
    </div>
</div>

<div class="row mb-4">
    <div class="col-12">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="<?=Yii::$app->request->baseUrl?>/member">พนักงานทั้งหมด</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/member/profile">เพิ่มพนักงาน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member/import">Import ข้อมูลพนักงาน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member/workday">ประวัติบันทึกเวลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/member/leave">ประวัติการลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"
                   href="<?=Yii::$app->request->baseUrl?>/member/change-time">ประวัติการปรับปรุงเวลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link "
                   href="<?=Yii::$app->request->baseUrl?>/member/ot">ประวัติการทำงานล่วงเวลา</a>
            </li>
        </ul>
    </div>
</div>

 <div class="row">
    <div class="col-12 mb-4">
        <div class="row ">
            <div class="col-2 ml-auto"></div>
            <div class="col-2"></div>
        </div>

        <form>
            <div class="form-row align-items-center mb-2">
                <div class="col-sm-3 my-1-">
                    <span>ชื่อ</span>
                    <input type="text" class="form-control " id="text_search" placeholder="คำที่ค้นหา">
                </div>
                <div class="col-6">
                    <div class="row">
                        <div class="col">
                            <label for="example-text-input" class="col-form-label">สถานะ</label>
                            <select class="form-control" name="brand" id="status_search">
                                <option   value="Y">Active </option>
                                <option   value="N">inActive</option>
                            </select>
                        </div>
                        <div class="col">
                            <label for="example-text-input" class="col-form-label">ประเภท</label>
                            <select class="form-control" name="brand" id="type_search">
                                <?php
                                $arrBrand  = Yii::$app->Utilities->getListType();

                                ?>
                                <?php foreach ($arrBrand as $key => $item): ?>
                                    <?php
                                    $selected = '';
                                    if(!empty($model->brand_id)){
                                        if($model->brand_id == $key){
                                            $selected = 'selected';
                                        }
                                    }

                                    ?>
                                    <option <?=$selected?>  value="<?=$key?>">
                                        <?=$item?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="col">
                            <label for="example-text-input" class="col-form-label">ประเภทย่อย</label>
                            <select class="form-control" name="brand" id="subtype_search">
                                <?php
                                $arrBrand  = Yii::$app->Utilities->getListSubType();

                                ?>
                                <?php foreach ($arrBrand as $key => $item): ?>
                                    <?php
                                    $selected = '';
                                    if(!empty($model->brand_id)){
                                        if($model->brand_id == $key){
                                            $selected = 'selected';
                                        }
                                    }

                                    ?>
                                    <option <?=$selected?>  value="<?=$key?>">
                                        <?=$item?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="col">
                            <label for="example-text-input" class="col-form-label">ตำแหน่ง</label>
                            <select class="form-control" name="brand" id="position_search">
                                <?php
                                $arrBrand  = Yii::$app->Utilities->getListPosition();

                                ?>
                                <?php foreach ($arrBrand as $key => $item): ?>
                                    <?php
                                    $selected = '';
                                    if(!empty($model->brand_id)){
                                        if($model->brand_id == $key){
                                            $selected = 'selected';
                                        }
                                    }

                                    ?>
                                    <option <?=$selected?>  value="<?=$key?>">
                                        <?=$item?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                        </div>

                    </div>
                </div>



            </div>



        </form>

    </div>
</div>

<!-- Modal -->
<div id="ModalFilter" class="modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>ค้นหาโดยละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="col-12 p-4">
                <div class="row">
                    <div class="col-6 mb-2"><span id="เพศ"></span></div>
                    <div class="col-6 mb-2 "><span id="เอเจนซี่"></span></div>
                    <div class="col-6 mb-2 "><span id="แผนก"></span></div>
                    <div class="col-6 mb-2"><span id="ตำแหน่ง"></span></div>
                    <div class="col-6 mb-2"><span id="ประเภท"></span></div>
                    <div class="col-6 mb-2"><span id="ประเภทย่อย"></span></div>
                    <div class="col-6 mb-2"><span id="พื้นที่"></span></div>
                    <div class="col-6 mb-2"><span id="สถานนะ"></span></div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                    <tr >
                        <th width="50px;">รูป</th>
                        <th width="80px;">รหัสพนักงาน</th>
                        <th width="150px;">บัตรประชาชน</th>
                        <th width="50px;">คำนำหน้า</th>
                        <th width="200px;">ชื่อ-นามสกุล</th>
                        <th width="50px;">เพศ</th>
                        <th width="80px;">เบอร์โทรศัพท์</th>
                        <th  width="200px;">อีเมล</th>
                        <th  width="80px;">เอเจนซี่</th>
                        <th  width="150px;">แผนก</th>
                        <th  width="80px;">ตำแหน่ง</th>
                        <th  width="80px;">ประเภท</th>
                        <th  width="80px;">ประเภทย่อย</th>

                        <th  width="150px;">ผู้ดูแล</th>
                        <th  width="80px;">พื้นที่</th>
                        <th  width="80px;">เขตพื้นที่</th>
                        <th  width="80px;">ช่องทาง</th>

                        <th  width="80px;">วันที่เริ่มงาน</th>
                        <th  width="80px;">วันที่สิ้นสุด</th>
                        <th  width="150px;">อายุงาน</th>
                        <th  width="80px;">สถานนะ</th>
                        <th  width="80px;">ดำเนินการ</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>รายละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>
            <!--div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div-->
        </div>

    </div>
</div>

<input type="hidden" id="phpBaseUrl" value="<?=Yii::$app->request->baseUrl?>">

<?php
$script = <<< JS

const url_base = $("#phpBaseUrl").val()

   
    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

      table = $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        "bLengthChange" : false,
// get data by ajax
        ajax: {
            "url": url_base+ "/member/member-dt",
            "type": "POST",
            "data" :{ _csrf:yii.getCsrfToken()},
           dataFilter: function(reps) {
                callDT();
                return reps;
            },
            error:function(err){
                  console.log(err);
            }
             
                         
        },
        searching: true,
        paging:   true,
        ordering: false,
        info:     false,
        scrollY: 500,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        
       createdRow: function( row, data, dataIndex ) {
            $(row).addClass( 'dt-edit' );
            
        },
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
           { targets: 21,
               createdCell: function (td, cellData, rowData, row, col) {
              
                       $(td).html('<a class="btn btn-outline-primary btn-sm mb-1" href="'+cellData+'">ประวัติ</a>')
                   
                    
                }
          },
        ],
        responsive: false,
        fixedHeader: true

    });
    
    $('#text_search,#type_search,#status_search,#subtype_search,#position_search').on( 'change', function () {
         let status_search = $('#status_search').val()
         let global_filter = $('#text_search').val()
         let type_search = $('#type_search').val()
         let subtype_search = $('#subtype_search').val()
         let position_search = $('#position_search').val()
         let strSearch = global_filter+'&'+type_search+'&'+status_search+'&'+subtype_search+'&'+position_search
         
         table.search( strSearch ).draw();
    } );
    $('#dataTable_filter').hide()


      var buttons = new $.fn.dataTable.Buttons(table, {
        buttons: [
     {
         extend: 'excel',
         text: 'Data Export',
         charset: 'utf-8',
         extension: '.xlsx',
         bom: true,
         className: 'btn btn-outline-primary btn-sm  d-none d-md-block',
     }]
 }).container().appendTo($('#btn_export'));


JS;
$script2 = <<< JS
function callDT() {
    setTimeout(function(){  dt();}, 500);
  }
  function dt() {
    var colum = ["รูป", "รหัสพนักงาน", "บัตรประชาชน","คำนำหน้า","ชื่อ-นามสกุล",  "เพศ", "เบอร์โทรศัพท์","อีเมล", "เอเจนซี่", "แผนก","ตำแหน่ง","ประเภท","ประเภทย่อย","ผู้ดูแล","พื้นที่","เขตพื้นที่","ช่องทาง","วันที่เริ่มงาน","วันสิ้นสุดงาน","อายุงาน","สถานนะ","ดำนเนินการ"];
   //Edit row buttons
  $('.dt-edit').each(function () {
    $(this).on('click', function(evt){
      _this = $(this);
      var dtRow = _this;
      $('div.modal-body').innerHTML='';
      $('div.modal-body').append( '<div class="row mb-4 ml-1 img-thumbmail lazy">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=1; i < dtRow[0].cells.length; i++){
         $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">'+colum[i]+'</span><span class="col-8">'+dtRow[0].cells[i].innerHTML+'</span><div/>');
            console.log(i);
      }
      $('#myModal').modal('show');
    });
  });

  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .modal-body').empty();
  });
}
JS;
$this->registerJs($script2, View::POS_HEAD);
$this->registerJs($script, View::POS_READY);
?>