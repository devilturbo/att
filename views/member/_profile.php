<?php
use app\components\Utilities;
use yii\bootstrap\ActiveForm;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'member-profile';
$this->params['breadcrumbs'][] = $this->title;

$arrTitleTH = Yii::$app->Utilities->getListTitleTH();
$arrTitleEN = Yii::$app->Utilities->getListTitleEN();
$arrGender = Yii::$app->Utilities->getListGender();
$arrRelation = Yii::$app->Utilities->getListRelation();

?>
<?php $form = ActiveForm::begin([

    'action' => ['profile?id=' . $model->id],
    'id' => 'memberProfile',
    'enableClientScript' => false,
    'method' => 'POST',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'options' => ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-4',
        ],
        'options' => ['class' => 'form-group row'],
    ],
]);?>
<style type="text/css">
.btn-file {
    position: relative;
    overflow: hidden;
    border-color: #dcdcdc;
    border-top-right-radius: 0px;
    border-bottom-right-radius: 0px;


}

.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload {
    width: 100%;
}
</style>
<div class="row" id="headPage">
    <div class="col-4">
        <div class="mb-2 ov-hidden"
            style="width: 100%; min-height: 350px; height: auto; background-color: #e9ecef; border-radius: .3rem;">
            <?php if (!empty($model->img)) {?>
            <img src="<?=Yii::$app->request->baseUrl?>/<?=$model->img?>" id='img-upload' />
            <?php } else {?>
            <img id='img-upload' />
            <?php }?>
        </div>
        <div class="form-group">
            <label>อัพโหลดรูปพนักงาน</label>
            <div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-default btn-sm btn-file"> Browse… <input type="file" id="imgInp"
                            name="Member[img]"></span>
                    <input type="hidden" name="Member[img]" value="<?=$model->img?>">
                </span>
                <input type="text" class="form-control" id="filename" readonly>
            </div>

        </div>
    </div>
    <div class="col-8">
        <div class="card mb-4">
            <div class="card-body">
                <div class="row">
                    <h5 class="col text-primary">ข้อมูลส่วนตัว</h5>
                    <!--  <div class="ml-auto mr-2">
                        <p class="mb-1">สถานนะ</p>
                        <label class="toggle-switch toggle-switch-success">
                            <input name="status" type="checkbox" value="Y" checked>
                            <span class="toggle-slider round p-2"></span>
                        </label>
                    </div> -->
                </div>


                <label for="example-text-input" class="col-form-label mr-2">ชื่อ-นามสกุล ภาษาไทย</label>
                <div class="form-row align-items-center">
                    <div class="col-2 my-1">
                        <select class="form-control" name="Member[title_th_id]">
                            <?php foreach ($arrTitleTH as $key => $item): ?>
                            <?php
$select_title_th = '';
if ($key == $model->title_th_id) {
    $select_title_th = 'selected';
}
?>
                            <option <?=$select_title_th?> value="<?=$key?>">
                                <?=$item?>
                            </option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="col-sm-5 my-1">
                        <div class="input-group">
                            <input type="text" name="Member[name_th]" value="<?=$model->name_th?>" class="form-control"
                                id="inlineFormInputGroupUsername" placeholder="ชื่อ">
                        </div>
                    </div>
                    <div class="col-5 my-1">
                        <div class="input-group">
                            <input type="text" name="Member[surname_th]" value="<?=$model->surname_th?>"
                                class="form-control" id="inlineFormInputGroupUsername" placeholder="ชื่อ">
                        </div>
                    </div>
                </div>

                <label for="example-text-input" class="col-form-label mr-2">ชื่อ-นามสกุล ภาษาอังกฤษ</label>
                <div class="form-row align-items-center">
                    <div class="col-2 my-1">
                        <select class="form-control" name="Member[title_en_id]">
                            <?php foreach ($arrTitleEN as $key => $item): ?>
                            <?php
$select_title_en = '';
if ($key == $model->title_en_id) {
    $select_title_en = 'selected';
}
?>
                            <option <?=$select_title_en?> value="<?=$key?>">
                                <?=$item?>
                            </option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="col-sm-5 my-1">
                        <div class="input-group">
                            <input type="text" name="Member[name_en]" value="<?=$model->name_en?>" class="form-control"
                                id="inlineFormInputGroupUsername" placeholder="Name">
                        </div>
                    </div>
                    <div class="col-5 my-1">
                        <div class="input-group">
                            <input type="text" name="Member[surname_en]" value="<?=$model->surname_en?>"
                                class="form-control" id="inlineFormInputGroupUsername" placeholder="Surname">
                        </div>
                    </div>
                </div>

                <div class="form-row align-items-center">
                    <div class="col-12 my-1">
                        <label for="example-text-input" class="col-form-label mr-2">เลขบัตรประชาชน</label>
                        <div class="input-group">
                            <input type="text" name="Member[citizen_id]" required="" value="<?=$model->citizen_id?>"
                                class="form-control" id="citizen" placeholder="xxxxxx">
                            <input type="hidden" value="<?=$model->citizen_id?>" id="citizen_old">
                        </div>
                    </div>
                </div>

                <div class="form-row align-items-center">
                    <div class="col-12 my-1">
                        <label for="example-text-input" 
                            class="col-form-label mr-2">บัญชีธนาคาร</label>
                        <div class="input-group">
                            <input name="Member[bookbank_no]" type="text" class="form-control" value="<?=$model->bookbank_no?>"
                                id="inlineFormInputGroupUsername" placeholder="xxxxxx">
                        </div>
                    </div>
                </div>

                <div class="form-row align-items-center">
                    <div class="col-6 my-1">
                        <label for="example-text-input" class="col-form-label mr-2">เพศ</label>
                        <select class="form-control" name="Member[gender_id]">
                            <?php foreach ($arrGender as $key => $item): ?>
                            <?php
$select_gender = '';
if ($key == $model->gender_id) {
    $select_gender = 'selected';
}
?>
                            <option <?=$select_gender?> value="<?=$key?>">
                                <?=$item?>
                            </option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="col-sm-6 my-1">
                        <label for="example-text-input" class="col-form-label mr-2">ชื่อเล่น</label>

                        <div class="input-group">
                            <input type="text" name="Member[nickname]" class="form-control"
                                value="<?=$model->nickname?>" id="inlineFormInputGroupUsername" placeholder="Name">
                        </div>
                    </div>

                </div>

                <div class="form-row align-items-center">
                    <div class="col-4 my-1">
                        <label for="example-text-input" class="col-form-label mr-2">เบอร์โทร</label>
                        <div class="input-group">
                            <input type="text" name="Member[phone_number]" value="<?=$model->phone_number?>"
                                class="form-control" id="inlineFormInputGroupUsername" placeholder="09999999">
                        </div>
                    </div>
                    <div class="col-4 my-1">
                        <label for="example-text-input" class="col-form-label mr-2">บัญชีไลน์</label>
                        <div class="input-group">
                            <input type="text" name="Member[line_id]" value="<?=$model->line_id?>" class="form-control"
                                id="inlineFormInputGroupUsername" placeholder="Line">
                        </div>
                    </div>
                    <div class="col-4 my-1">
                        <label for="example-text-input" class="col-form-label mr-2">อีเมลล์</label>
                        <div class="input-group">
                            <input type="text" name="Member[email]" value="<?=$model->email?>" class="form-control"
                                id="inlineFormInputGroupUsername" placeholder="E-mail">
                        </div>
                    </div>
                </div>

                <label for="example-text-input" class="col-form-label mr-2">บุคคลติดต่อในกรณีฉุกเฉิน</label>
                <div class="form-row align-items-center">
                    <div class="col-2 my-1">
                        <select class="form-control" name="Member[relation_type]">
                            <?php foreach ($arrRelation as $key => $item): ?>
                            <?php
$select_relation = '';
if ($key == $model->relation_type) {
    $select_relation = 'selected';
}
?>
                            <option <?=$select_relation?> value="<?=$key?>">
                                <?=$item?>
                            </option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="col-sm-5 my-1">
                        <div class="input-group">
                            <input type="text" name="Member[relation_name]" value="<?=$model->relation_name?>"
                                class="form-control" id="inlineFormInputGroupUsername" placeholder="ชื่อ นามสกุล">
                        </div>
                    </div>
                    <div class="col-5 my-1">
                        <div class="input-group">
                            <input type="text" name="Member[relation_phone_number]"
                                value="<?=$model->relation_phone_number?>" class="form-control"
                                id="inlineFormInputGroupUsername" placeholder="เบอร์โทรติดต่อ">
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-2 text-primary">ข้อมูลพนักงาน</h5>

                <div class="form-row align-items-center">
                    <div class="col-6 my-1">
                        <label for="example-text-input" class="col-form-label">รหัสพนักงาน</label>
                        <div class="input-group">
                            <input type="text" disabled
                                value="<?=!empty($model->member_no) ? $model->member_no : Utilities::getMemberNo()?>"
                                class="form-control" id="inlineFormInputGroupUsername" placeholder="09999999">
                        </div>
                    </div>
                    <div class="col-6 my-1">
                        <label for="example-text-input" class="col-form-label mb-2">ผู้ดูแล</label>
                        <div class="side-by-side clearfix">
                            <select data-placeholder="Select" name="Member[member_under_id]"
                                class="chosen-select-deselect" tabindex="">
                                <?php
$arrSup = Yii::$app->Utilities->getListSup();
?>
                                <?php foreach ($arrSup as $key => $item): ?>
                                <?php
$member_under_id = '';
if ($key == $model->member_under_id) {
    $member_under_id = 'selected';
}
?>
                                <option <?=$member_under_id?> value="<?=$key?>">
                                    <?=$item?>
                                </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="form-row align-items-center">
                    <div class="col-6 my-1">
                        <label for="example-text-input" class="col-form-label">สถานะ</label>
                        <div class="side-by-side clearfix">
                            <select data-placeholder="Select" name="Member[status]" class="chosen-select-deselect"
                                tabindex="">
                                <?php
$arrStatus = Yii::$app->Utilities->getListStatus();

?>
                                <?php foreach ($arrStatus as $key => $item): ?>
                                <?php
$status = '';
if ($key == $model->status) {
    $status = 'selected';
}
?>
                                <option <?=$status?> value="<?=$key?>">
                                    <?=$item?>
                                </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6 my-1">
                        <label for="example-text-input" class="col-form-label">แผนก</label>
                        <div class="side-by-side clearfix">
                            <select data-placeholder="Select" name="Member[department_id]"
                                class="chosen-select-deselect" tabindex="">
                                <?php
$arrPosition = Yii::$app->Utilities->getListDepartment();
?>
                                <?php foreach ($arrPosition as $key => $item): ?>
                                <?php
$department_id = '';
if ($key == $model->department_id) {
    $department_id = 'selected';
}
?>
                                <option <?=$department_id?> value="<?=$key?>">
                                    <?=$item?>
                                </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6 my-1">
                        <label for="example-text-input" class="col-form-label">ตำแหน่ง</label>
                        <div class="side-by-side clearfix">
                            <select data-placeholder="Select" name="Member[position_id]" class="chosen-select-deselect"
                                tabindex="">
                                <?php
$arrPosition = Yii::$app->Utilities->getListPosition();
?>
                                <?php foreach ($arrPosition as $key => $item): ?>
                                <?php
$position_id = '';
if ($key == $model->position_id) {
    $position_id = 'selected';
}
?>
                                <option <?=$position_id?> value="<?=$key?>">
                                    <?=$item?>
                                </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6 my-1">
                        <label for="example-text-input" class="col-form-label">ประเภท Type</label>
                        <div class="side-by-side clearfix">
                            <select data-placeholder="Select" name="Member[member_type_id]"
                                class="chosen-select-deselect" tabindex="">
                                <?php
$arrType = Yii::$app->Utilities->getListType();
?>
                                <?php foreach ($arrType as $key => $item): ?>
                                <?php
$member_type_id = '';
if ($key == $model->member_type_id) {
    $member_type_id = 'selected';
}
?>
                                <option <?=$member_type_id?> value="<?=$key?>">
                                    <?=$item?>
                                </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-row align-items-center">
                    <div class="col-6 my-1">
                        <label for="example-text-input" class="col-form-label">ประเภทย่อย Subtype</label>
                        <div class="side-by-side clearfix">
                            <select data-placeholder="Select" name="Member[member_subtype_id]"
                                class="chosen-select-deselect" tabindex="">
                                <?php
$arrSubType = Yii::$app->Utilities->getListSubType();
?>
                                <?php foreach ($arrSubType as $key => $item): ?>
                                <?php
$member_subtype_id = '';
if ($key == $model->member_subtype_id) {
    $member_subtype_id = 'selected';
}
?>
                                <option <?=$member_subtype_id?> value="<?=$key?>">
                                    <?=$item?>
                                </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6 my-1">
                        <label for="example-text-input" class="col-form-label">Agency </label>
                        <div class="side-by-side clearfix">
                            <select data-placeholder="Select" name="Member[agency_id]" class="chosen-select-deselect"
                                tabindex="">
                                <?php
$arrAgency = Utilities::getListAgency();
?>
                                <?php foreach ($arrAgency as $key => $item): ?>
                                <?php
$select_a = '';
if ($key == $model->agency_id) {
    $select_a = 'selected';
}
?>
                                <option <?=$select_a?> value="<?=$key?>">
                                    <?=$item?>
                                </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
$start_work_date = date('Y-m-d\TH:i');
$full_time_work_date = date('Y-m-d\TH:i');
if (!empty($model->start_work_date)) {
    $start_work_date = date('Y-m-d\TH:i', strtotime($model->start_work_date));
}

if (!empty($model->full_time_work_date)) {
    $full_time_work_date = date('Y-m-d\TH:i', strtotime($model->full_time_work_date));
}

?>
                <div class="form-row align-items-center">
                    <div class="col-6 my-1">
                        <label for="example-datetime-local-input" class="col-form-label">เริ่มเข้าทำงานวันแรก</label>
                        <input class="form-control" type="datetime-local" name="Member[start_work_date]"
                            value="<?=$start_work_date?>" id="example-datetime-local-input">
                    </div>

                </div>



            </div>
        </div>

        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-2 text-primary">ที่อยู่ตามบัตรประชาชน</h5>
                <div class="form-group">
                    <label for="example-text-input" class="col-form-label">ที่อยู่</label>
                    <textarea class="form-control" id="example-text-input" name="Member[address]"
                        placeholder="97/58 หมู่บ้านเปี่ยมสุข ซอยกรุงเทพนนท์ 3 เมือง นนทบุรี"
                        style="height:50px"><?=$model->address?></textarea>
                </div>

            </div>
        </div>

        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-2 text-primary">พื้นที่ทำงาน</h5>
                <div class="form-row align-items-center">
                    <div class="col-6 my-1">
                        <label for="example-text-input" class="col-form-label">พื้นที่</label>
                        <div class="side-by-side clearfix">
                            <select data-placeholder="Select" class="chosen-select-deselect" name="Member[area_id]"
                                tabindex="">
                                <?php
$arrArea = Yii::$app->Utilities->getMemberArea();
?>
                                <?php foreach ($arrArea as $key => $item): ?>
                                <?php
$area_id = '';
if ($key == $model->area_id) {
    $area_id = 'selected';
}
?>
                                <option <?=$area_id?> value="<?=$key?>">
                                    <?=$item?>
                                </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6 my-1">
                        <label for="example-text-input" class="col-form-label">เขตพื้นที่</label>
                        <div class="side-by-side clearfix">
                            <select data-placeholder="Select" class="chosen-select-deselect" name="Member[zone_id]"
                                tabindex="">
                                <?php
$arrZone = Yii::$app->Utilities->getMemberZone();
?>
                                <?php foreach ($arrZone as $key => $item): ?>
                                <?php
$zone_id = '';
if ($key == $model->zone_id) {
    $zone_id = 'selected';
}
?>
                                <option <?=$zone_id?> value="<?=$key?>">
                                    <?=$item?>
                                </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-row align-items-center">
                    <div class="col-6 my-1">
                        <label for="example-text-input" class="col-form-label">Channel</label>
                        <div class="side-by-side clearfix">
                            <select data-placeholder="Select" class="chosen-select-deselect" name="Member[channel_id]"
                                tabindex="">
                                <?php
$arrChannel = Yii::$app->Utilities->getMemberChannel();
?>
                                <?php foreach ($arrChannel as $key => $item): ?>
                                <?php
$channel_id = '';
if ($key == $model->channel_id) {
    $channel_id = 'selected';
}
?>
                                <option <?=$channel_id?> value="<?=$key?>">
                                    <?=$item?>
                                </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6 my-1">
                        <label for="example-text-input" class="col-form-label">Account</label>
                        <div class="side-by-side clearfix">
                            <select data-placeholder="Select" class="chosen-select-deselect" name="Member[account_id]"
                                tabindex="">
                                <?php
$arrAccount = Yii::$app->Utilities->getMemberAccountNews();
?>
                                <?php foreach ($arrAccount as $key => $item): ?>
                                <?php
$account_id = '';
if ($key == $model->account_id) {
    $account_id = 'selected';
}
?>
                                <option <?=$account_id?> value="<?=$key?>">
                                    <?=$item?>
                                </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">


                <?php echo $this->render('_location', ['modelArea' => $modelArea, 'modelLocationWork' => $modelLocationWork]); ?>



                <!--label for="example-text-input" class="col-form-label mt-1">กลุ่มสถานที่</label>
                <div class="side-by-side clearfix">
                <select data-placeholder="กลุ่มสถานที่" class="chosen-select-deselect" tabindex="">
                   <option value=""></option>
                   <option>xxxx1</option>
                   <option>xxxxxx</option>
                </select>
                </div-->


            </div>
        </div>

        <div class="row mt-4"  style="display: <?=Yii::$app->user->identity->auth_add_member ==2?'none':''?>">
            <a href="/member/profile-detail?id=<?=$model->id ?>" class="btn btn-outline-primary btn-sm  ml-auto">ยกเลิก</a>
            <button type="submit" class="btn btn-primary btn-sm mr-2 ml-2">บันทึก</button>

        </div>
        <input type="hidden" id="phpBaseUrl" value="<?=Yii::$app->request->baseUrl?>">
        <input type="hidden" id="idUser" value="<?=empty($_GET["id"]) ? "" : $_GET["id"]?>">



    </div>
</div>

<?php ActiveForm::end();?>
<?php
$script = <<< JS

// citizen
const url_base = $("#phpBaseUrl").val();


$("#memberProfile").on("submit", function(event) {
    event.preventDefault();
    var id_user = $("#idUser").val();
    var citizen_old = $("#citizen_old").val();
    var citizen_id = $('#citizen').val()

    // console.log('id',id_user)

    if (id_user){
        if  (citizen_old == citizen_id){
            $("#memberProfile").off("submit");

            $("#memberProfile").submit();
        }else{
            $.get(url_base + "/api/check_citizen",{
                id: citizen_id,
            },function(data){
                

                    if (data.code == 200){
                        
                        // window.location.href = url_base+ "/member-leave"
                        // this.submit();
                        $("#memberProfile").off("submit");

                        $("#memberProfile").submit();
                    }else{

                        alert("เลขบัตรประจำตัวประชาชนนี้ถูกใช้ไปแล้ว")
                        var aTag = $("#headPage");
                        $('html,body').animate({scrollTop: aTag.offset().top},'slow');
                    }

            });
        }
    }else{

        $.get(url_base + "/api/check_citizen",{
            id: citizen_id,
        },function(data){
            
                if (data.code == 200){
                    
                    // window.location.href = url_base+ "/member-leave"
                    // this.submit();
                    $("#memberProfile").off("submit");

                    $("#memberProfile").submit();
                }else{

                    alert("เลขบัตรประจำตัวประชาชนนี้ถูกใช้ไปแล้ว")
                    var aTag = $("#headPage");
                    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
                }

        });
}

})


        $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
             label = input.val().replace('','/').replace('', '');
        input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });
         var select = document.getElementById("location_select");
           multi(select, { enable_search: true});



JS;
$script2 = <<< JS

JS;
$this->registerJs($script2, View::POS_HEAD);
$this->registerJs($script, View::POS_READY);
?>