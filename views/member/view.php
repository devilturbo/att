<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Member */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="member-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title_id',
            'gender_id',
            'name_th',
            'surname_th',
            'name_en',
            'surname_en',
            'citizen_id',
            'bookbank_no',
            'bookbank_img',
            'nickname',
            'phone_number',
            'line_id',
            'email:email',
            'relation_type',
            'relation_name',
            'relation_phone_number',
            'start_work_date',
            'full_time_work_date',
            'address:ntext',
            'status',
            'img',
        ],
    ]) ?>

</div>
