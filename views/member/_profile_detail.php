  <?php
use app\components\Helpers;
  use app\components\Utilities;

  $this->title = 'ประวัติพนักงาน';
?>

  <style>
table tr td {
    border-top: 0 !important;

}
  </style>
  <div class="dashboard-header d-flex flex-column grid-margin">
      <!-- dashboard header -->
      <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
          <div class="d-flex align-items-center">
              <h4 class="mb-0 font-weight-bold mr-2">ประวัติพนักงาน</h4>
              <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าหลัก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">ประวัติพนักงาน</p>
              </div>
          </div>
          <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
              <!--button class="btn btn-primary btn-sm mr-3 d-none d-md-block">Download Report</button>
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button-->
          </div>
      </div>
  </div>

  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-body">
                  <div class="row">
                      <?php
$img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
if (!empty($model->img)) {

    $img = Yii::$app->request->baseUrl . '/' . $model->img;
}
?>
                      <div class="col-1 img-thumbmail"><img class="mr-2 lazy" src="<?=$img?>" align="left"></div>
                      <div class="col-3">
                          <table class="table-sm card-table-one">
                              <tbody>

                                  <tr>
                                      <td class="px-0"><strong>รหัสพนักงาน</strong></td>
                                      <td class="text-muted"><?=$model->member_no?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>ชื่อ-นามสกุล</strong></td>
                                      <td class="text-muted"><?=$model->fullname?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>เอเจนซี่</strong></td>
                                      <td class="text-muted"><?=$model->agency_name?></td>
                                  </tr>

                              </tbody>
                          </table>
                      </div>
                      <div class="col-3">
                          <table class="table-sm card-table-one">
                              <tbody>
                                  <tr>
                                      <td class="px-0"><strong>ตำแหน่ง</strong></td>
                                      <td class="text-muted"><?=$model->department_name?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>ประเภท</strong></td>
                                      <td class="text-muted"><?=$model->type_name?></td>
                                  </tr>

                                  <tr>
                                      <td class="px-0"><strong>ประเภทย่อย</strong></td>
                                      <td class="text-muted"><?=$model->subtype_name?></td>
                                  </tr>

                              </tbody>
                          </table>
                      </div>

                      <div class="col-3">
                          <table class="table-sm card-table-one">
                              <tbody>
                                  <tr>
                                      <td class="px-0"><strong>ผู้ดูแล</strong></td>
                                      <td class="text-muted"><?=$model->memberundder_name?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>อีเมล</strong></td>
                                      <td class="text-muted"><?=$model->email?></td>
                                  </tr>
                                  <tr>
                                      <td class="px-0"><strong>เบอร์โทร</strong></td>
                                      <td class="text-muted"><?=$model->phone_number?></td>
                                  </tr>

                              </tbody>
                          </table>
                      </div>

                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="row mt-3">
      <div class="col-12">
          <ul class="nav nav-pills" id="pills-tab" role="tablist">
              <li class="nav-item">
                  <a class="nav-link active"
                      href="<?=Yii::$app->homeUrl?>member/profile-detail?id=<?=$model->id?>">ประวัติพนักงาน</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link " href="<?=Yii::$app->homeUrl?>member/profile-workday?id=<?=$model->id?>">บันทึกเวลาทำงาน</a>
                </li>
              <li class="nav-item">
                  <a class="nav-link "
                      href="<?=Yii::$app->homeUrl?>member/profile-leave?id=<?=$model->id?>">บันทึกการลา</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link "
                      href="<?=Yii::$app->homeUrl?>member/profile-change-time?id=<?=$model->id?>">ปรับปรุงเวลา</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link "
                      href="<?=Yii::$app->homeUrl?>member/profile-ot?id=<?=$model->id?>">ทำงานล่วงเวลา</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link "
                      href="<?=Yii::$app->homeUrl?>member/profile-location?id=<?=$model->id?>">สถานที่ทำงาน</a>
              </li>

          </ul>
      </div>
  </div>

  <div class="row">

      <div class="col-12 mb-4">
          <div class="card">
              <div class="card-body">
                  <div class="row">
                      <div class="col-4">
                          <span class="h6 text-bold">ข้อมูลส่วนตัว</span> <br>

                          <span class="text-muted"><?=$model->title_th?> <?=$model->fullname?><br>
                              <?=$model->title_en?> <?=$model->fullname_en?><br>
                              เลขบัตรประชาชน <?=$model->citizen_id?><br>
                              เพศ <?=$model->gender_name?><br>
                              ชื่อเล่น <?=$model->nickname?><br>
                              เบอร์โทร <?=$model->phone_number?><br>
                              บัญชีไลน์ <?=$model->line_id?><br>
                              อีเมลล์ <?=$model->email?><br>
                              บุคคลติดต่อในกรณีฉุกเฉิน <?=$model->relation_name?> <?=$model->relation_type?>
                              <?=$model->relation_phone_number?> </span><br>
                      </div>

                      <div class="col-3">
                          <span class="h6 text-bold">รหัสพนักงาน</span><br>
                          <span class="text-muted">ผู้ดูแล <?=$model->memberundder_name?><br>
                              ตำแหน่ง <?=$model->position_name?><br>
                              ประเภท <?=$model->type_name?><br>
                              ประเภทย่อย <?=$model->subtype_name?><br>
                              Agency <?=$model->agency_name?><br>
                              เริ่มเข้าทำงานวันแรก <?=Helpers::convertDate($model->start_work_date)?><br>
                              เริ่มเป็นพนักงานประจำ <?=$model->start_date?><br>
                              อายุงาน <?=Utilities::timespan($model->start_work_date)?>
                          </span>
                      </div>

                      <?php

$calcLeave = \app\components\Utilities::getNormalHoliday($model->start_work_date,$model->start_date,$model->isLeave);
$probationStatus = $calcLeave['probationStatus'];
if($model->isLeave == 'Y'){
  $probationStatus = 1;
}

$holidayLeft = $calcLeave['hoilday_leave'];
$errandLeft = $calcLeave['errand_leave'];
$errandCutLeft = $modelCompany->errand_cut_leave;
$sickLeft = $modelCompany->sick_leave;
$sickCutLeft = $modelCompany->sick_cut_leave;
$maternityLeft = $modelCompany->maternity_leave;
$ordainedLeft = $modelCompany->ordained_leave;
// ordained_leave
foreach ($data as $key => $e) {
    // \app\components\Helpers::Debig(($e));
    $start = strtotime($e->start_date);
    $end = strtotime($e->end_date);
    $datediff = $end - $start;
    $day_ =  round($datediff / (60 * 60 * 24));

    if ($day_ == 0) {
        $day_ = 1;
    }
    // \app\components\Helpers::Debig(($day_));

    switch ($e->leave_type_id) {
        case 1:
            // ลาพักร้อน
            $holidayLeft -= $day_;
            break;
        case 2:
            // ลากิจไม่คิดเงิน
            $errandCutLeft -= $day_;
            break;
        case 3:
            // ลากิจคิดเงิน
            $errandLeft -= $day_;
            break;
        case 4:
            // ลาป่วยไม่คิดเงิน
            $sickLeft -= $day_;
            break;
        case 5:
            // ลาป่วยคิดเงิน
            $sickCutLeft -= $day_;
            break;
        case 6:
            // ลาคลอด
            $ordainedLeft -= $day_;
            break;
        case 7:
            // ลาบวช
            $maternityLeft -= $day_;
            break;

    }
}


                      if($holidayLeft < 0)
                          $holidayLeft = 0;
                      if($errandLeft < 0)
                          $errandLeft = 0;
                      if($errandCutLeft < 0)
                          $errandCutLeft = 0;
                      if($sickLeft < 0)
                          $sickLeft = 0;
                      if($sickCutLeft < 0)
                          $sickCutLeft = 0;
                      if($maternityLeft < 0)
                          $maternityLeft = 0;
                      if($ordainedLeft < 0)
                          $ordainedLeft = 0;

?>


                      <div class="col-3">
                          <span class="h6 text-bold">สิทธิ์ในการลา</span><br>
                          <span class="text-muted">
                              <?php


    if ($model->isCheckLeave == "N") {

        ?>
                              ลาพักร้อน ไม่ระบุ วัน - คงเหลือ ไม่ระบุ วัน<br>
                              วันลากิจ(คิดเงิน) ไม่ระบุ วัน - คงเหลือ ไม่ระบุ วัน<br>
                              วันลาป่วย(คิดเงิน) ไม่ระบุ วัน - คงเหลือ ไม่ระบุ วัน<br>
                              วันลากิจ(ไม่คิดเงิน) ไม่ระบุ วัน - คงเหลือ ไม่ระบุ วัน<br>
                              วันลาป่วย(ไม่คิดเงิน) ไม่ระบุ วัน - คงเหลือ ไม่ระบุ วัน<br>
                              ลาคลอด ไม่ระบุ วัน - คงเหลือ ไม่ระบุ วัน<br>
                              ลาบวช ไม่ระบุ วัน - คงเหลือ ไม่ระบุ วัน<br>
                              <?php

    } else {


        ?>

                              ลาพักร้อน <?=$probationStatus == 0 ?0:$calcLeave['hoilday_leave']?> วัน -
                              คงเหลือ <?=$probationStatus == 0 ?0:$holidayLeft?> วัน<br>
                              วันลากิจ(คิดเงิน) <?=$probationStatus == 0 ?0:$calcLeave['errand_leave']?> วัน -
                              คงเหลือ <?=$probationStatus == 0 ?0:$errandLeft?> วัน<br>
                              วันลาป่วย(คิดเงิน) <?=$probationStatus == 0 ?0:$modelCompany->sick_cut_leave?> วัน -
                              คงเหลือ <?=$probationStatus == 0 ?0:$sickCutLeft?> วัน<br>
                              วันลากิจ(ไม่คิดเงิน) <?=$probationStatus == 0 ?0:$modelCompany->errand_cut_leave?> วัน -
                              คงเหลือ  <?=$probationStatus == 0 ?0:$errandCutLeft?> วัน<br>
                              วันลาป่วย(ไม่คิดเงิน) <?=$probationStatus == 0 ?0:$modelCompany->sick_leave?> วัน -
                              คงเหลือ <?=$probationStatus == 0 ?0:$sickLeft?> วัน<br>
                              ลาคลอด <?=$probationStatus == 0 ?0:$modelCompany->maternity_leave?> วัน -
                              คงเหลือ <?=$probationStatus == 0 ?0:$maternityLeft?> วัน<br>
                              ลาบวช <?=$probationStatus == 0 ?0:$modelCompany->ordained_leave?> วัน - คงเหลือ <?=$probationStatus == 0 ?0:$ordainedLeft?> วัน<br>

                              <?php
}
?>

                          </span>

                      </div>





                      <div class="ml-auto mr-2">
                          <!-- <p class="mb-1">สถานนะ</p>
                      <label class="toggle-switch toggle-switch-success">
                        <input type="checkbox" checked>
                        <span class="toggle-slider round p-2"></span>
                      </label>       -->
                      </div>
                  </div>

              </div>
          </div>
      </div>



      <div class="col-12 mb-4">
          <div class="card">
              <div class="card-body">
                  <div class="row">
                      <div class="col-4">
                          <span class="h6 text-bold">ที่อยู่ถามบัตรประชาชน</span><br>
                          <span class="text-muted"><?=$model->address?>
                      </div>
                      <div class="col-4">
                          <span class="h6 text-bold">พื้นที่ทำงาน</span><br>
                          <span class="text-muted">

                              <?php

foreach ($modelArea as $key => $value) {
    //  \app\components\Helpers::Debig(($value));
    if (!empty($value->areasDetail->area_name)) {
        echo $value->areasDetail->area_name . '<br/>';
    }
}

?>
                          </span>

                      </div>
                  </div>

                  </span>
              </div>
          </div>
      </div>



  </div>

  <div class="row" style="display: <?=Yii::$app->user->identity->auth_member ==2?'none':''?>">
      <a href="<?=Yii::$app->request->baseUrl?>/member/profile?id=<?=$model->id?>" class="ml-auto"><button
              class="btn btn-primary btn-sm mr-2 ml-auto ">แก้ไข</button></a>
      <a class="btn btn-outline-primary btn-sm mr-2 " href="<?=Yii::$app->request->baseUrl?>/member">ยกเลิก</a>
  </div>