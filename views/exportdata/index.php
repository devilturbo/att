<?php
use yii\web\View;
$this->title = 'Export Data';

// echo "<pre style='background: #202020; color: #fff; overflow: auto;'>";
// print_r($data);
// // htmlspecialchars
// echo "<pre>";
// die;
?>

<div class="dashboard-header d-flex flex-column grid-margin">
    <!-- dashboard header -->


    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">Export ข้อมูล</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">Export ข้อมูล</p>
            </div>
        </div>
        <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
            <!--button class="btn btn-primary btn-sm mr-3 d-none d-md-block">Download Report</button>
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button-->
        </div>
    </div>
</div>

<?php

// $form = ActiveForm::begin([

//     'action' => ['getdata'],
//     'id' => 'formElem',
//     'enableClientScript' => false,
//     'method' => 'GET',
//     'enableAjaxValidation' => false,
//     'enableClientValidation' => true,
//     'options' => ['enctype' => 'multipart/form-data'],
//     'fieldConfig' => [
//         'horizontalCssClasses' => [
//             'label' => 'col-sm-2',
//             'offset' => 'col-sm-offset-2',
//             'wrapper' => 'col-sm-4',
//         ],
//         'options' => ['class' => 'form-group row'],
//     ],
// ]);

?>


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row mb-3"><span class="h5 ml-2">ข้อมูลการบันทึกเข้า-ออก ของพนักงาน</span></div>
                <div class="row">
                    <div class="col-4">
                        <label for="example-text-input" class="col-form-label">ตำแหน่ง</label>
                        <select name="position" id="position" class="form-control">

                            <option value="all">
                                All
                            </option>
                            <?php
$arrPosition = Yii::$app->Utilities->getListPositionNews();
?>
                            <?php foreach ($arrPosition as $key => $item):

?>

                            <option value="<?=$key?>">
                                <?=$item?>
                            </option>
                            <?php endforeach;?>
                        </select>

                    </div>

                    <div class="col-4">
                        <label for="example-text-input" class="col-form-label">ประเภท</label>
                        <select name="type" id="type" class="form-control">
                            <option value="all">
                                All
                            </option>
                            <?php
$arrType = Yii::$app->Utilities->getListTypeNews();
?>
                            <?php foreach ($arrType as $key => $item):

?>

                            <option value="<?=$key?>">
                                <?=$item?>
                            </option>
                            <?php endforeach;?>
                        </select>
                    </div>

                    <div class="col-3">
                        <label for="example-text-input" class="col-form-label">ประเภทย่อย</label>
                        <select name="subtype" id="subtype" class="form-control">
                            <option value="all">
                                All
                            </option>
                            <?php
$arrSubType = Yii::$app->Utilities->getListSubTypeNews();
?>
                            <?php foreach ($arrSubType as $key => $item):

?>

                            <option value="<?=$key?>">
                                <?=$item?>
                            </option>
                            <?php endforeach;?>
                        </select>
                    </div>

                    <div class="col-auto">
                        <label for="example-text-input" class="col-form-label">ประเภทไฟล์</label>
                        <select class="form-control mr-2">
                            <option selected>Excel</option>
                            <!-- <option>CVS</option>
                              <option>...</option> -->
                        </select>

                    </div>
                </div>

                <div class="col-12 my-1 ml-auto">
                    <span>ช่วงเวลา</span>
                    <div class="input-group input-daterange">
                        <input name="date_from" type="text" id="min-date" class="form-control date-range-filter"
                            data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น">
                        <div class="input-group-addon mr-2 ml-2"> ถึง </div>
                        <input name="date_to" type="text" id="max-date" class="form-control date-range-filter"
                            data-date-format="yyyy-mm-dd" placeholder="สิ้นสุด">



                        <button type="submit" id="getData" class="btn btn-primary btn-sm ml-2 ">Export</button>
                        <!-- <div style="margin-left:15px" class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0" id='btn_export'> -->
                        <div id="myModal" class="modal modaldetail" role="dialog">
                            <div class="modal-dialog " style="max-width: 100;">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <span>Download</span>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div style="margin-left:15px"
                                                class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0"
                                                id='btn_export'>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="col-12">

    <div class="row">
    <div class="col-12">
    <br><br>
            <div class="spinner-load text-center" id="loadingGrow" style="margin-right:auto;margin-left:auto;visibility:hidden" >
                <div>
                    <span>
                        <span class="spinner-border" style="width: 4rem; height: 4rem;" role="status">
                            <span class="sr-only">Loading...</span>
                        </span>
                    </span>
                </div>
            </div>
        </div>
    </div>
    </div>

    <input type="hidden" id="phpBaseUrl" value="<?=Yii::$app->request->baseUrl?>">
    <input type="hidden" id="dateToday" value="<?=date('Y-m-d')?>">




    <table id="dataTable" class="table " cellspacing="0" style="visibility:hidden">
        <!-- <table id="dataTable" class="table " cellspacing="0"> -->
        <thead>
            <tr>
                <th>รหัส</th>
                <th>ชื่อพนักงาน</th>
                <th>ตำแหน่ง</th>
                <th>ประเภท</th>

                <th>วันเริ่มงาน</th>
                <th>อายุงาน (วัน)</th>

                <th>วันทำงาน (วัน)</th>
                <th>วันหยุดปกติ</th>
                <th>ขาดงาน</th>
                <th>พักร้อน</th>
                <th>ลากิจ</th>
                <th>ลาป่วย</th>
                <th>มาสาย (นาที)</th>
                <th>ชั่วโมงทำงาน (ชั่วโมง)</th>
                <th>ชั่วโมง OT</th>
                <th>สถานที่</th>


            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <?php //ActiveForm::end();?>

</div>

<?php

$script = <<< JS

// $( document ).ready(function() {


// });
const url_base = $("#phpBaseUrl").val()
const dateToday = $("#dateToday").val()

$("#min-date").val(dateToday)
$("#max-date").val(dateToday)


table = $('#dataTable').DataTable({
      dom: 'it',
        searching: true,
        paging:   false,
        ordering: false,
        info:     false,
        scrollY: 300,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: true,
        fixedHeader: true,

    });

    var buttons = new $.fn.dataTable.Buttons(table, {
       buttons: [
    {
        extend: 'excel',
        text: 'Download',
        charset: 'utf-8',
        extension: '.xlsx',
        bom: true,
        className: 'btn btn-outline-primary btn-sm  d-none d-md-block testtest',
    }]
}).container().appendTo($('#btn_export'));



async function getData(){
    var pos = $("#position").val()
    var type = $("#type").val()
    var subtype = $("#subtype").val()
    var start = $("#min-date").val()
    var end = $("#max-date").val()


    var arr  = await $.get(url_base+ '/exportdata/getdataa', {position: pos,type:type,subtype:subtype,date_from:start,date_to:end})
    // console.log('arr,',arr);
    return arr.result
}

// click to get data  from api set to datatable 
$("#getData").on('click', async function(evt){
    $('#loadingGrow').css("visibility","visible");
    // console.log('ok');
   var data = await getData()
//    console.log(data);
   $('#dataTable').DataTable().clear().destroy();

   data.map((value)=>{
    var newRowContent = "<tr class='dt-edit'>"
    newRowContent += "<td>"+value.member_no+"</td>"
    newRowContent += "<td>"+value.fullname+"</td>"
    newRowContent += "<td>"+value.position_name+"</td>"
    newRowContent += "<td>"+value.type_name+"</td>"
    newRowContent += "<td>"+value.start_work_date+"</td>"
    newRowContent += "<td>"+value.work_life+"</td>"
    newRowContent += "<td>"+value.day_work+"</td>"
    newRowContent += "<td>"+value.normal_leave+"</td>"
    newRowContent += "<td>"+value.day_off+"</td>"
    newRowContent += "<td>"+value.hoilday_leave+"</td>"
    newRowContent += "<td>"+value.errand_leave+"</td>"
    newRowContent += "<td>"+value.sick_leave+"</td>"
    newRowContent += "<td>"+value.late_time+"</td>"
    newRowContent += "<td>"+value.work_time+"</td>"
    newRowContent += "<td>"+value.ot_time+"</td>"
    newRowContent += "<td>"+value.location_work+"</td>"
    newRowContent += "</tr>"

    $("#dataTable tbody").append(newRowContent);

   })



   table = $('#dataTable').DataTable({
      dom: 'it',
        searching: true,
        paging:   false,
        ordering: false,
        info:     false,
        scrollY: 300,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: true,
        fixedHeader: true,

    });


    var buttons = new $.fn.dataTable.Buttons(table, {
       buttons: [
    {
        extend: 'excel',
        text: 'Download',
        charset: 'utf-8',
        extension: '.xlsx',
        bom: true,
        className: 'btn btn-outline-primary btn-sm  d-none d-md-block testtest',
    }]
}).container().appendTo($('#btn_export'));

    $('#loadingGrow').css("visibility","hidden");
   $('#myModal').modal('show');

})


JS;
$script2 = <<< JS

function uploadFile()
{
      var filename =$('input[type=file]')[0].files[0].name;
      $('#filename').html(filename)
    //   $('#btn-upload').removeClass('input-group-text').addClass('btn btn-primary btn-sm');


}
JS;

$this->registerJs($script2, View::POS_HEAD);
$this->registerJs($script, View::POS_READY);

?>