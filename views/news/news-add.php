<?php
use yii\bootstrap\ActiveForm;
use yii\web\View;

$this->title = 'เพิ่มข่าว';
?>

<?php

$form = ActiveForm::begin([

    'action' => ['insert-news'],
    'id' => 'formElem',
    'enableClientScript' => false,
    'method' => 'POST',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'options' => ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-4',
        ],
        'options' => ['class' => 'form-group row'],
    ],
]);

?>

<style type="text/css">
.btn-file {
    position: relative;
    overflow: hidden;
    border-color: #dcdcdc;
    border-top-right-radius: 0px;
    border-bottom-right-radius: 0px;


}

.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload {
    width: 100%;
}
</style>

<div class="dashboard-header d-flex flex-column grid-margin" id="headPage">
    <!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">เพิ่มข่าว</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">ประกาศข่าว</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">เพิ่มข่าว</p>
            </div>
        </div>
        <div class=" d-flex align-items-center mt-md-3 mt-xl-0">
            <button type="submit" class="btn btn-primary btn-sm mr-2 d-none d-md-block">บันทึก</button>
            <a href="<?=Yii::$app->request->baseUrl?>/news"
                class="btn btn-outline-primary btn-sm  d-none d-md-block">ยกเลิก</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form class="forms-sample">

                    <div class="form-group row" id="alertFail">
                        <!-- <span class="badge badge-danger">Danger</span> -->

                    </div>
                    <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">หัวข้อ</label>
                        <div class="col-sm-9">
                            <input id="topic" name="News[topic]" type="text" class="form-control"
                                id="exampleInputUsername2" value="<?=$modelNews->topic?>" placeholder="หัวข้อ">
                        </div>
                    </div>



                    <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">อัพโหลดรูปภาพ</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <!-- <div class="input-group">
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-sm btn-file"> Upload
                                            <input type="file" id="imgInp" name="News[topic_img]">
                                        </span>
                                    </span>
                                    <input type="text" class="form-control" id="filename" readonly>
                                </div> -->

                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="News[topic_img]"
                                        id="inputGroupFile02" onchange="uploadFile()">
                                    <input type="hidden" name="News[topic_img]" value="<?=$modelNews->topic_img?>">
                                    <label class="custom-file-label" id="filename" for="inputGroupFile02">
                                        <?=!empty($modelNews->topic_img) ? $modelNews->topic_img : "Choose file"?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">รายละเอียด</label>
                        <div class="col-sm-9">
                            <div id="summernoteExample1">
                                <?=$modelNews->details?>
                            </div>

                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">วันที่ส่ง</label>
                        <div class="col-sm-9">
                            <div id="datepicker-popup" class="input-group date datepicker">
                                <input type="text" id="newsDate" name="News[newsDate]" autocomplete="off"
                                    value="<?=empty($modelNews->news_date) ? "" : Yii::$app->Utilities->formatDate('m/d/Y', $modelNews->news_date)?>"
                                    <?=empty($_GET['id'] )? "":"disabled" ?> class="form-control">
                                <?php
                                    if (!empty($_GET['id'] )){
                                        ?>
                                <input type="hidden" name="News[newsDate]"
                                    value="<?=Yii::$app->Utilities->formatDate('m/d/Y', $modelNews->news_date)?>" />
                                <?php
                                    }
                                    ?>
                                <span class="input-group-addon input-group-append border-left">
                                    <span class="mdi mdi-calendar input-group-text"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">รายละเอียดการส่ง</label>
                        <div class="col-sm-9">
                            <div class="form-row align-items-center">
                                <div class="col-4 my-1">
                                    <label for="example-text-input" class="col-form-label">ตำแหน่ง</label>
                                    <div class="side-by-side clearfix">
                                        <select name="News[position]" data-placeholder="ทั้งหมด"<?=empty($_GET['id'] )? "":"disabled" ?> class="chosen-select-deselect"tabindex="">

                                            <option value="9999">
                                                ทั้งหมด
                                            </option>
                                            <?php
$arrPosition = Yii::$app->Utilities->getListPositionNews();
?>
                                            <?php foreach ($arrPosition as $key => $item):
    $status = '';
    if ($key == $modelNews->position_id) {
        $status = 'selected';
    }
    ?>

                                            <option <?=$status?> value="<?=$key?>">
                                                <?=$item?>
                                            </option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4 my-1">
                                    <label for="example-text-input" class="col-form-label">ประเภท Type</label>
                                    <div class="side-by-side clearfix">
                                        <select name="News[type]" data-placeholder="เลือก" <?=empty($_GET['id'] )? "":"disabled" ?> class="chosen-select-deselect" tabindex="">
                                            <option value="9999">
                                                ทั้งหมด
                                            </option>
                                            <?php
$arrType = Yii::$app->Utilities->getListTypeNews();
?>
                                            <?php foreach ($arrType as $key => $item):
    $status = '';
    if ($key == $modelNews->member_type_id) {
        $status = 'selected';
    }
    ?>

                                            <option <?=$status?> value="<?=$key?>">
                                                <?=$item?>
                                            </option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4 my-1">
                                    <label for="example-text-input" class="col-form-label">ประเภทย่อย Subtype</label>
                                    <div class="side-by-side clearfix">
                                        <select name="News[subType]" data-placeholder="เลือก" <?=empty($_GET['id'] )? "":"disabled" ?> class="chosen-select-deselect"tabindex="">
                                            <option value="9999">
                                                ทั้งหมด
                                            </option>
                                            <?php
$arrSubType = Yii::$app->Utilities->getListSubTypeNews();
?>
                                            <?php foreach ($arrSubType as $key => $item):
    $status = '';
    if ($key == $modelNews->member_subtype_id) {
        $status = 'selected';
    }
    ?>

                                            <option <?=$status?> value="<?=$key?>">
                                                <?=$item?>
                                            </option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="col-4 my-1">
                                    <label for="example-text-input" class="col-form-label">พื้นที่</label>
                                    <div class="side-by-side clearfix">
                                        <select name="News[memberArea]" data-placeholder="Select" <?=empty($_GET['id'] )? "":"disabled" ?> class="chosen-select-deselect" tabindex="">
                                            <option value="9999">
                                                ทั้งหมด
                                            </option>
                                            <?php
$arrArea = Yii::$app->Utilities->getMemberAreaNews();
?>
                                            <?php foreach ($arrArea as $key => $item):
    $status = '';
    if ($key == $modelNews->member_area_id) {
        $status = 'selected';
    }
    ?>

                                            <option <?=$status?> value="<?=$key?>">
                                                <?=$item?>
                                            </option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4 my-1">
                                    <label for="example-text-input" class="col-form-label">เขตพื้นที่</label>
                                    <div class="side-by-side clearfix">
                                        <select name="News[memberZone]" data-placeholder="Select" <?=empty($_GET['id'] )? "":"disabled" ?> class="chosen-select-deselect" tabindex="">
                                            <option value="9999">
                                                ทั้งหมด
                                            </option>
                                            <?php
$arrZone = Yii::$app->Utilities->getMemberZoneNews();
?>
                                            <?php foreach ($arrZone as $key => $item):
    $status = '';
    if ($key == $modelNews->member_zone_id) {
        $status = 'selected';
    }
    ?>

                                            <option <?=$status?> value="<?=$key?>">
                                                <?=$item?>
                                            </option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-4 my-1">
                                    <label for="example-text-input" class="col-form-label">Channel</label>
                                    <div class="side-by-side clearfix">
                                        <select name="News[memberChannel]" data-placeholder="Select"
                                            <?=empty($_GET['id'] )? "":"disabled" ?> class="chosen-select-deselect"
                                            tabindex="">
                                            <option value="9999">
                                                ทั้งหมด
                                            </option>
                                            <?php
$arrChannel = Yii::$app->Utilities->getMemberChannelNews();
?>
                                            <?php foreach ($arrChannel as $key => $item):
    $status = '';
    if ($key == $modelNews->member_chanel_id) {
        $status = 'selected';
    }
    ?>

                                            <option <?=$status?> value="<?=$key?>">
                                                <?=$item?>
                                            </option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-4 my-1">
                                    <label for="example-text-input" class="col-form-label">Account</label>
                                    <div class="side-by-side clearfix">
                                        <select name="News[memberAccount]" data-placeholder="Select" <?=empty($_GET['id'] )? "":"disabled" ?> class="chosen-select-deselect" tabindex="">
                                            <option value="9999">
                                                ทั้งหมด
                                            </option>
                                            <?php
$arrAccount = Yii::$app->Utilities->getMemberAccountNews();
?>
                                            <?php foreach ($arrAccount as $key => $item):
    $status = '';
    if ($key == $modelNews->member_account_id) {
        $status = 'selected';
    }
    ?>

                                            <option <?=$status?> value="<?=$key?>">
                                                <?=$item?>
                                            </option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div> -->

                            </div> 
                        </div>

                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">หมายเหตุ</label>
                        <div class="col-sm-9">
                            <!-- <input type="text" name="News[note]" class="form-control" id="note" placeholder=""> -->
                            <textarea name="News[note]" class="form-control" id="note"
                                rows="4"><?=$modelNews->note?></textarea>
                        </div>
                    </div>



                    <input type="hidden" name="id" value="<?=$modelNews->news_id?>">

                </form>
                <div class="row">
                    <a id="submitAll" class="ml-auto">
                        <button type="submit" class="btn btn-primary btn-sm mr-2 ml-auto testtest">บันทึก</button></a>
                    <a href="/news">
                        <button class="btn btn-outline-primary btn-sm mr-2 ">ยกเลิก</button>
                    </a>

                </div>
            </div>
        </div>

    </div>

</div>

<?php ActiveForm::end();?>



<?php

$script = <<< JS

$('#summernoteExample1').summernote({
  toolbar: [
    ['style', ['style']],
    ['fontsize', ['fontsize']],
    ['style', ['bold', 'clear']],
    // ['font', ['strikethrough', 'superscript', 'subscript']],
    ['insert', ['link', 'picture']],
    ['para', [ 'paragraph']],
  ],
  styleTags: ['p', 'h1', 'h2', 'h3', 'h4', 'h5'],
  height: 400
});



$("#formElem").on("submit", function(event) {
    event.preventDefault();

    var newsDate = $("#newsDate").val()
    var topic = $("#topic").val()

    if (newsDate === "" || topic === "") {
        // alert("FAIL")
        $('#alertFail').html('<div class="col-sm-12"><div class="alert alert-danger" role="alert"> กรอกข้อมูลให้ครบ </div></div>');
        var aTag = $("#headPage");
        $('html,body').animate({scrollTop: aTag.offset().top},'slow');


    }else{

        var input = $("<textarea>").attr("name", "News[details]").val($('#summernoteExample1').summernote('code'));
        $('#formElem').append(input);

        $('#submitAll').html('<button class="btn btn-primary btn-sm mr-2 ml-auto" type="button" disabled><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Loading...</button>')

        $(this).off("submit");

        this.submit();
    }

});

$(document).on('change', '.btn-file :file', function() {
    // console.log('input')

        var input = $(this),
             label = input.val().replace('','/').replace('', '');
        input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                // console.log(input.files[0])
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });
         var select = document.getElementById("location_select");
           multi(select, { enable_search: true});


JS;
$script2 = <<< JS
function uploadFile()
{
      var filename =$('input[type=file]')[0].files[0].name;
      $('#filename').html(filename)
    //   $('#btn-upload').removeClass('input-group-text').addClass('btn btn-primary btn-sm');


}
JS;

$this->registerJs($script2, View::POS_HEAD);
$this->registerJs($script, View::POS_READY);

?>