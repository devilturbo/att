<?php
use yii\web\View;
$this->title = 'ประกาศข่าว';

// echo "<pre style='background: #202020; color: #fff; overflow: auto;'>";
// print_r($data);
// // htmlspecialchars
// echo "<pre>";
// die;
?>

<style>
/* @media (min-width: 576px) {
    .modal-dialog {
        max-width: '900px';
        margin: '30px auto';
    }
} */

img {
    max-width: 100%;
    height: auto;
   
}

.table td img, .jsgrid .jsgrid-table td img {
  width: 45px;
  height: auto;
  border-radius: .3rem;
}


@media (min-width: 1000px) {
    .modal-test {
        max-width: '900px';
        color: 'red';
    }

    .modal-dialog .modal-xl {
        max-width: 80%;
    }

    @media screen and (min-width: 992px) {
        .modal-lg {
            width: 70%;
            /* New width for large modal */
        }
    }

}

.spinner-load {
    display: block;
    position: fixed;
    z-index: 1031;
    top: 50%;
    right: 45%;

    height: 100%;
    /* background-color: gray; */
}
</style>


<div class="spinner-load text-center" id="loadingGrow">
    <div>
        <span>
            <span class="spinner-border" style="width: 4rem; height: 4rem;" role="status">
                <span class="sr-only">Loading...</span>
            </span>
        </span>
    </div>
</div>


<div class="dashboard-header d-flex flex-column grid-margin ">

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">ประกาศข่าว</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">ประกาศข่าว</p>
            </div>
        </div>
        <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
            <a href="<?=Yii::$app->request->baseUrl?>/news/news-add"><button type="button"
                    class="btn btn-primary btn-sm mr-2 d-none d-md-block">เพิ่มข้อมูล</button></a>
            <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0" id='btn_export'>

            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-12 mb-4">
        <div class="row ">
            <div class="col-2 ml-auto"></div>
            <div class="col-2"></div>
        </div>

        <form>
            <div class="form-row align-items-center mb-2">
                <div class="col-sm-3 my-1-">
                    <span>ค้นหา</span>
                    <input type="text" class="form-control global_filter" id="global_filter" placeholder="คำที่ค้นหา">
                </div>

                <div class="col-3">
                    <div class="row">

                        <div class="col">
                            <span id="ตำแหน่ง"></span>
                        </div>

                        <div class="col"><span id="ประเภท"></span></div>
                        <div class="col-auto">
                            <button type="button" class="btn btn-outline-primary btn-sm ml-auto bt-filter"
                                style="margin-top: 20px;" data-toggle="modal" data-target="#ModalFilter">Filter</button>
                        </div>
                    </div>

                </div>




                <div class="col-auto my-1 ml-auto" style="width: 350px;">
                    <span>ค้นหาจากช่วงเวลา</span>

                    <div class="input-group input-daterange">
                        <input type="text" id="min-date" class="form-control date-range-filter"
                            data-date-format="dd/mm/yyyy" placeholder="เริ่มต้น">
                        <div class="input-group-addon mr-2 ml-2"> ถึง </div>
                        <input type="text" id="max-date" class="form-control date-range-filter"
                            data-date-format="dd/mm/yyyy" placeholder="สิ้นสุด">
                        <button id="search" class="btn btn-primary btn-sm ml-2">ค้นหา</button>
                    </div>
                </div>
            </div>



        </form>

    </div>
</div>

<!-- Modal -->
<div id="ModalFilter" class="modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>ค้นหาโดยละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="col-12 p-4">
                <div class="row">
                    <!-- <div class="col-6 mb-2 "><span id="แผนก"></span></div> -->
                    <!-- <div class="col-6 mb-2">
                        <select class="form-control">
                        <option value="test">Select</option>
                        <option value="test">test</option>
                        </select>
                    </div> -->
                    <div class="col-6 mb-2"><span id="ตำแหน่ง"></span></div>
                    <div class="col-6 mb-2"><span id="ประเภท"></span></div>
                    <div class="col-6 mb-2"><span id="ประเภทย่อย"></span></div>
                    <div class="col-6 mb-2"><span id="พื้นที่"></span></div>
                    <div class="col-6 mb-2"><span id="เขตพื้นที่"></span></div>
                    <div class="col-6 mb-2"><span id="ช่องทาง"></span></div>
                    <div class="col-6 mb-2"><span id="Account"></span></div>
                    <div class="col-6 mb-2"><span id="สถานะ"></span></div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                        <tr>
                            <th>วันที่ส่ง</th>
                            <th>ผู้รับ</th>
                            <th>ตำแหน่ง</th>
                            <th>ประเภท</th>
                            <th>ประเภทย่อย</th>
                            <!-- <th>พื้นที่</th>
                            <th>เขตพื้นที่</th>
                            <th>ช่องทาง</th>
                            <th>Account</th> -->
                            <th>หัวข้อ</th>
                            <th>รายละเอียด</th>
                            <th>รูปแนบ</th>
                            <th>สถานะ</th>
                            <th>วันที่สร้าง</th>
                            <th>หมายเหตุ</th>
                            <th>ดำเนินการ</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
foreach ($data as $key => $e) {


    $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
    if (!empty($e->topic_img)) {

        $img = Yii::$app->request->baseUrl . '/' . $e->topic_img;
        
    }

    ?>
                        <tr class="dt-edit" style="height: 4px;line-height: 1; ">
                            <td><?=Yii::$app->Utilities->formatDate('d/m/Y', $e->news_date)?></td>
                            <td>All User</td>
                            <td><?=($e->position_id == "9999") ? "ทั้งหมด" : $e->position_name?></td>
                            <td><?=($e->member_type_id == "9999") ? "ทั้งหมด" : $e->type_name?></td>
                            <td><?=($e->member_subtype_id == "9999") ? "ทั้งหมด" : $e->subtype_name?></td>
                            <!-- <td><?=''//($e->member_area_id == "9999") ? "ทั้งหมด" : $e->area_name?></td>
                            <td><?=''//($e->member_zone_id == "9999") ? "ทั้งหมด" : $e->zone_name?></td>
                            <td><?=''//($e->member_chanel_id == "9999") ? "ทั้งหมด" : $e->channel_name?></td>
                            <td><?=''//($e->member_account_id == "9999") ? "ทั้งหมด" : $e->account_name?></td> -->

                            <td style="max-width: 200px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;">
                                <?=$e->topic?>
                            </td>
                            <td style="max-width: 400px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;">
                                <div style="height:30px; overflow:hidden">
                                    <?=$e->details?>
                                </div>
                            </td>
                            <td>
                                <span style="border-radius: .6rem;">
                                    <a href="<?=$img?>" target="blank">
                                        <img width="350px" src="<?=$img?>" />
                                    </a>
                                </span>


                            </td>
                            <td><?=($e->status == "N") ? "ยังไม่ส่ง" : "ส่งแล้ว";?></td>
                            <td><?=Yii::$app->Utilities->formatDate('d/m/Y', $e->createDate)?></td>
                            <td><?=$e->note?></td>

                            <td>
                                <button type="button"  id="<?=$e->news_id?>" class="btn btn-primary btn-sm mb-1 detailButton">
                                    รายละเอียด
                                </button>
                                <button id="<?=$e->news_id?>" type="button"
                                    class="btn btn-outline-primary btn-sm mb-1 confirmButton">
                                    ลบ
                                </button>
                            </td>
                        </tr>
                        <?php
}
?>

                    </tbody>
                </table>
                <input type="hidden" id="phpBaseUrl" value="<?=Yii::$app->request->baseUrl?>">

            </div>
        </div>
    </div>
</div>





<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>รายละเอียด</span>
                <button id="closeMymodal" type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">


            </div>

        </div>

    </div>
</div>

<?php

$script2 = <<< JS

const url_base = $("#phpBaseUrl").val()


$(document).on("click", ".detailButton", function(event) {
// console.log('detail')
window.location.href = url_base + "/news/news-add?id=" + this.id;

})

$(document).on("click", ".confirmButton", function(event) {
    // console.log(this.id)
    event.preventDefault()
    var r = confirm("ยืนยันที่จะลบข่าว?");
    if (r == true) {

        $.get(url_base+ "/news/delete",{
        // $.post("/news/delete",{
            id: this.id,
        },function(data){
            if (data == "Success"){
                window.location.href = url_base + "/news"
            }else{
                alert("FAIL: " , data)
            }
        });
    }
})


JS;

$this->registerJs($script2, View::POS_READY);

?>

<?php
$script = <<< JS



 async function filterGlobal () {
    $('#dataTable').DataTable().search(
        $('#global_filter').val()

       ).draw();
    }

    var columnsToSearch = {
        // 8: 'แผนก',
        9: 'ตำแหน่ง',
        10: 'ประเภท',
        11: 'ประเภทย่อย',
        4: 'พื้นที่',
        5: 'เขตพื้นที่',
        6: 'ช่องทาง',
        7: 'Account',
        2: 'สถานะ'
    };

    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging:   false,
        ordering: false,
        info:     false,
        scrollY: 300,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: false,
        fixedHeader: true,
        initComplete: async function () {

            for (var i in columnsToSearch) {
                var api = this.api();
                // console.log(api.column(i).data())

                var select = $('<select class="form-control"><option value="">Select</option></select>')
                    .appendTo($('#' + columnsToSearch[i]).empty().text(columnsToSearch[i] + ' '))

                    .attr('data-col', i)
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        // console.log(val.trim())
                        if (val.includes("7") && val.includes("11") &&  val.includes("-") ){
                            val = "7-11"
                        }
                        // api.column($(this).attr('data-col'))
                        //     .search(val ? '^' + val + '$' : '', true, true)
                        //     .draw();
                        table.search( val.trim() ).draw();

                    });
                    if (columnsToSearch[i] == "สถานะ"){
                        select.append('<option value="ยังไม่ส่ง">ยังไม่ส่ง</option>');
                        select.append('<option value="ส่งแล้ว">ส่งแล้ว</option>');

                    }else if (columnsToSearch[i] == "ประเภท"){
                        $("#loadingGrow").remove()
                    }else{
                        select.append('<option value="ทั้งหมด">ทั้งหมด</option>');

                    }
                    // console.log(columnsToSearch[i])
                    // var arr  = await $.get('/web/news/getlist', {list: columnsToSearch[i]})
                    var arr  = await $.get(url_base+ '/news/getlist', {list: columnsToSearch[i]})
                    var arrJson = JSON.parse(arr)

                    // console.log(arrJson)
                    Object.keys(arrJson).forEach(function(key) {
                                    select.append('<option value="' + arrJson[key] + '">' + arrJson[key] + '</option>');

                    })
            }

        }

    });

    
 var buttons = new $.fn.dataTable.Buttons(table, {
       buttons: [
    {
        extend: 'excel',
        text: 'Data Export',
        charset: 'utf-8',
        extension: '.xlsx',
        bom: true,
        className: 'btn btn-outline-primary btn-sm  d-none d-md-block',
    }]
}).container().appendTo($('#btn_export'));

 

// var colum = ["วันที่ส่ง", "ผู้รับ", "ตำแหน่ง","ประเภท", "ประเภทย่อย", "พื้นที่","เขตพื้นที่","ช่องทาง","Account","หัวข้อ","รายละเอียด","รูปแนบ","สถานะ","วันที่สร้าง","หมายเหตุ","ดำเนินการ"];
var colum = ["วันที่ส่ง", "ผู้รับ", "ตำแหน่ง","ประเภท", "ประเภทย่อย","หัวข้อ","รายละเอียด","รูปแนบ","สถานะ","วันที่สร้าง","หมายเหตุ","ดำเนินการ"];
   //Edit row buttons
  $('.dt-edit').each(function () {
    $(this).on('click', function(evt){
      \$this = $(this);
      var dtRow = \$this;
      $('div.modal-body').innerHTML='';
      //$('div.modal-body').append( '<div class="row mb-4 ml-1 img-thumbmail lazy">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=0; i < dtRow[0].cells.length; i++){
        // console.log(colum[i])
          if (colum[i] == "รายละเอียด"){
            var deleteDiv_1 = dtRow[0].cells[i].innerHTML.replace('<div style="height:30px; overflow:hidden">', "");
            var deleteDiv_2 = deleteDiv_1.replace('</div>', "");
            $('div.modal-body').append('<div class="row" style="margin-bottom:30px"> <span class="card-title mr-2 text-muted col-3 ">'+colum[i]+'</span><span class="col-8">'+deleteDiv_2+'</span><div/>');
            // รูปแนบ

        } else if (colum[i] == "รูปแนบ"){
            $('div.modal-body').append('<div class="row" style="margin-bottom:30px"> <span class="card-title mr-2 text-muted col-3 ">'+colum[i]+'</span><span class="col-8">'+dtRow[0].cells[i].innerHTML+'</span><div/>');
        }
    
        //  else if (colum[i] == "ดำเนินการ"){

        // } 
        else {
            $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">'+colum[i]+'</span><span class="col-8">'+dtRow[0].cells[i].innerHTML+'</span><div/>');

        }
            // console.log(i);
      }
      $('#myModal').modal('show');
    });
  });
  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .modal-body').empty();
  });

  $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
       var min = $('#min-date').val();
       var max = $('#max-date').val();
       var createdAt = data[2] || 0;
        if (
           (min == "" || max == "") ||
           (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
         ) {
            return true;
        }
            return false;
        }
    );

    $('.date-range-filter').change(function() {
      table.draw();
    });

    // $.fn.dataTable.ext.search.push(
    // function( settings, data, dataIndex ) {
    //     var min = parseInt( $('#min').val(), 10 );
    //     var max = parseInt( $('#max').val(), 10 );
    //     var age = parseFloat( data[3] ) || 0; // use data for the age column

    //     if ( ( isNaN( min ) && isNaN( max ) ) ||
    //          ( isNaN( min ) && age <= max ) ||
    //          ( min <= age   && isNaN( max ) ) ||
    //          ( min <= age   && age <= max ) )
    //     {
    //         return true;
    //     }
    //     return false;
    // });

    // $('#min, #max').change( function() {
    //     table.draw();
    // } );

    // $.fn.dataTable.ext.search.push(
    //     function (settings, data, dataIndex) {
    //         var min = $('#min').datepicker('getDate');
    //         var max = $('#max').datepicker('getDate');
    //         var startDate = new Date(data[4]);
    //         console.log(min)
    //         if (min == null && max == null) return true;
    //         if (min == null && startDate <= max) return true;
    //         if (max == null && startDate >= min) return true;
    //         if (startDate <= max && startDate >= min) return true;
    //         return false;
    //     }
    // );

    // $('#min').datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
    // $('#max').datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });

    // // Event listener to the two range filtering inputs to redraw on input
    // $('#min, #max').change(function () {
    //     table.draw();
    // });


    $('#my-table_filter').hide();

    $('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );

    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );

JS;
$this->registerJs($script, View::POS_READY);

?>