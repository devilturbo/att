  <?php
use app\components\Utilities;
use yii\web\View;

$this->title = "Attendant"
?>
  <div class="content-wrapper">

      <div class="dashboard-header d-flex flex-column grid-margin">
          <!-- header -->
          <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-1">
              <div class="d-flex align-items-center">
                  <h4 class="mb-0 font-weight-bold mr-2">บันทึกเข้า-ออก</h4>
                  <div class="d-none d-md-flex mt-1">
                      <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าหลัก</p>
                      <i class="mdi mdi-chevron-right text-muted"></i>
                      <p class="text-muted mb-0 tx-13 cursor-pointer">บันทึกเข้า-ออก</p>
                  </div>
              </div>



              <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">

                  <div class="btn-group mt-3 mt-md-0" role="group" aria-label="Button group">
                      <!-- <button type="button" class="btn btn-outline-secondary border">
                          <i class="mdi mdi-magnify text-body"></i>
                      </button> -->
                      <a href="<?=Yii::$app->request->baseUrl?>/work/user-working-today">
                      <button type="button" class="btn btn-outline-secondary border">
                          <i class="mdi mdi-reload text-body"></i>
                      </button>
                      </a>
                      
                  </div>
              </div>
          </div>
      </div><!-- end header -->




      <div class="row ">
          <div class="col-12 mb-4">
              <div class="card">
                  <div class="card-body">


                      <div class="row mb-2">
                          <span class="ml-2 mt-3">
                              <!-- บันทึกเข้า-ออก วันนี้ 12/09/19 - 12:10:11 -->
                              บันทึกเข้า-ออก วันนี้ <?=date('d/m/Y')?> - <?=date('H:i:s')?>


                          </span>
                          <div class="col-sm-3 ml-auto">
                              <span>ค้นหา</span>
                              <input type="text" class="form-control global_filter" id="global_filter"
                                  placeholder="คำที่ค้นหา">
                          </div>
                          <div class="col-3">
                              <div class="row">
                                  <div class="col"><span id="ตำแหน่ง"></span></div>
                                  <div class="col"><span id="ประเภท"></span></div>
                                  <div class="col-auto">
                                      <button type="button" class="btn btn-outline-primary btn-sm ml-auto bt-filter"
                                          style="margin-top: 20px;" data-toggle="modal"
                                          data-target="#ModalFilter">Filter</button>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <table id="dataTable" class="table " cellspacing="0">
                          <thead>
                              <tr>
                                  <th>รูป</th>
                                  <th>รหัสพนักงาน</th>
                                  <th>ชื่อ-นามสกุล</th>
                                  <th>เวลา</th>
                                  <th>สถานนะ</th>
                                  <th>สถานที่</th>
                                  <th>แผนก</th>
                                  <th>ตำแหน่ง</th>
                                  <th>ประเภท</th>
                                  <th>ประเภทย่อย</th>
                                  <th>วันที่</th>
                                  <th>ผู้ดูแล</th>
                                  <th>พื้นที่</th>
                                  <th>เขตพื้นที่</th>
                                  <th>ช่องทาง</th>
                                  <th>ห้างร้าน</th>
                                  <th>ดำเนินการ</th>
                              </tr>
                          </thead>
                          <tbody>

                          </tbody>
                      </table>

                  </div>

              </div>
          </div>
      </div>
      <!--div class="col-6">
      <div class="card">
        <div class="card-body">คำขอลาต่างๅ</div>
      </div>
    </div-->

  </div>

  <div id="myModal" class="modal modaldetail" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <span>รายละเอียด</span>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">

              </div>
          </div>

      </div>
  </div>

  <div id="ModalFilter" class="modal" role="dialog">
      <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <span>ค้นหาโดยละเอียด</span>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="col-12 p-4">
                  <div class="row">
                      <!-- <div class="col-6 mb-2"><span id="ตำแหน่ง"></span></div>
                      <div class="col-6 mb-2"><span id="ประเภท"></span></div> -->
                      <div class="col-6 mb-2"><span id="แผนก"></span></div>
                      <div class="col-6 mb-2"><span id="ตำแหน่ง"></span></div>
                      <div class="col-6 mb-2"><span id="ประเภท"></span></div>
                      <div class="col-6 mb-2"><span id="ประเภทย่อย"></span></div>
                      <div class="col-6 mb-2"><span id="พื้นที่"></span></div>
                      <div class="col-6 mb-2"><span id="เขตพื้นที่"></span></div>
                      <div class="col-6 mb-2"><span id="ช่องทาง"></span></div>
                      <div class="col-6 mb-2"><span id="ห้างร้าน"></span></div>
                  </div>

              </div>
          </div>
      </div>
  </div>

  <input type="hidden" id="phpBaseUrl" value="<?=Yii::$app->request->baseUrl?>">



  <?php
$script = <<< JS
    const url_base = $("#phpBaseUrl").val()

 
table = $('#dataTable').DataTable({
    processing: true,
    serverSide: true,
    "bLengthChange" : false,
    // "pageLength": 2,
    ajax: {
        "url": url_base+ "/member/workday-dt",
        "type": "POST",
        "data" :{ _csrf:yii.getCsrfToken()},
       dataFilter: function(reps) {
            callDT();
            // console.log(reps);
            return reps;
        },
        error:function(err){
              console.log(err.responseText);
        }
         
                     
    },
    searching: true,
    paging:   true,
    ordering: false,
    info:     false,
    scrollY: 720,
    scrollX:        true,
    scrollCollapse: false,
    autoWidth:      false,
    
   createdRow: function( row, data, dataIndex ) {
        $(row).addClass( 'dt-edit' );
        
    },
    fixedColumns:   {
      leftColumns: 1
    },
    columnDefs: [
      { "width": "50px", "targets": [ 0 ] },
    //    { targets: -1,
    //        createdCell: function (td, cellData, rowData, row, col) {
    //             $(td).html('<a class="btn btn-outline-primary btn-sm mb-1" href="'+cellData+'">ประวัติ</a>')
    //         }
    //   },
    ],
    responsive: false,
    fixedHeader: true

});


$('#dataTable_filter').hide()


// var buttons = new $.fn.dataTable.Buttons(table, {
//    buttons: [
// {
//     extend: 'excel',
//     text: 'Data Export',
//     charset: 'utf-8',
//     extension: '.xlsx',
//     bom: true,
//     className: 'btn btn-outline-primary btn-sm  d-none d-md-block',
// }]
// }).container().appendTo($('#btn_export'));



function callDT() {
setTimeout(function(){  dt();}, 500);
}

function dt() {

    var colum = ["รูป", "รหัสพนักงาน" , "ชื่อ-นามสกุล", "เวลา", "สถานะ", "สถานที่",
        "แผนก", "ตำแหน่ง", "ประเภท", "ประเภทย่อย", "วันที่", "ผู้ดูแล", "พื้นที่", "เขตพื้นที่", "ช่องทาง",
        "ห้างร้าน", "ดำเนินการ"
    ];
//Edit row buttons
$('.dt-edit').each(function() {
    $(this).on('click', function(evt) {
        \$this = $(this);
        var dtRow = \$this;
        $('div.modal-body').innerHTML = '';
        $('div.modal-body').append('<div class="row mb-4 ml-1 img-thumbmail">' + dtRow[0]
            .cells[0].innerHTML + '</div>');
        for (var i = 1; i < dtRow[0].cells.length; i++) {
            $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">' +colum[i] + '</span><span class="col-8">' + dtRow[0].cells[i].innerHTML +'</span><div/>');
            // console.log(i);
        }
        $('#myModal').modal('show');
    });
});
$('#myModal').on('hidden.bs.modal', function(evt) {
    $('.modal .modal-body').empty();
});
}


async function downloadDropDown(){
    var columnsToSearch = {
        6: 'แผนก',
        7: 'ตำแหน่ง',
        8: 'ประเภท',
        9: 'ประเภทย่อย',
        12: 'พื้นที่',
        13: 'เขตพื้นที่',
        14: 'ช่องทาง',
        15: 'ห้างร้าน',
        // 4: 'สถานะ'
    };
for (var i in columnsToSearch) {

console.log(columnsToSearch[i]);
var _id = "";

if (columnsToSearch[i] == "แผนก"){
    _id = "department-search"
}
 if (columnsToSearch[i] == "ตำแหน่ง"){
    _id = "position-search"
}
if (columnsToSearch[i] == "ประเภท"){
    _id = "type-search"
}
if (columnsToSearch[i] == "ประเภทย่อย"){
    _id = "subtype-search"
}
if (columnsToSearch[i] == "พื้นที่"){
    _id = "area-search"
}
if (columnsToSearch[i] == "เขตพื้นที่"){
    _id = "zone-search"
}
if (columnsToSearch[i] == "ช่องทาง"){
    _id = "channel-search"
}
if (columnsToSearch[i] == "ห้างร้าน"){
    _id = "account-search"
}


var select = $('<select id='+_id+' class="form-control"><option value="">Select</option></select>')
    .appendTo($('#' + columnsToSearch[i]).empty().text(columnsToSearch[i] + ' '))


// get list from api 
    var arr  = await $.get(url_base+ '/member/getlist', {list: columnsToSearch[i]})
    var arrJson = JSON.parse(arr)

    if (columnsToSearch[i] == "สถานะ"){
                    select.append('<option value="wait">รออนุมัติ</option>');
                    select.append('<option value="accept">อนุมัติแล้ว</option>');
                    select.append('<option value="decline">ไม่อนุมัติ</option>');

                }

    // console.log(arrJson)
    Object.keys(arrJson).forEach(function(key) {
        select.append('<option value="' + key + '">' + arrJson[key] + '</option>');

    })
}


$('#global_filter,#subtype-search,#department-search,#position-search,#type-search,#area-search,#zone-search,#channel-search,#account-search').on( 'change', function () {
    let global_filter = $('#global_filter').val() 
    let departmentSearch = $('#department-search').val()
    let positionSearch = $('#position-search').val()
    let typeSearch = $('#type-search').val()
    let subtypeSearch = $('#subtype-search').val()
    let areaSearch = $('#area-search').val()
    let zoneSearch = $('#zone-search').val()
    let channelSearch = $('#channel-search').val()
    let accountSearch = $('#account-search').val()
    let date = ""
    let start = ""
    let end = ""

     let strSearch = global_filter +'&'+departmentSearch+'&'+positionSearch+'&'+typeSearch +'&'+subtypeSearch +'&'+date +'&'+areaSearch +'&'+zoneSearch +'&'+channelSearch +'&'+accountSearch+ '&' +start + '&'+end 
     
    //  console.log(strSearch);
     table.search( strSearch ).draw();
} );

}


downloadDropDown()

$("#min-date").datepicker().datepicker("setDate", new Date());


$('#my-table_filter').hide();

$('input.column_filter').on('keyup click', function() {
    filterColumn($(this).parents('tr').attr('data-column'));
});


JS;
$this->registerJs($script, View::POS_READY);

?>