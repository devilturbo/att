<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

// use yii\helpers\Html;
// use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
.bg-bg {
    /* The image used */
    background-image: url("<?=Yii::$app->request->baseUrl?>/images/bg-login-1.jpg");

    /* Full height */
    height: 100%;

    /* Center and scale the image nicely */
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
</style>

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */
use yii\web\View;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>


<div class="login-area login-s2- bg-bg">
    <div class="container">
        <div class="login-box ptb--100">
            <?php 
            $form = ActiveForm::begin([

'action' => ['/login'],
'id' => 'login_user',
'enableClientScript' => false,
'method' => 'POST',
'enableAjaxValidation' => false,
'enableClientValidation' => true,

'fieldConfig' => [
    'horizontalCssClasses' => [
        'label' => 'col-sm-2',
        'offset' => 'col-sm-offset-2',
        'wrapper' => 'col-sm-4',
    ],
    'options' =>  ['class' => 'form-group row']
],
]); 
?>
<!-- <form id="login_user"> -->
                <div class="login-form-head" style="background-color: #1566ff;">
                    <h4>Sign In</h4>
                    <p>Hello there, Sign in and start managing your Admin Template</p>
                </div>
                <div class="login-form-body">
                    <div class="form-gp">
                        <input type="email" placeholder="Email address"  name="LoginForm[username]" id="username">
                        <i class="ti-email"></i>
                    </div>
                    <div class="form-gp">
                        <input type="password" placeholder="Password"  name="LoginForm[password]" id="password">
                        <i class="ti-lock"></i>
                    </div>
                    <div class="row mb-4 rmber-area">
                        <div class="col-6">
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                <label class="custom-control-label" for="customControlAutosizing">
                                    Remember
                                    Me</label>
                            </div>
                        </div>

                    </div>

                    <div class="submit-btn-area">
                        <!-- <button class="btn btn-primary " onclick="validateUser()"  type="submit">บันทึกข้อมูล</button> -->
                        <input type="button" onclick="validateUser()" class="btn btn-block btn-primary"  value="บันทึกข้อมูล">
                    </div>

                </div>


        </div>
        <!-- </form> -->
        <?php
        
        ActiveForm::end(); 
        ?>

    </div>
</div>



<?php
$script = <<< JS
function validateUser() {
   let username = $('#username').val()
   let password = $('#password').val()
   let text = ''
   if(!username){
       text+='username is require \\n'
   }
   if(!password){
       text+='password is require \\n'
   }
   if(text){
       alert(text)
       return false;
   }
   
//    alert("OK")
   $.ajax({
            
            url: './login/user-authen',
            type: 'post',
            crossDomain: true,
            headers: {
              "accept": "application/json",
              "Access-Control-Allow-Origin":"*"
            },
            data: {
            username:username,
            password:password,
            type:2,
            _csrf:yii.getCsrfToken()
            },
            success: function (res) {
             if(res.access_token == true){
                //  alert("test")
                //  console.log("res",res)
                $('#login_user').submit()
                
             }else{
                 alert(res.message)
             }
             
            }
        });
}
JS;
$this->registerJs($script, View::POS_HEAD);

?>