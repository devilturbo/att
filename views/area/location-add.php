<?php
use yii\bootstrap\ActiveForm;
use yii\web\View;
$this->title = 'เพ่ิมสถานที่';

?>
<style type="text/css">
.btn-file {
    position: relative;
    overflow: hidden;
    border-color: #dcdcdc;
    border-top-right-radius: 0px;
    border-bottom-right-radius: 0px;


}

.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload {
    width: 100%;
}
</style>

<div class="dashboard-header d-flex flex-column grid-margin" id="headPage">
    <!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">เพิ่มสถานที่</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">เพิ่มสถานที่</p>
            </div>
        </div>
        <!--div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button>
              </div-->
    </div>
</div>
<div class="row">
    <div class="col-12">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area">สถานที่ทั้งหมด</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="<?=Yii::$app->request->baseUrl?>/area/location-add">เพิ่มสถานที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/import">Import สถานที่</a>
            </li>
                        <!-- <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-group">กลุ่มสถานที่</a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-area">พื้นที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-zone">เขตพื้นที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-chanel">ช่องทาง</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-account">Account</a>
            </li>
        </ul>
    </div>
</div>


<?php

$form = ActiveForm::begin([

    'action' => ['insert-area'],
    'id' => 'formElem',
    'enableClientScript' => false,
    'method' => 'POST',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'options' => ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-4',
        ],
        'options' => ['class' => 'form-group row'],
    ],
]);

?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div id="map" style="height:570px"></div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12" style="margin:0">
                                <div class="form-group" id="alertFail" style="margin:0"></div>
                            </div>
                            <form class="forms-sample row">
                                <div class="form-group col-12">
                                    <label for="exampleInputUsername2" class="col-form-label">รูปสถานที่</label>
                                    <div class="input-group ">

                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="Area[img]"
                                                id="inputGroupFile02" onchange="uploadFile()">
                                            <input type="hidden" name="Area[img]" value="<?=$model->img?>">
                                            <label class="custom-file-label" id="filename" for="inputGroupFile02">
                                                <?=!empty($model->img) ? $model->img : "Choose file"?>
                                            </label>
                                        </div>

                                        <!-- <div class="input-group-prepend">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile01">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose
                                                file</label>
                                        </div> -->
                                    </div>

                                </div>

                                <div class="form-group col-6">
                                    <label for="exampleInputUsername1">รหัสสาขา</label>
                                    <input type="text" class="form-control" id="branchCode" placeholder=""
                                        value="<?=!empty($model->brach_no) ? $model->brach_no : ""?>"
                                        name="Area[brach_no]">
                                </div>
                                <div class="form-group col-6">
                                    <label for="exampleInputUsername1">ชื่อสาขา</label>
                                    <input type="text" class="form-control" id="branchName"
                                        value="<?=!empty($model->local_name) ? $model->local_name : ""?>"
                                        placeholder="" name="Area[local_name]">
                                </div>


                                <div class="form-group col-6">
                                    <label for="exampleInputEmail1">พิกัดละติจูด ( Latitude )</label>
                                    <input type="text" class="form-control" id="laText" 
                                        placeholder="13.7538739" onChange="initMap($('#laText').val()+','+$('#loText').val())"
                                        value="<?=!empty($model->latitude) ? $model->latitude : "13.7538739"?>"
                                        name="Area[latitude]">

                                </div>
                                <div class="form-group col-6">
                                    <label for="exampleInputPassword1">พิกัดลองจิจูด ( Longitude )</label>
                                    <input type="text" class="form-control" id="loText"
                                        placeholder="100.5146209" onChange="initMap($('#laText').val()+','+$('#loText').val())"
                                        value="<?=!empty($model->longitude) ? $model->longitude : "100.5146209"?>"
                                        name="Area[longitude]">

                                </div>

                                <div class="form-group col-12">
                                    <label>กำหนดระยะในการใช้งาน</label>
                                    <input id="range" type="range" min="10" max="1000" style="width: 100%" step="10"
                                        value="<?=!empty($model->distance) ? $model->distance : "100"?>"
                                        name="Area[distance]" />
                                    <p id="result"></p>

                                </div>
                                <div class="form-group  col-6">
                                    <label>Account</label>
                                    <select class="js-example-basic-single w-100" id="accountDropdown"
                                        name="Area[account]">
                                        <option default value="">เลือก</option>
                                        <?php
$arrAccount = Yii::$app->Utilities->getMemberAccountNews("account");
?>
                                        <?php foreach ($arrAccount as $key => $item):
    $status = '';
    if ($key == $model->account) {
        $status = 'selected';
    }
    ?>

		                                        <option <?=$status?> value="<?=$key?>">
		                                            <?=$item?>
		                                        </option>
		                                        <?php endforeach;?>

                                    </select>
                                </div>

                                <div class="form-group col-6">
                                    <label>พื้นที่</label>
                                    <select class="js-example-basic-single w-100" id="areaDropdown" name="Area[area]">
                                        <option default value="">เลือก</option>
                                        <?php
$arrArea = Yii::$app->Utilities->getMemberAreaNews("area");
?>
                                        <?php foreach ($arrArea as $key => $item):
    $status = '';
    if ($key == $model->area) {
        $status = 'selected';
    }
    ?>

		                                        <option <?=$status?> value="<?=$key?>">
		                                            <?=$item?>
		                                        </option>
		                                        <?php endforeach;?>
                                    </select>
                                </div>
                                <div class="form-group col-6">
                                    <label>เขตพื้นที่</label>
                                    <select class="js-example-basic-single w-100" id="zoneDropdown" name="Area[zone]">
                                        <option default value="">เลือก</option>

                                        <?php
$arrZone = Yii::$app->Utilities->getMemberZoneNews("zone");
?>
                                        <?php foreach ($arrZone as $key => $item):
    $status = '';
    if ($key == $model->zone) {
        $status = 'selected';
    }
    ?>

		                                        <option <?=$status?> value="<?=$key?>">
		                                            <?=$item?>
		                                        </option>
		                                        <?php endforeach;?>
                                    </select>
                                </div>
                                <div class="form-group col-6">
                                    <label>ช่องทาง</label>
                                    <select class="js-example-basic-single w-100" id="channelDropdown"
                                        name="Area[channel]">
                                        <option default value="">เลือก</option>

                                        <?php
$arrChannel = Yii::$app->Utilities->getMemberChannelNews("channel");
?>
                                        <?php foreach ($arrChannel as $key => $item):
    $status = '';
    if ($key == $model->channel) {
        $status = 'selected';
    }
    ?>
		                                        <option <?=$status?> value="<?=$key?>">
		                                            <?=$item?>
		                                        </option>
		                                        <?php endforeach;?>
                                    </select>
                                </div>
                                <div class="col-12" style="display: <?=Yii::$app->user->identity->auth_location ==2?'none':''?>" >
                                    <div class="row">
                                        <button type="submit" class=" ml-auto btn btn-primary btn-sm mr-2 " id="confirmButton">
                                            บันทึก
                                        </button>
                                        <a href="/area">
                                            <button type="button" class="btn btn-light btn-sm mr-2">ยกเลิก</button>
                                        </a>
                                    </div>
                                </div>

                                <input type="hidden" id="phpBaseUrl" value="<?=Yii::$app->request->baseUrl?>">
                                <input type="hidden" value="<?=yii::$app->user->identity->email?>" name="username">
                                <input type="hidden" name="id" value="<?=$model->id?>">
                                <input type="hidden" id="old_brach_no"
                                    value="<?=!empty($model->brach_no) ? $model->brach_no : ""?>">
                                <input type="hidden" id="idUser" value="<?=empty($_GET["id"]) ? "" : $_GET["id"]?>">


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end();?>

<?php
$script4 = <<< JS

$(document).ready(function(){
        GetGEO();   

})
function GetGEO() {

initMap($('#laText').val()+','+$('#loText').val());
// console.log("Get GEO");
// console.log("Lat val ="+ $('#laText').val());
}


var p = document.getElementById("range"),
    res = document.getElementById("result");
res.innerHTML = p.value + "&nbsp; " + "เมตร";
p.addEventListener("input", function() {
    res.innerHTML = p.value + "&nbsp; " + "เมตร";
}, false);
p.addEventListener("change", function() {
    GetGEO();
}, false);


$("#formElem").on("submit", function(event) {
    event.preventDefault();

    $("#confirmButton").prop("disabled", true);
    var branchCode = $("#branchCode").val()
    var branchName = $("#branchName").val()
    var laText = $("#laText").val()
    var loText = $("#loText").val()
    var range = $("#range").val()
    var accountDropdown = $("#accountDropdown").val()
    var areaDropdown = $("#areaDropdown").val()
    var zoneDropdown = $("#zoneDropdown").val()
    var channelDropdown = $("#channelDropdown").val()

    if (branchCode === "" || branchName === "" || laText === "" || loText === "" || range === "" ||
    accountDropdown === "" ||  areaDropdown === "" || zoneDropdown === "" || channelDropdown === "" ) {
        // alert("FAIL")
        $('#alertFail').html('<div class="col-sm-12"><div class="alert alert-danger" role="alert"> กรอกข้อมูลให้ครบ </div></div>');
        $("#confirmButton").prop("disabled", false);


    }else{
      // alert("OK")
      const url_base = $("#phpBaseUrl").val();
      var id_user = $("#idUser").val();

      var old_brach_no = $("#old_brach_no").val();
      if (id_user){
        if  (old_brach_no == branchCode){
          $(this).off("submit");
          this.submit();
            // alert("OK")
        }else{
            $.get(url_base + "/api/check_branch_no",{
                id: branchCode,
            },function(data){
                

                    if (data.code == 200){
                        
                        $("#formElem").off("submit");
                        $("#formElem").submit();

                    }else{
                        alert("รหัสสาขาถูกใช้ไปแล้ว")
                        $("#confirmButton").prop("disabled", false);
                    }

            });
        }
    }else{
      $.get(url_base + "/api/check_branch_no",{
                id: branchCode,
            },function(data){
                

                    if (data.code == 200){
                        
                        $("#formElem").off("submit");
                        $("#formElem").submit();
                    }else{
                        alert("รหัสสาขาถูกใช้ไปแล้ว")
                        $("#confirmButton").prop("disabled", false);
                    }

            });
}


    }
    // this.submit();

});



JS;
?>
<?php

$script = <<< JS


 function getgeolocation(){
   $('.clo').html('<i class="fa fa-location-arrow"></i> รอสักครู่...');
   if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(function(position) {
           initMap(position.coords.latitude+', '+position.coords.longitude);
           console.log(position.coords.latitude+', '+position.coords.longitude);
           $('.clo').html('<i class="fa fa-location-arrow"></i>  ตำแหน่งปัจจุบัน (คร่าวๆ)');
           $('#laText').val(position.coords.latitude);
           $('#loText').val(position.coords.longitude);
           $('#locationText').val(position.coords.latitude+', '+position.coords.longitude);
         });
   }
   else{
     $('.clo').html('<i class="fa fa-location-arrow"></i>  ดึงตำแหน่งไม่ได้');
   }
 }

 var marker;
 var sunCircle;
 var map;
 var map2;
 var cen;
 function initMap(location,d,t,rr) {

    //  console.log("init Map");

     var str='';
     var str2='';
     if(typeof rr === "undefined" || rr==''){
             var r = parseInt(document.getElementById("range").value);
           }
     else{
        var r = parseInt(rr);
     }
     if( r >= 800 ){
       z = 14;
     }
     else if( r >= 400  && r <= 800  ){
       z = 15;
     }
     else if(  r >= 200  && r <= 400  ){
       z = 16;
     }
     else if(  r >= 100  && r <= 200  ){
       z = 17;
     }
     else if(  r >= 50  && r <= 100  ){
       z = 18;
     }
     else if(  r >= 25  && r <= 50  ){
       z = 19;
     }
     else{
       z = 20;
     }

     if(typeof location === "undefined" || location==''){
       str = "13.674351, 100.517556";
       str2 = "13.674351,100.517556";
     }
     else{
        str = location;
        str2 = location;
     }
     if(typeof d === "undefined" ){
       d=true;
     }
     else{

       d=false;
     }
   if(typeof t === "undefined" || t == ''){
     t=false;
   }
   else{
     t=true;
   }
   //console.log(t);
   str = str.split(", ").map(function(t){return parseFloat(t)});
   str2 = str2.split(",").map(function(t){return parseFloat(t)});
   if(str.length==1 && str2.length==2){
     str = str2;
   }
   cen = {lat: str[0], lng: str[1]};

     if(t){
       var locations = [
     ["บริษัทเดอะเบสท์ โปรโมชั่น จำกัด(สำนักงานใหญ่)", 13.760931, 100.634258, 50]
      ];

    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: locations[0][1], lng: locations[0][2]},
      zoom: 15
    });
       var infowindow = new google.maps.InfoWindow();
       var marker, i;
       for (i = 0; i < locations.length; i++) {
         marker = new google.maps.Marker({
           position: new google.maps.LatLng(locations[i][1], locations[i][2]),
           animation: google.maps.Animation.DROP,
           map: map
         });
         console.log(locations[i][3]);
         var cityCircle = new google.maps.Circle({
           strokeColor: '#FF0000',
           strokeOpacity: 0.5,
           strokeWeight: 2,
           fillColor: '#FF0000',
           fillOpacity: 0.1,
           map: map,
           center: new google.maps.LatLng(locations[i][1], locations[i][2]),
           radius: parseInt(locations[i][3])
         });
         cityCircle.bindTo('center', marker, 'position')

         google.maps.event.addListener(marker, 'click', (function(marker, i) {
           return function() {
             infowindow.setContent(locations[i][0]);
             infowindow.open(map, marker);
           }
         })(marker, i));
       }
     }else{

       map = new google.maps.Map(document.getElementById('map'), {
         center: cen,
         zoom: z
       });
       marker = new google.maps.Marker({
         position: cen,
         draggable: d,
         animation: google.maps.Animation.DROP, //BOUNCE
         map: map
       });
       sunCircle = {
           strokeColor: "#306EFF",
           strokeOpacity: 0.5,
           strokeWeight: 1.5,
           fillColor: "#1589FF",
           fillOpacity: 0.1,
           map: map,
           center: cen,
           radius: r // in meters
       };
       cityCircle = new google.maps.Circle(sunCircle);
       cityCircle.bindTo('center', marker, 'position');

     }
         marker.addListener('drag', handleEvent);
         marker.addListener('dragend', handleEvent);
       }


     function handleEvent(event) {
       $('#laText').val(event.latLng.lat());
       $('#loText').val(event.latLng.lng());
       $('#locationText').val(event.latLng.lat()+', '+event.latLng.lng());
     }
     function handleEvent2(event,i) {

       $('#laText-'+i+', #TolaText-'+i).val(event.latLng.lat());
       $('#loText-'+i+', #ToloText-'+i).val(event.latLng.lng());

   }
JS;

?>

<?php
$script2 = <<< JS

function uploadFile()
{
      var filename =$('input[type=file]')[0].files[0].name;
      $('#filename').html(filename)
}
JS;
$this->registerJs($script, View::POS_HEAD);
$this->registerJs($script2, View::POS_HEAD);
$this->registerJs($script4, View::POS_READY);

?>