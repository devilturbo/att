<?php

use yii\bootstrap\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AreaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Areas';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin([

    'action' => ['area'],
    'id' => 'importArea',
    'enableClientScript' => false,
    'method' => 'POST',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'options' =>  ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-4',
        ],
        'options' =>  ['class' => 'form-group row']
    ],
]); ?>
<div class="dashboard-header d-flex flex-column grid-margin">
    <!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">สถานที่ทั้งหมด</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">สถานที่ทั้งหมด</p>
            </div>
        </div>
        <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0" id='btn_export'>

        </div>
    </div>
</div>
<div class="row mb-4">
    <div class="col-12">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="<?=Yii::$app->request->baseUrl?>/area">สถานที่ทั้งหมด</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-add">เพิ่มสถานที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/import">Import สถานที่</a>
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-group">กลุ่มสถานที่</a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-area">พื้นที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-zone">เขตพื้นที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-chanel">ช่องทาง</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-account">Account</a>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-12 mb-4">
        <div class="row ">
            <div class="col-2 ml-auto"></div>
            <div class="col-2"></div>
        </div>


        <div class="form-row align-items-center mb-2">
            <div class="col-sm-3 my-1-">
                <span>ค้นหา</span>
                <input type="text" class="form-control global_filter" id="global_filter" placeholder="คำที่ค้นหา">
            </div>
            <div class="col-4">
                <div class="row">
                    <div class="col">
                        <span id="Account"></span></div>
                    <div class="col"><span id="ช่องทาง"></span></div>
                    <div class="col"><span id="เขตพื้นที่"></span></div>
                    <div class="col"><span id="พื้นที่"></span></div>
                    <!-- <div class="col-auto">
                        <button type="button" class="btn btn-outline-primary btn-sm ml-auto bt-filter"
                            style="margin-top: 20px;" data-toggle="modal" data-target="#ModalFilter">Filter</button>
                    </div> -->
                </div>
            </div>

            <!-- <div class="col-auto my-1 ml-auto" style="width: 350px;">
                <span>ค้นหาจากช่วงเวลา</span>
                <div class="input-group input-daterange">
                    <input type="text" id="min-date" class="form-control date-range-filter"
                        data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น">
                    <div class="input-group-addon mr-2 ml-2"> ถึง </div>
                    <input type="text" id="max-date" class="form-control date-range-filter"
                        data-date-format="yyyy-mm-dd" placeholder="สิ้นสุด">
                    <button type="submit" id="search" class="btn btn-primary btn-sm ml-2">ค้นหา</button>
                </div>
            </div> -->



        </div>

        <!-- Modal -->
        <div id="ModalFilter" class="modal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <span>ค้นหาโดยละเอียด</span>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="col-12 p-4">
                        <div class="row">
                            <div class="col-6 mb-2"><span id="เขตพื้นที่"></span></div>
                            <div class="col-6 mb-2"><span id="พื้นที่"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<div class="row">
    <div class="col-12">
        <h5 class="font-weight-bold mb-4">สถานที่ทั้งหมด(<?=$modelRows?>)</h5>
        <div class="card">
            <div class="card-body">


                <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                        <tr>
                            <th>รูป</th>
                            <th>รหัสสาขา</th>
                            <th>Account</th>
                            <th>ชื่อ</th>
                            <th>พิกัดละติจูด</th>
                            <th>พิกัดลองจิจูด</th>
                            <th>ระยะใช้งาน</th>
                            <th>พื้นที่</th>
                            <th>เขตพื้นที่</th>
                            <th>ช่องทาง</th>
                            <th>เพิ่มโดย</th>
                            <th >ดำเนินการ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($model)):?>
                        <?php foreach ($model as $key => $item):

                          if ($item->type == 'web') {
                            $img = 'https://att.thebestpromote.com/web/' . $item->img;
                          } else if($item->type == 'mobile'){
                              $img = 'https://api.thebestpromote.com/web/' . $item->img;
                          }else {
                            $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
                          }

                          if(empty($item->img)){
                             $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
                          }

                          ?>
                        <tr class="dt-edit">
                            <td class="employee-col">
                                <img class="thumb mr-2 lazy" src="<?=$img?>" align="left">
                            </td>
                            <td><?=$item->brach_no?></td>
                            <td><?=$item->account?></td>
                            <td><?=$item->local_name?></td>
                            <td><?=$item->latitude?></td>
                            <td><?=$item->longitude?></td>
                            <td><?=$item->distance?></td>
                            <td><?=$item->area?></td>
                            <td><?=$item->zone?></td>
                            <td><?=$item->channel?></td>
                            <td><?=$item->created_by?></td>
                            <td>
                                <!--a href="location-edit.php"><button class="btn btn-outline-primary btn-sm mb-1">แก้ไข</button></a-->
                                <a href="<?=Yii::$app->request->baseUrl?>/area/location-add?id=<?=$item->id?>">
                                    <button class="btn btn-outline-primary btn-sm mb-1">แก้ไข</button>
                                </a>
                                <button class="btn btn-outline-primary btn-sm mb-1"
                                    onclick="delArea(<?=$item->id?>)">ลบ</button>

                            </td>
                        </tr>
                        <?php endforeach;?>
                        <?php endif;?>
                    </tbody>
                </table>
            </div>


            <!-- ลบข้ำมูล -->
            <div id="myModaldelete" class="modal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <span>ลบข้อมูล</span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="row p-4 text-center">
                            <h2 class="text-danger">ยืนยันการลบข้อมูล</h2>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">ยืนยัน</button>
                            <button type="button" class="btn btn-outline-primary btn-sm"
                                data-dismiss="modal">ยกเลิก</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div id="myModal" class="modal modaldetail" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <span>รายละเอียด</span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-7 mappanel">
                                    <div id="map" style="height:570px"></div>
                                </div>
                                <div class="col-3 mapdetail"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="phpBaseUrl" value="<?=Yii::$app->request->baseUrl?>">

    <input type="hidden" id="auth" value="<?=Yii::$app->user->identity->auth_location?>">

            <?php
$script = <<< JS
  const url_base = $("#phpBaseUrl").val()

    table = $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        "bLengthChange" : false,
        "columns": [
            { "data": "img" },
            { "data": "brach_no" },
            { "data": "account" },
            { "data": "local_name" },
            { "data": "latitude" },
            { "data": "longitude" },
            { "data": "distance" },
            { "data": "area" },
            { "data": "zone" },
            { "data": "channel" },
            { "data": "created_by" },
            { "data": "id" },
        ],
// get data by ajax
        ajax: {
            "url": url_base+ "/area/area-dt",
            "type": "POST",
            "data" :{ _csrf:yii.getCsrfToken()},
           dataFilter: function(reps) {
                callDT();
                // console.log(reps);
                return reps;
            },
            error:function(err){
                  console.log(err);
            }
             
                         
        },
        searching: true,
        paging:   true,
        ordering: false,
        info:     false,
        scrollY: 720,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        
       createdRow: function( row, data, dataIndex ) {
            $(row).addClass( 'dt-edit' );
            
        },
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
           { targets: 11,
               createdCell: function (td, cellData, rowData, row, col) {
                let auth = $('#auth').val();
              
                 $(td).html('<a href="'+url_base+'/area/location-add?id='+cellData+'"><button class="btn btn-outline-primary btn-sm mb-1">แก้ไข</button></a><button class="btn btn-outline-primary btn-sm mb-1"onclick="delArea('+cellData+')">ลบ</button>')
             //   $(td).html('<a href="'+url_base+'/area/location-add?id='+cellData+'"><button class="btn btn-outline-primary btn-sm mb-1">แก้ไข</button></a>')

                if(auth == 2)
                    $(td).html('')
                    
                }
          },
        ],
        responsive: false,
        fixedHeader: true

    });

    $('#dataTable_filter').hide()

async function callDT() {
    setTimeout(function(){  dt();}, 500);

    

  }
  function dt() {

   var colum = ["รูป", "รหัสสาขา", "Account","ชื่อ", "พิกัดละติจูด", "พิกัดลองจิจูด","ระยะใช้งาน", "พื้นที่","เขตพื้นที่","ช่องทาง","เพิ่มโดย","ดำเนินการ" ];

$('.dt-edit').each(function () {
      $(this).on('click', function(evt){
      \$this = $(this);
      var dtRow = \$this;
      $('div.mapdetail').innerHTML='';
      $('div.mapdetail').append( '<div class="row mb-4 ml-1 img-thumbmail-small lazy">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=1; i < dtRow[0].cells.length; i++){
         $('div.mapdetail').append('<div class="row mb-1"> <span class="col-4">'+colum[i]+'</span><span class="col-8">'+ dtRow[0].cells[i].innerHTML+'</span></div>');
            //console.log(i);
      }
      $('#myModal').modal('show');

// console.log(object);
      initMap(''+ dtRow[0].cells[4].innerHTML+','+ dtRow[0].cells[5].innerHTML+'',false,false,dtRow[0].cells[6].innerHTML);

      });
    });

$('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .mapdetail').empty();
  });
}

    $('#my-table_filter').hide();

    // $('input.global_filter').on( 'keyup click', function () {
    //     filterGlobal();
    // } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );

    var columnsToSearch = {
        1: 'Account',
        6: 'พื้นที่',
        8: 'ช่องทาง',
        7: 'เขตพื้นที่'
    };

  async function downloadDropDown(){
    for (var i in columnsToSearch) {

    // console.log(columnsToSearch[i]);
    var _id = "";
    if (columnsToSearch[i] == "Account"){
        _id = "account_search"
    } 
    if (columnsToSearch[i] == "ช่องทาง"){
        _id = "channel_search"
    }
     if (columnsToSearch[i] == "พื้นที่"){
        _id = "area_search"
    }
     if (columnsToSearch[i] == "เขตพื้นที่"){
        _id = "zone_search"
    }

    var select = $('<select id='+_id+' class="form-control"><option value="">Select</option></select>')
        .appendTo($('#' + columnsToSearch[i]).empty().text(columnsToSearch[i] + ' '))


        var arr  = await $.get(url_base+ '/news/getlist', {list: columnsToSearch[i]})
        var arrJson = JSON.parse(arr)

        // console.log(arrJson)
        Object.keys(arrJson).forEach(function(key) {
            select.append('<option value="' + arrJson[key] + '">' + arrJson[key] + '</option>');

        })
}

$('#global_filter,#account_search,#channel_search,#area_search,#zone_search').on( 'change', function () {
        // console.log('on change mul' );
        let global_filter = $('#global_filter').val()
         let account_search = $('#account_search').val()
         let channel = $('#channel_search').val()
         let area = $('#area_search').val()
         let zone = $('#zone_search').val()
        let strSearch = global_filter+'&'+account_search+'&'+channel+'&'+area+'&'+zone
         
        //  console.log(strSearch);
         table.search( strSearch ).draw();
    } );
    
  }

 downloadDropDown()


       


    var buttons = new $.fn.dataTable.Buttons(table, {
       buttons: [
    {
        extend: 'excel',
        text: 'Data Export',
        charset: 'utf-8',
        extension: '.xlsx',
        bom: true,
        className: 'btn btn-outline-primary btn-sm  d-none d-md-block',
        
    }]
}).container().appendTo($('#btn_export'));

var marker;
var sunCircle;
var map;
var map2;
var cen;
function initMap(location,d,t,rr) {

    console.log("init Map");

    var str='';
    var str2='';
    if(typeof rr === "undefined" || rr==''){
            //var r = parseInt(document.getElementById("range").value);
          }
    else{
       var r = parseInt(rr);
    }
    if( r >= 800 ){
      z = 14;
    }
    else if( r >= 400  && r <= 800  ){
      z = 15;
    }
    else if(  r >= 200  && r <= 400  ){
      z = 16;
    }
    else if(  r >= 100  && r <= 200  ){
      z = 17;
    }
    else if(  r >= 50  && r <= 100  ){
      z = 18;
    }
    else if(  r >= 25  && r <= 50  ){
      z = 19;
    }
    else{
      z = 20;
    }

    if(typeof location === "undefined" || location==''){
      str = "13.674351, 100.517556";
      str2 = "13.674351,100.517556";
    }
    else{
       str = location;
       str2 = location;
    }
    if(typeof d === "undefined" ){
      d=true;
    }
    else{

      d=false;
    }
  if(typeof t === "undefined" || t == ''){
    t=false;
  }
  else{
    t=true;
  }
  //console.log(t);
  str = str.split(", ").map(function(t){return parseFloat(t)});
  str2 = str2.split(",").map(function(t){return parseFloat(t)});
  if(str.length==1 && str2.length==2){
    str = str2;
  }
  cen = {lat: str[0], lng: str[1]};

    if(t){
      var locations = [
    ["locaton mane", 13.760931, 100.634258, 50]
     ];

   map = new google.maps.Map(document.getElementById('map'), {
     center: {lat: locations[0][1], lng: locations[0][2]},
     zoom: 18
   });
      var infowindow = new google.maps.InfoWindow();
      var marker, i;
      for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          animation: google.maps.Animation.DROP,
          map: map
        });
        //console.log(locations[i][3]);
        var cityCircle = new google.maps.Circle({
          strokeColor: '#FF0000',
          strokeOpacity: 0.5,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.1,
          map: map,
          center: new google.maps.LatLng(locations[i][1], locations[i][2]),
          radius: parseInt(locations[i][3])
        });
        cityCircle.bindTo('center', marker, 'position')

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));
      }
    }else{

      map = new google.maps.Map(document.getElementById('map'), {
        center: cen,
        zoom: z
      });
      marker = new google.maps.Marker({
        position: cen,
        draggable: d,
        animation: google.maps.Animation.DROP, //BOUNCE
        map: map
      });
      sunCircle = {
          strokeColor: "#306EFF",
          strokeOpacity: 0.5,
          strokeWeight: 1.5,
          fillColor: "#1589FF",
          fillOpacity: 0.1,
          map: map,
          center: cen,
          radius: r // in meters
      };
      cityCircle = new google.maps.Circle(sunCircle);
      cityCircle.bindTo('center', marker, 'position');

    }
       // marker.addListener('drag', handleEvent);
        //marker.addListener('dragend', handleEvent);
      }



JS;
$script2 = <<< JS
   

  function delArea(id){
    let isText=confirm('ยืนยันการลบ'); 
    let url_base_2 = $("#phpBaseUrl").val()
    if(isText)
    {
        $.ajax({
            // "url": url_base+ "/area/area-dt",

            url: url_base_2+"/area/del-area",
            type: 'post',
            data: {id:id,_csrf:yii.getCsrfToken()},
            success: function (res) {
              alert(res.message)
            location.reload();
            
            }
        });

    }

  }



JS;
$this->registerJs($script, View::POS_READY);

$this->registerJs($script2, View::POS_HEAD);
?>