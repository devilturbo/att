<?php

use yii\bootstrap\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AreaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Areas';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin([

    'action' => ['import'],
    'id' => 'importArea',
    'enableClientScript' => false,
    'method' => 'POST',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'options' =>  ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-4',
        ],
        'options' =>  ['class' => 'form-group row']
    ],
]); ?>
<div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->
            
            <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
              <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">Importสถานที่</h4>
                <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">Importสถานที่</p>
                </div>
              </div>
              <!--div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button>
              </div-->
            </div>
          </div>
          <div class="row mb-4">
            <div class="col-12">
            <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area">สถานที่ทั้งหมด</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-add">เพิ่มสถานที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="<?=Yii::$app->request->baseUrl?>area/import">Import สถานที่</a>
            </li>

            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-area">พื้นที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-zone">เขตพื้นที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-chanel">ช่องทาง</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-account">Account</a>
            </li>
        </ul>
             </div>
          </div>

          <div class="row">
            <div class="col-12 mb-4">
              <div class="row ">
                <div class="col-2 ml-auto"></div>
                  <div class="col-2"></div>
                </div>
                                    
               
                  <div class="form-row align-items-center mb-2">
                      <div class="col-sm-3 my-1-">
                        <span>ค้นหา</span>
                        <input type="text" class="form-control global_filter" id="global_filter" placeholder="คำที่ค้นหา">
                      </div>
                      <div class="col-3">
                        <div class="row">
                          <div class="col" ><span id="account"></span></div>
                          <div class="col" ><span id="ช่องทาง"></span></div>
                          <div class="col-auto">
                            <button type="button" class="btn btn-outline-primary btn-sm ml-auto bt-filter" style="margin-top: 20px;" data-toggle="modal" data-target="#ModalFilter">Filter</button>
                          </div>
                        </div>
                      </div>
                      

                  </div>
                  
                  <!-- Modal -->
                  <div id="ModalFilter" class="modal" role="dialog">
                    <div class="modal-dialog" >
                    <!-- Modal content-->
                      <div class="modal-content">
                          <div class="modal-header">
                              <span>ค้นหาโดยละเอียด</span>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                          <div class="col-12 p-4">
                            <div class="row">
                              <div class="col-6 mb-2" ><span id="เขตพื้นที่"></span></div>
                              <div class="col-6 mb-2"><span id="พื้นที่"></span></div>
                              <div class="col-6 mb-2"><span id="ช่องทาง"></span></div>
                              <div class="col-6 mb-2"><span id="account"></span></div>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>

                  
              
                                    
            </div>
          </div>

           <div class="row mb-4">
    <div class="col-12">
        <div class="card" style="display: <?=Yii::$app->user->identity->auth_location ==2?'none':''?>">
            <div class="card-body">
                <span class="mb-4 h5">เลือกไฟล์ที่ต้องการ</span> l <a href="<?=Yii::$app->homeUrl?>/shared/location/template/template_location.xlsx" target="blank" class="text-sm">ดาวน์โหลดไฟล์ตัวอย่าง</a><br>

                <div class="input-group mb-3">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="AreaTemp[file]" id="inputGroupFile02" onchange="uploadFile()">
                        <input type="hidden" name="AreaTemp[file]" value="">
                        <label class="custom-file-label" id="filename" for="inputGroupFile02">Choose file</label>
                    </div>
                    <div class="input-group-append">
                        <button class="input-group-text" id="btn-upload" onclick="importSlip()">Upload</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">

                  <div class="row pr-3 mb-2">
                      <span class="h5 ml-3 mt-2"><?=$rows?> รายการ</span>

                       <button style="display: <?=Yii::$app->user->identity->auth_location ==2?'none':''?>" class="btn btn-primary btn-sm mr-2 ml-auto" onclick="$('#importArea').attr('action', '<?=Yii::$app->request->baseUrl?>/area/create');">บันทึกทั้งหมด</button>
                       <button onclick="delAreaTempAll()" style="display: <?=Yii::$app->user->identity->auth_location ==2?'none':''?>" class="btn btn-outline-primary btn-sm ">ยกเลิก</button>
                  </div>


                  <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                      <tr>
                        <th>รหัสสาขา</th>
                        <th>account</th>
                        <th>ชื่อ</th>
                        <th>พิกัดละติจูด</th>
                        <th>พิกัดลองจิจูด</th>
                        <th>ระยะใช้งาน</th>
                        <th>พื้นที่</th>
                        <th>เขตพื้นที่</th>
                        <th>ช่องทาง</th>
                        <th>เพิ่มโดย</th>
                        <th style="display: <?=Yii::$app->user->identity->auth_location ==2?'none':''?>">ดำเนินการ</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($model)):?>
                      <?php foreach ($model as $key => $item):?>
                      <tr class="dt-edit" id="tr<?=$item->id?>" >
                        <td ><?=$item->brach_no?></td>
                        <td><?=$item->account?></td>
                        <td><?=$item->local_name?></td>
                        <td><?=$item->latitude?></td>
                          <td><?=$item->longitude?></td>
                        <td><?=$item->distance?></td>
                        <td><?=$item->area?></td>
                        <td><?=$item->zone?></td>
                        <td><?=$item->channel?></td>
                        <td><?=$item->created_by?></td>
                        <td style="display: <?=Yii::$app->user->identity->auth_location ==2?'none':''?>">
                          <input type="button" class="btn btn-outline-primary btn-sm mb-1"  onclick="delAreaTemp(<?=$item->id?>);" value="ลบ">
                        </td>
                      </tr> 
                       <?php endforeach;?>
                    <?php endif;?>                  
                    </tbody>
                  </table>
                  <div class="row pr-3 mb-2">

                       <button onclick="$('#importArea').attr('action', '<?=Yii::$app->request->baseUrl?>/area/create');" class="btn btn-primary btn-sm mr-2 ml-auto">บันทึกทั้งหมด</button>
                       <button onclick="delAreaTempAll()" class="btn btn-outline-primary btn-sm ">ยกเลิก</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- ลบข้ำมูล -->
<div id="myModaldelete" class="modal" role="dialog">
  <div class="modal-dialog" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <span>ลบข้อมูล</span>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="row p-4 text-center">
        <h2 class="text-danger">ยืนยันการลบข้อมูล</h2>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">ยืนยัน</button>
        <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal modaldetail" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <span>รายละเอียด</span>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-7 mappanel">

              <div class="map-container">
                    <div id="map-with-marker" class="google-map"></div>
                  </div>

            </div>
          <div class="col-3 mapdetail"></div>
        </div>
        
        
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div-->
    </div>

  </div>
</div>
<?php ActiveForm::end(); ?>
<?php
$script  = <<< JS
 function filterGlobal () {
    $('#dataTable').DataTable().search(
        $('#global_filter').val()

       ).draw();
    }
 
       var columnsToSearch = {
        1: 'account',
        6: 'พื้นที่',
        8: 'ช่องทาง',
        7: 'เขตพื้นที่'
    };

    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging:   false,
        ordering: false,
        info:     false,
        scrollY: 300,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: false,
        fixedHeader: true,
        initComplete: function () {
            for (var i in columnsToSearch) {
                var api = this.api();
                var select = $('<select class="form-control"><option value="">Select</option></select>')
                    .appendTo($('#' + columnsToSearch[i]).empty().text(columnsToSearch[i] + ' '))

                    .attr('data-col', i)
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        api.column($(this).attr('data-col'))
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                api.column(i).data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>');
                });
            }

        }

    });

    /*var colum = ["รูป", "รหัสสาขา", "Account","ชื่อ", "พิกัดละติจูด", "พิกัดลองจิจูด","ระยะใช้งาน", "พื้นที่","เขตพื้นที่","ช่องทาง","เพิ่มโดย","ดำเนินการ" ];
    $('.dt-edit').each(function () {
      $(this).on('click', function(evt){
      _this = $(this);
      var dtRow = _this;
      $('div.mapdetail').innerHTML='';
      for(var i=0; i < dtRow[0].cells.length; i++){
         $('div.mapdetail').append('<div class="row mb-1"> <span class="col-4">'+colum[i]+'</span><span class="col-8">'+ dtRow[0].cells[i].innerHTML+'</span></div>');
      }
      $('#myModal').modal('show');

      });
    });


  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .mapdetail').empty();
  });*/

 
  $('.bt-filter').on('click', function(evt){
    console.log("filter");


   });


    $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
       var min = $('#min-date').val();
       var max = $('#max-date').val();
       var createdAt = data[2] || 0; 
        if (
           (min == "" || max == "") ||
           (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
         ) {
            return true;
        }
            return false;
        }
    );

    $('.date-range-filter').change(function() {
      table.draw();
    });
    $('#my-table_filter').hide();

    $('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
 
JS;

$script2 = <<< JS
  function uploadFile()
  {
        var filename =$('input[type=file]')[0].files[0].name;
        $('#filename').html(filename)
        $('#btn-upload').removeClass('input-group-text').addClass('btn btn-primary btn-sm');
        
       
  }

  function importSlip() {
     $('#importArea').submit()
  }

  function delAreaTemp(id){
    let isText=confirm('ยืนยันการลบ'); 
    if(isText)
    {
        $.ajax({
            url: './del-areatemp',
            type: 'post',
            data: {id:id,_csrf:yii.getCsrfToken()},
            success: function (res) {
              alert(res.message)
              $('#tr'+id).remove();
            
             
            }
        });

    }

  }
  
    function delAreaTempAll(){
    let isText=confirm('ยืนยันการยกเลิก import ทั้งหมด'); 
    if(isText)
    {
        $.ajax({
            url: './del-areatemp',
            type: 'post',
            data: {_csrf:yii.getCsrfToken()},
            success: function (res) {
              alert(res.message)
              $('.dt-edit').remove();
            
            }
        });

    }

  }
JS;
$this->registerJs($script2, View::POS_HEAD);
$this->registerJs($script, View::POS_READY);
?>