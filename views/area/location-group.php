<?php
use yii\bootstrap\ActiveForm;
use yii\web\View;
$this->title = 'เพ่ิมสถานที่';

?>

<div class="dashboard-header d-flex flex-column grid-margin" id="headPage">
    <!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">เพิ่มกลุ่มสถานที่</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">เพิ่มกลุ่มสถานที่</p>
            </div>
        </div>
        <!--div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button>
              </div-->
    </div>
</div>
<div class="row">
    <div class="col-12">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area">สถานที่ทั้งหมด</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-add">เพิ่มสถานที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/import">Import สถานที่</a>
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link active" href="<?=Yii::$app->request->baseUrl?>/area/location-group">กลุ่มสถานที่</a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-area">พื้นที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-zone">เขตพื้นที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-chanel">ช่องทาง</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-account">Account</a>
            </li>
        </ul>
    </div>
</div>

<div class="row mb-4">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <span class="font-weight-bold h6">เพิ่มกลุ่มสถานที่</span>
                <!-- <form class="forms-sample row mt-2"> -->
                <?php
$form = ActiveForm::begin([
    'action' => ['insert-area-group'],
    'id' => 'formElem',
    'enableClientScript' => false,
    'method' => 'POST',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'options' => ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
      'horizontalCssClasses' => [
          'label' => 'col-sm-2',
          'offset' => 'col-sm-offset-2',
          'wrapper' => 'col-sm-4',
      ],
      'options' => ['class' => 'form-group row mt-2'],
  ],
]);
?>
                <div class="form-group col-12">
                    <input type="text" class="form-control" name="AreaGroup[group_name]" placeholder="ชื่อกลุ่ม">
                </div>

                <div class="form-group col-12">

                    <div class="con-12">
                        <form>
                            <select multiple="multiple" name="AreaList[area_id][]" id="location_select">
                                <!-- modelMemberAccount -->
                                <?php
                    foreach ($modelMemberAccount as $key => $e) {

                                ?>
                                <optgroup label="<?=$e->account?>">
                                    <?php
                    foreach ($e->area as $key => $a) {
                      $display = $a->account . " " . $a->brach_no . " " . $a->local_name;
?>
                                    <option value="<?=$a->id?>" id="<?=$a->brach_no?>"><?=$display?></option>

                                    <?php
                    }
      ?>
                                </optgroup>
                                <?php
                    }
                                    ?>
                            </select>
                    </div>
                </div>


<?php
$arrMemberNo = Yii::$app->Utilities->getListMemberNo();

?>
                <div class="form-group col-12">
                    <label for="exampleInputUsername1">ผู้ดูแล</label>

                    <!-- getListMemberNo -->
                    <select name="AreaGroup[keeper]" data-placeholder="Select" class="chosen-select-deselect" tabindex="">  
                                            <?php
$arrMemberNo = Yii::$app->Utilities->getListMemberNo();
?>
                                            <?php foreach ($arrMemberNo as $key => $item):

    ?>

                                            <option value="<?=$key?>">
                                                <?=$item?>
                                            </option>
                                            <?php endforeach;?>
                                        </select>

                    <!-- <input type="text" class="form-control" name="AreaGroup[keeper]" placeholder=" รหัสพนักงาน"> -->
                </div>

                <div class="row">
                    <button type="submit" class=" ml-auto btn btn-primary btn-sm mr-2 " id="confirmButton">
                        บันทึก
                    </button>

                    <a href="/area">
                        <button type="button" class="btn btn-light btn-sm mr-2">ยกเลิก</button>
                    </a>
                </div>


                <?php ActiveForm::end();?>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <h5 class="font-weight-bold mb-4"><?=$modelRows?> รายการ</h5>
        <div class="card">
            <div class="card-body">


                <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                        <tr>
                            <th>ชื่อกลุ่ม</th>
                            <th>ผู้ดูแล</th>
                            <th>จำนวน</th>
                            <th width="120px;">ดำเนินการ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                    foreach ($model as $key => $e) {

                    ?>
                        <tr class="dt-edit">
                            <td><?=$e->group_name?></td>
                            <td><?=$e->owner?></td>
                            <td><?=count($e->areaList)?></td>
                            <td>
                                <a href="<?=Yii::$app->request->baseUrl?>/area/location-group-detail?id=<?=$e->id?>">
                                    <button class="btn btn-outline-primary btn-sm mb-1">
                                        แก้ไข
                                    </button>
                                </a>
                                <button class="btn btn-outline-primary btn-sm mb-1 confirmButton"
                                    id="<?=$e->id?>">ลบ</button>
                            </td>
                        </tr>
                        <?php
                    }
?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="phpBaseUrl" value="<?=Yii::$app->request->baseUrl?>">

<?php
$script2 = <<< JS
   var select = document.getElementById("location_select");
    multi(select, {
                enable_search: true
    });


// $("#location_select").change(function() {
//   var selectedItem = $(this).val();
//   var selectedText = $("#location_select option:selected").html();
//   var show = []
//   $("#location_select option:selected").each(function () {
//     var \$this = $(this);
//     var all_text = \$this.attr("id")

//     show.push(all_text);
//   })
//   // console.log(show);
//   $("#group_name").text(show)
// });

const url_base = $("#phpBaseUrl").val();

$(document).on("click", ".confirmButton", function(event) {
    console.log(this.id)
    event.preventDefault()

    var r = confirm("ยืนยันที่จะลบกลุ่มสถานที่?");
    if (r == true) {
        $.post(url_base+ "/area/delete-area-group",{
        // $.post("/news/delete",{
            id: this.id,
        },function(data){
            if (data == "Success"){
              console.log(data);
                // window.location.href = url_base + "/news"
            }else{
                console.log("FAIL: " , data)
            }
        });
    }
})

function uploadFile()
{
      var filename =$('input[type=file]')[0].files[0].name;
      $('#filename').html(filename)
    //   $('#btn-upload').removeClass('input-group-text').addClass('btn btn-primary btn-sm');


}
JS;
$this->registerJs($script2, View::POS_READY);
?>