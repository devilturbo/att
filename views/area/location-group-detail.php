<?php
use yii\bootstrap\ActiveForm;
use yii\web\View;
$this->title = 'เพ่ิมสถานที่';

?>

<div class="dashboard-header d-flex flex-column grid-margin" id="headPage">
    <!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">เพิ่มกลุ่มสถานที่</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">เพิ่มกลุ่มสถานที่</p>
            </div>
        </div>
        <!--div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button>
              </div-->
    </div>
</div>
<div class="row">
    <div class="col-12">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area">สถานที่ทั้งหมด</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-add">เพิ่มสถานที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/import">Import สถานที่</a>
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link active" href="<?=Yii::$app->request->baseUrl?>/area/location-group">กลุ่มสถานที่</a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-area">พื้นที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-zone">เขตพื้นที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-chanel">ช่องทาง</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-account">Account</a>
            </li>
        </ul>
    </div>
</div>

<div class="row mb-4">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <span class="font-weight-bold h6">เพิ่มกลุ่มสถานที่</span>
                <!-- <form class="forms-sample row mt-2"> -->
                <?php
$form = ActiveForm::begin([
    'action' => ['update-area-group'],
    'id' => 'formElem',
    'enableClientScript' => false,
    'method' => 'POST',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'options' => ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-4',
        ],
        'options' => ['class' => 'form-group row mt-2'],
    ],
]);

?>
                <div class="form-group col-12">
                    <input type="text" class="form-control" name="AreaGroup[group_name]" value="<?=$model->group_name?>"
                        placeholder="ชื่อกลุ่ม">
                </div>

                <div class="form-group col-12">
                   


                </div>


                <input type="hidden" name="id" value="<?=$_GET['id']?>"/>

                <div class="form-group col-12">
                    <label for="exampleInputUsername1">ผู้ดูแล</label>

                        <select name="AreaGroup[keeper]" data-placeholder="Select" class="chosen-select-deselect" tabindex="">
                                            <?php
$arrMemberNo = Yii::$app->Utilities->getListMemberNo();
?>
                                            <?php foreach ($arrMemberNo as $key => $item):

    $status = '';
    if ($key == $model->keeper) {
        $status = 'selected';
    }

    ?>
	                                            <option <?=$status?> value="<?=$key?>">
	                                                <?=$item?>
	                                            </option>
	                                            <?php endforeach;?>
                                        </select>

                </div>

                <div class="row">
                    <button type="submit" class=" ml-auto btn btn-primary btn-sm mr-2 " id="confirmButton">
                        บันทึก
                    </button>

                    <a href="/area">
                        <button type="button" class="btn btn-light btn-sm mr-2">ยกเลิก</button>
                    </a>
                </div>


                <?php ActiveForm::end();?>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
    <h5 class="font-weight-bold mb-4"><?=$ListRows?> รายการ</h5>
        <div class="card">
            <div class="card-body">


                <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                        <tr>
                            <th>รูป</th>
                            <th>รหัสสาขา</th>
                            <th>Account</th>
                            <th>ชื่อ</th>
                            <th>พิกัดละติจูด</th>
                            <th>พิกัดลองจิจูด</th>
                            <th>ระยะใช้งาน</th>
                            <th>พื้นที่</th>
                            <th>เขตพื้นที่</th>
                            <th>ช่องทาง</th>
                            <th>เพิ่มโดย</th>
                            <th>ดำเนินการ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
foreach ($model->areaList as $key => $e) {
//    \app\components\Helpers::Debig($e);
if ($e->area->type == 'web') {
    $img = 'https://att.thebestpromote.com/web/' . $e->area->img;
  } else if($e->area->type == 'mobile'){
      $img = 'https://api.thebestpromote.com/web/' . $e->area->img;
  }else {
    $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
  }

    ?>
                        <tr class="dt-edit">
                            <td class="employee-col">
                                <img class="thumb mr-2 lazy" src="<?=$img?>" align="left"/>
                            </td>
                            <td><?=$e->area->brach_no?></td>
                            <td><?=$e->area->account?></td>
                            <td><?=$e->area->local_name?></td>
                            <td><?=$e->area->latitude?></td>
                            <td><?=$e->area->longitude?></td>
                            <td><?=$e->area->distance?></td>
                            <td><?=$e->area->area?></td>
                            <td><?=$e->area->zone?></td>
                            <td><?=$e->area->channel?></td>
                            <td><?=$model->keeper?></td>

                            <td>
                                <a href="location-add?id=<?=$e->area_id?>">
                                    <button class="btn btn-outline-primary btn-sm mb-1">แก้ไข</button>
                                </a>
                                <a href="group-list-del?id=<?=$e->area_id?>&link=<?=$_GET['id']?>">
                                <button class="btn btn-outline-primary btn-sm mb-1">ลบ</button>
                                </a>
                            </td>
                        </tr>

                        <?php
}
?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="myModal" class="modal modaldetail" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <span>รายละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-7 mappanel">
                        <div id="map" style="height:570px"></div>
                    </div>
                    <div class="col-3 mapdetail"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="phpBaseUrl" value="<?=Yii::$app->request->baseUrl?>">

<?php
$script2 = <<< JS


const url_base = $("#phpBaseUrl").val();

$(document).on("click", ".confirmButton", function(event) {
    console.log(this.id)
    event.preventDefault()

    var r = confirm("ยืนยันที่จะลบกลุ่มสถานที่?");
    if (r == true) {
        $.post(url_base+ "/area/delete-area-group",{
        // $.post("/news/delete",{
            id: this.id,
        },function(data){
            if (data == "Success"){
              console.log(data);
                // window.location.href = url_base + "/news"
            }else{
                console.log("FAIL: " , data)
            }
        });
    }
})

function uploadFile()
{
      var filename =$('input[type=file]')[0].files[0].name;
      $('#filename').html(filename)
    //   $('#btn-upload').removeClass('input-group-text').addClass('btn btn-primary btn-sm');


}


var colum = ["รูป", "รหัสสาขา", "Account","ชื่อ", "พิกัดละติจูด", "พิกัดลองจิจูด","ระยะใช้งาน", "พื้นที่","เขตพื้นที่","ช่องทาง","เพิ่มโดย","ดำเนินการ" ];

$('.dt-edit').each(function () {
    console.log('object');
      $(this).on('click', function(evt){
      \$this = $(this);
      var dtRow = \$this;
      $('div.mapdetail').innerHTML='';
      $('div.mapdetail').append( '<div class="row mb-4 ml-1 img-thumbmail-small lazy">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=1; i < dtRow[0].cells.length; i++){
         $('div.mapdetail').append('<div class="row mb-1"> <span class="col-4">'+colum[i]+'</span><span class="col-8">'+ dtRow[0].cells[i].innerHTML+'</span></div>');
            //console.log(i);
      }
      $('#myModal').modal('show');

// console.log(object);
      initMap(''+ dtRow[0].cells[4].innerHTML+','+ dtRow[0].cells[5].innerHTML+'',false,false,dtRow[0].cells[6].innerHTML);

      });
    });

$('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .mapdetail').empty();
  });


var marker;
var sunCircle;
var map;
var map2;
var cen;
function initMap(location,d,t,rr) {

    console.log("init Map");

    var str='';
    var str2='';
    if(typeof rr === "undefined" || rr==''){
            //var r = parseInt(document.getElementById("range").value);
          }
    else{
       var r = parseInt(rr);
    }
    if( r >= 800 ){
      z = 14;
    }
    else if( r >= 400  && r <= 800  ){
      z = 15;
    }
    else if(  r >= 200  && r <= 400  ){
      z = 16;
    }
    else if(  r >= 100  && r <= 200  ){
      z = 17;
    }
    else if(  r >= 50  && r <= 100  ){
      z = 18;
    }
    else if(  r >= 25  && r <= 50  ){
      z = 19;
    }
    else{
      z = 20;
    }

    if(typeof location === "undefined" || location==''){
      str = "13.674351, 100.517556";
      str2 = "13.674351,100.517556";
    }
    else{
       str = location;
       str2 = location;
    }
    if(typeof d === "undefined" ){
      d=true;
    }
    else{

      d=false;
    }
  if(typeof t === "undefined" || t == ''){
    t=false;
  }
  else{
    t=true;
  }
  //console.log(t);
  str = str.split(", ").map(function(t){return parseFloat(t)});
  str2 = str2.split(",").map(function(t){return parseFloat(t)});
  if(str.length==1 && str2.length==2){
    str = str2;
  }
  cen = {lat: str[0], lng: str[1]};

    if(t){
      var locations = [
    ["locaton mane", 13.760931, 100.634258, 50]
     ];

   map = new google.maps.Map(document.getElementById('map'), {
     center: {lat: locations[0][1], lng: locations[0][2]},
     zoom: 18
   });
      var infowindow = new google.maps.InfoWindow();
      var marker, i;
      for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          animation: google.maps.Animation.DROP,
          map: map
        });
        //console.log(locations[i][3]);
        var cityCircle = new google.maps.Circle({
          strokeColor: '#FF0000',
          strokeOpacity: 0.5,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.1,
          map: map,
          center: new google.maps.LatLng(locations[i][1], locations[i][2]),
          radius: parseInt(locations[i][3])
        });
        cityCircle.bindTo('center', marker, 'position')

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));
      }
    }else{

      map = new google.maps.Map(document.getElementById('map'), {
        center: cen,
        zoom: z
      });
      marker = new google.maps.Marker({
        position: cen,
        draggable: d,
        animation: google.maps.Animation.DROP, //BOUNCE
        map: map
      });
      sunCircle = {
          strokeColor: "#306EFF",
          strokeOpacity: 0.5,
          strokeWeight: 1.5,
          fillColor: "#1589FF",
          fillOpacity: 0.1,
          map: map,
          center: cen,
          radius: r // in meters
      };
      cityCircle = new google.maps.Circle(sunCircle);
      cityCircle.bindTo('center', marker, 'position');

    }
       // marker.addListener('drag', handleEvent);
        //marker.addListener('dragend', handleEvent);
      }

JS;

$this->registerJs($script2, View::POS_READY);
?>