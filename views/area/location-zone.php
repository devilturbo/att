<?php
use yii\bootstrap\ActiveForm;
use yii\web\View;
$this->title = 'เพ่ิมสถานที่';

?>

<div class="dashboard-header d-flex flex-column grid-margin" id="headPage">
    <!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">เพิ่มเขตพื้นที่</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">เพิ่มเขตพื้นที่</p>
            </div>
        </div>
        <!--div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button>
              </div-->
    </div>
</div>
<div class="row">
    <div class="col-12">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area">สถานที่ทั้งหมด</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-add">เพิ่มสถานที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/import">Import สถานที่</a>
            </li>
                        <!-- <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-group">กลุ่มสถานที่</a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-area">พื้นที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="<?=Yii::$app->request->baseUrl?>/area/location-zone">เขตพื้นที่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-chanel">ช่องทาง</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/area/location-account">Account</a>
            </li>
        </ul>
    </div>
</div>



<div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">

                 <div class="row mb-2 mt-1">
                    <span class="h5 text-title ml-2 mt-1">ทั้งหมด <?=$modelRows?> ราการ</span>
                     <a style="display: <?=Yii::$app->user->identity->auth_location ==2?'none':''?>" href="#edit_detail" class="btn btn-primary btn-sm mr-2 ml-auto" data-toggle="modal">เพิ่มเขตพื้นที่</a>
                  </div>


                  <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                      <tr class="bg-primary-bg text-ba">
                        <th>เขตพื้นที่</th>
                        <th width="50" style="display: <?=Yii::$app->user->identity->auth_location ==2?'none':''?>">ดำเนินการ</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                     <?php
                    foreach ($model as $key => $e) {
                     ?>
                      <tr class="dt-edit">
                        <td><?=$e->zone?></td>
                        <td style="display: <?=Yii::$app->user->identity->auth_location ==2?'none':''?>"><a href="#myModaldelete<?=$e->id?>" class="btn btn-outline-primary btn-sm mr-2 ml-auto" data-toggle="modal">ลบ</a></td>
                      </tr>


<div id="myModaldelete<?=$e->id?>" class="modal" role="dialog">
  <div class="modal-dialog" >
    <!-- Modal content-->
    <?php
$form = ActiveForm::begin([

    'action' => ['delete-member-zone'],
    'id' => 'formElem',
    'enableClientScript' => false,
    'method' => 'POST',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'options' => ['enctype' => 'multipart/form-data'],
]);
?>      
<div class="modal-content">
      <div class="modal-header">
        <span>ลบข้อมูล</span>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="row p-4 text-center">
        <h2 class="text-danger">ยืนยันการลบข้อมูล <?=$e->zone?></h2>
        <input type="hidden" name="id" value="<?=$e->id?>">
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-sm">ยืนยัน</button>
        <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>
    <?php ActiveForm::end();?>
  </div>
</div>
                   <?php
                    }
                   ?>

                                
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>


<div id="edit_detail" class="modal">
  <div class="modal-dialog">
    <div class="modal-content p-4">
    <?php
$form = ActiveForm::begin([

    'action' => ['insert-member-zone'],
    'id' => 'formElem',
    'enableClientScript' => false,
    'method' => 'POST',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'options' => ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-4',
        ],
        'options' => ['class' => 'form-group row'],
    ],
]);
?>        <div class="modal-body p-2"> 
          <div class="col-12 mb-3">
            <label for="validationCustom01">เพิ่มเขตพื้นที่</label>
            <input type="text" name="MemberZone[zone]" class="form-control"  placeholder="" value="" required>
          </div>
          <div class="row mt-4">
            <input type="button" class="btn btn-default ml-auto btn-sm" data-dismiss="modal" value="Close">
            <input type="submit" class="btn btn-primary btn-sm mr-3" value="บันทึกข้อมูล">
          </div>
        </div>
        <?php ActiveForm::end();?>
    </div>
  </div>
</div>


<?php
$script4 = <<< JS

JS;
?>

<?php
$script2 = <<< JS

$(document).on("click", ".confirmButton", function(event) {
    // console.log(this.id)
    event.preventDefault()
    var r = confirm("ยืนยันที่จะลบข่าว?");
    if (r == true) {

        $.post(url_base+ "/news/delete",{
        // $.post("/news/delete",{
            id: this.id,
        },function(data){
            if (data == "Success"){
                window.location.href = url_base + "/news"
            }else{
                alert("FAIL: " , data)
            }
        });
    }
})

function uploadFile()
{
      var filename =$('input[type=file]')[0].files[0].name;
      $('#filename').html(filename)
    //   $('#btn-upload').removeClass('input-group-text').addClass('btn btn-primary btn-sm');


}
JS;
$this->registerJs($script2, View::POS_HEAD);
$this->registerJs($script4, View::POS_READY);

?>