<?php
?>
<style type="text/css">
    h6{ font-size: 1.3rem; }
</style>
<div class="row">
    <div class="col-12 ">

        <div class="row">
            <div class="col-6">
                <div class="card mb-4">
                    <div class="card-body">

                        <form class="needs-validation" novalidate="">
                            <h6>เวลาทำงาน</h6>
                            <div class="form-row">
                                <div class="col-6 mb-2 mt-2 mr-4">
                                    <label for="validationCustom01">เวลาเข้างานปกติ</label>
                                    <input type="text" class="form-control" id="validationCustom01" placeholder="" name="time_attendance" value="<?=$model->time_attendance?>" required="">
                                </div>
                                <div class="col-5 mb-2 mt-2">
                                    <span class="text-muted mb-3 d-block">รูปแบบการเข้าทำงาน</span>
                                    <div class="radio icheck-peterriver">
                                        <input type="radio" <?=$model->attendance_type=='Y'?'checked':''?> id="leave6" value="Y" name="attendance_type" />
                                        <label for="leave6">เข้าทำงานปกติตามเวลาที่กำหนด</label>
                                    </div>
                                    <div class="radio icheck-peterriver">
                                        <input type="radio"  id="leave7" name="attendance_type" <?=$model->attendance_type=='N'?'checked':''?> value="N" />
                                        <label for="leave7">เข้าออกงานไม่เป็นเวลา</label>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>


            </div>
            <div class="col-6">
                <div class="card mb-4">
                    <div class="card-body">
                        <h6 class="text-muted mb-4">พนักงานขาย BA Performance</h6>
                        <div class="checkbox icheck-peterriver">
                            <input type="checkbox" id="addon2" name="ba" value="Y"  <?=$model->ba=='Y'?'checked':''?> />
                            <label for="addon2">กำหนดเป็นพนักงานขาย</label>
                        </div>
                        <div class="checkbox icheck-peterriver">
                            <input type="checkbox" id="addon2" name="isLeave" value="N"  
                            <?php if(!empty($model->isLeave)){
                               echo  $model->isLeave=='N' ? 'checked':'';
                            }?> />
                            <label for="addon2">ไม่แสดงสิทธิการลา</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="text-muted mb-4">เงื่อนไขการลา</h6>
                        <div class="row">

                            <div class="col-4" style="display:none;">
                                <span class="text-muted mb-3 d-block">หน่วยระยะเวลาการลาขั้นต่ำ</span>
                                <div class="radio icheck-peterriver">
                                    <input type="radio" <?=$model->leave_codition=='Y'?'checked':''?> id="peterriver1" name="leave_codition" value="Y" />
                                    <label for="peterriver1">อนุญาตให้ลาเป็นชั่วโมงได้</label>
                                </div>
                                <div class="radio icheck-peterriver">
                                    <input type="radio" checked id="peterriver2"  <?=$model->leave_codition=='N'?'checked':''?> name="leave_codition" value="N" />
                                    <label for="peterriver2">อนุญาตให้ลาเป็นวันเท่านั้น</label>
                                </div>


                            </div>

                            <div class="col-4">
                                <span class="text-muted mb-3 d-block">วันที่เริ่มได้สิทธิ์การลานี้</span>

                                <div class="radio icheck-peterriver">
                                    <input type="radio" id="leave2" name="leave_type" <?=$model->leave_type=='Y'?'checked':''?> value="Y" />
                                    <label for="leave2">วันแรกที่เริ่มทำงาน</label>
                                </div>
                                <div class="radio icheck-peterriver">
                                    <input type="radio"  id="leave3" name="leave_type" <?=$model->leave_type=='N'?'checked':''?> value="N" />
                                    <label for="leave3">วันแรกที่บรรจุเป็นพนักงานประจำ</label>
                                </div>


                            </div>

                            <div class="col-4">
                                <span class="text-muted mb-3 d-block">ยกยอดสะสมวันลาที่เหลืออยู่ ณ ปลายปี?</span>
                                <div class="radio icheck-peterriver">
                                    <input type="radio" id="leave4" name="leave_year" value="Y" <?=$model->leave_year=='Y'?'checked':''?> />
                                    <label for="leave4">ใช่</label>
                                </div>
                                <div class="radio icheck-peterriver">
                                    <input type="radio"  id="leave5" name="leave_year" value="N" <?=$model->leave_year=='N'?'checked':''?> />
                                    <label for="leave5">ไม่ใช่</label>
                                </div>


                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-12">
                                <h6 class="text-muted mb-4">วันหยุดประจำสัปดาห์</h6>


                                <div class="custom-control custom-checkbox custom-control-inline mb-2">
                                    <input type="checkbox" <?=$model->monday=='Y'?'checked':''?> id="daycheckbox1" name="monday" value="Y" class="custom-control-input">
                                    <label class="custom-control-label" for="daycheckbox1">จันทร์</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-control-inline mb-2">
                                    <input type="checkbox" <?=$model->thesday=='Y'?'checked':''?> id="daycheckbox2" name="thesday" class="custom-control-input" value="Y">
                                    <label class="custom-control-label" for="daycheckbox2">อังคาร</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-control-inline mb-2">
                                    <input type="checkbox" <?=$model->wednesday=='Y'?'checked':''?> id="daycheckbox7" name="wednesday" class="custom-control-input" value="Y">
                                    <label class="custom-control-label" for="daycheckbox7">พุธ</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-control-inline mb-2">
                                    <input type="checkbox" <?=$model->thursday=='Y'?'checked':''?> id="daycheckbox3" name="thursday" class="custom-control-input" value="Y">
                                    <label class="custom-control-label" for="daycheckbox3">พฤหัส</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-control-inline mb-2">
                                    <input type="checkbox" <?=$model->friday=='Y'?'checked':''?> id="daycheckbox4" name="friday" class="custom-control-input" value="Y">
                                    <label class="custom-control-label" for="daycheckbox4">ศุกร์</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-control-inline mb-2">
                                    <input type="checkbox" <?=$model->saturday=='Y'?'checked':''?> id="daycheckbox5" name="saturday" class="custom-control-input" value="Y">
                                    <label class="custom-control-label" for="daycheckbox5">เสาร์</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-control-inline mb-2">
                                    <input type="checkbox" <?=$model->sunday=='Y'?'checked':''?> id="daycheckbox6" name="sunday" class="custom-control-input" value="Y">
                                    <label class="custom-control-label" for="daycheckbox6">อาทิตย์</label>
                                </div>
                            </div>
                        </div></div>
                </div>
            </div>
            <div class="col-3" style="display: none;">
                <div class="card">
                    <div class="card-body">
                        <h6 class="text-muted mb-4">วันลาหยุดชดเชย</h6>
                        <div class="radio icheck-peterriver">
                            <input type="radio"  id="leave8" name="substitution_holiday" <?=$model->substitution_holiday=='Y'?'checked':''?> value="Y" />
                            <label for="leave8">บังคับหยุดในวันทำงานถัดไป</label>
                        </div>
                        <div class="radio icheck-peterriver">
                            <input type="radio"  id="leave9" name="substitution_holiday" <?=$model->substitution_holiday=='N'?'checked':''?>  value="N" />
                            <label for="leave9">สะสมเป็นวันหยุดชดเชย</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-3" style="display: none;">
                <div class="card">
                    <div class="card-body">
                        <h6 class="text-muted mb-4">บังคับทำงานตามสถานที่เท่านั้น</h6>
                        <div class="radio icheck-peterriver">
                            <input type="radio"  id="leave12" name="location_type" <?=$model->location_type=='Y'?'checked':''?> value="Y"  />
                            <label for="leave12">บังคับ</label>
                        </div>
                        <div class="radio icheck-peterriver">
                            <input type="radio"  id="leave11" name="location_type" value="N" <?=$model->location_type=='N'?'checked':''?> />
                            <label for="leave11">ไม่บังคับ</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>