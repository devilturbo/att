<?php
use yii\bootstrap\ActiveForm;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'company-position';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">ตำแหน่ง</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">ตำแหน่ง</p>
            </div>
        </div>
        <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0" id='btn_export'>
          
        </div>
    </div>
</div>

<?=$this->render('_company_header',[
    'isActive' => 'position'
])?>






<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <span class="h5 ml-3 mt-2">ตำแหน่ง(<?=$modelRows?>)</span>
                    <a href="./position-add" style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>"  class="ml-auto"><button class="btn btn-outline-primary btn-sm  mb-2 mr-2">เพิ่มตำแหน่ง</button></a>
                </div>

                <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                    <tr>
                        <th>ลำดับ</th>
                        <th>ตำแหน่ง</th>
                        <th>หมายเหตุ</th>
                        <th>เวลาในระบบ</th>
                        <th>ตั้งค่าบริษัท</th>
                        <th>พนักงานในระบบ</th>
                        <th>เพิ่มพนักงานในระบบ</th>
                        <th>นำเข้าข้อมูลพนักงาน</th>
<!--                        <th>กลุ่มพนักงาน</th>-->
<!--                        <th>รายงาน(BA)</th>-->
                        <th>อนุมัติรายการ</th>
                        <th>ประวัติบันทึกเวลา</th>
                        <th>ประวัติบันทึกการลา</th>
                        <th>ประวัติปรับปรุงเวลา</th>
                        <th>ประวัติทำงานล่วงเวลา</th>
                        <th>กำหนดพิกัดสถานที่</th>
                        <th>ข่าวสาร</th>
                        <th>Export</th>

<!--                        <th>Vacancy</th>-->
                        <th width="100px;" style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>"  >ดำเนินการ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($model)):?>
                    <?php
                        $i=0;
                        $arrAuth = [
                            '1' => 'จัดการได้',
                            '2' => 'แสดงเท่านั้น',
                            '3' => 'ไม่แสดง',
                        ];
                    ?>
                    <?php foreach ($model as $key => $item):?>
                    <tr class="dt-edit">
                        <td><?=++$i?></td>
                        <td><?=$item->position_name?></td>
                        <td><?=$item->remark?></td>
                        <td><?=$item->life_cycle?></td>
                        <td><?=$arrAuth[$item->auth_company]?></td>
                        <td><?=$arrAuth[$item->auth_member]?></td>
                        <td><?=$arrAuth[$item->auth_add_member]?></td>
                        <td><?=$arrAuth[$item->auth_import_member]?></td>
                        <td><?=$arrAuth[$item->auth_approve_order]?></td>
                        <td><?=$arrAuth[$item->auth_record_time]?></td>
                        <td><?=$arrAuth[$item->auth_record_leave]?></td>
                        <td><?=$arrAuth[$item->auth_update_time]?></td>
                        <td><?=$arrAuth[$item->auth_record_time]?></td>
                        <td><?=$arrAuth[$item->auth_location]?></td>
                        <td><?=$arrAuth[$item->auth_news]?></td>
                        <td><?=$arrAuth[$item->auth_export]?></td>
                        <td style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>"  >
                            <a  style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>"  href="<?=Yii::$app->homeUrl?>company/position-add?id=<?=$item->id?>" class="btn btn-outline-primary btn-sm ">แก้ไข</a>
                            <button style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>"  class="btn btn-outline-primary btn-sm" data-toggle="modal" onclick="delPosition(<?=$item->id?>)" data-target="#myModaldelete" data-dismiss="modal">ลบ</button>
                        </td>
                    </tr>
                        <?php endforeach;?>
                    <?php endif;?>



                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<!-- ลบข้ำมูล -->


<!-- table detail -->
<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>รายละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>
            <!--div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div-->
        </div>

    </div>
</div>

<?php


$script = <<< JS
    $(document).ready(function() {

        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });

        table = $('#dataTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                
            ],
            searching: true,
            paging:   false,
            ordering: false,
            info:     false,
            scrollY: 300,
            scrollX:        true,
            scrollCollapse: false,
            autoWidth:      false,
            fixedColumns:   {
                leftColumns: 1
            },
            columnDefs: [
                { "width": "50px", "targets": [ 0 ] },
            ],
            responsive: false,
            fixedHeader: true

        });

var buttons = new $.fn.dataTable.Buttons(table, {
       buttons: [
    {
        extend: 'excel',
        text: 'Data Export',
        charset: 'utf-8',
        extension: '.xlsx',
        bom: true,
        className: 'btn btn-outline-primary btn-sm  d-none d-md-block',
    }]
}).container().appendTo($('#btn_export'));

        var colum = ["ลำดับ", "ตำแหน่ง", "หมายเหตุ","เวลาในระบบ","ตั้งค่าบริษัท","พนักงานในระบบ","เพิ่มพนักงานในระบบ","นำเข้าข้อมูลพนักงาน","อนุมัติรายการ","ประวัติบันทึกเวลา","ประวัติบันทึกการลา","ประวัติปรับปรุงเวลา","ประวัติทำงานล่วงเวลา","กำหนดพิกัดสถานที่","ข่าวสาร","Export","ดำเนินการ"];


        //Edit row buttons
        $('.dt-edit').each(function () {
            $(this).on('click', function(evt){
                _this = $(this);
                var dtRow = _this;
                $('div.modal-body').innerHTML='';
                //$('div.modal-body').append( '<div class="row mb-4 h3">'+dtRow[0].cells[0].innerHTML+'</div>');
                for(var i=0; i < dtRow[0].cells.length; i++){
                    $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-4 ">'+colum[i]+'</span><span class="col-6">'+dtRow[0].cells[i].innerHTML+'</span><div/>');
                    console.log(i);
                }
                $('#myModal').modal('show');
            });
        });
        $('#myModal').on('hidden.bs.modal', function (evt) {
            $('.modal .modal-body').empty();
        });



    } );

    $('.buttons-html5').addClass('btn btn-outline-primary btn-sm  d-none d-md-block text-white')
JS;
$script2 = <<< JS



  function delPosition(id){
    
    let isText=confirm('ยืนยันการลบ'); 
    if(isText)
    {
        $.ajax({
            url: './del-position',
            type: 'post',
            data: {id:id,_csrf:yii.getCsrfToken()},
            success: function (res) {
              alert(res.message)
            location.reload();
            
            }
        });

    }
    
  }
  
  
JS;
$this->registerJs($script, View::POS_READY);
$this->registerJs($script2, View::POS_HEAD);
?>


