<?php
use yii\bootstrap\ActiveForm;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'company-add-type';
$this->params['breadcrumbs'][] = $this->title;
$arrPosition = Yii::$app->Utilities->getListPosition();

?>
<?php $form = ActiveForm::begin([

    'action' => ['add-type?id='.$model->id],
    'id' => 'formCompany',
    'enableClientScript' => false,
    'method' => 'POST',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-4',
        ],
        'options' =>  ['class' => 'form-group row']
    ],
]); ?>
<div class="main-content-inner mb-5">
    <div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->

        <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
            <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">เพิ่มตำแหน่ง</h4>
                <div class="d-none d-md-flex mt-1">
                    <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                    <i class="mdi mdi-chevron-right text-muted"></i>
                    <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                    <i class="mdi mdi-chevron-right text-muted"></i>
                    <p class="text-muted mb-0 tx-13 cursor-pointer">เพิ่มตำแหน่ง</p>
                </div>
            </div>
            <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">

                <a href="<?=Yii::$app->homeUrl?>company/type" class=" btn btn-outline-primary btn-sm  ml-auto">ยกเลิก</a>
                <a href="company-position.php"><button class="btn btn-primary btn-sm mr-3  ml-2">บันทึก</button></a>

            </div>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row mt-2 ">
                        <div class="col-8"><h4 class="text-header">เพิ่มประเภทพนักงาน Type</h4></div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-12">

                                <div class="form-row">

                                    <div class="col-12 mb-3">
                                        <label for="validationCustom01">ชื่อตำแหน่ง</label>
                                <input type="textbox" class="form-control" id="validationCustom01" placeholder="TBA" name="type_name" value="<?=$model->type_name?>" required="">
                                    </div>

                                    <div class="col-12 mb-3">
                                        <label for="validationCustom01">หมายเหตุ</label>
                                        <input type="textbox" class="form-control" id="validationCustom01" placeholder="" name="remark" value="<?=$model->remark?>" required="">
                                    </div>


                                </div>

                            <?php
                          echo  $this->render('_member_condition', [
                                'model' => $model,
                            ]);
                            ?>
                        </div>
                    </div>





                    <div class="row mt-3">
                        <a href="<?=Yii::$app->homeUrl?>company/type" class="ml-auto btn btn-outline-primary btn-sm ml-auto">ยกเลิก</a>
                        <a href="company-position.php"><button class="btn btn-primary btn-sm mr-3 ml-2">บันทึก</button></a>
                    </div>

                </div><!-- end card body-->
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>