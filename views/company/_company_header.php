
<div class="row mb-1">
    <div class="col-12">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link <?=($isActive=='company')?'active':null?> " href="<?=Yii::$app->homeUrl?>company">รายละเอียดบริษัท</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?=($isActive=='department')?'active':null?>" href="<?=Yii::$app->homeUrl?>company/department">แผนก</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?=($isActive=='position')?'active':null?>" href="<?=Yii::$app->homeUrl?>company/position">ตำแหน่ง</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?=($isActive=='type')?'active':null?>" href="<?=Yii::$app->homeUrl?>company/type">ประเภท</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?=($isActive=='agency')?'active':null?>" href="<?=Yii::$app->homeUrl?>company/agency">เอเจนซี่</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?=($isActive=='slip')?'active':null?>" href="<?=Yii::$app->homeUrl?>company/slip">สลิปเงินเดือน</a>
            </li>
        </ul>
    </div>
</div>
