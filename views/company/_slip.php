<?php
use yii\bootstrap\ActiveForm;
use yii\web\View;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'company-slip';
$this->params['breadcrumbs'][] = $this->title;

?>

<?php $form = ActiveForm::begin([

    'action' => ['import-slip'],
    'id' => 'importSlip',
    'enableClientScript' => false,
    'method' => 'POST',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'options' =>  ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-4',
        ],
        'options' =>  ['class' => 'form-group row']
    ],
]); ?>
<div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">สลิปเงินเดือน</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">สลิปเงินเดือน</p>
            </div>
        </div>
    </div>
</div>
<?=$this->render('_company_header',[
    'isActive' => 'slip'
])?>

<div class="row">
    <div class="col-12">
        <div class="row ">
            <div class="col-2 ml-auto"></div>
            <div class="col-2"></div>
        </div>


            <div class="form-row align-items-center mb-4">
                <div class="col-sm-3 my-1-">
                    <span>ค้นหา</span>
                    <input type="text" class="form-control global_filter" id="global_filter" placeholder="คำที่ค้นหา">
                </div>






                <div class="col-auto my-1 ml-auto" style="width: 350px;">
                    <span>ค้นหาจากช่วงเวลา</span>
                    <div class="input-group input-daterange">
                        <input type="text" id="min-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น" >
                        <div class="input-group-addon mr-2 ml-2"> ถึง </div>
                        <input type="text" id="max-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="สิ้นสุด" >
                        <button type="submit" id="search" class="btn btn-primary btn-sm ml-2">ค้นหา</button>
                    </div>
                </div>
            </div>




    </div>
</div>

<div class="row mb-4">
    <div class="col-12">
        <div class="card" style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>" >
            <div class="card-body">
              
                <span class="mb-4 h5">เลือกไฟล์ที่ต้องการ</span> l <a href="<?=Yii::$app->homeUrl?>/shared/slip/template/template_pay_slip.xlsx" target="blank" class="text-sm">ดาวน์โหลดไฟล์ตัวอย่าง</a><br>
     
                <div class="input-group mb-3">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="PaySlip[file]" id="inputGroupFile02" onchange="uploadFile()">
                        <input type="hidden" name="PaySlip[file]" value="">
                        <label class="custom-file-label" id="filename" for="inputGroupFile02">Choose file</label>
                    </div>
                    <div class="input-group-append">
                        <button class="input-group-text" id="btn-upload" onclick="importSlip()">Upload</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                    <div class="row pr-3 mb-2">
                      <span class="h5 ml-3 mt-2"><?=$modelRows?> รายการ</span>

                       <button style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>"  class="btn btn-primary btn-sm mr-2 ml-auto" onclick="addSlipAll()">บันทึกทั้งหมด</button>
                       <button style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>"  class="btn btn-outline-primary btn-sm ">ยกเลิก</button>
                  </div>


                <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                     <tr>
                        <th>ชื่อ-นามสกุล</th>
                        <th>รหัส</th>
                        <th>หน่วยงาน</th>
                        <th>เลขบัญชี</th>
                        <th>เงินเดือน</th>
                        <th>เบี้ยขยัน</th>
                        <th>ค่าโทรศัพท์</th>
                        <th>รายได้อื่นๆ</th>
                        <th>เงินได้รวม</th>
                        <th>ประกันสังคม</th>
                        <th>ประกันสังคมสะสม</th>
                        <th>เงินหัก</th>
                        <th>เงินได้สุทธิ</th>
                        <th>รายได้สะสม</th>
                        <th>ภาษีสะสม</th>
                        <th>เงินกองทุน</th>
                        <th>จำนวนวัน</th>
                        <th>วันที่จ่าย</th>
                        <th>ประจำเดือน</th>
                        <th>ดำเนินการ</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($model)):?>
                            <?php foreach ($model as $key => $item):?>
                       
                           <td><?=$item->name?></td>
                           <td><?=$item->member_no?></td>
                         
                           <td><?=$item->department?></td>
                           <td><?=$item->bank_no?></td>
                           <td><?=$item->salary?></td>
                           <td><?=$item->diligent_allowance?></td>
                           <td><?=$item->phone_allowance?></td>
                           <td><?=$item->other_allowance?></td>
                           <td><?=$item->total_salary?></td>
                           <td><?=$item->social_security?></td>
                           <td><?=$item->cumulative_social_security?></td>
                           <td><?=$item->deduction?></td>
                           <td><?=$item->net_income?></td>
                           <td><?=$item->net_total?></td>
                           <td><?=$item->tax_income?></td>
                           <td><?=$item->funds_income?></td>
                           <td><?=$item->total_days?></td>
                           <td><?=$item->pay_date?></td>
                           <td><?=$item->month_expend?></td>
                           <td>
                                <button class="btn btn-primary btn-sm ml-2" onclick="addSlip(<?=$item->id?>)">บันทึก</button>
                                <button class="btn btn-outline-primary btn-sm " onclick="delSlip(<?=$item->id?>)">ลบ</button>
                            </td>

                          
                          </td>

                        </tr>
                    <?php endforeach; ?>
                    <?php endif;?>
                 


                    </tbody>
                </table>
                <div class="row pr-3">
                    <button class="btn btn-primary btn-sm mr-2 ml-auto" onclick="addSlipAll()" >บันทึกทั้งหมด</button>
                    <button class="btn btn-outline-primary btn-sm ">ยกเลิก</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<!-- Modal -->
<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>รายละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>
            <!--div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div-->
        </div>

    </div>
</div>

<?php
$script = <<< JS
  function filterGlobal () {
    $('#dataTable').DataTable().search(
        $('#global_filter').val()

       ).draw();
    }
 
    
       var columnsToSearch = {
        /*8: 'แผนก',
        9: 'ตำแหน่ง',
        10: 'ประเภท',
        11: 'ประเภทย่อย',
        12: 'สัญญาจ้าง',
        14: 'พื้นที่',
        7: 'เอเจนซี่',
        15: 'เขตพื้นที่',
        16: 'ช่องทาง',
        17: 'ห้างร้าน',
        22: 'สถานะ'*/
    };

    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging:   false,
        ordering: false,
        info:     false,
        scrollY: 500,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: false,
        fixedHeader: true,
        initComplete: function () {
            for (var i in columnsToSearch) {
                var api = this.api();
                var select = $('<select class="form-control"><option value="">Select</option></select>')
                    .appendTo($('#' + columnsToSearch[i]).empty().text(columnsToSearch[i] + ' '))

                    .attr('data-col', i)
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        api.column($(this).attr('data-col'))
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                api.column(i).data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>');
                });
            }
        }

    });

var colum = ["ชื่อ-นามสกุล", "รหัส", "หน่วยงาน","เลขบัญชี", "เงินเดือน", "เบี้ยขยัน","ค่าโทรศัพท์", "รายได้อื่นๆ", "เงินได้รวม","ประกันสังคม","ประกันสังคมสะสม","เงินหัก","เงินได้สุทธิ","รายได้สะสม","ภาษีสะสม","เงินกองทุน","จำนวนวัน","วันที่จ่าย","ประจำเดือน","ดำเนินการ"];


   //Edit row buttons
  $('.dt-edit').each(function () {
    $(this).on('click', function(evt){
      _this = $(this);
      var dtRow = _this;
      $('div.modal-body').innerHTML='';
      $('div.modal-body').append( '<div class="row mb-4 h3">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=1; i < dtRow[0].cells.length; i++){
         $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">'+colum[i]+'</span><span class="col-8">'+dtRow[0].cells[i].innerHTML+'</span><div/>');
            console.log(i);
      }
      $('#myModal').modal('show');
    });
  });
  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .modal-body').empty();
  });
    $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
       var min = $('#min-date').val();
       var max = $('#max-date').val();
       var createdAt = data[2] || 0; 
        if (
           (min == "" || max == "") ||
           (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
         ) {
            return true;
        }
            return false;
        }
    );

    $('.date-range-filter').change(function() {
      table.draw();
    });
    $('#my-table_filter').hide();

    $('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );




JS;
$script2 = <<< JS
function uploadFile()
{
      var filename =$('input[type=file]')[0].files[0].name;
      $('#filename').html(filename)
      $('#btn-upload').removeClass('input-group-text').addClass('btn btn-primary btn-sm');
      
     
}

function importSlip() {
   $('#importSlip').submit()
}

function delSlip(id){
    let isText=confirm('ยืนยันการลบ'); 
    if(isText)
    {
        $.ajax({
            url: './del-slip',
            type: 'post',
            data: {id:id,_csrf:yii.getCsrfToken()},
            success: function (res) {
              alert(res.message)
            location.reload();
            
            }
        });

    }
}

function addSlip(id){
  
        $.ajax({
            url: './add-slip',
            type: 'post',
            data: {id:id,_csrf:yii.getCsrfToken()},
            success: function (res) {
              alert(res.message)
            location.reload();
            
            }
        });
}

function addSlipAll(){
  
        $.ajax({
            url: './add-slipall',
            type: 'post',
            data: {_csrf:yii.getCsrfToken()},
            success: function (res) {
              alert(res.message)
            location.reload();
            
            }
        });
}
JS;
$this->registerJs($script2, View::POS_HEAD);
$this->registerJs($script, View::POS_READY);
?>