<?php
use yii\bootstrap\ActiveForm;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'company-position';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php $form = ActiveForm::begin([

    'action' => ['position-add?id='.$model->id],
    'id' => 'formCompany',
    'enableClientScript' => false,
    'method' => 'POST',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-4',
        ],
        'options' =>  ['class' => 'form-group row','enctype' => 'multipart/form-data']
    ],
]); ?>
<div class="main-content-inner mb-5">
    <div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->

        <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
            <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">เพิ่มตำแหน่ง</h4>
                <div class="d-none d-md-flex mt-1">
                    <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                    <i class="mdi mdi-chevron-right text-muted"></i>
                    <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                    <i class="mdi mdi-chevron-right text-muted"></i>
                    <p class="text-muted mb-0 tx-13 cursor-pointer">เพิ่มตำแหน่ง</p>
                </div>
            </div>
            <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">

                <a href="<?=Yii::$app->homeUrl?>company/position"  class="ml-auto btn btn-outline-primary btn-sm  ml-auto">ยกเลิก</a>
               <button class="btn btn-primary btn-sm mr-3  ml-2" type="submit"  onclick="alert('บันทึกข้อมูล')">บันทึก</button>

            </div>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-12">
            <div class="card">

                <div class="card-body ">
                    <div class="row">
                        <div class="col-12">

                                <div class="form-row">

                                    <div class="col-12 mb-3">
                                        <label for="validationCustom01">ชื่อตำแหน่ง</label>
                                        <input type="text" class="form-control" id="validationCustom01" name="position_name" value="<?=$model->position_name?>"  placeholder="TBA"  required="">
                                    </div>

                                    <div class="col-12 mb-3">
                                        <label for="validationCustom01">หมายเหตุ</label>
                                        <input type="text" class="form-control" id="validationCustom01" placeholder=""  value="<?=$model->remark?>" name="remark" required="">
                                    </div>

                                    <div class="col-12 mb-4">
                                        <label for="validationCustom02">กำหนดระยะเวลาสูงสุดในการคงอยู่ในระบบ</label>
                                        <select class="form-control" name="life_cycle">
                                            <option  value="30" <?=$model->life_cycle == 30?'selected':null?> >30</option>
                                            <option value="40" <?=$model->life_cycle == 40?'selected':null?> >40</option>
                                            <option value="50" <?=$model->life_cycle == 50?'selected':null?> >50</option>
                                        </select>
                                    </div>
                                    <div class="cols-6 mb-3 mr-3">
                                        <div class="checkbox icheck-peterriver">
                                            <input type="checkbox" name="auth_member_role" value="1"  <?=$model->auth_member_role == 1?'checked':null?> >
                                            <label >เป็นผู้ดูแลพนักงาน</label>
                                        </div>
                                    </div>
                                    <div class="cols-6 mb-3">
                                        <div class="checkbox icheck-peterriver">
                                            <input type="checkbox"  name="auth_backend_role" value="1"  <?=$model->auth_backend_role == 1?'checked':null?> >
                                            <label >มีสิทธิ์ใช้งานระบบหลังบ้าน (Back-office)</label>
                                        </div>
                                    </div>
                                </div>

                        </div>
                    </div>


                    <div class="row mt-5">
                        <div class="col-12">

                            <h4>สิทธิ์ในการเข้าถึงข้อมูล</h4><br>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>รายละเอียด</th>
                                    <th class="bg-success text-white text-center" style="width: 100px;">จัดการได้</th>
                                    <th class="bg-warning text-white text-center" style="width: 100px;">แสดงเท่านั้น</th>
                                    <th class="bg-danger text-white text-center" style="width: 100px;">ไม่แสดง</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <?php
                                    echo $model->auth_company;
                                    ?>
                                    <td>1</td>
                                    <td>ตั้งค่าบริษัท</td>
                                    <td class=" text-center">
                                        <div class="radio icheck-greensea">
                                            <input type="radio" id="greensea3" value="1" <?=$model->auth_company == 1?'checked':null?> name="auth_company" />
                                            <label for="greensea3"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-orange">
                                            <input type="radio" id="greensea1" value="2" <?=$model->auth_company == 2?'checked':null?> name="auth_company" />
                                            <label for="greensea1"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-alizarin">
                                            <input type="radio"  id="greensea2" value="3"  <?=empty($model->auth_company)?'checked':null?> <?=$model->auth_company == 3?'checked':null?> name="auth_company" />
                                            <label for="greensea2"></label>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>2</td>
                                    <td>พนักงานในระบบ</td>
                                    <td class=" text-center">
                                        <div class="radio icheck-greensea">
                                            <input type="radio" id="che1" value="1" name="auth_member" <?=$model->auth_member == 1?'checked':null?> />
                                            <label for="che1"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-orange">
                                            <input type="radio" id="che2" value="2" name="auth_member" <?=$model->auth_member == 2?'checked':null?> />
                                            <label for="che2"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-alizarin">
                                            <input type="radio"  id="che3" value="3" name="auth_member" <?=empty($model->auth_member)?'checked':null?> <?=$model->auth_member == 3?'checked':null?> />
                                            <label for="che3"></label>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>3</td>
                                    <td>เพิ่มพนักงานในระบบ</td>
                                    <td class=" text-center">
                                        <div class="radio icheck-greensea">
                                            <input type="radio" id="31" value="1" name="auth_add_member"  <?=$model->auth_add_member == 1?'checked':null?>/>
                                            <label for="31"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-orange">
                                            <input type="radio" id="32" value="2" disabled name="auth_add_member"  <?=$model->auth_add_member == 2?'checked':null?>/>
                                            <label for="32"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-alizarin">
                                            <input type="radio"  id="33"  value="3" name="auth_add_member" <?=empty($model->auth_add_member)?'checked':null?>   <?=$model->auth_add_member == 3?'checked':null?>/>
                                            <label for="33"></label>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>4</td>
                                    <td>นำเข้าข้อมูลพนักงาน</td>
                                    <td class=" text-center">
                                        <div class="radio icheck-greensea">
                                            <input type="radio" id="41" value="1" name="auth_import_member"  <?=$model->auth_import_member == 1?'checked':null?> />
                                            <label for="41"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-orange">
                                            <input type="radio" id="42" value="2" disabled name="auth_import_member"  <?=$model->auth_import_member == 2?'checked':null?> />
                                            <label for="42"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-alizarin">
                                            <input type="radio"  id="43" value="3" name="auth_import_member"  <?=empty($model->auth_import_member)?'checked':null?>  <?=$model->auth_import_member == 3?'checked':null?> />
                                            <label for="43"></label>
                                        </div>
                                    </td>
                                </tr>



                                <tr>
                                    <td>5</td>
                                    <td>อนุมัติรายการ</td>
                                    <td class=" text-center">
                                        <div class="radio icheck-greensea">
                                            <input type="radio" id="71" value="1" name="auth_approve_order" <?=$model->auth_approve_order == 1?'checked':null?> />
                                            <label for="71"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-orange">
                                            <input type="radio" id="72" value="2" name="auth_approve_order" <?=$model->auth_approve_order == 2?'checked':null?> />
                                            <label for="72"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-alizarin">
                                            <input type="radio"  id="73" value="3" name="auth_approve_order" <?=empty($model->auth_approve_order)?'checked':null?> <?=$model->auth_approve_order == 3?'checked':null?> />
                                            <label for="73"></label>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>6</td>
                                    <td>ประวัติบันทึกเวลา</td>
                                    <td class=" text-center">
                                        <div class="radio icheck-greensea">
                                            <input type="radio" id="81" value="1" name="auth_record_time" <?=$model->auth_record_time == 1?'checked':null?> />
                                            <label for="81"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-orange">
                                            <input type="radio" id="82" value="2" disabled name="auth_record_time" <?=$model->auth_record_time == 2?'checked':null?> />
                                            <label for="82"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-alizarin">
                                            <input type="radio"  id="83" value="3"  name="auth_record_time" <?=empty($model->auth_record_time)?'checked':null?>   <?=$model->auth_record_time == 3?'checked':null?> />
                                            <label for="83"></label>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>7</td>
                                    <td>ประวัติบันทึกการลา</td>
                                    <td class=" text-center">
                                        <div class="radio icheck-greensea">
                                            <input type="radio" id="91" value="1"  name="auth_record_leave" <?=$model->auth_record_leave == 1?'checked':null?> />
                                            <label for="91"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-orange">
                                            <input type="radio" id="92" value="2" disabled name="auth_record_leave" <?=$model->auth_record_leave == 2?'checked':null?> />
                                            <label for="92"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-alizarin">
                                            <input type="radio"  id="93" value="3"  name="auth_record_leave" <?=empty($model->auth_record_leave)?'checked':null?> <?=$model->auth_record_leave == 3?'checked':null?> />
                                            <label for="93"></label>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>8</td>
                                    <td>ประวัติปรับปรุงเวลา</td>
                                    <td class=" text-center">
                                        <div class="radio icheck-greensea">
                                            <input type="radio" id="101" value="1"  name="auth_update_time" <?=$model->auth_update_time == 1?'checked':null?> />
                                            <label for="101"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-orange">
                                            <input type="radio" id="102" value="2" disabled name="auth_update_time" <?=$model->auth_update_time == 2?'checked':null?> />
                                            <label for="102"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-alizarin">
                                            <input type="radio"  id="103" value="3"  name="auth_update_time"  <?=empty($model->auth_update_time)?'checked':null?> <?=$model->auth_update_time == 3?'checked':null?> />
                                            <label for="103"></label>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>9</td>
                                    <td>ประวัติทำงานล่วงเวลา</td>
                                    <td class=" text-center">
                                        <div class="radio icheck-greensea">
                                            <input type="radio" id="111" value="1" name="auth_record_ot" <?=$model->auth_record_ot == 1?'checked':null?> />
                                            <label for="111"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-orange">
                                            <input type="radio" id="112" value="2" disabled name="auth_record_ot" <?=$model->auth_record_ot == 2?'checked':null?> />
                                            <label for="112"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-alizarin">
                                            <input type="radio"  id="113" value="3"   name="auth_record_ot"  <?=empty($model->auth_record_ot)?'checked':null?> <?=$model->auth_record_ot == 3?'checked':null?> />
                                            <label for="113"></label>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>10</td>
                                    <td>กำหนดพิกัดสถานที่</td>
                                    <td class=" text-center">
                                        <div class="radio icheck-greensea">
                                            <input type="radio" id="121" value="1" name="auth_location"  <?=$model->auth_location == 1?'checked':null?>/>
                                            <label for="121"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-orange">
                                            <input type="radio" id="122" value="2" name="auth_location"  <?=$model->auth_location == 2?'checked':null?> />
                                            <label for="122"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-alizarin">
                                            <input type="radio"  id="123" value="3" name="auth_location"  <?=empty($model->auth_location)?'checked':null?> <?=$model->auth_location == 3?'checked':null?> />
                                            <label for="123"></label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>11</td>
                                    <td>ข่าวสาร</td>
                                    <td class=" text-center">
                                        <div class="radio icheck-greensea">
                                            <input type="radio" id="121" value="1" name="auth_news"  <?=$model->auth_news == 1?'checked':null?>/>
                                            <label for="121"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-orange">
                                            <input type="radio" id="122" value="2" disabled name="auth_news"  <?=$model->auth_news == 2?'checked':null?> />
                                            <label for="122"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-alizarin">
                                            <input type="radio"  id="123" value="3" name="auth_news"  <?=empty($model->auth_news)?'checked':null?> <?=$model->auth_news == 3?'checked':null?> />
                                            <label for="123"></label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>12</td>
                                    <td>Export ข้อมูล</td>
                                    <td class=" text-center">
                                        <div class="radio icheck-greensea">
                                            <input type="radio" id="121" value="1" name="auth_export"  <?=$model->auth_export == 1?'checked':null?>/>
                                            <label for="121"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-orange">
                                            <input type="radio" id="122" value="2" disabled name="auth_export"  <?=$model->auth_export == 2?'checked':null?> />
                                            <label for="122"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio icheck-alizarin">
                                            <input type="radio"  id="123" value="3" name="auth_export"  <?=empty($model->auth_export)?'checked':null?> <?=$model->auth_export == 3?'checked':null?> />
                                            <label for="123"></label>
                                        </div>
                                    </td>
                                </tr>



                                </tbody>

                            </table>

                        </div>
                    </div>


                    <div class="row mt-3">
                        <a href="<?=Yii::$app->homeUrl?>company/position" class="ml-auto btn btn-outline-primary btn-sm ml-auto">ยกเลิก</a>
                        <button class="btn btn-primary btn-sm mr-3 ml-2" type="submit" onclick="alert('บันทึกข้อมูล')">บันทึก</button>
                    </div>
                </div>



            </div><!-- end card body-->
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php
$script = <<< JS
(function() {

    // for toggle related demo
    function toggleSub(box, id) {
        // get reference to related content to display/hide
        var el = document.getElementById(id);

        if ( box.checked ) {
            el.style.display = 'block';
        } else {
            el.style.display = 'none';
        }
    }
    var active2 = document.getElementById('addon2');
    active2.checked = false; // for soft reload

    var active = document.getElementById('addon');
    active.checked = false; // for soft reload

    // attach function that calls toggleSub to onclick property of checkbox
    // toggleSub args: checkbox clicked on (this), id of element to show/hide
    active.onclick = function() { toggleSub(this, 'active_sub'); toggleSub(this, 'active_sub2'); };

    active2.onclick = function() { toggleSub(this, 'active_sub_2');  };



    // disable submission of all forms on this page
    for (var i=0, len=document.forms.length; i<len; i++) {
        document.forms[i].onsubmit = function() { return false; };
    }

}());
JS;
$this->registerJS($script,View::POS_READY);
?>