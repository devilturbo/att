<?php
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'company-type';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">ประเภท</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">ประเภท</p>
            </div>
        </div>
      
    </div>
</div>

<?=$this->render('_company_header',[
    'isActive' => 'type'
])?>





<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <span class="h5 ml-3">ประเภทพนักงาน(<?=$modelRows?>)</span>
                    <a style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>"  href="<?=Yii::$app->homeUrl?>company/add-type" class="ml-auto"><button class="btn btn-outline-primary btn-sm  mb-2 mr-2">เพิ่มประเภทพนักงาน</button></a>
                </div>

                <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                    <tr>
                        <th>ลำดับ</th>
                        <th>ตำแหน่ง</th>
                        <th>หมายเหตุ</th>
                        <th>เวลาทำงาน</th>
                        <th>รูปแบบการเข้าทำงาน</th>

                        <th>วันที่เริ่มได้สิทธิ์</th>
                        <th>สะสมวันลา</th>
                        <th>วันหยุด</th>

                        <th width="100px;" style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>" >ดำเนินการ</th>
                    </tr>
                    </thead>
                    <tbody>
                <?php if(!empty($model)):?>
                    <?php $i=0;?>
                    <?php foreach ($model as $key => $item): ?>
                    <tr class="dt-edit">
                        <td><?=++$i?></td>
                        <td><?=$item->type_name?></td>
                        <td><?=$item->remark?></td>
                        <th><?=$item->time_attendance?></th>
                        <th><?=$item->attendance_type_name?></th>

                        <th><?=$item->leave_type_name?></th>
                        <th><?=$item->leave_year_name?></th>

                        <th>บังคับ</th>
                        <td style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>" >
                            <a href="<?=Yii::$app->homeUrl?>company/add-type?id=<?=$item->id?>" class="btn btn-outline-primary btn-sm ">แก้ไข</a>
                            <button class="btn btn-outline-primary btn-sm " onclick="delType(<?=$item->id?>)" >ลบ</button>
                        </td>
                    </tr>
                   <?php endforeach;?>    
                <?php endif;?>    
                    </tbody>
                </table>
                <!--div class="row pr-3">
                     <button class="btn btn-primary btn-sm mr-2 ml-auto">บันทึกทั้งหมด</button>
                     <button class="btn btn-outline-primary btn-sm ">ยกเลิก</button>
                </div-->
            </div>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <span class="h5 ml-3">ประเภทพนักงานกลุ่มย่อย(<?=$modelSubRows?>)</span>
                    <a style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>"  href="<?=Yii::$app->homeUrl?>company/add-subtype" class="ml-auto"><button class="btn btn-outline-primary btn-sm ml-auto mb-2 mr-2">เพิ่มกลุ่มย่อย</button></a>
                </div>

                <table id="dataTable2" class="table " cellspacing="0">
                    <thead>
                    <tr>
                        <th>ลำดับ</th>
                        <th>ตำแหน่ง</th>
                        <th>กลุ่มย่อย</th>
                        <th>หมายเหตุ</th>
                        <th>เวลาทำงาน</th>
                        <th>รูปแบบการเข้าทำงาน</th>

                        <th>วันที่เริ่มได้สิทธิ์</th>
                        <th>สะสมวันลา</th>
                        <th>วันหยุด</th>

                        <th width="100px;" style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>" >ดำเนินการ</th>
                    </tr>
                    </thead>
                    <tbody>
                     <?php if(!empty($modelSubRows)):?>
                    <?php $i=0;?>
                    <?php foreach ($modelSubType as $key => $item): ?>
                    <tr class="dt-edit">
                        <td><?=++$i?></td>
                        <td><?=$item->type_name?></td>
                        <td><?=$item->subtype_name?></td>
                        <td><?=$item->remark?></td>
                        <th><?=$item->time_attendance?></th>
                        <th><?=$item->attendance_type_name?></th>

                        <th><?=$item->leave_type_name?></th>
                        <th><?=$item->leave_year_name?></th>

                        <th>บังคับ</th>
                        <td style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>" >
                            <a href="<?=Yii::$app->homeUrl?>company/add-subtype?id=<?=$item->id?>" class="btn btn-outline-primary btn-sm ">แก้ไข</a>
                            <button class="btn btn-outline-primary btn-sm " onclick="delType(<?=$item->id?>)" >ลบ</button>
                        </td>
                    </tr>
                   <?php endforeach;?>    
                <?php endif;?>    

                    </tbody>
                </table>
                <!--div class="row pr-3">
                     <button class="btn btn-primary btn-sm mr-2 ml-auto">บันทึกทั้งหมด</button>
                     <button class="btn btn-outline-primary btn-sm ">ยกเลิก</button>
                </div-->
            </div>
        </div>
    </div>
</div>
<div id="myModaldelete" class="modal" role="dialog">
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>ลบข้อมูล</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="row p-4 text-center">
                <h2 class="text-danger">ยืนยันการลบข้อมูล</h2>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">ยืนยัน</button>
                <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">ยกเลิก</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>รายละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>
            <!--div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div-->
        </div>

    </div>
</div>

<?php
$script = <<< JS
$('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging:   false,
        ordering: false,
        info:     false,
        scrollY: 300,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: false,
        fixedHeader: true

    });
    table2 = $('#dataTable2').DataTable({
        dom: 'it',
        searching: true,
        paging:   false,
        ordering: false,
        info:     false,
        scrollY: 300,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: false,
        fixedHeader: true

    });




var colum = ["ลำดับ","ตำแหน่ง","หมายเหตุ","เวลาทำงาน","รูปแบบการเข้าทำงาน","การลาขั้นต่ำ","วันที่เริ่มได้สิทธิ์","สะสมวันลา","วันหยุด","ดำเนินการ","ดำเนินการ"];
var colum2 = ["ลำดับ","ตำแหน่ง","กลุ่มย่อย","หมายเหตุ","เวลาทำงาน","รูปแบบการเข้าทำงาน","การลาขั้นต่ำ","วันที่เริ่มได้สิทธิ์","สะสมวันลา","วันหยุด","ดำเนินการ","ดำเนินการ"];



   //Edit row buttons
  $('.dt-edit').each(function () {
    $(this).on('click', function(evt){
      _this = $(this);
      var dtRow = _this;
      $('div.modal-body').innerHTML='';
      for(var i=0; i < dtRow[0].cells.length; i++){
         $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">'+colum[i]+'</span><span class="col-8">'+dtRow[0].cells[i].innerHTML+'</span><div/>');
            console.log(i);
      }
      $('#myModal').modal('show');
    });
  });
  $('.dt-edit-2').each(function () {
    $(this).on('click', function(evt){
      _this = $(this);
      var dtRow = _this;
      $('div.modal-body').innerHTML='';
      for(var i=0; i < dtRow[0].cells.length; i++){
         $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">'+colum2[i]+'</span><span class="col-8">'+dtRow[0].cells[i].innerHTML+'</span><div/>');
            console.log(i);
      }
      $('#myModal').modal('show');
    });
  });
  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .modal-body').empty();
  });
JS;

$script2 = <<< JS
 
    function delType(id){
        var txt=confirm('ยืนยันการลบข้อมูล');
        if (txt) {
             
            $.ajax({
            url: './del-type',
            type: 'post',
            data: {id:id,_csrf:yii.getCsrfToken()},
            success: function (res) {
              alert(res.message)
              location.reload();
            
            }
        });

        } else {
             return false;
        }

    }

JS;

$this->registerJS($script,View::POS_READY);
$this->registerJS($script2,View::POS_HEAD);
?>