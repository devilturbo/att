<?php
use yii\bootstrap\ActiveForm;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'company-add-type';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php $form = ActiveForm::begin([

    'action' => ['add-subtype?id='.$model->id],
    'id' => 'formCompany',
    'enableClientScript' => false,
    'method' => 'POST',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-4',
        ],
        'options' =>  ['class' => 'form-group row']
    ],
]); ?>
    <div class="main-content-inner mb-5">
    <div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->

        <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
            <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">เพิ่มตำแหน่ง</h4>
                <div class="d-none d-md-flex mt-1">
                    <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                    <i class="mdi mdi-chevron-right text-muted"></i>
                    <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                    <i class="mdi mdi-chevron-right text-muted"></i>
                    <p class="text-muted mb-0 tx-13 cursor-pointer">เพิ่มตำแหน่ง</p>
                </div>
            </div>
            <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">

               <a href="<?=Yii::$app->homeUrl?>company/type" class="ml-auto btn btn-outline-primary btn-sm ml-auto">ยกเลิก</a>
                <a style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>"  href="company-position.php"><button class="btn btn-primary btn-sm mr-3  ml-2">บันทึก</button></a>

            </div>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row mt-2 ">
                        <div class="col-8"><h4 class="text-header">เพิ่มประเภทพนักงาน SubType</h4></div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-12">

                            <div class="form-row">

                                <div class="col-12 mb-3">
                                    <label for="validationCustom01">อยู่ในประเภทพนักงาน Type</label>
                                   <select data-placeholder="Select" name="type_id" class="chosen-select-deselect" onchange="getMembertype(this.value)" tabindex="">
                                <?php
                                $arrType  = Yii::$app->Utilities->getListType();
                                ?>
                                <?php foreach ($arrType as $key => $item): ?>
                                    <?php
                                        $select = '';
                                        if($key == $model->type_id){
                                            $select = 'selected';
                                        }
                                    ?>
                                    <option <?=$select?> value="<?=$key?>">
                                        <?=$item?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                                </div>
                                <div class="col-12 mb-3">
                                    <label for="validationCustom01">ชื่อประเภทพนักงานกลุ่มย่อย Subtype</label>
                                    <input type="text" class="form-control" id="validationCustom01" placeholder="TBA" name="subtype_name" value="<?=$model->subtype_name?>" required="">
                                </div>

                                <div class="col-12 mb-3">
                                    <label for="validationCustom01">หมายเหตุ</label>
                                    <input type="textbox" class="form-control" id="validationCustom01" placeholder="" name="remark" value="<?=$model->remark?>" required="">
                                </div>


                            </div>

                            <?php
                            echo  $this->render('_member_condition', [
                                'model' => $model,
                            ]);
                            ?>
                        </div>
                    </div>





                    <div class="row mt-3">
                        <a href="<?=Yii::$app->homeUrl?>company/type" class="ml-auto btn btn-outline-primary btn-sm ml-auto">ยกเลิก</a>
                        <a href="company-position.php"><button class="btn btn-primary btn-sm mr-3 ml-2">บันทึก</button></a>
                    </div>

                </div><!-- end card body-->
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php
$script2 = <<< JS
function getMembertype(id){
    if(!id){
        return false;
    }
    $.ajax({
            url: './get-membertype',
            type: 'post',
            data: {id:id,_csrf:yii.getCsrfToken()},
            success: function (res) {
                $('input[name="time_attendance"]').val(res.time_attendance)
                $("input[name=attendance_type][value='\${res.res.attendance_type}']").prop("checked",true);
                $("input[name=attendance_type][value='\${res.attendance_type}']").prop("checked",true);
                $("input[name=ba][value='\${res.ba}']").prop("checked",true);
                $("input[name=leave_codition][value='\${res.leave_codition}']").prop("checked",true);
                $("input[name=leave_type][value='\${res.leave_type}']").prop("checked",true);
                $("input[name=leave_year][value='\${res.leave_year}']").prop("checked",true);
                $("input[name=monday][value='\${res.monday}']").prop("checked",true);
                $("input[name=thesday][value='\${res.thesday}']").prop("checked",true);
                $("input[name=wednesday][value='\${res.wednesday}']").prop("checked",true);
                $("input[name=thursday][value='\${res.thursday}']").prop("checked",true);
                $("input[name=friday][value='\${res.friday}']").prop("checked",true);
                $("input[name=saturday][value='\${res.saturday}']").prop("checked",true);
                $("input[name=sunday][value='\${res.sunday}']").prop("checked",true);
                $("input[name=substitution_holiday][value='\${res.substitution_holiday}']").prop("checked",true);
                $("input[name=location_type][value='\${res.location_type}']").prop("checked",true);
        
            
            }
        });
}

JS;
$this->registerJs($script2,View::POS_HEAD);
?>