<?php
use yii\bootstrap\ActiveForm;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'company-department';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">แผนก</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">แผนก</p>
            </div>
        </div>
        <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0" id='btn_export'>
            
        </div>
    </div>
</div>

<?=$this->render('_company_header',[
    'isActive' => 'department'
])?>





<input type="hidden" id="actionDep" value="">

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <span class="h5 ml-3 mt-2">แผนก(<?=$modelRows?>)</span>
                    <button   style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>" class="btn btn-outline-primary btn-sm ml-auto  mb-2 mr-2" data-toggle="modal" onclick="$('#actionDep').val('')" data-target="#myModalAdd" >เพิ่มแผนก</button>
                </div>

                <table id="dataTable" class="table  " cellspacing="0">
                    <thead>
                    <tr>
                        <th>ลำดับ</th>
                        <th>แผนก</th>
                        <th>รายละเอียด</th>
                        <th>หมายเหตุ </th>
                        <th width="100px;">ดำเนินการ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($model)):?>
                    <?php $i=0;?>
                    <?php foreach ($model as $key => $item):?>
                    <tr class="dt-edit" onclick="$('#actionDep').val(<?=$item->id?>)">
                        <td><?=++$i?></td>
                        <td><?=$item->department_name?></td>
                        <td><?=$item->detail?></td>
                        <td><?=$item->remark?></td>
                        <td >
                            <button style="display: <?=Yii::$app->user->identity->auth_company ==2?'none':''?>"  class="btn btn-outline-primary btn-sm ml-2 " onclick="$('#actionDep').val(<?=$item->id?>)">รายละเอียด</button>
                        </td>
                    </tr>
                    <?php endforeach;?>
                    <?php endif;?>




                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="myModalAdd" class="modal" role="dialog">
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span id='ele_dep'>เพิ่มแผนก</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="row p-4">
                <form class="forms-sample col-12" id="form_department">

                    <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-4 col-form-label">แผนก</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="department" id="department" placeholder="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-4 col-form-label">รายละเอียด </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="department_detail" id="department_detail" placeholder="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-4 col-form-label">หมายเหตุ </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="department_remark" id="department_remark" placeholder="">
                        </div>
                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" onclick="saveDep()">บันทึก</button>
                <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">ยกเลิก</button>
            </div>
        </div>
    </div>
</div>

<!-- ลบข้ำมูล -->
<div id="myModaldelete" class="modal" role="dialog">
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>ลบข้อมูล</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="row p-4 text-center">
                <h2 class="text-danger">ยืนยันการลบข้อมูล</h2>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" onclick="delDep()">ยืนยัน</button>
                <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">ยกเลิก</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>รายละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>


            <div class="modal-footer">
                <button class="btn btn-outline-primary btn-sm ml-auto" data-toggle="modal" data-target="#myModalAdd" onclick="getDep()" data-dismiss="modal">แก้ไข</button>
                <button class="btn btn-outline-primary btn-sm " data-toggle="modal" data-target="#myModaldelete" data-dismiss="modal">ลบ</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>

<?php
$a = 1;
$script = <<< JS

    
   

        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });

        table = $('#dataTable').DataTable({
            dom: 'it',
            searching: true,
            paging:   false,
            ordering: false,
            info:     false,
            scrollY: 300,
            scrollX:        true,
            scrollCollapse: false,
            autoWidth:      false,
            fixedColumns:   {
                leftColumns: 1
            },
            columnDefs: [
                { "width": "50px", "targets": [ 0 ] },
            ],
            responsive: false,
            fixedHeader: true

        });


    var buttons = new $.fn.dataTable.Buttons(table, {
       buttons: [
        {
            extend: 'excel',
            text: 'Data Export',
            charset: 'utf-8',
            extension: '.xlsx',
            bom: true,
            className: 'btn btn-outline-primary btn-sm  d-none d-md-block',
        }]}).container().appendTo($('#btn_export'));


        var colum = ["ลำดับ", "แผนก", "รายละเอียด","หมายเหตุ"];


        //Edit row buttons
        $('.dt-edit').each(function () {
           
            $(this).on('click', function(evt){
                _this = $(this);
                var dtRow = _this;
                $('div.modal-body').innerHTML='';
                //$('div.modal-body').append( '<div class="row mb-4 h3">'+dtRow[0].cells[0].innerHTML+'</div>');
                for(var i=0; i < dtRow[0].cells.length-1; i++){
                    $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-4 ">'
                        +colum[i]+'</span><span class="col-6">'
                        +dtRow[0].cells[i].innerHTML
                        +'</span><div/>');
                    console.log(i);
                }

                $('#myModal').modal('show');
            });
        });
        $('#myModal').on('hidden.bs.modal', function (evt) {

            $('.modal .modal-body').empty();
        });



 
JS;
$script2 = <<< JS

  function getDep(){
    let id = $('#actionDep').val();

    if(!department){
        alert('กรุณาระบุ แผนก')
        return false;
    }
    $.ajax({
            url: './get-dep',
            type: 'post',
            data: {id:id,_csrf:yii.getCsrfToken()},
            success: function (res) {
                $('#department').val(res.department_name)
                $('#department_detail').val(res.detail)
                $('#department_remark').val(res.remark)
            
            }
        });
  }

  function delDep(){
    let id = $('#actionDep').val();
   
    $.ajax({
            url: './del-dep',
            type: 'post',
            data: {id:id,_csrf:yii.getCsrfToken()},
            success: function (res) {
              alert(res.message)
            location.reload();
            
            }
        });
  }
  
  function saveDep() {
    let id = $('#actionDep').val();
    let formData = $('#form_department').serialize()
    let department = $('#department').val();
    if(!department){
        alert('กรุณาระบุ แผนก')
        return false;
    }
    $.ajax({
            url: './set-dep',
            type: 'post',
            data: formData +'&'+ "_csrf="+yii.getCsrfToken()+'&'+"el="+id,
            success: function (res) {
             alert(res.message)
            location.reload();
            
            }
        });
  }
JS;
$this->registerJs($script2,View::POS_HEAD);
$this->registerJs($script, View::POS_READY);
?>




