<?php
use yii\bootstrap\ActiveForm;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'company-profile';
$this->params['breadcrumbs'][] = $this->title;
$arrMenu = \app\components\Utilities::setMenu();
$auth = \Yii::$app->user->identity;
?>
<div class="company-index">
  <?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'action' => ['index'],
    'id' => 'formCompany',
    'options' =>  ['enctype' => 'multipart/form-data'],
    'enableClientScript' => false,
    'method' => 'POST',
      'enableAjaxValidation' => false,
      'enableClientValidation' => true,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-4',
        ],
        'options' =>  ['class' => 'form-group row']
    ],
]); ?>

         <?=$this->render('_company_header',[
                 'isActive' => 'company'
         ])?>
          <!--1-->
          <div class="row mb-4">
            <div class="col-12">
              <div class="card">
                <div class="card-body">




                          <?php echo $form->field($model, 'name_th',[    
                                'template' => "{label}\n<div class='col-sm-4'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label', 'style' => 'text-align: left!important;' ]
                                ])->textInput(['placeholder' => "ชื่อบริษัทภาษาไทย" ,'required'=>true]);

                          ?>
                           <?php echo $form->field($model, 'name_en',[    
                                'template' => "{label}\n<div class='col-sm-4'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label', 'style' => 'text-align: left!important;' ]
                                ])->textInput(['placeholder' => "ชื่อบริษัทภาษาอังกฤษ"]) 
                          ?>
                             <div class="form-group row">
                              <label for="exampleInputUsername2" class="col-sm-3 col-form-label">โลโก้บริษัท 
                                  <a href="<?=Yii::$app->request->baseUrl.'/'.$model->img_url?>" target='_blank'>
                                    <img class="thumb mr-2 lazy" src="<?=Yii::$app->request->baseUrl?>/images/user/thumb/img-ico.jpg" align="right" width='15' >
                                  </a>

                              </label>
                              <div class="col-sm-4">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                              <span class="input-group-text">Upload</span>
                                    </div>
                                    <div class="custom-file">
                                      <input type="file" class="custom-file-input"  onchange="uploadFile()" name="Company[img_url]" id="inputGroupFile01">
                                      <input type="hidden" name="Company[img_url]" value="<?=$model->img_url?>">
                                      <label class="custom-file-label" for="inputGroupFile01" id="filename">Choose file โลโก้บริษัท</label>
                                      </div>
                                    </div>
                              </div>
                            </div>
                           
                           <?php echo $form->field($model, 'address',[    
                                'template' => "{label}\n<div class='col-sm-4'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label', 'style' => 'text-align: left!important;' ]
                                ])->textarea(['rows'=>4,'placeholder'=>'ที่อยู่บริษัท','required'=>true])
                          ?>
                          <?php echo $form->field($model, 'tel',[    
                                'template' => "{label}\n<div class='col-sm-4'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label', 'style' => 'text-align: left!important;' ]
                                ])->textInput(['placeholder' => "โทรศัพท์",'required'=>true,'maxlenght'=>true])
                          ?>
                          <?php echo $form->field($model, 'email',[    
                                'template' => "{label}\n<div class='col-sm-4'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label', 'style' => 'text-align: left!important;' ]
                                ])->Input('email',['placeholder'=>'email','maxlenght'=>true]);
                          ?>
                          <?php echo $form->field($model, 'tax_id',[    
                                'template' => "{label}\n<div class='col-sm-4'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label', 'style' => 'text-align: left!important;' ]
                                ])->textInput(['placeholder' => "เลขประจำตัวผู้เสียภาษี",'maxlenght'=>true])
                          ?>
                          <?php echo $form->field($model, 'hr_work',[    
                                'template' => "{label}\n<div class='col-sm-4'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label', 'style' => 'text-align: left!important;' ]
                                ])->textInput(['placeholder' => "8"]) 
                          ?>
                          <?php echo $form->field($model, 'time_attendance',[    
                                'template' => "{label}\n<div class='col-sm-4'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label', 'style' => 'text-align: left!important;' ]
                                ])->textInput(['placeholder' => "8:30",'maxlenght'=>true])
                          ?>
                          <?php echo $form->field($model, 'probation',[    
                                'template' => "{label}\n<div class='col-sm-4 probation'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label ', 'style' => 'text-align: left!important;' ],
                                ])->textInput(['placeholder' => "119วัน",'maxlenght'=>true,'type' => 'number','required'=>true])
                          ?>




                

                    <!--div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0 row">
                
               
                <button class="btn btn-primary btn-sm mr-3 d-none d-md-block ml-auto">บันทึก</button>
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">ยกเลิก</button>
              </div-->

                </div>
              </div>
            </div>
          </div>
           <!--1-->

         <?php echo $this->render('_leave',['form'=>$form,'model'=>$model]); ?>
         <?php if(empty($arrMenu['company_holidays'])) : ?>
          <div class="row mb-4">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <div class="row mb-4"><div class="col-12"><span class="h4 text-bold">วันหยุดประจำปี</span></div></div>
                  <div class="row mb-4">
                    <div class="col-12">


                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                  <label for="validationCustom01">วันที่</label>
                                    <div class="input-group input-daterange">
                                        <input type="text" id="holiday_date" class="form-control" placeholder="วันที่">
                                    </div>
                                </div>

                                <div class="col-md-4 mb-3">
                                  <label for="validationCustom02">รายละเอียด</label>
                                  <input type="text" class="form-control" id="holidays" placeholder="สงกรานต์" value="" >
                                </div>

                                <div class="col-md-4 mb-3">
                                  <label for="validationCustom03">หมายเหตุ</label>
                                  <input type="text" class="form-control" id="holidays_remark" placeholder="บังคับหยุดทุกคน" value="">
                                </div>
                            </div>

                      
                            <div class="row" >
                              <div class="ml-auto mr-3"><a class="btn btn-primary btn-sm ml-auto text-white" onClick="setHoliday()">บันทึกข้อมูล</a></div>
                            </div>

                         
                          </div>





                   

                    
                  </div>

                  <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                    <table class="table" id="holidaysTable">
                      <thead>
                        <tr>
                          <th>วันที่</th>
                          <th>รายละเอียด</th>
                          <th>หมายเหตุ</th>
                          <th width="120px;">ดำเนินการ</th>
                        </tr>
                      </thead>
                      <tbody >
                      <?php if(!empty($modelHoliday)):?>
                      <?php foreach ($modelHoliday as $key => $item):?>
                              <tr id="tr_<?=$item->id?>">
                                  <td><?=$item->days_leave?></td>
                                  <td><?=$item->subject?></td>
                                  <td><?=$item->remark?></td>
                                  <td>
                                  <input  type="button"  class="btn btn-outline-primary btn-sm ml-auto modelbox"   data="<?=$item->id?>" data-toggle="modal" data-target="#myModaldelete" onClick='$("#deleteHoliday").val(<?=$item->id?>)' value='ลบ'>
                                  <input  type="button" class="btn btn-outline-primary btn-sm ml-auto" data-toggle="modal" data-target="#myModaledit" onclick="edit(this);$('#holidays_idedit').val(<?=$item->id?>)"  value='แก้ไข'>
                                  </td>
                              </tr>

                       <?php endforeach;?>
                       <?php endif;?>
                      </tbody>
                    </table>
                  </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
            <input type="hidden" value="" id="holidays_v" name="holidaysModel">
          <div class="row">
            <button onclick="saveCompany()" class="btn btn-primary btn-sm mr-2 ml-auto">บันทึก</button>
            <button class="btn btn-outline-primary btn-sm mr-3">ยกเลิก</button>
          </div>
        <?php endif; ?>
</div>

<input type="hidden" id="deleteHoliday" name="">
<input type="hidden" id="holidays_idedit" name="">
<?php ActiveForm::end(); ?>
<!-- ลบข้ำมูล -->
<div id="myModaldelete" class="modal" role="dialog">
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>ลบข้อมูล</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="row p-4 text-center">
                <h2 class="text-danger">ยืนยันการลบข้อมูล</h2>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm"  id="modelformbuttonclick" onClick='delHoliday()' data-dismiss="modal">ยืนยัน</button>
                <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">ยกเลิก</button>
            </div>
        </div>
    </div>
</div>

<!-- แก้ไขวันหยุด -->
<div id="myModaledit" class="modal" role="dialog">
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>แก้ไขวันหยุด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="row p-4">
                <form class="forms-sample col-12">

                    <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-4 col-form-label">วันที่</label>
                        <div class="col-sm-8">
                            <div class="input-group input-daterange">
                                <input type="date" id="max-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="วันที่" >
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-4 col-form-label">รายละเอียด </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="holidays_edit" placeholder="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-4 col-form-label">หมายเหตุ </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="holidays_remark_edit" placeholder="">
                        </div>
                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" onclick="editHolidays();">บันทึก</button>
                <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">ยกเลิก</button>
            </div>
        </div>
    </div>
</div>
<?php
$script = <<< JS

$('#holiday_date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true
    });
$('#holiday_date').on('change', function(){
    $(this).datepicker('hide');
});


var tbid;
var tdata;
$('tr .modelbox').click(function() {
   
   var _row = $(this).closest('tr');
   tbid = _row;
   tdata = $(this).attr('data');
   
});

$('#modelformbuttonclick').click(function() {
    tbid.remove();
});



    $('.probation').append("<span class='text-muted text-small'>*ระบบจะนับตั้งแต่วันตั้งแต่วันที่ทำงานวันแรกและจะปรับเป็นพนักงานประจำให้อัตโนมัติ</span>");
    $('.errand_cut_leave').append('<span class="text-muted text-small">*กรณีผ่านทดลองงาน119วัน</span>');
    $('.errand_leave').append('<span class="text-muted text-small">*ไม่คิดเงิน</span>');
    $('.sick_cut_leave').append('<span class="text-muted text-small">*พนักงานออฟฟิต ลาป่วย1วันไม่มีใบรับรองแพทย์ ได้รับค่าจ้างปกติ แต่ถ้าเกิน3วันต้องมีใบรับรองแพทย์เท่านั้น และพนักงานสาขา จะต้องมีใบรับรองแพทย์ทุกครั้งถึงจะได้รับค่าจ้าง</span>');
    $('.sick_leave').append('<span class="text-muted text-small">*ไม่คิดเงิน</span>');
    $('.holiday_leave').append('<span class="text-muted text-small">*ทำงานครบ 1ปีได้วันหยุด 6วัน, 3ปี ได้วันหยุด 10วัน, 5ปี ได้วันหยุด 12วัน</span>');
JS;

?>
<?php
$script2 = <<< JS

    function edit(ele) {
        const rowIndex  = parseInt($(ele).closest('tr').index())+1;

        let cell1 = $('#holidaysTable tr:eq('+rowIndex+') td:eq(0)').text();
        let cell2 = $('#holidaysTable tr:eq('+rowIndex+') td:eq(1)').text();
        let cell3 = $('#holidaysTable tr:eq('+rowIndex+') td:eq(2)').text();

        if(cell1){
             var conDate = cell1.split("/");
            cell1 = conDate[2]+'-'+conDate[1]+'-'+conDate[0]
        }
        console.log(cell2)
        $('#max-date').val(cell1);
        $('#holidays_edit').val(cell2);
        $('#holidays_remark_edit').val(cell3);

    }

    function editHolidays(){
      let id = $('#holidays_idedit').val();
      const holiday_date = $('#max-date').val();
      const holidays = $('#holidays_edit').val();
      const holidays_remark = $('#holidays_remark_edit').val();
      let text =''
        if(!holiday_date){
            text ='กรุณาระบุวันที่ ,'
        }
        if(!holidays){
           text+=' กรุณาระบุรายละเอียด'
        }
        if(text){
          alert(text)
          return false;
        }
      $.ajax({
            url: 'company/edit-holiday',
            type: 'post',
            data: {
              id:id,
              holiday_date:holiday_date,
              holidays:holidays,
              holidays_remark:holidays_remark,
              _csrf:yii.getCsrfToken(),
            },
            success: function (res) {
                alert(res.message)
                $('#tr_'+id).html(
              `<td class='get_holidays' data ='\${holiday_date}' >\${holiday_date}</td>` +
              `<td class='get_holidays' data ='\${holidays}' >\${holidays}</td>` +
              `<td class='get_holidays' data ='\${holidays_remark}' >\${holidays_remark}</td>` +
              `<td>` +
              `<input  type="button" class="btn btn-outline-primary btn-sm ml-auto" data-toggle="modal" onclick='$("#deleteHoliday").val(\${id});$(this).closest("tr").remove();' data-target="#myModaldelete" value='ลบ'>`+
              ` <input  type="button" class="btn btn-outline-primary btn-sm ml-auto" data-toggle="modal" data-target="#myModaledit" onclick="edit(this);$('#holidays_idedit').val(\${id})"  value='แก้ไข'>`+
              `</td>` 
            
                )
            }
        });
    }

    function setHoliday(){

        const holiday_date = $('#holiday_date').val();
        const holidays = $('#holidays').val();
        const holidays_remark = $('#holidays_remark').val();

        let text =''
        if(!holiday_date){
            text ='กรุณาระบุวันที่ ,'
        }
        if(!holidays){
           text+=' กรุณาระบุรายละเอียด'
        }
        if(text){
          alert(text)
          return false;
        }
         $.ajax({
            url: 'company/set-holiday',
            type: 'post',
            data: {
              holiday_date:holiday_date,
              holidays:holidays,
              holidays_remark:holidays_remark,
              _csrf:yii.getCsrfToken(),
            },
            success: function (res) {

            if(res.message){
              alert(res.message)
              return false;
            }
            $('#holidaysTable > tbody').append(
            `<tr id='tr_\${res.id}'>` +
            `<td class='get_holidays' data ='\${holiday_date}' >\${holiday_date}</td>` +
            `<td class='get_holidays' data ='\${holidays}' >\${holidays}</td>` +
            `<td class='get_holidays' data ='\${holidays_remark}' >\${holidays_remark}</td>` +
            `<td>` +
            `<input  type="button" class="btn btn-outline-primary btn-sm ml-auto" data-toggle="modal" onclick='$("#deleteHoliday").val(\${res.id});$(this).closest("tr").remove();' data-target="#myModaldelete" value='ลบ'>`+
            ` <input  type="button" class="btn btn-outline-primary btn-sm ml-auto" data-toggle="modal" data-target="#myModaledit" onclick="edit(this);$('#holidays_idedit').val(\${res.id})"  value='แก้ไข'>`+
            `</td>` +
            `</tr>`
            )
            $('#holiday_date').val('');
            $('#holidays').val('');
            $('#holidays_remark').val('');
          }
        });
    
    }

    function delHoliday(){
        let id = $('#deleteHoliday').val();
        if(!id){
          return false;
        }
        $.ajax({
            url: 'company/del-holiday',
            type: 'post',
            data: {
              id:id,
              _csrf:yii.getCsrfToken(),
            },
            success: function (res) {
                alert(res.message)
            }
        });
    }

    function saveCompany() {
        let days = "";
        let holidays = "";
        let remark = "";
        let strData = ""
        $("#holidaysTable tr:gt(0)").each(function () {
            var this_row = $(this);
            days = $.trim(this_row.find('td:eq(0)').html());
            holidays = $.trim(this_row.find('td:eq(1)').html());
            remark = $.trim(this_row.find('td:eq(2)').html());

            strData += days+'|x|'+holidays+'|x|'+remark+',';
        });

        $('#holidays_v').val(strData);
        $('#formCompany').submit();
    }

  function uploadFile()
  {
        var filename =$('input[type=file]')[0].files[0].name;
        $('#filename').html(filename)
        $('#btn-upload').removeClass('input-group-text').addClass('btn btn-primary btn-sm');
        
       
  }
JS;
$this->registerJs($script, View::POS_READY);
$this->registerJs($script2, View::POS_HEAD);