
<?php
use yii\helpers\Html;
use yii\grid\GridView;
?>
 <div class="row mb-4">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <div class="row mb-4"><div class="col-12"><span class="h4 text-bold">วันลา</span></div></div>
                  

                      <?php echo $form->field($model, 'errand_leave',[    
                                'template' => "{label}\n<div class='col-sm-4 errand_leave'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label ', 'style' => 'text-align: left!important;' ],
                                ])->textInput(['placeholder' => "6",'type' => 'number','required'=>true])
                      ?>
                    <?php echo $form->field($model, 'errand_cut_leave',[
                        'template' => "{label}\n<div class='col-sm-4 errand_cut_leave'>{input}</div>\n{hint}\n{error}",
                        'labelOptions' => [ 'class' => 'col-sm-3 col-form-label ', 'style' => 'text-align: left!important;' ],
                    ])->textInput(['placeholder' => "6",'type' => 'number','required'=>true])
                    ?>
                       <?php echo $form->field($model, 'sick_cut_leave',[    
                                'template' => "{label}\n<div class='col-sm-4 sick_cut_leave'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label ', 'style' => 'text-align: left!important;' ],
                                ])->textInput(['placeholder' => "30",'type' => 'number','required'=>true])
                      ?>
                      <?php echo $form->field($model, 'sick_leave',[    
                                'template' => "{label}\n<div class='col-sm-4 sick_leave'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label ', 'style' => 'text-align: left!important;' ],
                                ])->textInput(['placeholder' => "30",'type' => 'number','required'=>true])
                      ?>
                      <?php echo $form->field($model, 'hoilday_leave',[    
                                'template' => "{label}\n<div class='col-sm-4 hoilday_leave'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label ', 'style' => 'text-align: left!important;' ],
                                ])->textInput(['placeholder' => "6",'type' => 'number','required'=>true])
                      ?>
                      <?php echo $form->field($model, 'maternity_leave',[    
                                'template' => "{label}\n<div class='col-sm-4 '>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label ', 'style' => 'text-align: left!important;' ],
                                ])->textInput(['placeholder' => "90",'type' => 'number','required'=>true])
                      ?>
                      <?php echo $form->field($model, 'ordained_leave',[    
                                'template' => "{label}\n<div class='col-sm-4 '>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => [ 'class' => 'col-sm-3 col-form-label ', 'style' => 'text-align: left!important;' ],
                                ])->textInput(['placeholder' => "15",'type' => 'number','required'=>true])
                      ?>

                 



                
                </div>
              </div>
            </div>
          </div>
