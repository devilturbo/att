<?php
use yii\web\View;
$this->title = 'อนุมัติการปรับปรุงเวลา';

?>


<style>
.spinner-load {
    display: block;
    position: fixed;
    z-index: 1031;
    top: 50%;
    right: 45%;

    height: 100%;
    /* background-color: gray; */
}
</style>


<div class="spinner-load text-center" id="loadingGrow">
    <div>
        <span>
            <span class="spinner-border" style="width: 4rem; height: 4rem;" role="status">
                <span class="sr-only">Loading...</span>
            </span>
        </span>
    </div>
</div>



<div class="dashboard-header d-flex flex-column grid-margin">
    <!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">อนุมัติการปรับปรุงเวลา(<?=$dataCount?>)</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">อนุมัติการปรับปรุงเวลา</p>
            </div>
        </div>
        <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
            <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0" id='btn_export'>

            </div>
        </div>
    </div>
</div>

<div class="row mb-4">
    <div class="col-12">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member-leave">อนุมัติการลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="<?=Yii::$app->request->baseUrl?>/member-ot">อนุมัติการทำงานล่วงเวลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active"
                    href="<?=Yii::$app->request->baseUrl?>/member-change-time">อนุมัติการปรับปรุงเวลา</a>
            </li>

        </ul>
    </div>
</div>

<div class="row">
    <div class="col-12 mb-4">
        <div class="row ">
            <div class="col-2 ml-auto"></div>
            <div class="col-2"></div>
        </div>

        <form>
            <div class="form-row align-items-center mb-2">
                <div class="col-sm-3 my-1-">
                    <span>ค้นหา</span>
                    <input type="text" class="form-control global_filter" id="global_filter" placeholder="คำที่ค้นหา">
                </div>
                <div class="col-3">
                    <div class="row">

                        <div class="col"><span id="ตำแหน่ง"></span></div>
                        <div class="col"><span id="ประเภท"></span></div>
                        <div class="col-auto">
                            <button type="button" class="btn btn-outline-primary btn-sm ml-auto bt-filter"
                                style="margin-top: 20px;" data-toggle="modal" data-target="#ModalFilter">Filter</button>
                        </div>
                    </div>
                </div>


                <div class="col-auto my-1 ml-auto" style="width: 350px;">
                    <span>ค้นหาจากช่วงเวลา</span>
                    <div class="input-group input-daterange">
                        <input type="text" id="min-date" class="form-control date-range-filter"
                            data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น">
                        <div class="input-group-addon mr-2 ml-2"> ถึง </div>
                        <input type="text" id="max-date" class="form-control date-range-filter"
                            data-date-format="yyyy-mm-dd" placeholder="สิ้นสุด">
                        <button type="submit" id="search" class="btn btn-primary btn-sm ml-2">ค้นหา</button>
                    </div>
                </div>
            </div>



        </form>

    </div>
</div>

<!-- Modal -->
<div id="ModalFilter" class="modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>ค้นหาโดยละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="col-12 p-4">
                <div class="row">
                    <div class="col-6 mb-2"><span id="เพศ"></span></div>
                    <div class="col-6 mb-2 "><span id="แผนก"></span></div>
                    <div class="col-6 mb-2"><span id="ตำแหน่ง"></span></div>
                    <div class="col-6 mb-2"><span id="ประเภท"></span></div>
                    <div class="col-6 mb-2"><span id="ประเภทย่อย"></span></div>
                    <div class="col-6 mb-2"><span id="สถานะ"></span></div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                        <tr>
                            <th>รูป</th>
                            <th>รหัสพนักงาน</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>เพศ</th>
                            <th>แผนก</th>
                            <th>ตำแหน่ง</th>
                            <th>ประเภท</th>
                            <th>ประเภทย่อย</th>
                            <th>ผู้ดูแล</th>
                            <th>รายละเอียด</th>
                            <th>ตั้งวันที่/เวลา</th>
                            <th>ถึงวันที่/เวลา</th>
                            <th>สถานที่</th>
                            <th>หมายเหตุ</th>
                            <th>สถานะ</th>
                            <th>เอกสารแนบ</th>
                            <th style="display: <?=Yii::$app->user->identity->auth_approve_order ==2?'none':''?>">ดำเนินการ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php


foreach ($data as $key => $e) {

    // \app\components\Helpers::Debig(($e));

    $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
    if (!empty($e->img)) {

        $img = Yii::$app->request->baseUrl . '/' . $e->img;
        
    }

    $img_2 = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';

    if (!empty($e->img_url)) {

        // $img_2 = Yii::$app->request->baseUrl . '/' . $e->img_url;
        $img_2 ='https://api.thebestpromote.com/web/' . $e->img_url;

    }

    $displayActive = "";
    if ($e->isActive == "W") {
        $displayActive = 'รออนุมัติ';
    } else if ($e->isActive == "Y") {
        $displayActive = "อนุมัติแล้ว";
    }else if ($e->isActive == "N") {
        $displayActive = "ไม่อนุมัติ";
    }

    ?>

                        <tr class="dt-edit">
                            <td class="employee-col">
                                <img class="thumb mr-2" src="<?=$img?>" align="left">
                            </td>

                            <td><?=$e->member_no?></td>
                            <td><?=$e->name_th . ' ' . $e->surname_th?></td>
                            <td><?=$e->gender_name?></td>
                            <td><?=$e->department_name?></td>
                            <td><?=$e->position_name?></td>
                            <td><?=$e->type_name?></td>
                            <td><?=$e->subtype_name?></td>
                            <td><?=$e->memberundder_name?></td>
                            <td>ปรับปรุงเวลา</td>
                            <td>
                                <?=Yii::$app->Utilities->formatDate('d/m/Y - H:i', $e->start_date . " " . $e->start_time)?>
                            </td>
                            <td>
                                <?=Yii::$app->Utilities->formatDate('d/m/Y - H:i', $e->end_date . " " . $e->end_time)?>
                            </td>

                            <td><?=$e->member->areas->area?></td>
                            <td><?=$e->remark?></td>
                            <td><?=$displayActive?></td>
                            <td>
                                <span class="img-thumbmail"><a href="<?=$img_2?>" target="blank"><img src="<?=$img_2?>"
                                            align="left"></a>
                                </span>

                            </td>
                            <td style="display: <?=Yii::$app->user->identity->auth_approve_order ==2?'none':''?>">
                                <button id="<?=$e->id?>" class="btn btn-primary btn-sm confirmButton">อนุมัติ</button>
                                <button id="<?=$e->id?>"
                                    class="btn btn-outline-primary btn-sm ml-2 declineButton">ปฏิเสธ</button>
                            </td>
                        </tr>

                        <?php

}

?>

                    </tbody>
                </table>
                <input type="hidden" id="phpBaseUrl" value="<?=Yii::$app->request->baseUrl?>">

            </div>
        </div>
    </div>
</div>




<!-- Modal -->
<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>รายละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>


        </div>

    </div>
</div>

<?php
$script2 = <<< JS

const url_base = $("#phpBaseUrl").val()

// console.log(url_base)
$(document).on("click",'.confirmButton', function(event) {
    event.preventDefault()
    var r = confirm("ยืนยันที่จะอนุมัติการปรับปรุงเวลา?");
    if (r == true) {

        $.get( url_base + "/member-change-time/accept",{
            id: this.id,
        },function(data){
            if (data == "Success"){
                
                window.location.href = url_base +"/member-change-time"
            }else{
                alert("FAIL: " , data)
            }
        });
    }
})


$(document).on("click",'.declineButton', function(event) {
    event.preventDefault()
    // console.log('id',this.id)
    var r = confirm("ยืนยันที่จะปฏิเสธการปรับปรุงเวลา?");
    if (r == true) {

        $.get(url_base + "/member-change-time/decline",{
            id: this.id,
        },function(data){
            if (data == "Success"){
                
                window.location.href = url_base + "/member-change-time"
            }else{
                alert("FAIL: " , data)
            }
        });
    }
})

JS;

$this->registerJs($script2, View::POS_READY);

?>

<?php
$script = <<< JS

    function filterGlobal () {
    $('#dataTable').DataTable().search(
        $('#global_filter').val()

       ).draw();
    }

       var columnsToSearch = {
        4: 'แผนก',
        6: 'ตำแหน่ง',
        7: 'ประเภท',
        8: 'ประเภทย่อย',
        3: 'เพศ',
        13: 'สถานะ'
    };

    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging:   false,
        ordering: false,
        info:     false,
        scrollY: 500,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: false,
        fixedHeader: true,
        initComplete: async function () {

for (var i in columnsToSearch) {
    var api = this.api();

    var select = $('<select class="form-control"><option value="">เลือก</option></select>')
        .appendTo($('#' + columnsToSearch[i]).empty().text(columnsToSearch[i] + ' '))

        .attr('data-col', i)
        .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex(
                $(this).val()
            );
            if (val.includes("7") && val.includes("11") &&  val.includes("-") ){
                val = "7-11"
            }

            table.search( val.trim() ).draw();

        });
        if (columnsToSearch[i] == "สถานะ"){
            select.append('<option value="อนุมัติแล้ว">อนุมัติแล้ว</option>');
            select.append('<option value="ไม่อนุมัติ">ไม่อนุมัติ</option>');
            select.append('<option value="รออนุมัติ">รออนุมัติ</option>');

        }else if (columnsToSearch[i] == "ประเภท"){
            $("#loadingGrow").remove()
        }
        var arr  = await $.get(url_base+'/member-change-time/getlist', {list: columnsToSearch[i].trim() })
        var arrJson = JSON.parse(arr)

        Object.keys(arrJson).forEach(function(key) {
            select.append('<option value="' + arrJson[key] + '">' + arrJson[key] + '</option>');

        })
}

}

    });

    var buttons = new $.fn.dataTable.Buttons(table, {
       buttons: [
    {
        extend: 'excel',
        text: 'Data Export',
        charset: 'utf-8',
        extension: '.xlsx',
        bom: true,
        className: 'btn btn-outline-primary btn-sm  d-none d-md-block',
    }]
}).container().appendTo($('#btn_export'));


var colum = ["รูป", "รหัสพนักงาน","ชื่อ-นามสกุล", "เพศ", "แผนก","ตำแหน่ง","ประเภท","ประเภทย่อย","ผู้ดูแล","รายละเอียด", "ตั้งวันที่/เวลา","ถึงวันที่/เวลา","สถานที่","หมายเหตุ","สถานะ","เอกสารแนบ","ดำเนินการ",];
   //Edit row buttons
  $('.dt-edit').each(function () {
    $(this).on('click', function(evt){
        \$this = $(this);
      var dtRow = \$this;
      $('div.modal-body').innerHTML='';
      $('div.modal-body').append( '<div class="row mb-4 ml-1 img-thumbmail">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=1; i < dtRow[0].cells.length; i++){
        //   console.log(colum[i])

            $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">'+colum[i]+'</span><span class="col-8">'+dtRow[0].cells[i].innerHTML+'</span><div/>');

      }
      $('#myModal').modal('show');
    });
  });
  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .modal-body').empty();
  });
    $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
       var min = $('#min-date').val();
       var max = $('#max-date').val();
       var createdAt = data[2] || 0;
        if (
           (min == "" || max == "") ||
           (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
         ) {
            return true;
        }
            return false;
        }
    );

    $('.date-range-filter').change(function() {
      table.draw();
    });
    $('#my-table_filter').hide();

    $('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );

    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );



JS;
$this->registerJs($script, View::POS_READY);

?>