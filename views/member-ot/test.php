<?php
use yii\web\View;
$this->title = 'อนุมัติการทำงานล่วงเวลา';
// \app\components\Helpers::Debig(( $data));

?>



<style>
.sidebarmap {
    position: absolute;
    width: 250px;
    height: 100%;
    top: 0px;
    left: 240px;
    background-color: white;
    overflow: hidden;
    padding-top: 80px;
    z-index: 99;
    border-right: 1px solid rgba(0, 0, 0, 0.25);
    background-image: cover;
}

.pad2 {
    padding: 20px;
}

.map {
    position: absolute;
    left: 0;
    margin-left: 0;
    width: 100%;
    top: 60px;
    bottom: 0;
}

.map-overlay {
    position: absolute;
    width: 25%;
    top: 50%;
    bottom: 0;
    left: 50%;
    background-color: #fff;
    max-height: 50px;
    overflow: hidden;
    z-index: 9999999;
}

.map-overlay fieldset {
    display: block;
    background: #ddd;
    border: none;
    padding: 10px;
    margin: 0;
    z-index: 9999999;
}

.map-overlay input {
    display: block;
    border: none;
    width: 100%;
    border-radius: 3px;
    padding: 10px;
    margin: 0;
    background-color: red;
    z-index: 9999999;
}

.menu-ui {
    background: #fff;
    position: absolute;
    top: 70px;
    right: 50px;
    z-index: 1;
    border-radius: 3px;
    width: 120px;
    border: 1px solid rgba(0, 0, 0, 0.4);
    z-index: 99;
}

.menu-ui a {
    font-size: 13px;
    color: #404040;
    display: block;
    margin: 0;
    padding: 0;
    padding: 10px;
    text-decoration: none;
    border-bottom: 1px solid rgba(0, 0, 0, 0.25);
    text-align: center;
}

.menu-ui a:first-child {
    border-radius: 3px 3px 0 0;
}

.menu-ui a:last-child {
    border: none;
    border-radius: 0 0 3px 3px;
}

.menu-ui a:hover {
    background: #f8f8f8;
    color: #404040;
}

.menu-ui a.active,
.menu-ui a.active:hover {
    background: #3887BE;
    color: #FFF;
}

.listings {
    height: 100%;
    overflow: auto;
    padding-bottom: 0px;
}

.listings .item {
    display: block;
    border-bottom: 1px solid #eee;
    padding: 0px;
    min-height: 50px;
    text-decoration: none;
    font-size: .8rem;
    color: #7987a1;


}

.listings .item a {
    text-decoration: none;
    height: 100%;
    padding: 10px;

}

.listings .item a span {
    font-style: normal;
    font-size: .8rem;
    line-height: .5rem;
}

.listings .item a h6 {
    font-style: bold;
    font-weight: 600;
    line-height: .5rem;
    color: #0096e5;
}


.listings .item :hover {
    background-color: rgba(234, 245, 247, 0.8);
    transition: all 0.4s;
}

.listings .item a img {
    float: left;
    width: 40px;
    margin-top: -2px;
    height: 40px;
    border-radius: 100%;
}

.listings .item:last-child {
    border-bottom: none;
}

.listings .item .title {
    display: block;
    font-size: .9rem;
    color: #19283b;
    font-weight: 700;
}

.listings .item .title small {
    font-weight: 400;
}

.listings .item.active .title,
.listings .item .title:hover {
    color: #0096e5;
}

.listings .item.active {
    background-color: #f8f8f8;
}

::-webkit-scrollbar {
    width: 3px;
    height: 3px;
    border-left: 0;
    background: rgba(0, 0, 0, 0.1);
}

::-webkit-scrollbar-track {
    background: none;
}

::-webkit-scrollbar-thumb {
    background: #0096e5;
    border-radius: 0;
}

.marker {
    display: block;
    border: none;
    border-radius: 50%;
    cursor: pointer;
    padding: 0;
    background-size: cover;
    border: 4px solid white;
    box-shadow: 0px 0 10px 0 rgba(46, 61, 73, 0.3);



}

.clearfix {
    display: block;
}

.clearfix:after {
    content: '.';
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

/* Marker tweaks */
.mapboxgl-popup {
    padding-bottom: 50px;

}

.mapboxgl-popup p {
    line-height: .75rem;
    font-size: .8rem;
}

.mapboxgl-popup .mapboxgl-popup-close-button {
    background-color: transparent;
    color: white;
    font-size: 20px;
    width: 30px;
    height: 30px;
    top: -15px;
}

.mapboxgl-popup .mapboxgl-popup-close-button:hover {
    background-color: black;
    transition: all 0.4s;
}

.mapboxgl-popup-close-button {
    /*display:none;*/
}

.mapboxgl-popup-content {
    padding: 0;
    width: 250px;
}

.mapboxgl-popup-content-wrapper {
    padding: 1%;
}

.mapboxgl-popup-content h3 {
    /*background:#0096e5;*/
    color: #fff;
    font-size: .9rem;
    margin: 0;
    display: block;
    padding: 10px;
    border-radius: 3px 3px 0 0;
    font-weight: 700;
    margin-top: -15px;
}

.mapboxgl-popup-content h4 {
    margin: 0;
    display: block;
    padding: 10px 10px 10px 10px;
    font-weight: 400;
}

.mapboxgl-popup-content div {
    padding: 10px;
}

.mapboxgl-container .leaflet-marker-icon {
    cursor: pointer;
}

.mapboxgl-popup-anchor-top>.mapboxgl-popup-content {
    margin-top: 15px;
}

.mapboxgl-popup-anchor-top>.mapboxgl-popup-tip {
    border-bottom-color: #91c949;
}

.loader {
    margin: -10px 0 0 -250px;
    height: 100%;
    width: 100%;
    position: absolute;
    text-align: center;
    padding: 1em;
    top: 0;
    left: 0;
    margin: 0 auto 1em;
    z-index: 99;
    /*background-color: black;*/
    background-color: rgba(0, 0, 0, 0.05);


}

.loader svg {
    top: 50%;
    left: 55%;
    position: absolute;
}

.loader svg path,
.loader svg rect {
    fill: #FF6700;
}
</style>

<div class="main-panel">
                <div class="content-wrapper p-0">

                    <!--iv class='map-overlay'>
              <fieldset>
                <input id='feature-filter' type='text' placeholder='Filter results by name' />
               </fieldset>
            </div-->

                    <nav class='menu-ui'>
                        <a href='user-working-today.php'>ดูทั้งหมด</a>
                        <a href='user_workday.php'>ประวัติบันทึกเวลา</a>
                        <!--a href='#' class='active' data-filter='all'>Show all</a>
              <a href='#' data-filter='rentals'>Rentals</a>
              <a href='#' data-filter='tackleshop'>Tackle shop</a>
              <a href='#' data-filter='fuel'>Fuel station</a-->
                    </nav>



                    <div class='sidebarmap'>
                        <div id='listings' class='listings'></div>
                    </div>

                    <!--nav class='menu-ui' style="margin-top: 100px;">
             <a href='#' class='active' data-filter='all'>Show all</a>
  <a href='#' data-filter='rentals'>Rentals</a>
  <a href='#' data-filter='tackleshop'>Tackle shop</a>
  <a href='#' data-filter='fuel'>Fuel station</a>
</nav-->

                    <div id='map' class='map'> </div>

                    <div class="loader loader--style1" title="0" id="loader">
                        <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px"
                            viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                            <path opacity="0.2" fill="#000"
                                d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946
                 s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634
                 c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z" />
                            <path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0
                   C22.32,8.481,24.301,9.057,26.013,10.047z">
                                <animateTransform attributeType="xml" attributeName="transform" type="rotate"
                                    from="0 20 20" to="360 20 20" dur="0.5s" repeatCount="indefinite" />
                            </path>
                        </svg>
                    </div>





                </div>
                <!-- content-wrapper ends -->
                <!-- copy right -->

                <!-- end copy right -->
            </div>
            <!-- main-panel ends -->
        </div>
   

    <?php
$script = <<< JS
if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function() {
        if (this.parentNode) {
            this.parentNode.removeChild(this);
        }
    };
}

mapboxgl.accessToken = 'pk.eyJ1IjoibWFuaXR4eHgiLCJhIjoiY2sycTNzM291MGIycTNtdG90ZGY2bndmdiJ9.p-3DT7hNQocbSHNZ3fXIVA';

var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v9',
    center: [100.5408343, 13.6966586],
    zoom: 10,
    scrollZoom: false
});




var stores = {
    "type": "FeatureCollection",
    "features": [{
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.54840496046484,
                    13.894420245416647
                ]
            },
            "properties": {
                "userid": "1233123",
                "name": "สมชาย หอมหวล",
                "type": "BA",
                "subtype": "BA Hair",
                "stationid": "2348",
                "chanel": "7-11",
                "station": "Tops ธัญญาพาร์ท",
                "timecheckin": "12:00",
                "timecheckout": "13:00",
                "imgin": "1",
                "status": "บันทึกเวลา",
                "statuscolor": "#19283b",
                "imgout": "",
                "phone": "2022347336",
                "iconSize": [40, 40],
                "imgmarker": "1"
            }
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.5408343,
                    13.6966586
                ]
            },
            "properties": {
                "userid": "1233123",
                "name": "กนิษฐา  เปลี่ยนศรี",
                "type": "BA",
                "subtype": "BA Hair",
                "stationid": "1234",
                "chanel": "Makro",
                "station": "บิกซีบางบอน",
                "timecheckin": "12:00",
                "timecheckout": "13:00",
                "imgin": "2",
                "status": "บันทึกเวลา",
                "statuscolor": "#19283b",
                "imgout": "",
                "phone": "2022347336",
                "iconSize": [40, 40],
                "imgmarker": "2"
            }
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.5249646,
                    13.7482271
                ]
            },
            "properties": {
                "userid": "1233123",
                "name": "กมลทิพย์ เกตุสิงห์",
                "type": "BA",
                "subtype": "BA Hair",
                "stationid": "2348",
                "chanel": "Lotus",
                "station": "รหัสร้าน 3135 ปรีชา8",
                "timecheckin": "12:00",
                "timecheckout": "13:00",
                "imgin": "3",
                "status": "บันทึกเวลา",
                "statuscolor": "#19283b",
                "imgout": "",
                "phone": "2022347336",
                "iconSize": [40, 40],
                "imgmarker": "3"
            }
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.4086236,
                    13.8236184
                ]
            },
            "properties": {
                "userid": "1233123",
                "name": "กมลทิพย์ เรืองจุ้ย",
                "type": "BA",
                "subtype": "BA Hair",
                "stationid": "2348",
                "chanel": "7-11",
                "station": "เซนทรัลลาดพร้าว",
                "timecheckin": "12:00",
                "timecheckout": "13:00",
                "imgin": "4",
                "status": "บันทึกเวลา",
                "statuscolor": "#19283b",
                "imgout": "",
                "phone": "2022347336",
                "iconSize": [40, 40],
                "imgmarker": "4"
            }
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.5864175,
                    13.7266766
                ]
            },
            "properties": {
                "userid": "1233123",
                "name": "กมลวรรณ นันทวิทิตกร",
                "type": "BA",
                "subtype": "BA Hair",
                "stationid": "2348",
                "chanel": "Big C",
                "station": "โลตัส บางใหญ่",
                "timecheckin": "12:00",
                "timecheckout": "13:00",
                "imgin": "5",
                "status": "บันทึกเวลา",
                "statuscolor": "#19283b",
                "imgout": "",
                "phone": "2022347336",
                "iconSize": [40, 40],
                "imgmarker": "5"
            }
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.4056321,
                    13.6884946
                ]
            },
            "properties": {
                "userid": "1233123",
                "name": "กมลวรรณ ศักดิ์ศรี",
                "type": "BA",
                "subtype": "BA Hair",
                "stationid": "2348",
                "chanel": "7-11",
                "station": "วัตสันเซ็นทรัลบางนา",
                "timecheckin": "12:00",
                "timecheckout": "13:00",
                "imgin": "6",
                "status": "บันทึกเวลา",
                "statuscolor": "#19283b",
                "imgout": "",
                "phone": "2022347336",
                "iconSize": [40, 40],
                "imgmarker": "6"
            }
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.4115615,
                    13.6540153
                ]
            },
            "properties": {
                "userid": "1233123",
                "name": "กรกมล วิสุทธิแพทย์",
                "type": "BA",
                "subtype": "BA Hair",
                "stationid": "6971",
                "chanel": "7-11",
                "station": "6971 สาขา ชุมชนไฟฟ้าบางปะกง",
                "timecheckin": "12:00",
                "timecheckout": "13:00",
                "imgin": "7",
                "status": "บันทึกเวลา",
                "statuscolor": "#19283b",
                "imgout": "",
                "phone": "2022347336",
                "iconSize": [40, 40],
                "imgmarker": "7"
            }
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.6489887,
                    13.5714548
                ]
            },
            "properties": {
                "userid": "1233123",
                "name": "กรกมลวรรณ ข่าขันมณี",
                "type": "BA",
                "subtype": "BA Hair",
                "stationid": "10377",
                "chanel": "7-11",
                "station": " สาขา ตลาดฟ้าไทย",
                "timecheckin": "12:00",
                "timecheckout": "13:00",
                "imgin": "8",
                "status": "บันทึกเวลา",
                "statuscolor": "#19283b",
                "imgout": "",
                "phone": "2022347336",
                "iconSize": [40, 40],
                "imgmarker": "8"
            }
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.5118974,
                    13.805875
                ]
            },
            "properties": {
                "userid": "1233123",
                "name": "กวินนาถ เสนขวัญแก้ว",
                "type": "BA",
                "subtype": "BA Hair",
                "stationid": "5504",
                "chanel": "7-11",
                "station": "ซากสมอ",
                "timecheckin": "12:00",
                "timecheckout": "13:00",
                "imgin": "9",
                "status": "บันทึกเวลา",
                "statuscolor": "#19283b",
                "imgout": "",
                "phone": "2022347336",
                "iconSize": [40, 40],
                "imgmarker": "9"
            }
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.7567174,
                    13.6292888
                ]
            },
            "properties": {
                "userid": "1233123",
                "name": "กันตวี แซะอาหลำ",
                "type": "BA",
                "subtype": "BA Hair",
                "stationid": "13374",
                "chanel": "7-11",
                "station": "สาขา เดอะวอล์คพานทอง",
                "timecheckin": "12:00",
                "timecheckout": "13:00",
                "imgin": "10",
                "status": "บันทึกเวลา",
                "statuscolor": "#19283b",
                "imgout": "",
                "phone": "2022347336",
                "iconSize": [40, 40],
                "imgmarker": "10"
            }
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.6586357,
                    13.534416
                ]
            },
            "properties": {
                "userid": "1233123",
                "name": "กัลยวรรธน์ สำรวมศักดิ์",
                "type": "BA",
                "subtype": "BA Hair",
                "stationid": "13127",
                "chanel": "7-11",
                "station": "สาขาเจริญสินธานี",
                "timecheckin": "12:00",
                "timecheckout": "13:00",
                "imgin": "11",
                "status": "บันทึกเวลา",
                "statuscolor": "#19283b",
                "imgout": "",
                "phone": "2022347336",
                "iconSize": [40, 40],
                "imgmarker": "11"

            }

        }
    ]
};


// This adds the data to the map
map.on('load', function(e) {
    // This is where your '.addLayer()' used to be, instead add only the source without styling a layer
    map.addSource("stores", {
        "type": "geojson",
        "data": stores
    });




    // Initialize the list
    buildLocationList(stores);

    //var markers = L.mapbox.featureLayer()



});



// This is where your interactions with the symbol layer used to be
// Now you have interactions with DOM markers instead
stores.features.forEach(function(marker, i) {
    // Create an img element for the marker
    var el = document.createElement('div');
    el.id = "marker-" + i;
    el.className = 'marker';
    el.style.backgroundImage = 'url(images/user/thumb/' + marker.properties.imgmarker + '.jpg';
    console.log('url(https://placekitten.com/g/40');

    el.style.width = '50px';
    el.style.height = '50px';

    // Add markers to the map at all points
    new mapboxgl.Marker(el, {
            offset: [0, -23]
        })
        .setLngLat(marker.geometry.coordinates)
        //.setGeoJSON(stores)
        .addTo(map);


    //.setGeoJSON(stores)
    //.addTo(map);



    el.addEventListener('mouseover', function(e) {
        console.log("Mouse Over");
        createPopUp(marker);
    });

    el.addEventListener('mouseout', function(e) {
        console.log("Mouse Out");
    });

    el.addEventListener('click', function(e) {
        console.log("Mouse Click");
        // 1. Fly to the point
        flyToStore(marker);
        // 2. Close all other popups and display popup for clicked store
        createPopUp(marker);
        // 3. Highlight listing in sidebar (and remove highlight for all other listings)
        var activeItem = document.getElementsByClassName('active');
        e.stopPropagation();
        if (activeItem[0]) {
            activeItem[0].classList.remove('active');
        }
        var listing = document.getElementById('listing-' + i);
        listing.classList.add('active');

    });
});

/* filterInput.addEventListener('keyup', function(e) {
   var value = e.target.value.trim().toLowerCase();
   layerIDs.forEach(function(layerID) {
   map.setLayoutProperty(layerID, 'visibility',
   layerID.indexOf(value) > -1 ? 'visible' : 'none');

   });
 });*/


function flyToStore(currentFeature) {
    map.flyTo({
        center: currentFeature.geometry.coordinates,
        zoom: 15
    });
}

function createPopUp(currentFeature) {
    var popUps = document.getElementsByClassName('mapboxgl-popup');
    if (popUps[0]) popUps[0].remove();

    var popup = new mapboxgl.Popup({
            closeOnClick: false
        })
        .setLngLat(currentFeature.geometry.coordinates)
        .setHTML('<h3 style ="background:' + currentFeature.properties.statuscolor + ';">' + currentFeature.properties
            .status + '</h3>' +
            '<div class="p-2 img-thumbmail"><img src="images/user/thumb/' + currentFeature.properties.imgin +
            '.jpg" class="mb-2"><h5>' + currentFeature.properties.name + '</h5><p>รหัสพนักงาน ' + currentFeature
            .properties.userid + '</p><p>ประเภท ' + currentFeature.properties.type + ' , ' + currentFeature.properties
            .subtype + '</p><p>สถานที่ ' + currentFeature.properties.stationid + ' , ' + currentFeature.properties
            .chanel + ' , ' + currentFeature.properties.station + '</p><p>เบอร์โทร ' +
            currentFeature.properties.phone +
            '</p><a href="user-profile-workday-map.php" target="blank"><button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลา</button></a></div>'
        )
        .addTo(map);
}

function buildLocationList(data) {
    for (i = 0; i < data.features.length; i++) {
        var currentFeature = data.features[i];
        var prop = currentFeature.properties;

        var listings = document.getElementById('listings');
        var listing = listings.appendChild(document.createElement('div'));
        listing.className = 'item';
        listing.id = "listing-" + i;

        var link = listing.appendChild(document.createElement('a'));
        link.href = '#';
        link.className = 'title';
        link.dataPosition = i;
        link.innerHTML =
            '<img src="images/user/thumb/' + currentFeature.properties.imgin + '.jpg" class="mr-2 " >' +
            '<h6 class="">' + currentFeature.properties.name + '</h6>' +
            '<span class="text-muted ">' + currentFeature.properties.stationid + ' - ' + currentFeature.properties
            .chanel + ' - ' + currentFeature.properties.station + '</span><br>'

        link.addEventListener('click', function(e) {
            console.log("mouse Click")
            var clickedListing = data.features[this.dataPosition];
            flyToStore(clickedListing);
            createPopUp(clickedListing);
            var activeItem = document.getElementsByClassName('active');

            if (activeItem[0]) {
                activeItem[0].classList.remove('active');
            }
            this.parentNode.classList.add('active');

        });
    }
}

map.on('data', function(e) {
    if (e.dataType === 'source' && e.sourceId === 'stores') {
        document.getElementById("loader").style.visibility = "hidden";
        console.log("map on");
    }
})

$('.menu-ui a').on('click', function() {
    var filter = $(this).data('filter');
    $(this).addClass('active').siblings().removeClass('active');
    markers.setFilter(function(f) {
        return (filter === 'all') ? true : f.properties[filter] === true;
    });
    return false;
});


// Add zoom and rotation controls to the map.
map.addControl(new mapboxgl.NavigationControl());

JS;
$this->registerJs($script, View::POS_READY);

?>