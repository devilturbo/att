<?php
use yii\web\View;
$this->title = 'ประกาศข่าว';

// echo "<pre style='background: #202020; color: #fff; overflow: auto;'>";
// print_r($data);
// // htmlspecialchars
// echo "<pre>";
// die;
?>
<div class="dashboard-header d-flex flex-column grid-margin">
    <!-- dashboard header -->

    <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
        <div class="d-flex align-items-center">
            <h4 class="mb-0 font-weight-bold mr-2">อนุมัติการปรับปรุงเวลา(9)</h4>
            <div class="d-none d-md-flex mt-1">
                <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                <i class="mdi mdi-chevron-right text-muted"></i>
                <p class="text-muted mb-0 tx-13 cursor-pointer">อนุมัติการปรับปรุงเวลา</p>
            </div>
        </div>
        <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
            <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button>
        </div>
    </div>
</div>

<div class="row mb-4">
    <div class="col-12">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link " href="approve-leave.php">อนุมัติการลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="approve-ot.php">อนุมัติการทำงานล่วงเวลา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="approve-changetime.php">อนุมัติการปรับปรุงเวลา</a>
            </li>

        </ul>
    </div>
</div>

<div class="row">
    <div class="col-12 mb-4">
        <div class="row ">
            <div class="col-2 ml-auto"></div>
            <div class="col-2"></div>
        </div>

        <form>
            <div class="form-row align-items-center mb-2">
                <div class="col-sm-3 my-1-">
                    <span>ค้นหา</span>
                    <input type="text" class="form-control global_filter" id="global_filter" placeholder="คำที่ค้นหา">
                </div>
                <div class="col-3">
                    <div class="row">
                        <div class="col"><span id="ตำแหน่ง"></span></div>
                        <div class="col"><span id="ประเภท"></span></div>
                        <div class="col-auto">
                            <button type="button" class="btn btn-outline-primary btn-sm ml-auto bt-filter"
                                style="margin-top: 20px;" data-toggle="modal" data-target="#ModalFilter">Filter</button>
                        </div>
                    </div>
                </div>


                <div class="col-auto my-1 ml-auto" style="width: 350px;">
                    <span>ค้นหาจากช่วงเวลา</span>
                    <div class="input-group input-daterange">
                        <input type="text" id="min-date" class="form-control date-range-filter"
                            data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น">
                        <div class="input-group-addon mr-2 ml-2"> ถึง </div>
                        <input type="text" id="max-date" class="form-control date-range-filter"
                            data-date-format="yyyy-mm-dd" placeholder="สิ้นสุด">
                        <button type="submit" id="search" class="btn btn-primary btn-sm ml-2">ค้าหา</button>
                    </div>
                </div>
            </div>



        </form>

    </div>
</div>

<!-- Modal -->
<div id="ModalFilter" class="modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>ค้นหาโดยละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="col-12 p-4">
                <div class="row">
                    <div class="col-6 mb-2"><span id="เพศ"></span></div>
                    <div class="col-6 mb-2 "><span id="เอเจนซี่"></span></div>
                    <div class="col-6 mb-2 "><span id="แผนก"></span></div>
                    <div class="col-6 mb-2"><span id="ตำแหน่ง"></span></div>
                    <div class="col-6 mb-2"><span id="ประเภท"></span></div>
                    <div class="col-6 mb-2"><span id="ประเภทย่อย"></span></div>
                    <div class="col-6 mb-2"><span id="สัญญาจ้าง"></span></div>
                    <div class="col-6 mb-2"><span id="พื้นที่"></span></div>
                    <div class="col-6 mb-2"><span id="ช่องทาง"></span></div>
                    <div class="col-6 mb-2"><span id="ห้างร้าน"></span></div>
                    <div class="col-6 mb-2"><span id="สถานนะ"></span></div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                        <tr>
                            <th>รูป</th>
                            <th>รหัสพนักงาน</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>เพศ</th>
                            <th>แผนก</th>
                            <th>ตำแหน่ง</th>
                            <th>ประเภท</th>
                            <th>ประเภทย่อย</th>
                            <th>ผู้ดูแล</th>
                            <th>รายละเอียด</th>
                            <th>ตั้งวันที่/เวลา</th>
                            <th>ถึงวันที่/เวลา</th>
                            <th>สถานที่</th>
                            <th>หมายเหตุ</th>
                            <th>สถานะ</th>
                            <th>แนบเอกสาร</th>
                            <th>ดำเนินการ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="dt-edit">
                            <td class="employee-col">
                                <img class="thumb mr-2" src="images/user/3.jpg" align="left">
                            </td>
                            <td>123435245</td>
                            <td>คมศักดิ์ รัชนีกร</td>
                            <td>หญิง</td>
                            <td>UT</td>
                            <td>Straff</td>
                            <td>BA</td>
                            <td>BA Hair</td>
                            <td>มานิตย์ รุ่งเรือง</td>
                            <td>ทำงานล่วงเวลา</td>
                            <td>12/12/19-12:00</td>
                            <td>15/12/19-12:00</td>
                            <td>Makro/23489/บางบัวทอง</td>
                            <td>งานยังไม่เสร็จ</td>
                            <td>รออนุมัติ</td>
                            <td>
                                <span class="img-thumbmail"><a href="images/leave.jpg" target="blank"><img
                                            src="images/leave.jpg" align="left"></a>
                                </span>

                            </td>
                            <td>
                                <button class="btn btn-primary btn-sm ">อนุมัติ</button>
                                <button class="btn btn-outline-primary btn-sm ml-2">ปฤิเสธ</button>
                            </td>
                        </tr>










                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>


<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span>รายละเอียด</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>
            <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div-->
        </div>

    </div>
</div>

<?php
$script = <<< JS

function filterGlobal() {
    $('#dataTable').DataTable().search(
        $('#global_filter').val()

    ).draw();
}

    var columnsToSearch = {
        4: 'แผนก',
        6: 'ตำแหน่ง',
        7: 'ประเภท',
        8: 'ประเภทย่อย',
        3: 'เพศ',
        14: 'สถานนะ'
    };

    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging: true,
        ordering: false,
        info: false,
        scrollY: 500,
        scrollX: true,
        scrollCollapse: false,
        autoWidth: false,
        fixedColumns: {
            leftColumns: 1
        },
        columnDefs: [{
            "width": "50px",
            "targets": [0]
        }, ],
        responsive: false,
        fixedHeader: true,
        initComplete: function() {
            for (var i in columnsToSearch) {
                var api = this.api();
                var select = $(
                        '<select class="form-control"><option value="">Select</option></select>')
                    .appendTo($('#' + columnsToSearch[i]).empty().text(columnsToSearch[i] + ' '))

                    .attr('data-col', i)
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        api.column($(this).attr('data-col'))
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                api.column(i).data().unique().sort().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>');
                });
            }
        }

    });

    var colum = ["รูป", "รหัสพนักงาน", "ชื่อ-นามสกุล", "เพศ", "แผนก", "ตำแหน่ง", "ประเภท", "ประเภทย่อย",
        "ผู้ดูแล", "สาเหตุ", "วันที่เริ่มงาน", "วันสิ้นสุดงาน", "สถานที่", "รายละเอียด", "สถานนะ",
        "เอกสารแนบ", "ดำเนินการ"
    ];
    //Edit row buttons
    $('.dt-edit').each(function() {
        $(this).on('click', function(evt) {
            \$this = $(this);
            var dtRow = \$this;
            $('div.modal-body').innerHTML = '';
            $('div.modal-body').append('<div class="row mb-4 ml-1 img-thumbmail">' + dtRow[0]
                .cells[0].innerHTML + '</div>');
            for (var i = 1; i < dtRow[0].cells.length; i++) {
                $('div.modal-body').append(
                    '<div class="row"> <span class="card-title mr-2 text-muted col-3 ">' +
                    colum[i] + '</span><span class="col-8">' + dtRow[0].cells[i].innerHTML +
                    '</span><div/>');
                console.log(i);
            }
            $('#myModal').modal('show');
        });
    });
    $('#myModal').on('hidden.bs.modal', function(evt) {
        $('.modal .modal-body').empty();
    });
    $.fn.dataTable.ext.search.push(
        function(settings, data, dataIndex) {
            var min = $('#min-date').val();
            var max = $('#max-date').val();
            var createdAt = data[2] || 0;
            if (
                (min == "" || max == "") ||
                (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
            ) {
                return true;
            }
            return false;
        }
    );

    $('.date-range-filter').change(function() {
        table.draw();
    });
    $('#my-table_filter').hide();

    $('input.global_filter').on('keyup click', function() {
        filterGlobal();
    });

    $('input.column_filter').on('keyup click', function() {
        filterColumn($(this).parents('tr').attr('data-column'));
    });


JS;
$this->registerJs($script, View::POS_READY);

?>