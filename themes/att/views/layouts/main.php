<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\web\View;
use yii\helpers\Url;
AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="<?=Yii::$app->request->baseUrl?>/images/favicon.png" />
    <?php $this->registerCsrfMetaTags() ?>
    <title><?=!Yii::$app->user->isGuest?'ATT-Thebestpromote.Co.,Ltd':Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body class="sidebar-fixed">
<?php $this->beginBody() ?>
<?php  if (empty(yii::$app->user->identity->email)){?>
            <?=$content?>

<?php }else{ ?>

    <div class="container-scroller">
         <div class="container-fluid page-body-wrapper" >
           <?=$this->render('//layouts/_topbar');  ?>
            <?=$this->render('//layouts/_sidebar');  ?>
              <div class="main-panel" >
                 <div class="content-wrapper" >
                     <?=$content?>
                </div>
            </div>
         </div>
    </div>
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019
              <a href="https://www.http://thebestpromote.com/" target="_blank" class="text-muted">Thebestpromote</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Farmers</span>
        </div>
    </footer>
<?php  } ?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>