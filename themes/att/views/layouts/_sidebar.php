<!-- partial:partials/_sidebar.html -->
<?php
use app\components\Utilities;
$arrMenu = Utilities::setMenu();
?>
      <nav class="sidebar sidebar-offcanvas" id="sidebar" style="z-index: 999;">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/">
              <span class="menu-title">หน้าหลัก</span>
            </a>
          </li>

          <li class="nav-item"  style="display: <?=$arrMenu['company']?>">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <span class="menu-title">ตั้งค่าบริษัท</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu" >
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/company">รายละเอียดบริษัท</a></li>
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/company/department">แผนก</a></li>
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/company/position">ตำแหน่ง</a></li>
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/company/type">ประเภทพนักงาน</a></li>
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/company/agency">เอเจนซี่</a></li>
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/company/slip">สลิปเงินเดือน</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item"  >
            <a class="nav-link" data-toggle="collapse" href="#ui-advanced" aria-expanded="false" aria-controls="ui-advanced">
              <span class="menu-title">พนักงาน</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-advanced">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item" style="display: <?=$arrMenu['member']?>"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member">พน้กงานทั้งหมด</a></li>
                <li class="nav-item" style="display: <?=$arrMenu['member_add']?>"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member/profile">เพิ่มพนักงาน</a></li>
                <li class="nav-item" style="display: <?=$arrMenu['member_import']?>"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member/import">Import ข้อมูลพนักงาน</a></li>
                <li class="nav-item" style="display: <?=$arrMenu['history_add']?>"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member/workday">ประวัติบันทึกเวลา</a></li>
                <li class="nav-item" style="display: <?=$arrMenu['history_leave']?>"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member/leave">ประวัติการลา</a></li>
                <li class="nav-item" style="display: <?=$arrMenu['history_update']?>"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member/change-time">ประวัติการปรับปรุงเวลา</a></li>
                <li class="nav-item" style="display: <?=$arrMenu['history_ot']?>"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member/ot">ประวัติการทำงานล่วงเวลา</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item" style="display: <?=$arrMenu['approve']?>">
            <a class="nav-link" data-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
              <span class="menu-title">อนุมัติรายการ</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="form-elements">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item" ><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member-ot">ทำงานล่วงเวลา(<?=Yii::$app->Utilities->getMemberOtCount()?>)</a></li>
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member-leave">การลาต่างๆ(<?=Yii::$app->Utilities->getMemberLeaveCount()?>)</a></li>
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/member-change-time">ปรับปรุงเวลา(<?=Yii::$app->Utilities->getMemberChangeTimeCount()?>)</a></li>
              </ul>
            </div>
          </li>
          
          <li class="nav-item" style="display: <?=$arrMenu['area']?>">
            <a class="nav-link" data-toggle="collapse" href="#charts" aria-expanded="false" aria-controls="charts">
              <span class="menu-title">สถานที่</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="charts">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/area">สถานที่ทั้งหมด</a></li>
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/area/location-add">เพิ่มสถานที่</a></li>
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/area/import">Import สถานที่</a></li>
                <!-- <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/area/location-group" >กลุ่มสถานที่</a></li> -->
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/area/location-area" >พื้นที่</a></li>
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/area/location-zone" >เขตพื้นที่</a></li>
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/area/location-chanel" >ช่องทาง</a></li>
                <li class="nav-item"><a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/area/location-account" >Account</a></li>              </ul>
            </div>
          </li>
 
          <li class="nav-item" style="display: <?=$arrMenu['export']?>">
            <a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/exportdata">
              <span class="menu-title">Export ข้อมูล</span>
            </a>
          </li>

           <li class="nav-item" style="display: <?=$arrMenu['news']?>">
           <!-- <a class="nav-link" href="news"> -->
           <a class="nav-link" href="<?=Yii::$app->request->baseUrl?>/news">
              <span class="menu-title">ประกาศข่าว</span>
            </a>
          </li>

           <!--li class="nav-item">
            <a class="nav-link" href="vacancy.php">
              <span class="menu-title">Vacancy</span>
            </a>
          </li-->
        </ul>
      </nav>