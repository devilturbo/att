  <!-- base:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->

    <!-- Plugin js for this page-->
  <script src="vendors/jquery.flot/jquery.flot.js"></script>
  <script src="vendors/jquery.flot/jquery.flot.pie.js"></script>
  <script src="vendors/jquery.flot/jquery.flot.resize.js"></script>
  <script src="vendors/jqvmap/jquery.vmap.min.js"></script>
  <script src="vendors/jqvmap/maps/jquery.vmap.world.js"></script>
  <script src="vendors/jqvmap/maps/jquery.vmap.usa.js"></script>
  <script src="vendors/peity/jquery.peity.min.js"></script>
  <!--script src="js/jquery.flot.dashes.js"></script-->

  <script src="js/chosen.jquery.js" type="text/javascript"></script>
  <script src="js/prism.js" type="text/javascript" charset="utf-8"></script>
  <script src="js/init.js" type="text/javascript" charset="utf-8"></script>


  <script src="vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <script src="vendors/typeahead.js/typeahead.bundle.min.js"></script>
  <script src="vendors/select2/select2.min.js"></script>

  <script src="js/off-canvas.js"></script>

  <script src="js/dataTables.fixedHeader.min.js"></script>
  <script src="js/moment.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/dataTables.fixedColumns.min.js"></script>
  <script src="js/datepicker.js"></script>

  <script src="js/typeahead.js"></script>
  <script src="js/select2.js"></script>

  <script src="js/lazyload.js"></script>

  <!--script src="js/jquery.czMore-1.5.3.2.js"></script>
    <script type="text/javascript">
        $("#czContainer").czMore();
  </script-->



  <script src="js/dashboard.js"></script>
  <!-- End custom js for this page-->

