<!DOCTYPE html>
<html lang="en">
<?php include('header.php') ?>

<body class="sidebar-fixed">
  <div class="container-scroller">
    <!-- topbar -->
    <?php include('topbar.php') ?>
    <!-- end topbar -->
    <div class="container-fluid page-body-wrapper">
      <?php include('sidebar.php') ?>
      <!-- end sidebar -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->
            
            <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
              <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">เพิ่มตำแหน่ง</h4>
                <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">เพิ่มตำแหน่ง</p>
                </div>
              </div>
              <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                
                <a href="company-position.php" class="ml-auto"><button class="btn btn-outline-primary btn-sm  ml-auto">ยกเลิก</button></a> 
                <a href="company-position.php"><button class="btn btn-primary btn-sm mr-3  ml-2">บันทึก</button></a> 
                
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">

                  <div class="row">
                        <div class="col-12">
                          <form class="needs-validation" novalidate="">
                              <div class="form-row">
                                
                                <div class="col-12 mb-3">
                                  <label for="validationCustom01">ชื่อตำแหน่ง</label>
                                  <input type="text" class="form-control" id="validationCustom01" placeholder="ชื่อบริษัทภาษาไทย" value="TBA" required="">
                                </div>

                                <div class="col-12 mb-3">
                                  <label for="validationCustom01">หมายเหตุ</label>
                                  <input type="text" class="form-control" id="validationCustom01" placeholder="" value="" required="">
                                </div>
                                
                                <div class="col-12">
                                  <label for="validationCustom02">กำหนดระยะเวลาสูงสุดในการคงอยู่ในระบบ</label>
                                  <select class="form-control">
                                    <option>30</option>
                                    <option>40</option>
                                    <option>50</option>
                                  </select>
                                </div>
                              </div>
                          </form>
                        </div>
                      </div>

                      <div class="row mt-5">
                        <div class="col-12">

                        <h4>สิทธิ์ในการเข้าถึงข้อมูล</h4><br>
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                 <th>#</th>
                                 <th>รายละเอียด</th>
                                 <th class="bg-success text-white text-center" style="width: 100px;">จัดการได้</th>
                                 <th class="bg-warning text-white text-center" style="width: 100px;">แสดงเท่านั้น</th>
                                 <th class="bg-danger text-white text-center" style="width: 100px;">ไม่แสดง</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>1</td>
                                <td>ตั้งค่าบริษัท</td>
                                <td class=" text-center">
                                  <div class="radio icheck-greensea">
                                    <input type="radio" id="greensea3" name="greensea" />
                                    <label for="greensea3"></label>
                                  </div>
                                </td>
                                <td class="text-center"> 
                                  <div class="radio icheck-orange">
                                    <input type="radio" id="greensea1" name="greensea" />
                                    <label for="greensea1"></label>
                                  </div>
                                </td>
                                <td class="text-center">
                                  <div class="radio icheck-alizarin">
                                    <input type="radio" checked id="greensea2" name="greensea" />
                                    <label for="greensea2"></label>
                                  </div>
                                </td>
                              </tr>

                              <tr>
                                <td>2</td>
                                <td>พนักงานในระบบ</td>
                                <td class=" text-center">
                                  <div class="radio icheck-greensea">
                                    <input type="radio" id="che1" name="check2" />
                                    <label for="che1"></label>
                                  </div>
                                </td>
                                <td class="text-center"> 
                                  <div class="radio icheck-orange">
                                    <input type="radio" id="che2" name="check2" />
                                    <label for="che2"></label>
                                  </div>
                                </td>
                                <td class="text-center">
                                  <div class="radio icheck-alizarin">
                                    <input type="radio" checked id="che3" name="check2" />
                                    <label for="che3"></label>
                                  </div>
                                </td>
                              </tr>

                              <tr>
                                <td>3</td>
                                <td>เพิ่มพนักงานในระบบ</td>
                                <td class=" text-center">
                                  <div class="radio icheck-greensea">
                                    <input type="radio" id="31" name="check3" />
                                    <label for="31"></label>
                                  </div>
                                </td>
                                <td class="text-center"> 
                                  <div class="radio icheck-orange">
                                    <input type="radio" id="32" name="check3" />
                                    <label for="32"></label>
                                  </div>
                                </td>
                                <td class="text-center">
                                  <div class="radio icheck-alizarin">
                                    <input type="radio" checked id="33" name="check3" />
                                    <label for="33"></label>
                                  </div>
                                </td>
                              </tr>

                              <tr>
                                <td>4</td>
                                <td>นำเข้าข้อมูลพนักงาน</td>
                                <td class=" text-center">
                                  <div class="radio icheck-greensea">
                                    <input type="radio" id="41" name="check4" />
                                    <label for="41"></label>
                                  </div>
                                </td>
                                <td class="text-center"> 
                                  <div class="radio icheck-orange">
                                    <input type="radio" id="42" name="check4" />
                                    <label for="42"></label>
                                  </div>
                                </td>
                                <td class="text-center">
                                  <div class="radio icheck-alizarin">
                                    <input type="radio" checked id="43" name="check4" />
                                    <label for="43"></label>
                                  </div>
                                </td>
                              </tr>

                              <tr>
                                <td>5</td>
                                <td>กลุ่มพนักงาน</td>
                                <td class=" text-center">
                                  <div class="radio icheck-greensea">
                                    <input type="radio" id="51" name="check5" />
                                    <label for="51"></label>
                                  </div>
                                </td>
                                <td class="text-center"> 
                                  <div class="radio icheck-orange">
                                    <input type="radio" id="52" name="check5" />
                                    <label for="52"></label>
                                  </div>
                                </td>
                                <td class="text-center">
                                  <div class="radio icheck-alizarin">
                                    <input type="radio" checked id="53" name="check5" />
                                    <label for="53"></label>
                                  </div>
                                </td>
                              </tr>

                              <tr>
                                <td>6</td>
                                <td>รายงาน(BA)</td>
                                <td class=" text-center">
                                  <div class="radio icheck-greensea">
                                    <input type="radio" id="61" name="check6" />
                                    <label for="61"></label>
                                  </div>
                                </td>
                                <td class="text-center"> 
                                  <div class="radio icheck-orange">
                                    <input type="radio" id="62" name="check6" />
                                    <label for="62"></label>
                                  </div>
                                </td>
                                <td class="text-center">
                                  <div class="radio icheck-alizarin">
                                    <input type="radio" checked id="63" name="check6" />
                                    <label for="63"></label>
                                  </div>
                                </td>
                              </tr>

                              <tr>
                                <td>7</td>
                                <td>อนุมัติรายการ</td>
                                <td class=" text-center">
                                  <div class="radio icheck-greensea">
                                    <input type="radio" id="71" name="check7" />
                                    <label for="71"></label>
                                  </div>
                                </td>
                                <td class="text-center"> 
                                  <div class="radio icheck-orange">
                                    <input type="radio" id="72" name="check7" />
                                    <label for="72"></label>
                                  </div>
                                </td>
                                <td class="text-center">
                                  <div class="radio icheck-alizarin">
                                    <input type="radio" checked id="73" name="check7" />
                                    <label for="73"></label>
                                  </div>
                                </td>
                              </tr>

                              <tr>
                                <td>8</td>
                                <td>ประวัติบันทึกเวลา</td>
                                <td class=" text-center">
                                  <div class="radio icheck-greensea">
                                    <input type="radio" id="81" name="check8" />
                                    <label for="81"></label>
                                  </div>
                                </td>
                                <td class="text-center"> 
                                  <div class="radio icheck-orange">
                                    <input type="radio" id="82" name="check8" />
                                    <label for="82"></label>
                                  </div>
                                </td>
                                <td class="text-center">
                                  <div class="radio icheck-alizarin">
                                    <input type="radio" checked id="83" name="check8" />
                                    <label for="83"></label>
                                  </div>
                                </td>
                              </tr>

                              <tr>
                                <td>9</td>
                                <td>ประวัติบันทึกการลา</td>
                                <td class=" text-center">
                                  <div class="radio icheck-greensea">
                                    <input type="radio" id="91" name="check9" />
                                    <label for="91"></label>
                                  </div>
                                </td>
                                <td class="text-center"> 
                                  <div class="radio icheck-orange">
                                    <input type="radio" id="92" name="check9" />
                                    <label for="92"></label>
                                  </div>
                                </td>
                                <td class="text-center">
                                  <div class="radio icheck-alizarin">
                                    <input type="radio" checked id="93" name="check9" />
                                    <label for="93"></label>
                                  </div>
                                </td>
                              </tr>

                              <tr>
                                <td>10</td>
                                <td>ประวัติปรับปรุงเวลา</td>
                                <td class=" text-center">
                                  <div class="radio icheck-greensea">
                                    <input type="radio" id="101" name="check10" />
                                    <label for="101"></label>
                                  </div>
                                </td>
                                <td class="text-center"> 
                                  <div class="radio icheck-orange">
                                    <input type="radio" id="102" name="check10" />
                                    <label for="102"></label>
                                  </div>
                                </td>
                                <td class="text-center">
                                  <div class="radio icheck-alizarin">
                                    <input type="radio" checked id="103" name="check10" />
                                    <label for="103"></label>
                                  </div>
                                </td>
                              </tr>

                              <tr>
                                <td>11</td>
                                <td>วันหยุดประจำปี</td>
                                <td class=" text-center">
                                  <div class="radio icheck-greensea">
                                    <input type="radio" id="111" name="check11" />
                                    <label for="111"></label>
                                  </div>
                                </td>
                                <td class="text-center"> 
                                  <div class="radio icheck-orange">
                                    <input type="radio" id="112" name="check11" />
                                    <label for="112"></label>
                                  </div>
                                </td>
                                <td class="text-center">
                                  <div class="radio icheck-alizarin">
                                    <input type="radio" checked id="113" name="check11" />
                                    <label for="113"></label>
                                  </div>
                                </td>
                              </tr>

                              <tr>
                                <td>12</td>
                                <td>กำหนดพิกัดสถานที่</td>
                                <td class=" text-center">
                                  <div class="radio icheck-greensea">
                                    <input type="radio" id="121" name="check12" />
                                    <label for="121"></label>
                                  </div>
                                </td>
                                <td class="text-center"> 
                                  <div class="radio icheck-orange">
                                    <input type="radio" id="122" name="check12" />
                                    <label for="122"></label>
                                  </div>
                                </td>
                                <td class="text-center">
                                  <div class="radio icheck-alizarin">
                                    <input type="radio" checked id="123" name="check12" />
                                    <label for="123"></label>
                                  </div>
                                </td>
                              </tr>

                              <tr>
                                <td>13</td>
                                <td>Vacancy</td>
                                <td class=" text-center">
                                  <div class="radio icheck-greensea">
                                    <input type="radio" id="131" name="check13" />
                                    <label for="131"></label>
                                  </div>
                                </td>
                                <td class="text-center"> 
                                  <div class="radio icheck-orange">
                                    <input type="radio" id="132" name="check13" />
                                    <label for="132"></label>
                                  </div>
                                </td>
                                <td class="text-center">
                                  <div class="radio icheck-alizarin">
                                    <input type="radio" checked id="133" name="check13" />
                                    <label for="133"></label>
                                  </div>
                                </td>
                              </tr>

                            </tbody>

                          </table>
                  
                        </div>
                      </div>

                      

                 

                      <div class="row mt-3">
                           <a href="company-position.php" class="ml-auto"><button class="btn btn-outline-primary btn-sm ml-auto">ยกเลิก</button></a> 
                          <a href="company-position.php"><button class="btn btn-primary btn-sm mr-3 ml-2">บันทึก</button></a> 
                      </div>

                </div>
              </div>
            </div>
          </div>



          

        </div>
        <!-- content-wrapper ends -->
        <!-- copy right -->
       <?php include('copyright.php') ?>
        <!-- end copy right -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
<?php include('footer.php') ?>

</body>

</html>

