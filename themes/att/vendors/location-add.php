<!DOCTYPE html>
<html lang="en">
<?php include('header.php') ?>
  <!--link rel="stylesheet" href="vendors/nouislider/nouislider.min.css">
  <link rel="stylesheet" href="vendors/ion-rangeslider/css/ion.rangeSlider.css"-->

<!--body class="sidebar-fixed"-->
<body class="sidebar-fixed" >

  <div class="container-scroller">
    <!-- topbar -->
    <?php include('topbar.php') ?>
    <!-- end topbar -->
    <div class="container-fluid page-body-wrapper">
      <?php include('sidebar.php') ?>
      <!-- end sidebar -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->
            
            <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
              <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">เพิ่มสถานที่</h4>
                <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">เพิ่มสถานที่</p>
                </div>
              </div>
              <!--div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button>
              </div-->
            </div>
          </div>
          <div class="row mb-4">
            <div class="col-12">
              <ul class="nav nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link " href="location-all.php">สถานที่ทั้งหมด</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" href="location-add.php">เพิ่มสถานที่</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="location-import.php">Importสถานที่</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="location-group.php">กลุ่มสถานที่</a>
                </li>

              </ul>
             </div>
          </div>

          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-6">
                      <div id="map" style="height:570px"></div>
                    </div>
                    <div class="col-6">
                      <div class="row">
                      <form class="forms-sample row">
                        <div class="form-group col-12">
                      <label for="exampleInputUsername2" class="col-form-label">รูปสถานที่</label>
                        <div class="input-group ">
                          <div class="input-group-prepend">
                             <span class="input-group-text">Upload</span>
                          </div>
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" id="inputGroupFile01">
                             <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                          </div>
                      </div>
  
                      </div>

                        <div class="form-group col-6">
                          <label for="exampleInputUsername1">รหัสสถานที่</label>
                          <input type="text" class="form-control" id="exampleInputUsername1" placeholder="รหัส">
                        </div>
                         <div class="form-group col-6">
                          <label for="exampleInputUsername1">ชื่อสถานที่</label>
                          <input type="text" class="form-control" id="exampleInputUsername1" placeholder="คู้บอน 27 แยก 15">
                        </div>
                       
                       
                        <div class="form-group col-6">
                          <label for="exampleInputEmail1">พิกัดละติจูด ( Latitude )</label>
                          <input type="text" class="form-control" id="laText" value="13.760931" placeholder="13.7538739" name="La">

                        </div>
                        <div class="form-group col-6">
                          <label for="exampleInputPassword1">พิกัดลองจิจูด ( Longitude )</label>
                          <input type="text" class="form-control" id="loText" value="100.634258" placeholder="100.5146209" name="Lo">

                        </div>

                        <div class="form-group col-12">
                          <label for="rangeInput">กำหนดระยะในการใช้งาน</label>
                          <input id="range" type="range" name="rangeInput" min="10" max="1000" style="width: 100%" step="10" value="100" />
                          <p id="result"></p>
                          <script>
                            var p = document.getElementById("range"),
                            res = document.getElementById("result");
                            res.innerHTML =  p.value +"&nbsp; "+"เมตร";
                            p.addEventListener("input", function() {res.innerHTML =  p.value +"&nbsp; "+"เมตร";}, false);
                            p.addEventListener("change", function() {GetGEO();}, false);
                          </script>

                        </div>
                         <div class="form-group  col-6">
                          <label>Account</label>
                          <select class="js-example-basic-single w-100">
                            <option value="AL">7-11</option>
                            <option value="WY">Makro</option>
                          </select>
                        </div>
                       
                        <div class="form-group col-6">
                          <label>พื้นที่</label>
                          <select class="js-example-basic-single w-100">
                            <option value="AL">HubBKK</option>
                            <option value="WY">Makro</option>
                          </select>
                        </div>
                        <div class="form-group col-6">
                          <label>เขตพื้นที่</label>
                          <select class="js-example-basic-single w-100">
                            <option value="AL">BK1</option>
                            <option value="WY">Makro</option>
                          </select>
                        </div>
                        <div class="form-group col-6">
                          <label>ช่องทาง</label>
                          <select class="js-example-basic-single w-100">
                            <option value="AL">CVS</option>
                            <option value="WY">Makro</option>
                          </select>
                        </div>
                        <div class="col-12">
                          <div class="row">
                            <a href="location-all.php" class="ml-auto"><button type="submit" class="btn btn-primary btn-sm mr-2 ">บันทึก</button></a>
                            <a href="location-all.php"><button class="btn btn-light btn-sm mr-2">ยกเลิก</button></a>
                          </div>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          




          

        </div>
        <!-- content-wrapper ends -->
        <!-- copy right -->
       <?php include('copyright.php') ?>
        <!-- end copy right -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
<?php include('footer.php') ?>
<!-- partial -->
 <!--script type="text/javascript" src="assets/js/js"></script>
  <script type="text/javascript" src="assets/js/gmaps.js"></script-->
  <script type="text/javascript" src="assets/js/prettify.js"></script>
  <script src="vendors/nouislider/nouislider.min.js"></script>
  <script src="vendors/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
  <script src="js/no-ui-slider.js"></script>
  <script src="js/ion-range-slider.js"></script>

</body>

</html>




  <!-- Custom js for this page-->
<script src="js/gmap.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChL0G7E3QQ3EzvcjoHqbxHzSFwlVjxslQ&callback=initMap"async defer></script>
<!--script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5Bcaf99cC1FELKjMQYJ3mJ6OGusDTIk0&language=th"></script-->

<!--script src='https://rawgit.com/Logicify/jquery-locationpicker-plugin/master/dist/locationpicker.jquery.js'></script-->
<!--script  src="js/googlemap.js"></script>
<script src="js/google-maps.js"></script-->

<script>
/*function editr(tk,i){
  var n = $("#nameplace"+i).val();
  var r = $("#range"+i).val();
  var La = $("#TolaText-"+i).val();
  var Lo = $("#ToloText-"+i).val();
  console.log(La);
  console.log(Lo);

   $.post( "save_place.php", { etk: 1, tk: tk, r:r, n:n, La:La, Lo:Lo })
    .done(function( data ) {
      console.log(data);
      if(data=='แก้ไขข้อมูลสำเร็จ') $(".tkname"+i).html(n);
      alert(data);
    });

}*/
function GetGEO() {
   
  /*if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
        position.coords.latitude
        position.coords.longitude
        initMap(position.coords.latitude+', '+position.coords.longitude);
    });
  }
  else{

  }*/
  
  //initMap($('#locationText').val());

  initMap($('#laText').val()+','+$('#loText').val());
  console.log("Get GEO");
  console.log("Lat val ="+ $('#laText').val());
}
/*function senttk(detk,tkid,tkn,tklo) {
  if(confirm('ต้องการลบสถานที่: '+tkn+'\nพิกัด: '+tklo)){
    $.post( "save_place.php", { dtk: 1, tk: detk, tkid: tkid})
    .done(function( data ) {
      console.log(data);
      alert(data);
      window.location.reload();
    });
  }


}*/

function getgeolocation(){
  $('.clo').html('<i class="fa fa-location-arrow"></i> รอสักครู่...');
  if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          initMap(position.coords.latitude+', '+position.coords.longitude);
          console.log(position.coords.latitude+', '+position.coords.longitude);
          $('.clo').html('<i class="fa fa-location-arrow"></i>  ตำแหน่งปัจจุบัน (คร่าวๆ)');
          $('#laText').val(position.coords.latitude);
          $('#loText').val(position.coords.longitude);
          $('#locationText').val(position.coords.latitude+', '+position.coords.longitude);
        });
  }
  else{
    $('.clo').html('<i class="fa fa-location-arrow"></i>  ดึงตำแหน่งไม่ได้');
  }
}

var marker;
var sunCircle;
var map;
var map2;
var cen;
function initMap(location,d,t,rr) {

    console.log("init Map");

    var str='';
    var str2='';
    if(typeof rr === "undefined" || rr==''){
            var r = parseInt(document.getElementById("range").value);
          }
    else{
       var r = parseInt(rr);
    }
    if( r >= 800 ){
      z = 14;
    }
    else if( r >= 400  && r <= 800  ){
      z = 15;
    }
    else if(  r >= 200  && r <= 400  ){
      z = 16;
    }
    else if(  r >= 100  && r <= 200  ){
      z = 17;
    }
    else if(  r >= 50  && r <= 100  ){
      z = 18;
    }
    else if(  r >= 25  && r <= 50  ){
      z = 19;
    }
    else{
      z = 20;
    }

    if(typeof location === "undefined" || location==''){
      str = "13.674351, 100.517556";
      str2 = "13.674351,100.517556";
    }
    else{
       str = location;
       str2 = location;
    }
    if(typeof d === "undefined" ){
      d=true;
    }
    else{

      d=false;
    }
  if(typeof t === "undefined" || t == ''){
    t=false;
  }
  else{
    t=true;
  }
  //console.log(t);
  str = str.split(", ").map(function(t){return parseFloat(t)});
  str2 = str2.split(",").map(function(t){return parseFloat(t)});
  if(str.length==1 && str2.length==2){
    str = str2;
  }
  cen = {lat: str[0], lng: str[1]};

    if(t){
      var locations = [
    ["บริษัทเดอะเบสท์ โปรโมชั่น จำกัด(สำนักงานใหญ่)", 13.760931, 100.634258, 50]
     ];

   map = new google.maps.Map(document.getElementById('map'), {
     center: {lat: locations[0][1], lng: locations[0][2]},
     zoom: 15
   });
      var infowindow = new google.maps.InfoWindow();
      var marker, i;
      for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          animation: google.maps.Animation.DROP,
          map: map
        });
        console.log(locations[i][3]);
        var cityCircle = new google.maps.Circle({
          strokeColor: '#FF0000',
          strokeOpacity: 0.5,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.1,
          map: map,
          center: new google.maps.LatLng(locations[i][1], locations[i][2]),
          radius: parseInt(locations[i][3])
        });
        cityCircle.bindTo('center', marker, 'position')

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));
      }
    }else{

      map = new google.maps.Map(document.getElementById('map'), {
        center: cen,
        zoom: z
      });
      marker = new google.maps.Marker({
        position: cen,
        draggable: d,
        animation: google.maps.Animation.DROP, //BOUNCE
        map: map
      });
      sunCircle = {
          strokeColor: "#306EFF",
          strokeOpacity: 0.5,
          strokeWeight: 1.5,
          fillColor: "#1589FF",
          fillOpacity: 0.1,
          map: map,
          center: cen,
          radius: r // in meters
      };
      cityCircle = new google.maps.Circle(sunCircle);
      cityCircle.bindTo('center', marker, 'position');

    }
        marker.addListener('drag', handleEvent);
        marker.addListener('dragend', handleEvent);
      }
     

    function handleEvent(event) {
      $('#laText').val(event.latLng.lat());
      $('#loText').val(event.latLng.lng());
      $('#locationText').val(event.latLng.lat()+', '+event.latLng.lng());
    }
    function handleEvent2(event,i) {

      $('#laText-'+i+', #TolaText-'+i).val(event.latLng.lat());
      $('#loText-'+i+', #ToloText-'+i).val(event.latLng.lng());

  }

</script>


  <!-- End custom js for this page-->


          

