<!DOCTYPE html>
<html lang="en">
<?php include('header.php') ?>

<body class="sidebar-fixed">
  <div class="container-scroller">
    <!-- topbar -->
    <?php include('topbar.php') ?>
    <!-- end topbar -->
    <div class="container-fluid page-body-wrapper">
      <?php include('sidebar.php') ?>
      <!-- end sidebar -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->
            
            <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
              <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">เพิ่มกลุ่มย่อย</h4>
                <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">เพิ่มกลุ่มย่อย</p>
                </div>
              </div>
              <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                
                <a href="company-type.php" class="ml-auto"><button class="btn btn-outline-primary btn-sm  ml-auto">ยกเลิก</button></a> 
                          <a href="company-type.php"><button class="btn btn-primary btn-sm mr-3  ml-2">บันทึก</button></a> 
                
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  

                  <div class="row">
                        <div class="col-12">
                          <form class="needs-validation" novalidate="">
                              <div class="form-row">
                                
                                <div class="col-12 mb-3">
                                  <label for="validationCustom02">อยู่ในประเภทพนักงาน Type</label>
                                  <select class="form-control">
                                    <option>BA</option>
                                    <option>BA Week</option>
                                    <option>MC BAT</option>
                                    <option>PI</option>
                                    <option>MC</option>
                                  </select>
                                </div>
                                
                                <div class="col-12 mb-3">
                                  <label for="validationCustom01">ชื่อประเภทพนักงานกลุ่มย่อย Subtype</label>
                                  <input type="text" class="form-control" id="validationCustom01" placeholder="ชื่อบริษัทภาษาไทย" value="TBA" required="">
                                </div>

                                 <div class="col-12 mb-3">
                                  <label for="validationCustom01">หมายเหตุ</label>
                                  <input type="textbox" class="form-control" id="validationCustom01" placeholder="" value="" required="">
                                </div>
                                
                                
                              </div>
                          </form>
                          <?php include('user_condition.php') ?>
                        </div>
                      </div>

                      <div class="row mt-3">
                           <a href="company-type.php" class="ml-auto"><button class="btn btn-outline-primary btn-sm ml-auto">ยกเลิก</button></a> 
                          <a href="company-type.php"><button class="btn btn-primary btn-sm mr-3 ml-2">บันทึก</button></a> 
                      </div>

                </div>
              </div>
            </div>
          </div>



          

        </div>
        <!-- content-wrapper ends -->
        <!-- copy right -->
       <?php include('copyright.php') ?>
        <!-- end copy right -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
<?php include('footer.php') ?>

</body>

</html>

