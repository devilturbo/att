<!DOCTYPE html>
<html lang="en">
<?php include('header.php') ?>

<body class="sidebar-fixed" >
  <div class="container-scroller" >
    <!-- topbar -->
    <?php include('topbar.php') ?>
    <!-- end topbar -->
    <div class="container-fluid page-body-wrapper" >
      <?php include('sidebar.php') ?>
      <!-- end sidebar -->
      <div class="main-panel" >
        <div class="content-wrapper" >
          
          <div class="dashboard-header d-flex flex-column grid-margin"><!-- header -->
            <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-1">
              <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">Attendant</h4>
                <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">Home</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">Dashboard</p>
                </div>
              </div>



              <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">

                <div class="btn-group mt-3 mt-md-0" role="group" aria-label="Button group">
                  <button type="button" class="btn btn-outline-secondary border">
                    <i class="mdi mdi-magnify text-body"></i>
                  </button>
                  <button type="button" class="btn btn-outline-secondary border">
                    <i class="mdi mdi-reload text-body"></i>
                  </button>
                </div>
              </div>
            </div>
          </div><!-- end header -->
          <!-- Main content --->
          <div class="row mb-2">
              <p class=" ml-2 mt-2 text-muted">Last Sync 12/08/19 - 20:03:39</p>
              <span class="ml-auto mr-3" style="width: 350px;">
                  <div class="input-group input-daterange">
                      <input type="text" id="min-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น" >
                      <div class="input-group-addon mr-2 ml-2"> ถึง </div>
                      <input type="text" id="max-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="สิ้นสุด" >
                      <button type="submit" id="search" class="btn btn-primary btn-sm ml-2">ค้าหา</button>
                    </div>
              </span>
          </div>

          <div class="row mb-4">
            <div class="col-3 ">
              <div class="card shadow-sm">
                <div class="card-body">
                  <div class="row">
                    <p class="card-title ml-3 mt-1 mb-3 text-muted ">พนักงานทั้งหมด</p>
                    <nav class="ml-auto" >
                      <ul class="navbar-nav mr-2">
                        <li class="dropdown" >
                          <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                          </a>
                          <div class="dropdown-menu dropdown-menu-right animated--fade-in" aria-labelledby="navbarDropdown" >
                            <a class="dropdown-item" href="user_all.php">ดูรายละเอียด</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item bg" href="#"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>Export รายงาน</a>
                          </div>
                        </li>
                      </ul>
                    </nav>
                  </div>
                  <div class="row">
                    <span class="ml-3"><span class="h2 mr-2">350</span>คน</span>
                    <span class="badge badge-primary ml-auto mr-2 mt-1 font-weight-bold" style="height: 20px; padding: 5px;">32.1%</span>
                    <!--p class="mb-0 tx-13 text-muted ml-3 mt-3">จำนวนพนักงานที่มีอยู่ในระบบทั้งหมด</p-->
                  </div>
                </div>
              </div>
            </div>

            <div class="col-3">
              <div class="card shadow-sm">
                <div class="card-body">
                  <div class="row">
                    <p class="card-title ml-3 mt-1 mb-3 text-muted ">พนักงานที่ใช้งานวันนี้</p>
                    <nav class="ml-auto" >
                      <ul class="navbar-nav mr-2">
                        <li class="dropdown" >
                          <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                          </a>
                          <div class="dropdown-menu dropdown-menu-right animated--fade-in" aria-labelledby="navbarDropdown" >
                            <a class="dropdown-item" href="user_workday.php">ดูรายละเอียด</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item bg" href="#"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>Export รายงาน</a>
                          </div>
                        </li>
                      </ul>
                    </nav>
                  </div>
                  <div class="row">
                    <span class="ml-3"><span class="h2 mr-2">150</span>คน</span>
                    <span class="badge badge-success ml-auto mr-2 mt-1 font-weight-bold" style="height: 20px; padding: 5px;">32.1%</span>
                    <!--p class="mb-0 tx-13 text-muted ml-3 mt-3">จำนวนพนักงานที่มีการเช็คอินเข้างาน</p-->
                  </div>
                </div>
              </div>
            </div>

            <div class="col-3">
              <div class="card shadow-sm">
                <div class="card-body">
                  <div class="row">
                    <p class="card-title ml-3 mt-1 mb-3 text-muted ">พนักงานแจ้งปรับปรุงเวลา</p>
                    <nav class="ml-auto" >
                      <ul class="navbar-nav mr-2">
                        <li class="dropdown" >
                          <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                          </a>
                          <div class="dropdown-menu dropdown-menu-right animated--fade-in" aria-labelledby="navbarDropdown" >
                            <a class="dropdown-item" href="user_changetime.php">ดูรายละเอียด</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item bg" href="#"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>Export รายงาน</a>
                          </div>
                        </li>
                      </ul>
                    </nav>
                  </div>
                  <div class="row">
                    <span class="ml-3"><span class="h2 mr-2">12</span>คน</span>
                    <span class="badge badge-info ml-auto mr-2 mt-1 font-weight-bold" style="height: 20px; padding: 5px;">0.1%</span>
                    <!--p class="mb-0 tx-13 text-muted ml-3 mt-2">จำนวนการเช็คอินและเช็คเอ้าท์</p-->

                  </div>
                </div>
              </div>
            </div>
            <div class="col-3">
              <div class="card shadow-sm">
                <div class="card-body">
                  <div class="row">
                    <p class="card-title ml-3 mt-1 mb-3 text-muted ">พนักงานที่ลา</p>
                    <nav class="ml-auto" >
                      <ul class="navbar-nav mr-2">
                        <li class="dropdown" >
                          <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                          </a>
                          <div class="dropdown-menu dropdown-menu-right animated--fade-in" aria-labelledby="navbarDropdown" >
                            <a class="dropdown-item" href="user_leave.php">ดูรายละเอียด</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item bg" href="#"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>Export รายงาน</a>
                          </div>
                        </li>
                      </ul>
                    </nav>
                  </div>
                  <div class="row">
                    <span class="ml-3"><span class="h2 mr-2">20</span>คน</span>
                    <span class="badge badge-danger ml-auto mr-2 mt-1 font-weight-bold" style="height: 20px; padding: 5px;">.5%</span>
                    <!--p class="mb-0 tx-13 text-muted ml-3 mt-3">พนักงานที่ได้รับการอนุมัติให้ลาได้</p-->
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row mb-4">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                  <span class="ml-2 mt-2"><span class="h5">12/09/19 - 12:10:11</span></span>
                  <nav class="ml-3 mt-1">
                      <ul class="navbar-nav mr-3">
                        <li class="dropdown" >
                          <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                          </a>
                          <div class="dropdown-menu dropdown-menu-right animated--fade-in" aria-labelledby="navbarDropdown" >
                            <a class="dropdown-item" href="all_user_working.php">วันนี้</a>
                            <a class="dropdown-item" href="all_user_working.php">7วัน</a>
                            <a class="dropdown-item" href="all_user_working.php">30วัน</a>
                            <a class="dropdown-item" href="all_user_working.php">12เดือน</a>
                            
                          </div>
                        </li>
                      </ul>
                    </nav>

                      <a href="user_profile_add.php" class="ml-auto"><button class="btn btn-primary btn-sm mr-2 ">เพิ่มพนักงานใหม่</button></a> 
                      <a href="user_workday_map.php"><button type="button" class="btn btn-outline-secondary btn-sm  border">แผนที่</button></a> 
                      <a href="user-working-today.php"><button  class="btn btn-outline-secondary border btn-sm ml-2 mr-3" >ดูทั้งหมด</button></a> 
               
                  </div>
                  <div class="visitors-chart-wrapper" >
                    <div id="visitorsFlotChart" class="visitors-flot-chart" style="height: 400px;"></div>
                  </div>

                  
                </div>
              </div>
            </div>
           
          </div>
          <div class="row ">
             <div class="col-12 mb-4">
              <div class="card">
                <div class="card-body">
               

                <div class="row mb-2">
                  <span class="ml-2 mt-3"> บันทึกเข้า-ออก วันนี้ 12/09/19 - 12:10:11</span>
                  <div class="col-sm-3 ml-auto">
                        <span>ค้นหา</span>
                        <input type="text" class="form-control global_filter" id="global_filter" placeholder="คำที่ค้นหา">
                  </div>
                  <div class="col-3">
                    <div class="row">
                      <div class="col" ><span id="ตำแหน่ง"></span></div>
                      <div class="col" ><span id="ประเภท"></span></div>
                      <div class="col-auto">
                      <button type="button" class="btn btn-outline-primary btn-sm ml-auto bt-filter" style="margin-top: 20px;" data-toggle="modal" data-target="#ModalFilter">Filter</button>
                      </div>
                    </div>
                  </div>
                </div>

                  <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                      <tr>
                        <th>รูป</th>
                        <th>รหัสพนักงาน</th>
                        <th>ชื่อ-นามสกุล</th>
                        <th>เวลา</th>
                        <th>สถานนะ</th>
                        <th>สถานที่</th>
                        <th>แผนก</th>
                        <th>ตำแหน่ง</th>
                        <th>ประเภท</th>
                        <th>ประเภทย่อย</th>
                        <th>วันที่</th>
                        <th>ผู้ดูแล</th>
                        <th>พื้นที่</th>
                        <th>เขตพื้นที่</th>
                        <th>ช่องทาง</th>
                        <th>ห้างร้าน</th>
                        <th>ดำเนินการ</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/2.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกเข้า</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>11/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/3.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกเข้า</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>9/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/1.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>

                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/user/thumb/thumb.jpg" align="left">
                        </td>
                        <td>123435245</td>
                        <td>คมศักดิ์ รัชนีกร</td>
                        <td>12:00</td>
                        <td>บันทึกออก</td>
                        <td>Makro/8123/แม็คโครบางใหญ่</td>
                        <td>UT</td>
                        <td>Straff</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>19/10/19</td>
                        <td>มานิตย์ รุ่งเรือง</td>
                        <td>Hub BKK</td>
                        <td>BKK1</td>
                        <td>CVS</td>
                        <td>7-11</td>
                        <td>
                          <a href="user-profile-workday.php">
                            <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
                          </a>
                        </td>
                      </tr>





                   





                                    
                                
                    </tbody>
                  </table>

                </div>

              </div>
            </div>

            <!--div class="col-6">
              <div class="card">
                <div class="card-body">คำขอลาต่างๅ</div>
              </div>
            </div-->

          </div>

           
          <!-- End Main content --->

        </div>
        <!-- content-wrapper ends -->
        <!-- copy right -->
       <?php include('copyright.php') ?>
        <!-- end copy right -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
<?php include('footer.php') ?>

</body>

</html>
<div id="myModal" class="modal modaldetail" role="dialog">
  <div class="modal-dialog" >
    <div class="modal-content">
      <div class="modal-header">
        <span>รายละเอียด</span>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        
      </div>
    </div>

  </div>
</div>

<div id="ModalFilter" class="modal" role="dialog">
                    <div class="modal-dialog" >
                    <!-- Modal content-->
                      <div class="modal-content">
                          <div class="modal-header">
                              <span>ค้นหาโดยละเอียด</span>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                          <div class="col-12 p-4">
                            <div class="row">
                              <div class="col-6 mb-2" ><span id="ตำแหน่ง"></span></div>
                              <div class="col-6 mb-2" ><span id="ประเภท"></span></div>
                              <div class="col-6 mb-2" ><span id="แผนก"></span></div>
                              <div class="col-6 mb-2" ><span id="ตำแหน่ง"></span></div>
                              <div class="col-6 mb-2" ><span id="ประเภท"></span></div>
                              <div class="col-6 mb-2" ><span id="ประเภทย่อย"></span></div>
                              <div class="col-6 mb-2"><span id="พื้นที่"></span></div>
                              <div class="col-6 mb-2"><span id="ช่องทาง"></span></div>
                              <div class="col-6 mb-2"><span id="ห้างร้าน"></span></div>
                              <div class="col-6 mb-2"><span id="สถานะ"></span></div>
                            </div>
        
                          </div>
                      </div>
                    </div>
                  </div>

<script type="text/javascript">
    function filterGlobal () {
    $('#dataTable').DataTable().search(
        $('#global_filter').val()

       ).draw();
    }
 
    $(document).ready(function() {
       var columnsToSearch = {
        6: 'แผนก',
        7: 'ตำแหน่ง',
        8: 'ประเภท',
        9: 'ประเภทย่อย',
        12: 'พื้นที่',
        13: 'เขตพื้นที่',
        14: 'ช่องทาง',
        15: 'ห้างร้าน',
        4: 'สถานะ'
    };

    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging:   true,
        ordering: false,
        info:     false,
        scrollY: 500,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: false,
        fixedHeader: true,
        initComplete: function () {
            
            for (var i in columnsToSearch) {
                var api = this.api();
                var select = $('<select class="form-control"><option value="">Select</option></select>')
                    .appendTo($('#' + columnsToSearch[i]).empty().text(columnsToSearch[i] + ' '))

                    .attr('data-col', i)
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        api.column($(this).attr('data-col'))
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                api.column(i).data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>');
                });
            }

        }

    });

  var colum = ["รูป", "รหัสพนักงาน", "บัตรประชาชน","คำนำหน้า", "เพศ", "เบอร์โทรศัพท์","อีเมล", "เอเจนซี่", "แผนก","ตำแหน่ง","ประเภท","ประเภทย่อย","วันที่","ผู้ดูแล","พื้นที่","เขตพื้นที่","ช่องทาง","ห้างร้าน","สถานที่","วันที่เริ่มงาน","วันสิ้นสุดงาน","อายุงาน","สถานนะ","ดำนเนินการ"];
   //Edit row buttons
   $('.dt-edit').each(function () {
    $(this).on('click', function(evt){
      $this = $(this);
      var dtRow = $this;
      $('div.modal-body').innerHTML='';
      $('div.modal-body').append( '<div class="row mb-4 ml-1 img-thumbmail lazy">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=1; i < dtRow[0].cells.length; i++){
         $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">'+colum[i]+'</span><span class="col-8">'+dtRow[0].cells[i].innerHTML+'</span><div/>');
            console.log(i);
      }
      $('#myModal').modal('show');
    });
  });
  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .modal-body').empty();
  });

 
  $('.bt-filter').on('click', function(evt){
    console.log("filter");


   });


    $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
       var min = $('#min-date').val();
       var max = $('#max-date').val();
       var createdAt = data[2] || 0; 
        if (
           (min == "" || max == "") ||
           (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
         ) {
            return true;
        }
            return false;
        }
    );

    $('.date-range-filter').change(function() {
      table.draw();
    });
    $('#my-table_filter').hide();

    $('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );

} );
</script>


