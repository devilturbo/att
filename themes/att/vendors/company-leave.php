<!DOCTYPE html>
<html lang="en">
<?php include('header.php') ?>

<body class="sidebar-fixed">
  <div class="container-scroller">
    <!-- topbar -->
    <?php include('topbar.php') ?>
    <!-- end topbar -->
    <div class="container-fluid page-body-wrapper">
      <?php include('sidebar.php') ?>
      <!-- end sidebar -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->
            
            <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
              <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">วันลา</h4>
                <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าหลัก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">ตั้งค่าบริษัท</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">วันลา</p>
                </div>
              </div>
              <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                
               
                <!--button class="btn btn-primary btn-sm mr-3 d-none d-md-block">บันทึก</button>
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">ยกเลิก</button-->
              </div>
            </div>

          </div>

          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <div class="table-wrapper">
            <div class="table-title mb-3">
                <div class="row">
                    <div class="col-sm-8"><h5 class="mt-2">รายการทั้งหมด</b></h5></div>
                    <div class="ml-auto">
                        <button type="button" class="btn btn-outline-primary btn-sm add-new mr-3">เพิ่มข้อมูล</button>

                    </div>
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>วันลา</th>
                        <th>จำนวน(วัน)</th>
                        <th>ดำเนินการ</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                      <td>1</td>
                        <td>วันลากิจ</td>
                        <td>20</td>
                        <td>
                            <a class="add" title="Add" data-toggle="tooltip"><i class="fas fa-plus-square"></i></a>
                            <a class="edit" title="Edit" data-toggle="tooltip"><i class="fas fa-edit"></i></a>
                            <a class="delete" title="Delete" data-toggle="tooltip"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>

                    <tr>
                      <td>2</td>
                        <td>วันลาป่วย</td>
                        <td>30</td>
                        <td>
                            <a class="add" title="Add" data-toggle="tooltip"><i class="fas fa-plus-square"></i></a>
                            <a class="edit" title="Edit" data-toggle="tooltip"><i class="fas fa-edit"></i></a>
                            <a class="delete" title="Delete" data-toggle="tooltip"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>

                    <tr>
                      <td>3</td>
                        <td>วันลาพักร้อน</td>
                        <td>6</td>
                        <td>
                            <a class="add" title="Add" data-toggle="tooltip"><i class="fas fa-plus-square"></i></a>
                            <a class="edit" title="Edit" data-toggle="tooltip"><i class="fas fa-edit"></i></a>
                            <a class="delete" title="Delete" data-toggle="tooltip"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>

                    <tr>
                      <td>4</td>
                        <td>วันลาคลอด</td>
                        <td>60</td>
                        <td>
                            <a class="add" title="Add" data-toggle="tooltip"><i class="fas fa-plus-square"></i></a>
                            <a class="edit" title="Edit" data-toggle="tooltip"><i class="fas fa-edit"></i></a>
                            <a class="delete" title="Delete" data-toggle="tooltip"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>

                    <tr>
                      <td>5</td>
                        <td>วันลาบวช</td>
                        <td>15</td>
                        <td>
                            <a class="add" title="Add" data-toggle="tooltip"><i class="fas fa-plus-square"></i></a>
                            <a class="edit" title="Edit" data-toggle="tooltip"><i class="fas fa-edit"></i></a>
                            <a class="delete" title="Delete" data-toggle="tooltip"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>

                    <tr>
                      <td>6</td>
                        <td>วันลาเรียกฝึกกำลังพล</td>
                        <td>60</td>
                        <td>
                            <a class="add" title="Add" data-toggle="tooltip"><i class="fas fa-plus-square"></i></a>
                            <a class="edit" title="Edit" data-toggle="tooltip"><i class="fas fa-edit"></i></a>
                            <a class="delete" title="Delete" data-toggle="tooltip"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>

                    <tr>
                      <td>7</td>
                        <td>วันลาทำหมัน</td>
                        <td>60</td>
                        <td>
                            <a class="add" title="Add" data-toggle="tooltip"><i class="fas fa-plus-square"></i></a>
                            <a class="edit" title="Edit" data-toggle="tooltip"><i class="fas fa-edit"></i></a>
                            <a class="delete" title="Delete" data-toggle="tooltip"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>
                    
                </tbody>


            </table>
            <div class="row">
                <div class="ml-auto mr-2 mt-3"><button class="btn btn-sm btn-primary ml-auto" type="submit">บันทึกข้อมูล</button></div>
            </div>
                </div>
              </div>
            </div>
          </div>





          

        </div>
        <!-- content-wrapper ends -->
        <!-- copy right -->
       <?php include('copyright.php') ?>
        <!-- end copy right -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
<?php include('footer.php') ?>

</body>

</html>

   
<script type="text/javascript">
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
  var actions = $("table td:last-child").html();
  // Append table with add row form on add new button click
    $(".add-new").click(function(){
    $(this).attr("disabled", "disabled");
    var index = $("table tbody tr:last-child").index();
        var row = '<tr>' +
            '<td><input type="text" class="form-control" name="name" id="name"></td>' +
            '<td><input type="text" class="form-control" name="department" id="department"></td>' +
            '<td><input type="text" class="form-control" name="phone" id="phone"></td>' +
      '<td>' + actions + '</td>' +
        '</tr>';
      $("table").append(row);   
    $("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
    });
  // Add row on add button click
  $(document).on("click", ".add", function(){
    var empty = false;
    var input = $(this).parents("tr").find('input[type="text"]');
        input.each(function(){
      if(!$(this).val()){
        $(this).addClass("error");
        empty = true;
      } else{
                $(this).removeClass("error");
            }
    });
    $(this).parents("tr").find(".error").first().focus();
    if(!empty){
      input.each(function(){
        $(this).parent("td").html($(this).val());
      });     
      $(this).parents("tr").find(".add, .edit").toggle();
      $(".add-new").removeAttr("disabled");
    }   
    });
  // Edit row on edit button click
  $(document).on("click", ".edit", function(){    
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
      $(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
    });   
    $(this).parents("tr").find(".add, .edit").toggle();
    $(".add-new").attr("disabled", "disabled");
    });
  // Delete row on delete button click
  $(document).on("click", ".delete", function(){
        $(this).parents("tr").remove();
    $(".add-new").removeAttr("disabled");
    });
});
</script>



