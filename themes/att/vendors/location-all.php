<!DOCTYPE html>
<html lang="en">
<?php include('header.php') ?>
<body class="sidebar-fixed">
  <div class="container-scroller">
    <!-- topbar -->
    <?php include('topbar.php') ?>
    <!-- end topbar -->
    <div class="container-fluid page-body-wrapper">
      <?php include('sidebar.php') ?>
      <!-- end sidebar -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->
            
            <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
              <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">สถานที่ทั้งหมด</h4>
                <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">สถานที่ทั้งหมด</p>
                </div>
              </div>
              <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button>
              </div>
            </div>
          </div>
          <div class="row mb-4">
            <div class="col-12">
              <ul class="nav nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" href="location-all.php">สถานที่ทั้งหมด</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="location-add.php">เพิ่มสถานที่</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="location-import.php">Import สถานที่</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="location-group.php">กลุ่มสถานที่</a>
                </li>

              </ul>
             </div>
          </div>

          <div class="row">
            <div class="col-12 mb-4">
              <div class="row ">
                <div class="col-2 ml-auto"></div>
                  <div class="col-2"></div>
                </div>
                                    
                <form>
                  <div class="form-row align-items-center mb-2">
                      <div class="col-sm-3 my-1-">
                        <span>ค้นหา</span>
                        <input type="text" class="form-control global_filter" id="global_filter" placeholder="คำที่ค้นหา">
                      </div>
                      <div class="col-3">
                        <div class="row">
                          <div class="col" ><span id="Account"></span></div>
                          <div class="col" ><span id="ช่องทาง"></span></div>
                          <div class="col-auto">
                            <button type="button" class="btn btn-outline-primary btn-sm ml-auto bt-filter" style="margin-top: 20px;" data-toggle="modal" data-target="#ModalFilter">Filter</button>
                          </div>
                        </div>
                      </div>
                      

                      <div class="col-auto my-1 ml-auto" style="width: 350px;">
                          <span>ค้นหาจากช่วงเวลา</span>
                          <div class="input-group input-daterange">
                            <input type="text" id="min-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น" >
                            <div class="input-group-addon mr-2 ml-2"> ถึง </div>
                            <input type="text" id="max-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="สิ้นสุด" >
                            <button id="search" class="btn btn-primary btn-sm ml-2">ค้าหา</button>
                          </div>
                      </div>
                  </div>
                  
                  <!-- Modal -->
                  <div id="ModalFilter" class="modal" role="dialog">
                    <div class="modal-dialog" >
                    <!-- Modal content-->
                      <div class="modal-content">
                          <div class="modal-header">
                              <span>ค้นหาโดยละเอียด</span>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                          <div class="col-12 p-4">
                            <div class="row">
                              <div class="col-6 mb-2" ><span id="เขตพื้นที่"></span></div>
                              <div class="col-6 mb-2"><span id="พื้นที่"></span></div>
                              <div class="col-6 mb-2"><span id="ช่องทาง"></span></div>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>

                  
                </form>
                                    
            </div>
          </div>

          <div class="row">
            <div class="col-12">
              <h5 class="font-weight-bold mb-4">วันพุธที่ 6 พฤศจิกายน 2562</h5>
              <div class="card">
                <div class="card-body">


                  <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                      <tr>
                        <th>รูป</th>
                        <th>รหัสสาขา</th>
                        <th>Account</th>
                        <th>ชื่อ</th>
                        <th>พิกัดละติจูด</th>
                        <th>พิกัดลองจิจูด</th>
                        <th>ระยะใช้งาน</th>
                        <th>พื้นที่</th>
                        <th>เขตพื้นที่</th>
                        <th>ช่องทาง</th>
                        <th>เพิ่มโดย</th>
                        <th>ดำเนินการ</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="dt-edit">
                        <td class="employee-col">
                          <img class="thumb mr-2 lazy" src="images/location/1.jpg" align="left">
                        </td>
                        <td>14071</td>
                        <td>7-11</td>
                        <td>คู้บอน 27 แยก 15</td>
                        <td>13.8396538</td>
                        <td>100.661831</td>
                        <td id="range" value="13.760931">100</td>
                        <td>HubBKK</td>
                        <td>BK1</td>
                        <td>CVS</td>
                        <td>กู้ชาติ พัฒภักดิ์</td>
                        <td>
                          <a href="location-edit.php"><button class="btn btn-outline-primary btn-sm mb-1">แก้ไข</button></a>
                          <button class="btn btn-outline-primary btn-sm mb-1">ลบ</button>
                          
                        </td>
                      </tr>

                    





                   





                                    
                                
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>




          

        </div>
        <!-- content-wrapper ends -->
        <!-- copy right -->
       <?php include('copyright.php') ?>
        <!-- end copy right -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
<?php include('footer.php') ?>
<!-- partial -->
 <!--script type="text/javascript" src="assets/js/js"></script>
  <script type="text/javascript" src="assets/js/gmaps.js"></script>
  <script type="text/javascript" src="assets/js/prettify.js"></script-->

<script src="js/gmap.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChL0G7E3QQ3EzvcjoHqbxHzSFwlVjxslQ&callback=initMap"async defer></script>

</body>

</html>

          <!-- Modal -->
<div id="myModal" class="modal modaldetail" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <span>รายละเอียด</span>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-7 mappanel">

                    <!--div id="map-with-marker" class="google-map"></div-->
                    <div id="map" style="height:570px"></div>

                   <!--input id="range" type="range" name="rangeInput" min="10" max="1000" style="width: 100%" step="10" value="100" /-->

            </div>
          <div class="col-3 mapdetail"></div>
        </div>
        
        
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div-->
    </div>

  </div>
</div>
<!--script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChL0G7E3QQ3EzvcjoHqbxHzSFwlVjxslQ&callback=initMap"async defer></script-->

<script type="text/javascript">
    function filterGlobal () {
    $('#dataTable').DataTable().search(
        $('#global_filter').val()

       ).draw();
    }
 
    $(document).ready(function() {
       var columnsToSearch = {
        2: 'Account',
        7: 'พื้นที่',
        8: 'ช่องทาง',
        4: 'เขตพื้นที่'
    };

    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging:   true,
        ordering: false,
        info:     false,
        scrollY: 300,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: false,
        fixedHeader: true,
        initComplete: function () {
            for (var i in columnsToSearch) {
                var api = this.api();
                var select = $('<select class="form-control"><option value="">Select</option></select>')
                    .appendTo($('#' + columnsToSearch[i]).empty().text(columnsToSearch[i] + ' '))

                    .attr('data-col', i)
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        api.column($(this).attr('data-col'))
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                api.column(i).data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>');
                });
            }

        }

    });

    var colum = ["รูป", "รหัสสาขา", "Account","ชื่อ", "พิกัดละติจูด", "พิกัดลองจิจูด","ระยะใช้งาน", "พื้นที่","เขตพื้นที่","ช่องทาง","เพิ่มโดย","ดำเนินการ" ];
    
    $('.dt-edit').each(function () {
      $(this).on('click', function(evt){
      $this = $(this);
      var dtRow = $this;
      $('div.mapdetail').innerHTML='';
      $('div.mapdetail').append( '<div class="row mb-4 ml-1 img-thumbmail-small lazy">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=1; i < dtRow[0].cells.length; i++){
         $('div.mapdetail').append('<div class="row mb-1"> <span class="col-4">'+colum[i]+'</span><span class="col-8">'+ dtRow[0].cells[i].innerHTML+'</span></div>');
            //console.log(i);
      }
      //$('div.mapdetail').append( '<div class="row mb-1 mt-2"><span class="col-4"></span><span class="col-8">'+dtRow[0].cells[11].innerHTML+'</span></div>');
      $('#myModal').modal('show');

     // initMap('dtRow[0].cells[4].innerHTML+'', '+ dtRow[0].cells[5].innerHTML+'',null,null,'100');
      initMap(''+ dtRow[0].cells[4].innerHTML+','+ dtRow[0].cells[5].innerHTML+'');


     //initMap($(dtRow[0].cells[4].innerHTML+', '+dtRow[0].cells[5].innerHTML);}, false);
     // initMap(location,d,t,rr)
      //console.log("init map = "+ dtRow[0].cells[4].innerHTML);

      });
    });


  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .mapdetail').empty();
  });

 
  $('.bt-filter').on('click', function(evt){
    console.log("filter");


   });


    $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
       var min = $('#min-date').val();
       var max = $('#max-date').val();
       var createdAt = data[2] || 0; 
        if (
           (min == "" || max == "") ||
           (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
         ) {
            return true;
        }
            return false;
        }
    );

    $('.date-range-filter').change(function() {
      table.draw();
    });
    $('#my-table_filter').hide();

    $('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );



 

} );
</script>

<script>
/*function editr(tk,i){
  var n = $("#nameplace"+i).val();
  var r = $("#range"+i).val();
  var La = $("#TolaText-"+i).val();
  var Lo = $("#ToloText-"+i).val();
  console.log(La);
  console.log(Lo);

   $.post( "save_place.php", { etk: 1, tk: tk, r:r, n:n, La:La, Lo:Lo })
    .done(function( data ) {
      console.log(data);
      if(data=='แก้ไขข้อมูลสำเร็จ') $(".tkname"+i).html(n);
      alert(data);
    });

}*/



var marker;
var sunCircle;
var map;
var map2;
var cen;
function initMap(location,d,t,rr) {

    console.log("init Map");

    var str='';
    var str2='';
    if(typeof rr === "undefined" || rr==''){
            //var r = parseInt(document.getElementById("range").value);
          }
    else{
       var r = parseInt(rr);
    }
    if( r >= 800 ){
      z = 14;
    }
    else if( r >= 400  && r <= 800  ){
      z = 15;
    }
    else if(  r >= 200  && r <= 400  ){
      z = 16;
    }
    else if(  r >= 100  && r <= 200  ){
      z = 17;
    }
    else if(  r >= 50  && r <= 100  ){
      z = 18;
    }
    else if(  r >= 25  && r <= 50  ){
      z = 19;
    }
    else{
      z = 20;
    }

    if(typeof location === "undefined" || location==''){
      str = "13.674351, 100.517556";
      str2 = "13.674351,100.517556";
    }
    else{
       str = location;
       str2 = location;
    }
    if(typeof d === "undefined" ){
      d=true;
    }
    else{

      d=false;
    }
  if(typeof t === "undefined" || t == ''){
    t=false;
  }
  else{
    t=true;
  }
  //console.log(t);
  str = str.split(", ").map(function(t){return parseFloat(t)});
  str2 = str2.split(",").map(function(t){return parseFloat(t)});
  if(str.length==1 && str2.length==2){
    str = str2;
  }
  cen = {lat: str[0], lng: str[1]};

    if(t){
      var locations = [
    ["locaton mane", 13.760931, 100.634258, 50]
     ];

   map = new google.maps.Map(document.getElementById('map'), {
     center: {lat: locations[0][1], lng: locations[0][2]},
     zoom: 18
   });
      var infowindow = new google.maps.InfoWindow();
      var marker, i;
      for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          animation: google.maps.Animation.DROP,
          map: map
        });
        //console.log(locations[i][3]);
        var cityCircle = new google.maps.Circle({
          strokeColor: '#FF0000',
          strokeOpacity: 0.5,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.1,
          map: map,
          center: new google.maps.LatLng(locations[i][1], locations[i][2]),
          radius: parseInt(locations[i][3])
        });
        cityCircle.bindTo('center', marker, 'position')

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            //infowindow.setContent(locations[i][0]);
            //infowindow.open(map, marker);
          }
        })(marker, i));
      }
    }else{

      map = new google.maps.Map(document.getElementById('map'), {
        center: cen,
        zoom: z
      });
      marker = new google.maps.Marker({
        position: cen,
        draggable: d,
        animation: google.maps.Animation.DROP, //BOUNCE
        map: map
      });
      sunCircle = {
          strokeColor: "#306EFF",
          strokeOpacity: 0.5,
          strokeWeight: 1.5,
          fillColor: "#1589FF",
          fillOpacity: 0.1,
          map: map,
          center: cen,
          radius: r // in meters
      };
      cityCircle = new google.maps.Circle(sunCircle);
      cityCircle.bindTo('center', marker, 'position');

    }
        //marker.addListener('drag', handleEvent);
        //marker.addListener('dragend', handleEvent);
      }
     


</script>


<!--script>
      var citymap = {
        chicago: {
          center: {lat: 41.878, lng: -87.629},
          population: 12
        },
        newyork: {
          center: {lat: 40.714, lng: -74.005},
          population: 8405837
        },
        losangeles: {
          center: {lat: 34.052, lng: -118.243},
          population: 3857799
        },
        vancouver: {
          center: {lat: 49.25, lng: -123.1},
          population: 603502
        }
      };

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: {lat: 41.878, lng: -87.629},
          mapTypeId: 'terrain'
        });
        for (var city in citymap) {
          var cityCircle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: citymap[city].center,
            radius: Math.sqrt(12) * 100
          });
        }
      }
    </script-->

  <!-- Custom js for this page-->
<!--script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChL0G7E3QQ3EzvcjoHqbxHzSFwlVjxslQ&callback=initMap"async defer></script>
<script src='https://rawgit.com/Logicify/jquery-locationpicker-plugin/master/dist/locationpicker.jquery.js'></script>
<script src="js/google-maps.js"></script-->


  <!-- End custom js for this page-->


          

