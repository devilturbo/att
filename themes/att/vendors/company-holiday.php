<!DOCTYPE html>
<html lang="en">
<?php include('header.php') ?>

<body class="sidebar-fixed">
  <div class="container-scroller">
    <!-- topbar -->
    <?php include('topbar.php') ?>
    <!-- end topbar -->
    <div class="container-fluid page-body-wrapper">
      <?php include('sidebar.php') ?>
      <!-- end sidebar -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->
            
            <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
              <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">วันหยุดประจำปี</h4>
                <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าหลัก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">ตั้งค่าบริษัท</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">วันหยุดประจำปี</p>
                </div>
              </div>
              <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                
               
                <button class="btn btn-primary btn-sm mr-3 d-none d-md-block">บันทึก</button>
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">ยกเลิก</button>
              </div>
            </div>

          </div>

          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">sdf</div>
              </div>
            </div>
          </div>





          

        </div>
        <!-- content-wrapper ends -->
        <!-- copy right -->
       <?php include('copyright.php') ?>
        <!-- end copy right -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
<?php include('footer.php') ?>

</body>

</html>
          <!-- Modal -->
<div id="myModal" class="modal" role="dialog">
  <div class="modal-dialog" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <span>รายละเอียด</span>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div-->
    </div>

  </div>
</div>
<script type="text/javascript">
  
 
    $(document).ready(function() {


    table = $('#dataTable').DataTable({
        dom: 'it'

    });


} );
</script>


