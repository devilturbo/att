<!DOCTYPE html>
<html lang="en">
<?php include('header.php') ?>

<body class="sidebar-fixed">
  <div class="container-scroller">
    <!-- topbar -->
    <?php include('topbar.php') ?>
    <!-- end topbar -->
    <div class="container-fluid page-body-wrapper">
      <?php include('sidebar.php') ?>
      <!-- end sidebar -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->
            
            <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
              <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">รายละเอียดบริษัท</h4>
                <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าหลัก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">ตั้งค่าบริษัท</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">รายละเอียดบริษัท</p>
                </div>
              </div>
              <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                
               
                <button class="btn btn-primary btn-sm mr-3 d-none d-md-block">บันทึก</button>
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">ยกเลิก</button>
              </div>
            </div>

          </div>

          <div class="row mb-1">
            <div class="col-12">
              <ul class="nav nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" href="company-profile.php">รายละเอียดบริษัท</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="company-department.php">แผนก</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="company-position.php">ตำแหน่ง</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="company-type.php">ประเภท</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="company-slip.php">สลิปเงินเดือน</a>
                </li>
              </ul>
             </div>
          </div>
          

          <div class="row mb-4">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <form class="forms-sample">
                    
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Companyname Thai</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" id="exampleInputUsername2" placeholder="ชื่อบริษัทภาษาไทย">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Companyname English </label>
                      <div class="col-sm-4">
                      <input type="text" class="form-control" id="validationCustom02" placeholder="ชื่อบริษัทภาษาอังกฤษ" value="The Bestpromote Co.,Ltd" required="">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Conpany Logo</label>
                      <div class="col-sm-4">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                      <span class="input-group-text">Upload</span>
                            </div>
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="inputGroupFile01">
                              <label class="custom-file-label" for="inputGroupFile01">Choose file โลโก้บริษัท</label>
                              </div>
                            </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Address</label>
                      <div class="col-sm-4">
<textarea id="validationCustom03" type="text" class="form-control" name="shop_address" placeholder="ที่อยู่บริษัท" style="height:100px" maxlength="200" data-bv-trigger="blur" data-bv-notempty="true" data-bv-notempty-message="Shop address is required and cannot be empty" required=""></textarea>                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Telephone</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" id="validationCustom04" placeholder="โทรศัพท์" required="">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">E-Mail</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" id="validationCustom05" placeholder="อีเมลล์" required="">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">TAX ID</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" id="validationCustom06" placeholder="เลขประจำตัวผู้เสียภาษี" required="">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">ชั่วโมงการทำงาน</label>
                      <div class="col-sm-4">
                      <input type="text" class="form-control" id="validationCustom06" placeholder="8" required="">
                      </div>

                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">เวลาเข้างานมตรฐาน</label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" id="validationCustom06" placeholder="8:30" required="">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">ช่วงเวลาทดลองงาน</label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" id="validationCustom06" placeholder="119วัน" required=""><span class="text-muted text-small">*ระบบจะนับตั้งแต่วันตั้งแต่วันที่ทำงานวันแรกและจะปรับเป็นพนักงานประจำให้อัตโนมัติ</span>
                      </div>
                    </div>



                  </form>

                    <!--div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0 row">
                
               
                <button class="btn btn-primary btn-sm mr-3 d-none d-md-block ml-auto">บันทึก</button>
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">ยกเลิก</button>
              </div-->

                </div>
              </div>
            </div>
          </div>

          <div class="row mb-4">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <div class="row mb-4"><div class="col-12"><span class="h4 text-bold">วันลา</span></div></div>
                  <form>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">ลากิจ คิดเงิน</label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" id="validationCustom06" placeholder="6วัน" required=""><span class="text-muted text-small">*กรณีผ่านทดลองงาน119วัน</span>                 
                          <!--button type="button" class="btn btn-outline-primary btn-sm" data-toggle="popover" title="Popover title" data-content="Sed posuere consectetur est at lobortis. Aenean eu leo quam.">เงื่อนไข</button-->

                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">ลากิจ ไม่คิดเงิน</label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" id="validationCustom06" placeholder="6วัน" required=""><span class="text-muted text-small">*ไม่คิดเงิน</span>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">ลาป่วย คิดเงิน</label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" id="validationCustom06" placeholder="30วัน" required=""><span class="text-muted text-small">*พนักงานออฟฟิต ลาป่วย1วันไม่มีใบรับรองแพทย์ ได้รับค่าจ้างปกติ แต่ถ้าเกิน3วันต้องมีใบรับรองแพทย์เท่านั้น และพนักงานสาขา จะต้องมีใบรับรองแพทย์ทุกครั้งถึงจะได้รับค่าจ้าง</span>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">ลาป่วย ไม่คิดเงิน</label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" id="validationCustom06" placeholder="30วัน" required=""><span class="text-muted text-small">*ไม่คิดเงิน</span>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">ลาพักร้อน</label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" id="validationCustom06" placeholder="6วัน" required=""><span class="text-muted text-small">*ทำงานครบ 1ปีได้วันหยุด 6วัน, 3ปี ได้วันหยุด 10วัน, 5ปี ได้วันหยุด 12วัน</span>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">ลาคลอด</label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" id="validationCustom06" placeholder="90วัน" required="">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">ลาบวช</label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" id="validationCustom06" placeholder="15วัน" required="">
                      </div>
                    </div>


                  </form>
                </div>
              </div>
            </div>
          </div>


          <div class="row mb-4">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <div class="row mb-4"><div class="col-12"><span class="h4 text-bold">วันหยุดประจำปี</span></div></div>
                  <div class="row mb-4">
                    <div class="col-12">

                    <form class="needs-validation" novalidate="">
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                  <label for="validationCustom01">วันที่</label>
                                     <div class="input-group input-daterange">
                      <input type="text" id="max-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="สิ้นสุด" >
                    </div>
                                </div>

                                <div class="col-md-4 mb-3">
                                  <label for="validationCustom02">รายละเอียด</label>
                                  <input type="text" class="form-control" id="validationCustom02" placeholder="สงกรานต์" value="" required="">
                                </div>

                                <div class="col-md-4 mb-3">
                                  <label for="validationCustom03">หมายเหตุ</label>
                                  <input type="text" class="form-control" id="validationCustom03" placeholder="บังคับหยุดทุกคน" value="" required="">
                                </div>
                            </div>

                      
                            <div class="row">
                              <div class="ml-auto mr-3"><button class="btn btn-primary ml-auto" type="submit">บันทึกข้อมูล</button></div>
                            </div>

                          </form>
                          </div>





                   

                    
                  </div>

                  <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>วันที่</th>
                          <th>รายละเอียด</th>
                          <th>หมายเหตุ</th>
                          <th width="120px;">ดำเนินการ</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>12/09/19</td>
                          <td>ชดเชยปีใหม่ 2562</td>
                          <td></td>
                          <td>
                            <button class="btn btn-outline-primary btn-sm ml-auto" type="submit">ลบ</button>
                            <button class="btn btn-outline-primary btn-sm ml-auto ml-2" type="submit">แก้ไข</button>
                          </td>
                        </tr>
                        <tr>
                          <td>02/01/2019</td>
                          <td>วันมาฆบูชา</td>
                          <td></td>
                          <td>
                            <button class="btn btn-outline-primary btn-sm ml-auto" type="submit">ลบ</button>
                            <button class="btn btn-outline-primary btn-sm ml-auto ml-2" type="submit">แก้ไข</button>
                          </td>
                        </tr>
                    
                      </tbody>
                    </table>
                  </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <button class="btn btn-primary btn-sm mr-2 ml-auto">บันทึก</button>
            <button class="btn btn-outline-primary btn-sm mr-3">ยกเลิก</button>
          </div>



          

        </div>
        <!-- content-wrapper ends -->
        <!-- copy right -->
       <?php include('copyright.php') ?>
        <!-- end copy right -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
<?php include('footer.php') ?>
  <script src="js/tooltips.js"></script>
  <script src="js/popover.js"></script>

</body>

</html>

<script type="text/javascript">

$(document).ready(function() {
      
    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging:   true,
        ordering: false,
        info:     false,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false
        
    });


} );
</script>

