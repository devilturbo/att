<!DOCTYPE html>
<html lang="en">
<?php include('header.php') ?>
<body class="sidebar-fixed">
  <div class="container-scroller">
    <!-- topbar -->
    <?php include('topbar.php') ?>
    <!-- end topbar -->
    <div class="container-fluid page-body-wrapper">
      <?php include('sidebar.php') ?>
      <!-- end sidebar -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->
            
            <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
              <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">ตำแหน่ง</h4>
                <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">ตำแหน่ง</p>
                </div>
              </div>
              <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button>
              </div>
            </div>
          </div>

          <div class="row mb-1">
            <div class="col-12">
              <ul class="nav nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link " href="company-profile.php">รายละเอียดบริษัท</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="company-department.php">แผนก</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" href="company-position.php">ตำแหน่ง</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="company-type.php">ประเภท</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="company-slip.php">สลิปเงินเดือน</a>
                </li>
                <!--li class="nav-item">
                  <a class="nav-link" href="user_changetime.php">ประวัติการปรับปรุงเวลา</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="user_ot.php">ประวัติการทำงานล่วงเวลา</a>
                </li-->
              </ul>
             </div>
          </div>

          

        


          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  
                  <div class="row">
                    <span class="h5 ml-3 mt-2">ตำแหน่ง(3)</span>
                   <a href="company-add-position.php" class="ml-auto"><button class="btn btn-outline-primary btn-sm  mb-2 mr-2">เพิ่มตำแหน่ง</button></a> 
                  </div>

                  <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                      <tr>
                        <th>ลำดับ</th>
                        <th>ตำแหน่ง</th>
                        <th>หมายเหตุ</th>
                        <th>เวลาในระบบ</th>
                        <th>ตั้งค่าบริษัท</th>
                        <th>พนักงานในระบบ</th>
                        <th>เพิ่มพนักงานในระบบ</th>
                        <th>นำเข้าข้อมูลพนักงาน</th>
                        <th>กลุ่มพนักงาน</th>
                        <th>รายงาน(BA)</th>
                        <th>อนุมัติรายการ</th>
                        <th>ประวัติบันทึกเวลา</th>
                        <th>ประวัติบันทึกการลา</th>
                        <th>ประวัติปรับปรุงเวลา</th>
                        <th>วันหยุดประจำปี</th>
                        <th>กำหนดพิกัดสถานที่</th>
                        <th>Vacancy</th>
                        <th width="100px;">ดำเนินการ</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="dt-edit">
                        <td>1</td>
                        <td>Admin</td>
                        <td>xxxxxxxxxx</td>
                        <th>30</th>
                        <th>จัดการได้</th>
                        <th>แสดงเท่านั้น</th>
                        <th>ไม่แสดง</th>
                        <th>จัดการได้</th>
                        <th>แสดงเท่านั้น</th>
                        <th>ไม่แสดง</th>
                        <th>จัดการได้</th>
                        <th>แสดงเท่านั้น</th>
                        <th>ไม่แสดง</th>
                        <th>จัดการได้</th>
                        <th>แสดงเท่านั้น</th>
                        <th>ไม่แสดง</th>
                        <th>ไม่แสดง</th>
                          <td>
                            <button class="btn btn-outline-primary btn-sm ">แก้ไข</button>
                            <button class="btn btn-outline-primary btn-sm ml-2">ลบ</button>
                            <button class="btn btn-primary btn-sm ml-2">บันทึก</button>
                          </td>
                      </tr>

                      <tr class="dt-edit">
                        <td>2</td>
                        <td>Straff</td>
                        <td>xxxxxxxxxx</td>
                        <th>30</th>
                        <th>จัดการได้</th>
                        <th>แสดงเท่านั้น</th>
                        <th>ไม่แสดง</th>
                        <th>จัดการได้</th>
                        <th>แสดงเท่านั้น</th>
                        <th>ไม่แสดง</th>
                        <th>จัดการได้</th>
                        <th>แสดงเท่านั้น</th>
                        <th>ไม่แสดง</th>
                        <th>จัดการได้</th>
                        <th>แสดงเท่านั้น</th>
                        <th>ไม่แสดง</th>
                        <th>ไม่แสดง</th>
                          <td>
                            <button class="btn btn-outline-primary btn-sm ">แก้ไข</button>
                            <button class="btn btn-outline-primary btn-sm ml-2">ลบ</button>
                            <button class="btn btn-primary btn-sm ml-2">บันทึก</button>
                          </td>
                      </tr>

                      <tr class="dt-edit">
                        <td>3</td>
                        <td>TBP</td>
                        <td>xxxxxxxxxx</td>
                        <th>30</th>
                        <th>จัดการได้</th>
                        <th>แสดงเท่านั้น</th>
                        <th>ไม่แสดง</th>
                        <th>จัดการได้</th>
                        <th>แสดงเท่านั้น</th>
                        <th>ไม่แสดง</th>
                        <th>จัดการได้</th>
                        <th>แสดงเท่านั้น</th>
                        <th>ไม่แสดง</th>
                        <th>จัดการได้</th>
                        <th>แสดงเท่านั้น</th>
                        <th>ไม่แสดง</th>
                        <th>ไม่แสดง</th>
                          <td>
                            <button class="btn btn-outline-primary btn-sm ">แก้ไข</button>
                            <button class="btn btn-outline-primary btn-sm ml-2">ลบ</button>
                            <button class="btn btn-primary btn-sm ml-2">บันทึก</button>
                          </td>
                      </tr>

                      <!--tr class="dt-edit">
                        <td>2</td>
                        <td>Straff</td>
                        <td>xxxxxxxx</td>
                          <td>
                            <button class="btn btn-outline-primary btn-sm ">แก้ไข</button>
                            <button class="btn btn-outline-primary btn-sm ml-2">ลบ</button>
                            <button class="btn btn-primary btn-sm ml-2">บันทึก</button>
                          </td>
                      </tr>

                      <tr class="dt-edit">
                        <td>3</td>
                        <td>TBP</td>
                        <td>xxxxxxxxxxxxxxxxx</td>
                          <td>
                            <button class="btn btn-outline-primary btn-sm ">แก้ไข</button>
                            <button class="btn btn-outline-primary btn-sm ml-2">ลบ</button>
                            <button class="btn btn-primary btn-sm ml-2">บันทึก</button>
                          </td>
                      </tr-->

                    </tbody>
                  </table>
                  <!--div class="row pr-3">
                       <button class="btn btn-primary btn-sm mr-2 ml-auto">บันทึกทั้งหมด</button>
                       <button class="btn btn-outline-primary btn-sm ">ยกเลิก</button>
                  </div-->
                </div>
              </div>
            </div>
          </div>

          


          

        </div>
        <!-- content-wrapper ends -->
        <!-- copy right -->
       <?php include('copyright.php') ?>
        <!-- end copy right -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
<?php include('footer.php') ?>
<!-- partial -->


</body>

</html>

          <!-- Modal -->
<div id="myModal" class="modal" role="dialog">
  <div class="modal-dialog" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <span>รายละเอียด</span>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div-->
    </div>

  </div>
</div>

<script type="text/javascript">
 
 
    $(document).ready(function() {
       
    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging:   true,
        ordering: false,
        info:     false,
        scrollY: 500,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: false,
        fixedHeader: true

    });


var colum = ["ลำดับ", "ตำแหน่ง", "หมายเหตุ","ดำเนินการ","เวลาในระบบ","ตั้งค่าบริษัท","พนักงานในระบบ","เพิ่มพนักงานในระบบ","นำเข้าข้อมูลพนักงาน","กลุ่มพนักงาน","รายงาน(BA)","อนุมัติรายการ","ประวัติบันทึกเวลา","ประวัติบันทึกการลา","ประวัติปรับปรุงเวลา","วันหยุดประจำปี","กำหนดพิกัดสถานที่","Vacancy","ดำเนินการ"];


   //Edit row buttons
  $('.dt-edit').each(function () {
    $(this).on('click', function(evt){
      $this = $(this);
      var dtRow = $this;
      $('div.modal-body').innerHTML='';
      //$('div.modal-body').append( '<div class="row mb-4 h3">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=0; i < dtRow[0].cells.length; i++){
         $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-4 ">'+colum[i]+'</span><span class="col-6">'+dtRow[0].cells[i].innerHTML+'</span><div/>');
            console.log(i);
      }
      $('#myModal').modal('show');
    });
  });
  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .modal-body').empty();
  });



} );
</script>


