<!DOCTYPE html>
<html lang="en">
<?php include('header.php') ?>
<body class="sidebar-fixed">
  <div class="container-scroller">
    <!-- topbar -->
    <?php include('topbar.php') ?>
    <!-- end topbar -->
    <div class="container-fluid page-body-wrapper">
      <?php include('sidebar.php') ?>
      <!-- end sidebar -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->
            
            <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
              <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">สลิปเงินเดือน</h4>
                <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">สลิปเงินเดือน</p>
                </div>
              </div>
              <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button>
              </div>
            </div>
          </div>

          <div class="row mb-1">
            <div class="col-12">
              <ul class="nav nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link " href="company-profile.php">รายละเอียดบริษัท</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="company-department.php">แผนก</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="company-position.php">ตำแหน่ง</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="company-type.php">ประเภท</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" href="company-slip.php">สลิปเงินเดือน</a>
                </li>
                <!--li class="nav-item">
                  <a class="nav-link" href="user_changetime.php">ประวัติการปรับปรุงเวลา</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="user_ot.php">ประวัติการทำงานล่วงเวลา</a>
                </li-->
              </ul>
             </div>
          </div>

          <div class="row">
            <div class="col-12">
              <div class="row ">
                <div class="col-2 ml-auto"></div>
                  <div class="col-2"></div>
                </div>
                                    
                <form>
                  <div class="form-row align-items-center mb-4">
                      <div class="col-sm-3 my-1-">
                        <span>ค้นหา</span>
                        <input type="text" class="form-control global_filter" id="global_filter" placeholder="คำที่ค้นหา">
                      </div>

                      <!--div class="col-3">
                        <div class="row">
                          <div class="col" ><span id="ตำแหน่ง"></span></div>
                          <div class="col" ><span id="ประเภท"></span></div>
                          <div class="col-auto">
                            <button type="button" class="btn btn-outline-primary btn-sm ml-auto bt-filter" style="margin-top: 20px;" data-toggle="modal" data-target="#ModalFilter">Filter</button>
                          </div>
                        </div>
                      </div-->




                      <div class="col-auto my-1 ml-auto" style="width: 350px;">
                          <span>ค้นหาจากช่วงเวลา</span>
                          <div class="input-group input-daterange">
                            <input type="text" id="min-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น" >
                            <div class="input-group-addon mr-2 ml-2"> ถึง </div>
                            <input type="text" id="max-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="สิ้นสุด" >
                            <button type="submit" id="search" class="btn btn-primary btn-sm ml-2">ค้าหา</button>
                          </div>
                      </div>
                  </div>

                  
                </form>
                                    
            </div>
          </div>

          <!-- Modal -->
          <div id="ModalFilter" class="modal" role="dialog">
            <div class="modal-dialog" >
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                              <span>ค้นหาโดยละเอียด</span>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="col-12 p-4">
                    
                      <div class="row mb-4">
                      <div class="col-6 mb-2"><span id="เพศ"></span></div>
                      <div class="col-6 mb-2 " ><span id="เอเจนซี่"></span></div>
                      <div class="col-6 mb-2 " ><span id="แผนก"></span></div>
                      <div class="col-6 mb-2" ><span id="ตำแหน่ง"></span></div>
                      <div class="col-6 mb-2" ><span id="ประเภท"></span></div>
                      <div class="col-6 mb-2" ><span id="ประเภทย่อย"></span></div>
                      <div class="col-6 mb-2"><span id="สัญญาจ้าง"></span></div>
                      <div class="col-6 mb-2"><span id="พื้นที่"></span></div>
                      <div class="col-6 mb-2"><span id="ช่องทาง"></span></div>
                      <div class="col-6 mb-2"><span id="ห้างร้าน"></span></div>
                      <div class="col-6 mb-2"><span id="สถานนะ"></span></div>
                  </div>
        
                  </div>
              </div>
            </div>
          </div>

           <div class="row mb-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="mb-4">เลือกไฟล์ที่ต้องการ</h6>
                            <div class="input-group mb-3">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile02">
                                    <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
                                    </div>
                                    <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                                      
                        </div>
                    </div>
                </div>
          </div>

          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">




                  <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                      <tr>
                        <th>ชื่อ-นามสกุล</th>
                        <th>รหัส</th>
                        <th>หน่วยงาน</th>
                        <th>เลขบัญชี</th>
                        <th>เงินเดือน</th>
                        <th>เบี้ยขยัน</th>
                        <th>ค่าโทรศัพท์</th>
                        <th>รายได้อื่นๆ</th>
                        <th>เงินได้รวม</th>
                        <th>ประกันสังคม</th>
                        <th>เงินหัก</th>
                        <th>เงินได้สุทธิ</th>
                        <th>รายได้สะสม</th>
                        <th>ภาษีสะสม</th>
                        <th>เงินกองทุน</th>
                        <th>จำนวนวัน</th>
                        <th>วันที่จ่าย</th>
                        <th>ประจำเดือน</th>
                        <th>ดำเนินการ</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="dt-edit">
                        <td>คมศักดิ์ รัชนีกร</td>
                          <td>232323232</td>
                          <td>Straff</td>
                          <td>230-2330-222</td>
                          <td>9,500</td>
                          <td>200</td>
                          <td>200</td>
                          <td>0</td>
                          <td>9,8000</td>
                          <td>500</td>
                          <td>200</td>
                          <td>12,000</td>
                          <td>5,000</td>
                          <td>2,000</td>
                          <td>0</td>
                          <td>28</td>
                          <td>20/09/29</td>
                          <td>09/29</td>
                          <td>
                            <button class="btn btn-outline-primary btn-sm ">แก้ไข</button>
                            <button class="btn btn-outline-primary btn-sm ml-2">ลบ</button>
                            <button class="btn btn-primary btn-sm ml-2">บันทึก</button>

                          
                          </td>
                      </tr>

                   
                                
                    </tbody>
                  </table>
                  <div class="row pr-3">
                       <button class="btn btn-primary btn-sm mr-2 ml-auto">บันทึกทั้งหมด</button>
                       <button class="btn btn-outline-primary btn-sm ">ยกเลิก</button>
                  </div>
                </div>
              </div>
            </div>
          </div>




          

        </div>
        <!-- content-wrapper ends -->
        <!-- copy right -->
       <?php include('copyright.php') ?>
        <!-- end copy right -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
<?php include('footer.php') ?>
<!-- partial -->


</body>

</html>

          <!-- Modal -->
<div id="myModal" class="modal" role="dialog">
  <div class="modal-dialog" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <span>รายละเอียด</span>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div-->
    </div>

  </div>
</div>

<script type="text/javascript">
    function filterGlobal () {
    $('#dataTable').DataTable().search(
        $('#global_filter').val()

       ).draw();
    }
 
    $(document).ready(function() {
       var columnsToSearch = {
        /*8: 'แผนก',
        9: 'ตำแหน่ง',
        10: 'ประเภท',
        11: 'ประเภทย่อย',
        12: 'สัญญาจ้าง',
        14: 'พื้นที่',
        7: 'เอเจนซี่',
        15: 'เขตพื้นที่',
        16: 'ช่องทาง',
        17: 'ห้างร้าน',
        22: 'สถานะ'*/
    };

    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging:   true,
        ordering: false,
        info:     false,
        scrollY: 500,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: false,
        fixedHeader: true,
        initComplete: function () {
            for (var i in columnsToSearch) {
                var api = this.api();
                var select = $('<select class="form-control"><option value="">Select</option></select>')
                    .appendTo($('#' + columnsToSearch[i]).empty().text(columnsToSearch[i] + ' '))

                    .attr('data-col', i)
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        api.column($(this).attr('data-col'))
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                api.column(i).data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>');
                });
            }
        }

    });

var colum = ["ชื่อ-นามสกุล", "รหัส", "หน่วยงาน","เลขบัญชี", "เงินเดือน", "เบี้ยขยัน","ค่าโทรศัพท์", "รายได้อื่นๆ", "เงินได้รวม","ประกันสังคม","เงินหัก","เงินได้สุทธิ","รายได้สะสม","ภาษีสะสม","เงินกองทุน","จำนวนวัน","วันที่จ่าย","ประจำเดือน","ดำเนินการ"];


   //Edit row buttons
  $('.dt-edit').each(function () {
    $(this).on('click', function(evt){
      $this = $(this);
      var dtRow = $this;
      $('div.modal-body').innerHTML='';
      $('div.modal-body').append( '<div class="row mb-4 h3">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=1; i < dtRow[0].cells.length; i++){
         $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">'+colum[i]+'</span><span class="col-8">'+dtRow[0].cells[i].innerHTML+'</span><div/>');
            console.log(i);
      }
      $('#myModal').modal('show');
    });
  });
  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .modal-body').empty();
  });
    $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
       var min = $('#min-date').val();
       var max = $('#max-date').val();
       var createdAt = data[2] || 0; 
        if (
           (min == "" || max == "") ||
           (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
         ) {
            return true;
        }
            return false;
        }
    );

    $('.date-range-filter').change(function() {
      table.draw();
    });
    $('#my-table_filter').hide();

    $('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );

} );
</script>


