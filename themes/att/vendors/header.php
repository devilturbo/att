<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Thebestpromote</title>
  <!-- base:css -->
  <link rel="stylesheet" href="vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="vendors/jqvmap/jqvmap.min.css">
  <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
  <link href="https://fonts.googleapis.com/css?family=Kanit:200,300&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">

  <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap4.min.css">

  <link rel="stylesheet" href="css/datepicker.css">
  <link rel="stylesheet" href="css/fixedHeader.dataTables.min.css">
  <link rel="stylesheet" href="css/fixedColumns.dataTables.min.css">

  <link rel="stylesheet" href="css/datepicker.css?v=1001">
  <link rel="stylesheet" href="css/select.css">
  <link rel="stylesheet" href="css/prism.css">
  <link rel="stylesheet" href="css/chosen.css">

    <link rel="stylesheet" href="vendors/select2/select2.min.css">
  <link rel="stylesheet" href="vendors/select2-bootstrap-theme/select2-bootstrap.min.css">

   <!--link href="js/plugins/nucleo/css/nucleo.css" rel="stylesheet" />
   <link href="js/plugins/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" />

  <link rel="stylesheet" href="css/typography.css">
  <link rel="stylesheet" href="css/default-css.css"-->

  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
      <!-- modernizr css -->
  <!--script src="js/vendor/modernizr-2.8.3.min.js"></script-->
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>