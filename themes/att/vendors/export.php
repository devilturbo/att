<!DOCTYPE html>
<html lang="en">
<?php include('header.php') ?>

<body class="sidebar-fixed">
  <div class="container-scroller">
    <!-- topbar -->
    <?php include('topbar.php') ?>
    <!-- end topbar -->
    <div class="container-fluid page-body-wrapper">
      <?php include('sidebar.php') ?>
      <!-- end sidebar -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->
            
            <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3 mb-3">
              <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">Export ข้อมูล</h4>
                <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">Export ข้อมูล</p>
                </div>
              </div>
              <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                <!--button class="btn btn-primary btn-sm mr-3 d-none d-md-block">Download Report</button>
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button-->
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  
                  <div class="row mb-3"><span class="h5 ml-2">ข้อมูลการบันทึกเข้า-ออก ของพนักงาน</span></div>
                  <div class="row">
                    <div class="col-4">
                        <label for="example-text-input" class="col-form-label">ตำแหน่ง</label>
                        <select class="form-control">
                          <option selected="">All</option>
                          <option>Straff</option>
                          <option>TBA</option>
                          <option>...</option>
                        </select>
                    </div>

                    <div class="col-4">
                        <label for="example-text-input" class="col-form-label">ประเภท</label>
                        <select class="form-control">
                          <option selected="">All</option>
                          <option>BA</option>
                          <option>SIA</option>
                          <option>...</option>
                        </select>
                    </div>

                    <div class="col-3">
                        <label for="example-text-input" class="col-form-label">ประเภทย่อย</label>
                        <select class="form-control">
                          <option selected="">All</option>
                          <option>BA Hair</option>
                          <option>BA Weekend</option>
                          <option>...</option>
                        </select>
                    </div>

                    <div class="col-auto">
                      <label for="example-text-input" class="col-form-label">ประเภทไฟล์</label>
                      <select class="form-control mr-2">
                              <option selected >Excel</option>
                              <option>CVS</option>
                              <option>...</option>
                            </select>
                      
                    </div>
                  </div>

                  <div class="col-12 my-1 ml-auto">
                          <span>ช่วงเวลา</span>
                          <div class="input-group input-daterange">
                            <input type="text" id="min-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="เริ่มต้น" >
                            <div class="input-group-addon mr-2 ml-2"> ถึง </div>
                            <input type="text" id="max-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="สิ้นสุด" >
                            
                            

                            <button type="submit" id="search" class="btn btn-primary btn-sm ml-2">Export</button>
                          </div>
                  </div>


                </div>
              </div>
            </div>
          </div>



          

        </div>
        <!-- content-wrapper ends -->
        <!-- copy right -->
       <?php include('copyright.php') ?>
        <!-- end copy right -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
<?php include('footer.php') ?>

</body>

</html>
<script type="text/javascript">
      $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });
</script>

