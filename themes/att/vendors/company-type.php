<!DOCTYPE html>
<html lang="en">
<?php include('header.php') ?>
<body class="sidebar-fixed">
  <div class="container-scroller">
    <!-- topbar -->
    <?php include('topbar.php') ?>
    <!-- end topbar -->
    <div class="container-fluid page-body-wrapper">
      <?php include('sidebar.php') ?>
      <!-- end sidebar -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="dashboard-header d-flex flex-column grid-margin"><!-- dashboard header -->
            
            <div class="d-flex align-items-center justify-content-between flex-wrap border-bottom pb-3">
              <div class="d-flex align-items-center">
                <h4 class="mb-0 font-weight-bold mr-2">ประเภท</h4>
                <div class="d-none d-md-flex mt-1">
                  <p class="text-muted mb-0 tx-13 cursor-pointer">หน้าแรก</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">พนักงาน</p>
                  <i class="mdi mdi-chevron-right text-muted"></i>
                  <p class="text-muted mb-0 tx-13 cursor-pointer">ประเภท</p>
                </div>
              </div>
              <div class="button-wrapper d-flex align-items-center mt-md-3 mt-xl-0">
                <button class="btn btn-outline-primary btn-sm  d-none d-md-block">Data Export</button>
              </div>
            </div>
          </div>

          <div class="row mb-1">
            <div class="col-12">
              <ul class="nav nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link " href="company-profile.php">รายละเอียดบริษัท</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="company-department.php">แผนก</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="company-position.php">ตำแหน่ง</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" href="company-type.php">ประเภท</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="company-slip.php">สลิปเงินเดือน</a>
                </li>
                <!--li class="nav-item">
                  <a class="nav-link" href="user_changetime.php">ประวัติการปรับปรุงเวลา</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="user_ot.php">ประวัติการทำงานล่วงเวลา</a>
                </li-->
              </ul>
             </div>
          </div>

          

        


          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  
                  <div class="row">
                    <span class="h5 ml-3">ประเภทพนักงาน(3)</span>
                   <a href="company-add-type.php" class="ml-auto"><button class="btn btn-outline-primary btn-sm  mb-2 mr-2">เพิ่มประเภทพนักงาน</button></a> 
                  </div>

                  <table id="dataTable" class="table " cellspacing="0">
                    <thead>
                      <tr>
                        <th>ลำดับ</th>
                        <th>ตำแหน่ง</th>
                        <th>หมายเหตุ</th>
                        <th>เวลาทำงาน</th>
                        <th>รูปแบบการเข้าทำงาน</th>
                        <th>BA</th>
                        <th>การลาขั้นต่ำ</th>
                        <th>วันที่เริ่มได้สิทธิ์</th>
                        <th>สะสมวันลา</th>
                        <th>วันหยุด</th>
                        <th>วันลาหยุดชดเชย</th>
                        <th>สถานที่</th>
                        <th width="100px;">ดำเนินการ</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="dt-edit">
                        <td>1</td>
                        <td>BA</td>
                        <td>พนักงานเชียร์ขายสินค้า</td>
                        <th>8:00</th>
                        <th>เข้าทำงานปกติตามเวลาที่กำหนด</th>
                        <th>YES</th>
                        <th>อนุญาตให้ลาเป็นชั่วโมงได้</th>
                        <th>วันแรกที่เริ่มทำงาน</th>
                        <th>ไม่ใช่</th>
                        <th>เสาร์,อาทิตย์</th>
                        <th>บังคับหยุดในวันทำงานถัดไป</th>
                        <th>บังคับ</th>
                        <td>
                          <button class="btn btn-outline-primary btn-sm ">แก้ไข</button>
                          <button class="btn btn-outline-primary btn-sm ml-2">ลบ</button>
                          <button class="btn btn-primary btn-sm ml-2">บันทึก</button>
                        </td>
                      </tr>

                    </tbody>
                  </table>
                  <!--div class="row pr-3">
                       <button class="btn btn-primary btn-sm mr-2 ml-auto">บันทึกทั้งหมด</button>
                       <button class="btn btn-outline-primary btn-sm ">ยกเลิก</button>
                  </div-->
                </div>
              </div>
            </div>
          </div>

          <div class="row mt-4">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  
                  <div class="row">
                    <span class="h5 ml-3">ประเภทพนักงานกลุ่มย่อย(12)</span>
                    <a href="company-add-subtype.php" class="ml-auto"><button class="btn btn-outline-primary btn-sm ml-auto mb-2 mr-2">เพิ่มกลุ่มย่อย</button></a> 
                  </div>

                  <table id="dataTable2" class="table " cellspacing="0">
                    <thead>
                      <tr>
                        <th>ลำดับ</th>
                        <th>ตำแหน่ง</th>
                        <th>กลุ่มย่อย</th>
                        <th>หมายเหตุ</th>
                        <th>เวลาทำงาน</th>
                        <th>รูปแบบการเข้าทำงาน</th>
                        <th>BA</th>
                        <th>การลาขั้นต่ำ</th>
                        <th>วันที่เริ่มได้สิทธิ์</th>
                        <th>สะสมวันลา</th>
                        <th>วันหยุด</th>
                        <th>วันลาหยุดชดเชย</th>
                        <th>สถานที่</th>
                        <th width="100px;">ดำเนินการ</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="dt-edit">
                        <td>1</td>
                        <td>BA</td>
                        <td>BA Hair</td>
                        <td>พนักงานเชียร์ขายสินค้า</td>
                        <th>8:00</th>
                        <th>เข้าทำงานปกติตามเวลาที่กำหนด</th>
                        <th>YES</th>
                        <th>อนุญาตให้ลาเป็นชั่วโมงได้</th>
                        <th>วันแรกที่เริ่มทำงาน</th>
                        <th>ไม่ใช่</th>
                        <th>เสาร์,อาทิตย์</th>
                        <th>บังคับหยุดในวันทำงานถัดไป</th>
                        <th>บังคับ</th>
                          <td>
                            <button class="btn btn-outline-primary btn-sm ">แก้ไข</button>
                            <button class="btn btn-outline-primary btn-sm ml-2">ลบ</button>
                            <button class="btn btn-primary btn-sm ml-2">บันทึก</button>
                          </td>
                      </tr>

                    </tbody>
                  </table>
                  <!--div class="row pr-3">
                       <button class="btn btn-primary btn-sm mr-2 ml-auto">บันทึกทั้งหมด</button>
                       <button class="btn btn-outline-primary btn-sm ">ยกเลิก</button>
                  </div-->
                </div>
              </div>
            </div>
          </div>




          

        </div>
        <!-- content-wrapper ends -->
        <!-- copy right -->
       <?php include('copyright.php') ?>
        <!-- end copy right -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
<?php include('footer.php') ?>
<!-- partial -->


</body>

</html>

          <!-- Modal -->
<div id="myModal" class="modal" role="dialog">
  <div class="modal-dialog" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <span>รายละเอียด</span>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div-->
    </div>

  </div>
</div>

<script type="text/javascript">
 
 
    $(document).ready(function() {
       
    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    table = $('#dataTable').DataTable({
        dom: 'it',
        searching: true,
        paging:   true,
        ordering: false,
        info:     false,
        scrollY: 300,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: false,
        fixedHeader: true

    });
    table = $('#dataTable2').DataTable({
        dom: 'it',
        searching: true,
        paging:   true,
        ordering: false,
        info:     false,
        scrollY: 300,
        scrollX:        true,
        scrollCollapse: false,
        autoWidth:      false,
        fixedColumns:   {
          leftColumns: 1
        },
        columnDefs: [
          { "width": "50px", "targets": [ 0 ] },
        ],
        responsive: false,
        fixedHeader: true

    });

var colum = ["ลำดับ","ตำแหน่ง","หมายเหตุ","เวลาทำงาน","รูปแบบการเข้าทำงาน","การลาขั้นต่ำ","วันที่เริ่มได้สิทธิ์","สะสมวันลา","วันหยุด","วันลาหยุดชดเชย","สถานที่","ดำเนินการ","ดำเนินการ"];


   //Edit row buttons
  $('.dt-edit').each(function () {
    $(this).on('click', function(evt){
      $this = $(this);
      var dtRow = $this;
      $('div.modal-body').innerHTML='';
      //$('div.modal-body').append( '<div class="row mb-4 h3">'+dtRow[0].cells[0].innerHTML+'</div>');
      for(var i=0; i < dtRow[0].cells.length; i++){
         $('div.modal-body').append('<div class="row"> <span class="card-title mr-2 text-muted col-3 ">'+colum[i]+'</span><span class="col-8">'+dtRow[0].cells[i].innerHTML+'</span><div/>');
            console.log(i);
      }
      $('#myModal').modal('show');
    });
  });
  $('#myModal').on('hidden.bs.modal', function (evt) {
    $('.modal .modal-body').empty();
  });



} );
</script>


