<?php

namespace app\controllers;

use app\components\Utilities;
use app\models\MemberForPunch;
use app\models\MemberLeave;
use app\models\MemberPunchClock;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * MemberOtController implements the CRUD actions for MemberOt model.
 */
class ExportdataController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [

                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],

            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MemberOt models.
     * @return mixed
     */
    public function beforeAction($action)
    {
        $auth = Utilities::setAuthController();
        if ($auth['export'] == 'N') {
            throw new NotFoundHttpException('page not found');
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {

        return $this->render('index', [
            // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetdata($position = null, $type = null, $subtype = null, $date_from, $date_to)
    {
        date_default_timezone_set("Asia/Bangkok");

        $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $date_from);
        $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $date_to);
        // \app\components\Helpers::Debig($date_from);

        // $model = MemberPunchClock::find()->andWhere(['between', 'punch_datetime', $date_from, $date_to])->all();

        $query = MemberPunchClock::find()
            ->leftJoin('member', 'member.id = member_punch_clock.member_id')
        // ->joinWith('punch')
            ->andWhere(['between', 'punch_datetime', $from, $to])
            ->orderBy(['member_id' => SORT_ASC])
        // ->all()
        ;
        // \app\components\Helpers::Debig(count($query[0]->punch));

        if ($position != null && $position != 'all') {
            $query->andWhere(['=', 'member.position_id', $position]);
        }

        if ($type != null && $type != 'all') {
            $query->andWhere(['=', 'member.member_type_id', $type]);
        }

        if ($subtype != null && $subtype != 'all') {
            $query->andWhere(['=', 'member.member_subtype_id', $subtype]);
        }
        $dataQuery = $query->all();
        // $query->andWhere(['between', 'member_punch_clock.punch_datetime', $from, $to])->all();
        $count = 0;
        $output = [];
        $tmp = array();
        $data = [];

        $pre = $dataQuery[0]->member_id;

        foreach ($dataQuery as $key => $e) {

            if ($pre == $e->member_id) {
                $pre = $e->member_id;

                // เช็คว่า เข้างาน หรืออ อกงาน
                if ($e->punch_type == "in") {

                    $tmp['punch_time_in'] = Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime);
                    $tmp['lat_long_in'] = $e->latitude . "," . $e->longitude;
                    $tmp['day_work'] = Yii::$app->Utilities->formatDate('Y-m-d', $e->punch_datetime);

                    // เช็คหาเวลาเข้างาน
                    if (!empty($e->member->membersubtype)) {
                        $start_time_work = strtotime($e->member->membersubtype->time_attendance);
                    } else if (!empty($e->member->membertype)) {
                        $start_time_work = strtotime($e->member->membertype->time_attendance);
                    }

                    if (!empty($e->member->membersubtype) || !empty($e->member->membertype)) {
                        // คำนวณหาเวลาเข้าเลท
                        $check_in = strtotime(Yii::$app->Utilities->formatDate('H:i:s', $e->punch_datetime));
                        $diff = $check_in - $start_time_work;
                        $tmp['late'] = round($diff / (60));
                        
                        if ($tmp['late'] < 0) {
                            $tmp['late'] = 0;
                        }
                    }

                } else {
                    $tmp['punch_time_out'] = Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime);
                    $tmp['lat_long_out'] = $e->latitude . "," . $e->longitude;

                    // คำนวณเวลาเข้างานทั้งหมด (ชั่วโมง)
                    $out = strtotime($tmp['punch_time_out']);
                    $in = strtotime($tmp['punch_time_in']);
                    $time_work = $out - $in;
                    $tmp['time_work'] = round($time_work / (60 * 60));
                }

                $start = strtotime(Yii::$app->Utilities->formatDate('Y-m-d', $e->member->start_work_date));
                $end = strtotime(Yii::$app->Utilities->formatDate('Y-m-d', date("Y-m-d")));
                $datediff = $end - $start;
                $day_ = round($datediff / (60 * 60 * 24));

                $tmp['member_id'] = $e->member->id;
                $tmp['member_no'] = $e->member->member_no;
                $tmp['fullname'] = $e->member->fullname;
                $tmp['position_name'] = $e->member->position_name;
                $tmp['type_name'] = $e->member->type_name;
                $tmp['account'] = $e->area->account;
                $tmp['local_name'] = $e->area->local_name;
                $tmp['location'] = $e->area->area_name;
                $tmp['lat_long_location'] = $e->area->latitude . "," . $e->area->longitude;
                $tmp['area'] = $e->area->area;
                $tmp['start_work_date'] = Yii::$app->Utilities->formatDate('Y-m-d', $e->member->start_work_date);
                $tmp['work_life'] = $day_;
                $count++;

                // นับแบบจับคู่เนื่องจากเก็บข้อมูลเข้าและออกงานแยก record กัน
                if ($count == 2) {
                    array_push($data, $tmp);
                    $tmp = array();
                    $count = 0;
                }
            } else {

                if (!empty($tmp['punch_time_in'])) {
                    $tmp['punch_time_out'] = "ไม่พบเวลาบันทึกออก";
                    $tmp['lat_long_out'] = "";
                    $tmp['time_work'] = "";
                    // $tmp['late'] = "";

                    // array_push($data, $tmp);
                    $tmp = array();
                    $count = 0;
                }

                $pre = $e->member_id;

                if ($e->punch_type == "in") {
                    $tmp['punch_time_in'] = Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime);
                    $tmp['lat_long_in'] = $e->latitude . "," . $e->longitude;
                    $tmp['day_work'] = Yii::$app->Utilities->formatDate('Y-m-d', $e->punch_datetime);

                    if (!empty($e->member->membersubtype)) {
                        $start_time_work = strtotime($e->member->membersubtype->time_attendance);
                    } else if (!empty($e->member->membertype)) {
                        $start_time_work = strtotime($e->member->membertype->time_attendance);
                    }
                    if (!empty($e->member->membersubtype) || !empty($e->member->membertype)) {
                        $check_in = strtotime(Yii::$app->Utilities->formatDate('H:i:s', $e->punch_datetime));
                        $diff = $check_in - $start_time_work;
                        $tmp['late'] = round($diff / (60));
                        if ($tmp['late'] < 0) {
                            $tmp['late'] = 0;
                        }
                    }

                } else {
                    $tmp['punch_time_out'] = Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime);
                    $tmp['lat_long_out'] = $e->latitude . "," . $e->longitude;

                    $out = strtotime($tmp['punch_time_out']);
                    $in = strtotime($tmp['punch_time_in']);
                    $time_work = $out - $in;
                    $tmp['time_work'] = round($time_work / (60 * 60));

                }

                $start = strtotime(Yii::$app->Utilities->formatDate('Y-m-d', $e->member->start_work_date));
                $end = strtotime(Yii::$app->Utilities->formatDate('Y-m-d', date("Y-m-d")));
                $datediff = $end - $start;
                $day_ = round($datediff / (60 * 60 * 24));

                // \app\components\Helpers::Debig($e->punch);
                // \app\components\Helpers::Debig($e->punch);
                $tmp['member_id'] = $e->member->id;
                $tmp['member_no'] = $e->member->member_no;
                $tmp['fullname'] = $e->member->fullname;
                $tmp['position_name'] = $e->member->position_name;
                $tmp['type_name'] = $e->member->type_name;
                $tmp['account'] = $e->area->account;
                $tmp['local_name'] = $e->area->local_name;
                $tmp['location'] = $e->area->area_name;
                $tmp['lat_long_location'] = $e->area->latitude . "," . $e->area->longitude;
                $tmp['area'] = $e->area->area;
                $tmp['start_work_date'] = Yii::$app->Utilities->formatDate('Y-m-d', $e->member->start_work_date);
                $tmp['work_life'] = $day_;
                $count++;

            }

            //  กรณีเป็นตัวสุดท้ายหากเป็นเข้างาน แต่ไม่ได้ กดออกงาน
            if ($count == 1 && count($dataQuery) == $key + 1) {
                if ($e->punch_type == "in") {
                    $tmp['punch_time_in'] = Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime);
                    $tmp['lat_long_in'] = $e->latitude . "," . $e->longitude;
                    $tmp['day_work'] = Yii::$app->Utilities->formatDate('Y-m-d', $e->punch_datetime);

                    if (!empty($e->member->membersubtype)) {
                        $start_time_work = strtotime($e->member->membersubtype->time_attendance);
                    } else if (!empty($e->member->membertype)) {
                        $start_time_work = strtotime($e->member->membertype->time_attendance);
                    }
                    if (!empty($e->member->membersubtype) || !empty($e->member->membertype)) {
                        $check_in = strtotime(Yii::$app->Utilities->formatDate('H:i:s', $e->punch_datetime));
                        $diff = $check_in - $start_time_work;
                        $tmp['late'] = round($diff / (60));
                        if ($tmp['late'] < 0) {
                            $tmp['late'] = 0;
                        }

                    }

                } else {
                    $tmp['punch_time_out'] = Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime);
                    $tmp['lat_long_out'] = $e->latitude . "," . $e->longitude;

                    $out = strtotime($tmp['punch_time_out']);
                    $in = strtotime($tmp['punch_time_in']);
                    $time_work = $out - $in;
                    $tmp['time_work'] = round($time_work / (60 * 60));
                }

                $start = strtotime(Yii::$app->Utilities->formatDate('Y-m-d', $e->member->start_work_date));
                $end = strtotime(Yii::$app->Utilities->formatDate('Y-m-d', date("Y-m-d")));
                $datediff = $end - $start;
                $day_ = round($datediff / (60 * 60 * 24));

                // \app\components\Helpers::Debig($e->punch);
                $tmp['member_id'] = $e->member->id;
                $tmp['member_no'] = $e->member->member_no;
                $tmp['fullname'] = $e->member->fullname;
                $tmp['position_name'] = $e->member->position_name;
                $tmp['type_name'] = $e->member->type_name;
                $tmp['account'] = $e->area->account;
                $tmp['local_name'] = $e->area->local_name;
                $tmp['location'] = $e->area->area_name;
                $tmp['lat_long_location'] = $e->area->latitude . "," . $e->area->longitude;
                $tmp['area'] = $e->area->area;
                $tmp['start_work_date'] = Yii::$app->Utilities->formatDate('Y-m-d', $e->member->start_work_date);
                $tmp['work_life'] = $day_;
                $count++;

                if (empty($tmp['punch_time_out'])) {
                    $tmp['punch_time_out'] = "";
                }

                if (empty($tmp['lat_long_out'])) {
                    $tmp['lat_long_out'] = "";
                }

                if (empty($tmp['time_work'])) {
                    $tmp['time_work'] = "";
                }

                array_push($data, $tmp);
                $tmp = array();
                $count = 0;
            }
        }

        // \app\components\Helpers::Debig($data);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $modelJson = array(
            'result' => $data,
            'code' => 200,
            'status' => true,
            'message' => 'Success',
        );

    }

    public function actionGetdataa($position = null, $type = null, $subtype = null, $date_from, $date_to)
    {
        date_default_timezone_set("Asia/Bangkok");

        $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $date_from);
        $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $date_to);
  
        $query = MemberForPunch::find()
            ->leftJoin('member_punch_clock', 'member_punch_clock.member_id = member.id')
        // ->joinWith('punch')
        // ->where(['in', 'member_punch_clock.member_id', $subQuery])
            ->andWhere(['between', 'member_punch_clock.punch_datetime', $from, $to])
            ->orderBy(['member_punch_clock.member_id' => SORT_ASC])
        // ->all()
        ;

        if ($position != null && $position != 'all') {
            $query->andWhere(['=', 'member.position_id', $position]);
        }

        if ($type != null && $type != 'all') {
            $query->andWhere(['=', 'member.member_type_id', $type]);
        }

        if ($subtype != null && $subtype != 'all') {
            $query->andWhere(['=', 'member.member_subtype_id', $subtype]);
        }

        $dataQuery = $query->all();

        $count = 0;
        $output = [];
        $tmp = array();
        $data = [];
        $ot_time = 8;

        foreach ($dataQuery as $key => $e) {

            $countDayOff = 0;
            $countDayWork = 0;
            // เช็ควันหยุดของพนักงานคนนั้นๆจาก subtype หากไม่มีเช็คจาก type
            if (!empty($e->membersubtype)) {
                $member_day_monday = $e->membersubtype->monday;
                $member_day_thesday = $e->membersubtype->thesday;
                $member_day_wednesday = $e->membersubtype->wednesday;
                $member_day_thursday = $e->membersubtype->thursday;
                $member_day_friday = $e->membersubtype->friday;
                $member_day_saturday = $e->membersubtype->saturday;
                $member_day_sunday = $e->membersubtype->sunday;

            } else if (!empty($e->membertype)) {
                $member_day_monday = $e->membertype->monday;
                $member_day_thesday = $e->membertype->thesday;
                $member_day_wednesday = $e->membertype->wednesday;
                $member_day_thursday = $e->membertype->thursday;
                $member_day_friday = $e->membertype->friday;
                $member_day_saturday = $e->membertype->saturday;
                $member_day_sunday = $e->membertype->sunday;

            }

            $cal_day_start = Yii::$app->Utilities->formatDate('Y-m-d', $date_from);
            $cal_day_end = Yii::$app->Utilities->formatDate('Y-m-d', $date_to);
            while ($cal_day_start <= $cal_day_end) {
                $today = Yii::$app->Utilities->formatDate('w', $cal_day_start);
                // เช็คว่าวันนี้ตรงกับวันหยุดหรือไม่ N = ไม่หยุด Y = หยุด
                if (1 == 1) {
                    if ($member_day_monday == "N") {
                        if ($today == 1) {
                            $countDayWork++;
                        }
                    }
                    if ($member_day_thesday == "N") {
                        if ($today == 2) {
                            $countDayWork++;
                        }
                    }
                    if ($member_day_wednesday == "N") {
                        if ($today == 3) {
                            $countDayWork++;
                        }
                    }
                    if ($member_day_thursday == "N") {
                        if ($today == 4) {
                            $countDayWork++;
                        }
                    }
                    if ($member_day_friday == "N") {
                        if ($today == 5) {
                            $countDayWork++;
                        }
                    }
                    if ($member_day_saturday == "N") {
                        if ($today == 6) {
                            $countDayWork++;
                        }
                    }
                    if ($member_day_sunday == "N") {
                        if ($today == 0) {
                            $countDayWork++;
                        }
                    }

                    if ($member_day_monday == "Y") {
                        if ($today == 1) {
                            $countDayOff++;
                        }
                    }
                    if ($member_day_thesday == "Y") {
                        if ($today == 2) {
                            $countDayOff++;
                        }
                    }
                    if ($member_day_wednesday == "Y") {
                        if ($today == 3) {
                            $countDayOff++;
                        }
                    }
                    if ($member_day_thursday == "Y") {
                        if ($today == 4) {
                            $countDayOff++;
                        }
                    }
                    if ($member_day_friday == "Y") {
                        if ($today == 5) {
                            $countDayOff++;
                        }
                    }
                    if ($member_day_saturday == "Y") {
                        if ($today == 6) {
                            $countDayOff++;
                        }
                    }
                    if ($member_day_sunday == "Y") {
                        if ($today == 0) {
                            $countDayOff++;
                        }
                    }
                }

                $cal_day_start = date('Y-m-d', strtotime($cal_day_start . "+1 days"));
            }
            $dataLeave = MemberLeave::find()
                ->where(['member_id' => $e->id, 'isActive' => 'Y', 'isDelete' => 'N'])
                ->all();

            $tmp['hoilday_leave'] = 0;
            $tmp['errand_leave'] = 0;
            $tmp['sick_leave'] = 0;

            $start_ = strtotime(Yii::$app->Utilities->formatDate('Y-m-d', $date_from));
            $end_ = strtotime(Yii::$app->Utilities->formatDate('Y-m-d', $date_to));

            $datediff_ = $end_ - $start_;
            $day_all = round($datediff_ / (60 * 60 * 24));
            foreach ($dataLeave as $dl => $dleave) {

                $start_leave = strtotime(Yii::$app->Utilities->formatDate('Y-m-d', $date_from));

                $leave_start = strtotime(Yii::$app->Utilities->formatDate('Y-m-d', $dleave->start_date));
                $leave_end = strtotime(Yii::$app->Utilities->formatDate('Y-m-d', $dleave->end_date));

                for ($x = 0; $x < $day_all; $x++) {

                    if (($start_leave >= $leave_start) && ($start_leave <= $leave_end)) {

                        $start = strtotime($dleave->start_date);
                        $end = strtotime($dleave->end_date);
                        $datediff = $end - $start;
                        $day_ = round($datediff / (60 * 60 * 24));

                        switch ($dleave->leave_type_id) {
                            case 1:
                                // ลาพักร้อน
                                $tmp['hoilday_leave'] += 1;
                                break;

                            case 3:
                                // ลากิจคิดเงิน
                                $tmp['errand_leave'] += 1;
                                break;

                            case 5:
                                // ลาป่วยคิดเงิน
                                $tmp['sick_leave'] += 1;
                                break;

                        }
                    }

                    $start_leave = date('Y-m-d', strtotime($start_leave . "+1 days"));
                }

            }

            $start = strtotime(Yii::$app->Utilities->formatDate('Y-m-d', $e->start_work_date));
            $end = strtotime(Yii::$app->Utilities->formatDate('Y-m-d', date("Y-m-d")));
            $datediff = $end - $start;
            $day_ = round($datediff / (60 * 60 * 24));

            $tmp['member_id'] = $e->id;
            $tmp['member_no'] = $e->member_no;
            $tmp['fullname'] = $e->fullname;
            $tmp['position_name'] = $e->position_name;
            $tmp['type_name'] = $e->type_name;
            $tmp['start_work_date'] = Yii::$app->Utilities->formatDate('Y-m-d', $e->start_work_date);
            $tmp['work_life'] = $day_;

            // $from
            // $today = date('w');
            $remember_day = [];
            $remember_location = [];
            $count_punch = 0;
            $count_late_time = 0;
            $count_work_time = 0;
            $count_ot_time = 0;
            foreach ($e->punch as $kk => $v) {

                $tmp_check = false;
                $tmp_check_location = false;
                $tmp_check_dayoff = false;
                // $tmp['area'] = $v->area->area;

                if ($v->punch_type == "in") {

                    foreach ($remember_location as $l => $lc) {
                        if ($lc == $v->area->area_name) {
                            $tmp_check_location = true;
                        }
                    }

                    if ($tmp_check_location == false) {
                        array_push($remember_location, $v->area->area_name);
                    }

                    $punch_time_in = Yii::$app->Utilities->formatDate('H:i', $v->punch_datetime);

                    if (!empty($e->membersubtype)) {
                        $start_time_work = strtotime($e->membersubtype->time_attendance);
                    } else if (!empty($e->membertype)) {
                        $start_time_work = strtotime($e->membertype->time_attendance);
                    } else {
                        $start_time_work = strtotime("08:00");
                    }

                    if (!empty($e->membersubtype) || !empty($e->membertype)) {

                        $check_in = strtotime(Yii::$app->Utilities->formatDate('H:i', $v->punch_datetime));
                        $diff = $check_in - $start_time_work;
                        $tmp_time = round($diff / (60));

                        if ($tmp_time < 0) {
                            $tmp_time = 0;
                        }

                        if (!empty($e->membersubtype)) {
                            if ($e->membersubtype->attendance_type == 'N') {
                                $tmp_time = 0;
                            }
                        }
                        if (!empty($e->membertype)) {
                            if ($e->membertype->attendance_type == 'N') {
                                $tmp_time = 0;
                            }
                        }
                        
                        $count_late_time += $tmp_time;
                    }

                    $count_punch++;
                } else if ($v->punch_type == "out") {
                    $punch_time_out = Yii::$app->Utilities->formatDate('H:i', $v->punch_datetime);

                    $out = strtotime($punch_time_out);
                    $in = strtotime($punch_time_in);
                    $time_work = $out - $in;
                    $tmp_timework = round($time_work / (60 * 60));
                    $count_work_time += $tmp_timework;

                    if ($tmp_timework > $ot_time) {
                        $count_ot_time += $tmp_timework - $ot_time;
                    }
                    $count_punch = 0;

                }

                if (1 == 1) {
                    $work_today = Yii::$app->Utilities->formatDate('w', $v->punch_datetime);

                    if ($member_day_monday == "Y") {
                        if ($work_today == 1) {
                            $tmp_check_dayoff = true;
                        }
                    }
                    if ($member_day_thesday == "Y") {
                        if ($work_today == 2) {
                            $tmp_check_dayoff = true;
                        }
                    }
                    if ($member_day_wednesday == "Y") {
                        if ($work_today == 3) {
                            $tmp_check_dayoff = true;
                        }
                    }
                    if ($member_day_thursday == "Y") {
                        if ($work_today == 4) {
                            $tmp_check_dayoff = true;
                        }
                    }
                    if ($member_day_friday == "Y") {
                        if ($work_today == 5) {
                            $tmp_check_dayoff = true;
                        }
                    }
                    if ($member_day_saturday == "Y") {
                        if ($work_today == 6) {
                            $tmp_check_dayoff = true;
                        }
                    }
                    if ($member_day_sunday == "Y") {
                        if ($work_today == 0) {
                            $tmp_check_dayoff = true;
                        }
                    }
                }

                foreach ($remember_day as $d => $rd) {

                    if ($rd == Yii::$app->Utilities->formatDate('Y-m-d', $v->punch_datetime)) {
                        $tmp_check = true;
                    }
                }

                if ($tmp_check == false && $tmp_check_dayoff == false) {
                    array_push($remember_day, Yii::$app->Utilities->formatDate('Y-m-d', $v->punch_datetime));
                }

            }

            $tmp['day_off'] = ($countDayWork - count($remember_day) < 0) ? 0: $countDayWork - count($remember_day) ;
            $tmp['normal_leave'] = $countDayOff;
            $tmp['day_work'] = count($remember_day);
            $tmp['location_work'] = implode(",", $remember_location);
            $tmp['late_time'] = $count_late_time;
            $tmp['work_time'] = $count_work_time;
            $tmp['ot_time'] = $count_ot_time;

            array_push($data, $tmp);
            $tmp = array();
            // $count = 0;

        }

        // \app\components\Helpers::Debig($data);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $modelJson = array(
            'result' => $data,
            'code' => 200,
            'status' => true,
            'message' => 'Success',
        );

    }

}
