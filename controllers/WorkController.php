<?php

namespace app\controllers;

use app\components\Utilities;
use app\models\Member;
use app\models\MemberOt;
use app\models\MemberPunchClock;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

/**
 * MemberOtController implements the CRUD actions for MemberOt model.
 */
class WorkController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [

                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MemberOt models.
     * @return mixed
     */

    // public function actionIndex()
    // {

    //     // return $this->render('index', [
    //     //     'searchModel' => $searchModel,
    //     //     'dataProvider' => $dataProvider,
    //     // ]);
    // }

    public function actionUserWorkingToday()
    {
        date_default_timezone_set("Asia/Bangkok");

        return $this->render('user-working-today', [
            // 'model' => $model,
            // 'modelSync' => $modelSync,
            // 'leaveToday' => $leaveToday,
            // 'dayOffToday' => $dayOffTodays
        ]);

    }

    public function actionUserWorkingMap()
    {

        return $this->render('user-working-map', [
            // 'model' => $model,
        ]);
    }

    // get user that woking today
    public function actionGetWorkingMap()
    {

        Yii::$app->response->format = Response::FORMAT_JSON;

        // try {
        date_default_timezone_set("Asia/Bangkok");

        //code...
        $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', date('Y-m-d H:i:s'));
        $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', date('Y-m-d H:i:s'));
        // ->andWhere(['between', 'created_date', $from, $to])

        $member_under_role = null;
        $position = null;

        // check if admin
        if (Yii::$app->user->identity->auth_member_role == 1) {
            if (Yii::$app->user->identity->status != 'X') {
                $position = Yii::$app->user->identity->position_id;
            }

            if (Yii::$app->user->identity->id) {
                $member_under_role = Yii::$app->user->identity->id;
            }

        }

        if (Yii::$app->user->identity->position_id == 8) {
            $member_under_role = null;
            $position = null;
        }

        $subQuery = MemberPunchClock::find()->select('MAX(member_punch_clock.id)')
            ->leftJoin('member', 'member.id = member_punch_clock.member_id')
            ->where(['punch_type' => 'in'])
            ->andWhere(['!=', 'member.status', 'X'])
            ->andFilterWhere(['<>', 'member.id', $member_under_role])
        // ->andFilterWhere(['member.position_id' => $position])
            ->andFilterWhere(['member.member_under_id' => $member_under_role])
        // ->orFilterWhere(['member.member_under_id' => $member_under_role])
            ->andWhere(['between', 'punch_datetime', $from, $to])
            ->orderBy(['punch_datetime' => SORT_DESC])
            ->groupBy('member_id');
        $query = MemberPunchClock::find()->where(['in', 'id', $subQuery]);
        $model = $query->all();

        $arrData = [];
        foreach ($model as $key => $e) {
            $tmp = array(
                'member_id' => $e->member->id,
                'img' => $e->img,
                'fullname' => $e->member->fullname,
                'member_no' => $e->member->member_no,
                'type_name' => $e->member->type_name,
                'subtype_name' => $e->member->subtype_name,
                'phone_number' => $e->member->phone_number,
                'area_name' => $e->area->area_name,
                'brach_no' => $e->area->brach_no,
                'account' => $e->area->account,
                'local_name' => $e->area->local_name,
                'latitude' => $e->latitude,
                'longitude' => $e->longitude,
                'detail' => Yii::$app->user->identity,
                'member_under_role' => $member_under_role,
            );
            array_push($arrData, $tmp);
        }
        // \app\components\Helpers::Debig($arrData );

        // ->createCommand()
        // ->queryAll();
        // $test=2;

        return $modelJson = array(
            'result' => $arrData,
            'code' => 200,
            'status' => true,
            'message' => 'Success',
        );

    }

    // get data in graph index
    public function actionCurrentWorking($date)
    {

        try {
            // date_default_timezone_set("Asia/Bangkok");

            $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $date);
            $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $date);
            // \app\components\Helpers::Debig($from );

            $member_under_role = null;
            $position = null;

            if (Yii::$app->user->identity->auth_member_role == 0) {
                if (Yii::$app->user->identity->status != 'X') {
                    $position = Yii::$app->user->identity->position_id;
                }

                if (Yii::$app->user->identity->id) {
                    $member_under_role = Yii::$app->user->identity->id;
                }

            }

            $subQuery = MemberPunchClock::find()->select('MAX(member_punch_clock.id)')
                ->leftJoin('member', 'member.id = member_punch_clock.member_id')
            // ->where(['punch_type' => ['in','out']])
            // ->where(['!=', 'punch_type', 'X'])

                ->andWhere(['between', 'punch_datetime', $from, $to])
                ->andWhere(['!=', 'member.status', 'X'])
                ->andFilterWhere(['<>', 'member.id', $member_under_role])
                ->andFilterWhere(['member.position_id' => $position])
            // ->orFilterWhere(['member.member_under_id' => $member_under_role])
                ->andFilterWhere(['member.member_under_id' => $member_under_role])

                ->groupBy(['member_id', 'punch_type'])
            ;
            $query = MemberPunchClock::find()->where(['in', 'id', $subQuery])->orderBy(['member_id' => SORT_ASC]);
            $today = $query->all();
            // \app\components\Helpers::Debig($today);

            $time6_8 = 0;
            $time8_10 = 0;
            $time10_12 = 0;
            $time12_14 = 0;
            $time14_16 = 0;
            $time16_18 = 0;
            $time18_20 = 0;
            $time20_22 = 0;
            $time22_24 = 0;

            $out6_8 = 0;
            $out8_10 = 0;
            $out10_12 = 0;
            $out12_14 = 0;
            $out14_16 = 0;
            $out16_18 = 0;
            $out18_20 = 0;
            $out20_22 = 0;
            $out22_24 = 0;

            $late6_8 = 0;
            $late8_10 = 0;
            $late10_12 = 0;
            $late12_14 = 0;
            $late14_16 = 0;
            $late16_18 = 0;
            $late18_20 = 0;
            $late20_22 = 0;
            $late22_24 = 0;

            foreach ($today as $key => $e) {
                // \app\components\Helpers::Debig($e);

                if ($e->punch_type == "in") {
                    $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 07:59:59', $date);
                    if ($e->punch_datetime > $from && $e->punch_datetime < $to) {

                        if (empty($e->member->time_att)) {
                            $start = "08:00";
                        } else {
                            $start = $e->member->time_att;
                        }

                        $start_work_time = strtotime(Yii::$app->Utilities->formatDate('H:i', $start));
                        $check_in = strtotime(Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime));
                        $diff = $check_in - $start_work_time;

                        $tmp_time = round($diff / (60));
                        // \app\components\Helpers::Debig(Yii::$app->Utilities->formatDate('H:i', $start));

                        // if ($tmp_time > 0) {
                        //     $late6_8++;
                        // }

                        if (!empty($e->member->membersubtype)) {
                            if ($e->member->membersubtype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late6_8++;
                            }
                        }

                        if (!empty($e->member->membertype)) {
                            if ($e->member->membertype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late6_8++;
                            }
                        }
                        $time6_8++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 08:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 09:59:59', $date);
                    if ($e->punch_datetime >= $from && $e->punch_datetime <= $to) {

                        if (empty($e->member->time_att)) {
                            $start = "08:00";
                        } else {
                            $start = $e->member->time_att;
                        }

                        $start_work_time = strtotime(Yii::$app->Utilities->formatDate('H:i', $start));
                        $check_in = strtotime(Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime));
                        $diff = $check_in - $start_work_time;

                        $tmp_time = round($diff / (60));

                        // if ($tmp_time > 0) {
                        //     $late8_10++;
                        // }

                        if (!empty($e->member->membersubtype)) {
                            if ($e->member->membersubtype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late8_10++;
                            }
                        }

                        if (!empty($e->member->membertype)) {
                            if ($e->member->membertype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late8_10++;
                            }
                        }

                        $time8_10++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 10:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 11:59:59', $date);
                    if ($e->punch_datetime >= $from && $e->punch_datetime <= $to) {

                        if (empty($e->member->time_att)) {
                            $start = "08:00";
                        } else {
                            $start = $e->member->time_att;
                        }

                        $start_work_time = strtotime(Yii::$app->Utilities->formatDate('H:i', $start));
                        $check_in = strtotime(Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime));
                        $diff = $check_in - $start_work_time;

                        $tmp_time = round($diff / (60));

                        // if ($tmp_time > 0) {
                        //     $late10_12++;
                        // }

                        if (!empty($e->member->membersubtype)) {
                            if ($e->member->membersubtype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late10_12++;
                            }
                        }

                        if (!empty($e->member->membertype)) {
                            if ($e->member->membertype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late10_12++;
                            }
                        }
                        $time10_12++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 12:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 13:59:59', $date);
                    if ($e->punch_datetime >= $from && $e->punch_datetime <= $to) {

                        if (empty($e->member->time_att)) {
                            $start = "08:00";
                        } else {
                            $start = $e->member->time_att;
                        }

                        $start_work_time = strtotime(Yii::$app->Utilities->formatDate('H:i', $start));
                        $check_in = strtotime(Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime));
                        $diff = $check_in - $start_work_time;

                        $tmp_time = round($diff / (60));

                        // if ($tmp_time > 0) {
                        //     $late12_14++;
                        // }

                        if (!empty($e->member->membersubtype)) {
                            if ($e->member->membersubtype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late12_14++;
                            }
                        }

                        if (!empty($e->member->membertype)) {
                            if ($e->member->membertype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late12_14++;
                            }
                        }
                        $time12_14++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 14:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 15:59:59', $date);
                    if ($e->punch_datetime >= $from && $e->punch_datetime <= $to) {

                        if (empty($e->member->time_att)) {
                            $start = "08:00";
                        } else {
                            $start = $e->member->time_att;
                        }

                        $start_work_time = strtotime(Yii::$app->Utilities->formatDate('H:i', $start));
                        $check_in = strtotime(Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime));
                        $diff = $check_in - $start_work_time;

                        $tmp_time = round($diff / (60));

                        // if ($tmp_time > 0) {
                        //     $late14_16++;
                        // }

                        if (!empty($e->member->membersubtype)) {
                            if ($e->member->membersubtype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late14_16++;
                            }
                        }

                        if (!empty($e->member->membertype)) {
                            if ($e->member->membertype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late14_16++;
                            }
                        }

                        $time14_16++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 16:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 17:59:59', $date);
                    if ($e->punch_datetime >= $from && $e->punch_datetime <= $to) {

                        if (empty($e->member->time_att)) {
                            $start = "08:00";
                        } else {
                            $start = $e->member->time_att;
                        }

                        $start_work_time = strtotime(Yii::$app->Utilities->formatDate('H:i', $start));
                        $check_in = strtotime(Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime));
                        $diff = $check_in - $start_work_time;

                        $tmp_time = round($diff / (60));

                        // if ($tmp_time > 0) {
                        //     $late16_18++;
                        // }

                        if (!empty($e->member->membersubtype)) {
                            if ($e->member->membersubtype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late16_18++;
                            }
                        }

                        if (!empty($e->member->membertype)) {
                            if ($e->member->membertype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late16_18++;
                            }
                        }
                        $time16_18++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 18:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 19:59:59', $date);
                    if ($e->punch_datetime >= $from && $e->punch_datetime <= $to) {

                        if (empty($e->member->time_att)) {
                            $start = "08:00";
                        } else {
                            $start = $e->member->time_att;
                        }

                        $start_work_time = strtotime(Yii::$app->Utilities->formatDate('H:i', $start));
                        $check_in = strtotime(Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime));
                        $diff = $check_in - $start_work_time;

                        $tmp_time = round($diff / (60));

                        // if ($tmp_time > 0) {
                        //     $late18_20++;
                        // }

                        if (!empty($e->member->membersubtype)) {
                            if ($e->member->membersubtype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late18_20++;
                            }
                        }

                        if (!empty($e->member->membertype)) {
                            if ($e->member->membertype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late18_20++;
                            }
                        }
                        $time18_20++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 20:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 21:59:59', $date);
                    if ($e->punch_datetime >= $from && $e->punch_datetime <= $to) {

                        if (empty($e->member->time_att)) {
                            $start = "08:00";
                        } else {
                            $start = $e->member->time_att;
                        }

                        $start_work_time = strtotime(Yii::$app->Utilities->formatDate('H:i', $start));
                        $check_in = strtotime(Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime));
                        $diff = $check_in - $start_work_time;

                        $tmp_time = round($diff / (60));

                        if (!empty($e->member->membersubtype)) {
                            if ($e->member->membersubtype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late20_22++;
                            }
                        }

                        if (!empty($e->member->membertype)) {
                            if ($e->member->membertype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late20_22++;
                            }
                        }
                        $time20_22++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 22:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $date);
                    if ($e->punch_datetime >= $from && $e->punch_datetime <= $to) {

                        if (empty($e->member->time_att)) {
                            $start = "08:00";
                        } else {
                            $start = $e->member->time_att;
                        }

                        $start_work_time = strtotime(Yii::$app->Utilities->formatDate('H:i', $start));
                        $check_in = strtotime(Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime));
                        $diff = $check_in - $start_work_time;

                        $tmp_time = round($diff / (60));

                        // if ($tmp_time > 0) {
                        //     $late22_24++;
                        // }
                        if (!empty($e->member->membersubtype)) {
                            if ($e->member->membersubtype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late22_24++;
                            }
                        }

                        if (!empty($e->member->membertype)) {
                            if ($e->member->membertype->attendance_type == 'Y' && $tmp_time > 0) {
                                $late22_24++;
                            }
                        }
                        $time22_24++;
                    }
                } else if ($e->punch_type == "out") {
                    $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 07:59:59', $date);
                    if ($e->punch_datetime > $from && $e->punch_datetime < $to) {
                        $out6_8++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 08:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 09:59:59', $date);
                    if ($e->punch_datetime > $from && $e->punch_datetime < $to) {
                        $out8_10++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 10:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 11:59:59', $date);
                    if ($e->punch_datetime > $from && $e->punch_datetime < $to) {
                        $out10_12++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 12:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 13:59:59', $date);
                    if ($e->punch_datetime > $from && $e->punch_datetime < $to) {
                        $out12_14++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 14:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 15:59:59', $date);
                    if ($e->punch_datetime > $from && $e->punch_datetime < $to) {
                        $out14_16++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 16:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 17:59:59', $date);
                    if ($e->punch_datetime > $from && $e->punch_datetime < $to) {
                        $out16_18++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 18:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 19:59:59', $date);
                    if ($e->punch_datetime > $from && $e->punch_datetime < $to) {
                        $out18_20++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 20:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 21:59:59', $date);
                    if ($e->punch_datetime > $from && $e->punch_datetime < $to) {
                        $out20_22++;
                    }

                    $from = Yii::$app->Utilities->formatDate('Y-m-d 22:00:00', $date);
                    $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $date);
                    if ($e->punch_datetime > $from && $e->punch_datetime < $to) {
                        $out22_24++;
                    }
                }

            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            return $modelJson = array(
                'result' => array(
                    'time6_8' => (int) $time6_8,
                    'time8_10' => (int) $time8_10,
                    'time10_12' => (int) $time10_12,
                    'time12_14' => (int) $time12_14,
                    'time14_16' => (int) $time14_16,
                    'time16_18' => (int) $time16_18,
                    'time18_20' => (int) $time18_20,
                    'time20_22' => (int) $time20_22,
                    'time22_24' => (int) $time22_24,
                    'out6_8' => (int) $out6_8,
                    'out8_10' => (int) $out8_10,
                    'out10_12' => (int) $out10_12,
                    'out12_14' => (int) $out12_14,
                    'out14_16' => (int) $out14_16,
                    'out16_18' => (int) $out16_18,
                    'out18_20' => (int) $out18_20,
                    'out20_22' => (int) $out20_22,
                    'out22_24' => (int) $out22_24,
                    'late6_8' => (int) $late6_8,
                    'late8_10' => (int) $late8_10,
                    'late10_12' => (int) $late10_12,
                    'late12_14' => (int) $late12_14,
                    'late14_16' => (int) $late14_16,
                    'late16_18' => (int) $late16_18,
                    'late18_20' => (int) $late18_20,
                    'late20_22' => (int) $late20_22,
                    'late22_24' => (int) $late22_24,
                ),
                'code' => 200,
                'status' => true,
                'message' => 'Success',
            );
        } catch (\Exception $th) {
            //throw $th;
            // \app\components\Helpers::Debig(($th));
            Yii::$app->response->format = Response::FORMAT_JSON;

            return $modelJson = [
                'code' => 400,
                'status' => false,
                'message' => $th,
            ];

        }

    }

    public function actionGetdata()
    {
        date_default_timezone_set("Asia/Bangkok");

        $request = Yii::$app->request->queryParams;
        // \app\components\Helpers::Debig($request['start']); 

        $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', date('Y-m-d H:i:s'));
        $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', date('Y-m-d H:i:s'));

        if ($request['start'] && $request['end']) {
            $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $request['start']);
            $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $request['end']);
        }

        $query = MemberPunchClock::find()
            ->andWhere(['between', 'punch_datetime', $from, $to])
            ->orderBy(['punch_datetime' => SORT_DESC])
            ->all();

        $count = 0;
        $output = [];
        $data = [];
        $tmp = array();
        foreach ($query as $key => $e) {
            // \app\components\Helpers::Debig($e);
            $location = $e->area->account . "/" . $e->area->brach_no . "/" . $e->area->local_name;

            $tmp['member_no'] = $e->member->member_no;
            $tmp['fullname'] = $e->member->name_th . ' ' . $e->member->surname_th;
            $tmp['punch_time'] = Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime);
            $tmp['punch_type'] = $e->punch_type == "in" ? "บันทึกเข้า" : "บันทึกออก";
            $tmp['location'] = $location;
            $tmp['department_name'] = $e->member->department_name;
            $tmp['position_name'] = $e->member->position_name;
            $tmp['type_name'] = $e->member->type_name;
            $tmp['subtype_name'] = $e->member->subtype_name;
            $tmp['punch_date'] = Yii::$app->Utilities->formatDate('d/m/yy', $e->punch_datetime);
            $tmp['memberundder_name'] = $e->member->memberundder_name;
            $tmp['area'] = $e->member->area_name;
            $tmp['zone'] = $e->member->zone_name;
            $tmp['channel'] = $e->member->channel_name;
            $tmp['account'] = $e->member->account_name;

            array_push($data, $tmp);
            $tmp = array();

        }

        // \app\components\Helpers::Debig($data);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $modelJson = array(
            'result' => $data,
            'code' => 200,
            'status' => true,
            'message' => 'Success',
        );

    }

    public function actionGetRows()
    {
        date_default_timezone_set("Asia/Bangkok");

        $Sup = Yii::$app->Utilities->getRowsPosition('Sup');
        $PI = Yii::$app->Utilities->getRowsPosition('PI');
        $CVS = Yii::$app->Utilities->getRowsType('CVS');
        $MT = Yii::$app->Utilities->getRowsType('MT');
        $BA = Yii::$app->Utilities->getRowsType('BA');
        $BASkin = Yii::$app->Utilities->getRowsSubType('BA HBA Skin');
        $BAHair = Yii::$app->Utilities->getRowsSubType('BA Hair');
        $BAFab = Yii::$app->Utilities->getRowsSubType('BA Fab Sol');
        $BAWatsons = Yii::$app->Utilities->getRowsSubType('BA Watsons');

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $modelJson = array(
            'result' => array(
                'Sup' => $Sup,
                'PI' => $PI,
                'CVS' => $CVS,
                'MT' => $MT,
                'BA' => $BA,
                'BASkin' => $BASkin,
                'BAHair' => $BAHair,
                'BAFab' => $BAFab,
                'BAWatsons' => $BAWatsons,

            ),
            'code' => 200,
            'status' => true,
            'message' => 'Success',
        );

    }

    public function actionGetDaily()
    {

        date_default_timezone_set("Asia/Bangkok");

        $MemberCount = Member::find()->count();

        $leaveToday = Yii::$app->Utilities->getMemberLeaveToday();

        $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', date('Y-m-d H:i:s'));
        $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', date('Y-m-d H:i:s'));
        // ->andWhere(['between', 'created_date', $from, $to])
        // \app\components\Helpers::Debig( 'test '.date('Y-m-d H:i:s'));

        // check if admin
        if (Yii::$app->user->identity->auth_member_role == 1) {
            if (Yii::$app->user->identity->status != 'X') {
                $position = Yii::$app->user->identity->position_id;
            }

            if (Yii::$app->user->identity->id) {
                $member_under_role = Yii::$app->user->identity->id;
            }

        }

        if (Yii::$app->user->identity->position_id == 8) {
            $member_under_role = null;
            $position = null;
        }

        $subQuery = MemberPunchClock::find()->select('MAX(member_punch_clock.id)')
            ->leftJoin('member', 'member.id = member_punch_clock.member_id')
            ->where(['punch_type' => 'in'])
            ->andWhere(['!=', 'member.status', 'X'])
            ->andFilterWhere(['<>', 'member.id', $member_under_role])
        // ->andFilterWhere(['member.position_id' => $position])
        // ->orFilterWhere(['member.member_under_id' => $member_under_role])
            ->andFilterWhere(['member.member_under_id' => $member_under_role])
            ->andWhere(['between', 'punch_datetime', $from, $to])
            ->groupBy('member_id');

        $query = MemberPunchClock::find()->where(['in', 'member_punch_clock.id', $subQuery]);
        $WorkTotal = $query->count();

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $modelJson = array(
            'result' => array(
                'MemberCount' => $MemberCount,
                'leaveToday' => $leaveToday,
                'WorkTotal' => $WorkTotal,

            ),
            'code' => 200,
            'status' => true,
            'message' => 'Success',
        );

    }
    public function actionGetDayoff()
    {
        date_default_timezone_set("Asia/Bangkok");

        $dayOffToday = Yii::$app->Utilities->getMemberDayOffToday();

        Yii::$app->response->format = Response::FORMAT_JSON;
        $modelJson = array(
            'result' => array(
                'dayOffToday' => $dayOffToday,
            ),
            'code' => 200,
            'status' => true,
            'message' => 'Success',
        );

        return $modelJson;

    }

}
