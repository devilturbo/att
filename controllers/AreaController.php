<?php

namespace app\controllers;

use app\components\Helpers;
use app\components\Utilities;
use app\models\Area;
use app\models\AreaGroup;
use app\models\AreaList;
use app\models\AreaTemp;
use app\models\MemberAccount;
use app\models\MemberArea;
use app\models\MemberChannel;
use app\models\MemberZone;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\BaseFileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * AreaController implements the CRUD actions for Area model.
 */
class AreaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error', 'login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [

                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],

            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }



    public function beforeAction($action)
    {
        $auth = Utilities::setAuthController();
        if($auth['area'] == 'N')
            throw new NotFoundHttpException('page not found');

        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }



    /**
     * Lists all Area models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = Area::find()
        // ->offset(0)
            ->limit(10)
            ->where(['isDelete' => 'N'])->all();
        $modelRows = Area::find()
            ->where(['isDelete' => 'N'])
            ->count();

        return $this->render('index', [
            'model' => $model,
            'modelRows' => $modelRows,

        ]);
    }

    /**
     * Displays a single Area model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionImport()
    {
        $model = new AreaTemp();
        $path = Yii::$app->params['path_area'];

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            if ($file) {
                $basePath = $path;
                BaseFileHelper::createDirectory($basePath);
                $realFileName = md5($file->tempName . time()) . '.' . $file->extension;
                $fullPath = $basePath . $realFileName;

                if ($file->saveAs($fullPath)) {
                    $objPHPExcel = \PHPExcel_IOFactory::load($fullPath);
                    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                    foreach ($sheetData as $key => $value) {
                        if ($key > 1 && $value['A'] && $value['B'] && $value['C']) {

                            $modelSave = new AreaTemp();
                            $modelSave->brach_no = Helpers::checkEmpty($value['A']);
                            $modelSave->account = Helpers::checkEmpty($value['B']);
                            $modelSave->local_name = Helpers::checkEmpty($value['C']);
                            $modelSave->longitude = Helpers::checkEmpty($value['E']);
                            $modelSave->latitude = Helpers::checkEmpty($value['D']);
                            $modelSave->distance = Helpers::checkEmpty($value['F']);
                            $modelSave->area = Helpers::checkEmpty($value['G']);
                            $modelSave->zone = Helpers::checkEmpty($value['H']);
                            $modelSave->channel = Helpers::checkEmpty($value['I']);
                            $modelSave->created_by = Helpers::checkEmpty($value['J']);
                            $modelSave->save(false);
                        }

                    }
                }
            }
        }
        $modelRows = AreaTemp::find()->count();
        $modelTemp = AreaTemp::find()->all();
        return $this->render('_area_import', [
            'model' => $modelTemp,
            'rows' => $modelRows,
        ]);
    }

    public function actionDelAreatemp()
    {
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!empty($request['id'])) {
            $model = AreaTemp::find()->where(['id' => trim($request['id'])])->One();
            $model->delete();
            return [
                'code' => 200,
                'message' => 'ลบข้อมูลเรียบร้อย',
            ];
        }else{
            AreaTemp::deleteAll();
            return [
                'code' => 200,
                'message' => 'ลบข้อมูลเรียบร้อย',
            ];
        }
    }

    public function actionDelArea()
    {
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($request['id']) {
            $model = Area::find()->where(['id' => trim($request['id'])])->One();
            $model->isDelete = "Y";
            $model->save(false);
            // $model->delete();
            return [
                'code' => 200,
                'message' => 'ลบข้อมูลเรียบร้อย',
            ];
        }
    }
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Area model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelAreaTemp = AreaTemp::find()->all();
        foreach ($modelAreaTemp as $key => $value) {
            if (!empty($value->brach_no)) {
                $modelSave = Area::find()->where(['brach_no' => $value->brach_no])->One();
                if (empty($modelSave)) {
                    $modelSave = new Area();
                }

                $modelSave->brach_no = Helpers::checkEmpty($value->brach_no);
                $modelSave->account = Helpers::checkEmpty($value->account);
                $modelSave->local_name = Helpers::checkEmpty($value->local_name);
                $modelSave->longitude = Helpers::checkEmpty($value->longitude);
                $modelSave->latitude = Helpers::checkEmpty($value->latitude);
                $modelSave->distance = Helpers::checkEmpty($value->distance);
                $modelSave->area = Helpers::checkEmpty($value->area);
                $modelSave->zone = Helpers::checkEmpty($value->zone);
                $modelSave->channel = Helpers::checkEmpty($value->channel);
                $modelSave->created_by = Helpers::checkEmpty($value->created_by);
                $modelSave->isDelete = "N";
                $modelSave->save(false);
            }

        }
        AreaTemp::deleteAll();

        return $this->redirect('index');
    }

    /**
     * Updates an existing Area model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Area model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Area model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Area the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Area::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionLocationAdd($id = null)
    {
        if ($id) {
            $model = Area::find()->where(['id' => $id])->andWhere(['=', 'isDelete', "N"])->One();
            // \app\components\Helpers::Debig($model);
            if (empty($model)) {
                throw new NotFoundHttpException('สถานที่ถูกลบไปแล้ว');
            }

            return $this->render('location-add', [
                'model' => $model,
            ]);
        } else {
            $model = new Area();
            return $this->render('location-add', [
                'model' => $model,
            ]);
        }

    }
    public function actionInsertArea()
    {
        // date_default_timezone_set("Asia/Bangkok");

        $request = Yii::$app->request->post();

        if (!empty($request)) {
            // \app\components\Helpers::Debig($request);
            Yii::$app->response->format = Response::FORMAT_JSON;

            $transaction = Yii::$app->db->beginTransaction();

            try {
                //code...

                if (!empty($request['id'])) {

                    $model = Area::find()->where(['id' => $request['id']])->One();
                    $model->id = $request['id'];

                } else {
                    $model = new Area();
                }

                $model->load($request);
                $file = UploadedFile::getInstance($model, 'img');
                $path = Yii::$app->params['path_area'];

                if ($file) {

                    $basePath = $path;
                    BaseFileHelper::createDirectory($basePath);
                    $realFileName = md5($file->tempName . time()) . '.' . $file->extension;
                    $fullPath = $basePath . $realFileName;
                    $file->saveAs($fullPath);
                    $model->img = $fullPath;

                }

                $model->brach_no = $request['Area']['brach_no'];
                $model->local_name = $request['Area']['local_name'];
                $model->latitude = $request['Area']['latitude'];
                $model->longitude = $request['Area']['longitude'];
                $model->distance = $request['Area']['distance'];
                $model->account = $request['Area']['account'];
                $model->area = $request['Area']['area'];
                $model->zone = $request['Area']['zone'];
                $model->channel = $request['Area']['channel'];
                $model->created_by = $request['username'];
                $model->type = 'web';

                // \app\components\Helpers::Debig($model );

                $model->save(false);
                $transaction->commit();
                return $this->redirect(Yii::$app->request->baseUrl . '/area');

            } catch (\Exception $th) {
                //throw $th;
                $transaction->rollBack();
                \app\components\Helpers::Debig($th);

                // $modelJson = [
                //     'code' => 400,
                //     'status' => false,
                //     'message' => "Fail",
                // ];
            }
        }
    }

    public function actionLocationArea()
    {
        $model = MemberArea::find()->all();
        $modelRows = MemberArea::find()->count();

        return $this->render('location-area', [
            'model' => $model,
            'modelRows' => $modelRows,
        ]);
    }

    public function actionInsertMemberArea()
    {

        $modelCreate = new MemberArea();

        if ($modelCreate->load(Yii::$app->request->post()) && $modelCreate->save()) {
            return $this->redirect(['location-area']);
        }

    }
    public function actionDeleteMemberArea()
    {
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($request['id']) {
            $model = MemberArea::find()->where(['id' => trim($request['id'])])->One();
            $model->delete();
            return $this->redirect(['location-area']);
        }
    }

    public function actionLocationZone()
    {
        $model = MemberZone::find()->all();
        $modelRows = MemberZone::find()->count();

        return $this->render('location-zone', [
            'model' => $model,
            'modelRows' => $modelRows,
        ]);
    }

    public function actionInsertMemberZone()
    {

        $modelCreate = new MemberZone();

        if ($modelCreate->load(Yii::$app->request->post()) && $modelCreate->save()) {
            return $this->redirect(['location-zone']);
        }

    }

    public function actionDeleteMemberZone()
    {
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($request['id']) {
            $model = MemberZone::find()->where(['id' => trim($request['id'])])->One();
            $model->delete();
            return $this->redirect(['location-zone']);
        }
    }

    public function actionLocationChanel()
    {
        $model = MemberChannel::find()->all();
        $modelRows = MemberChannel::find()->count();

        return $this->render('location-channel', [
            'model' => $model,
            'modelRows' => $modelRows,
        ]);
    }

    public function actionInsertMemberChannel()
    {

        $modelCreate = new MemberChannel();

        if ($modelCreate->load(Yii::$app->request->post()) && $modelCreate->save()) {
            return $this->redirect(['location-chanel']);
        }

    }

    public function actionDeleteMemberChannel()
    {
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($request['id']) {
            $model = MemberChannel::find()->where(['id' => trim($request['id'])])->One();
            $model->delete();
            return $this->redirect(['location-chanel']);
        }
    }

    public function actionLocationAccount()
    {
        $model = MemberAccount::find()->all();
        $modelRows = MemberAccount::find()->count();

        return $this->render('location-account', [
            'model' => $model,
            'modelRows' => $modelRows,
        ]);
    }

    public function actionInsertMemberAccount()
    {

        $modelCreate = new MemberAccount();

        if ($modelCreate->load(Yii::$app->request->post()) && $modelCreate->save()) {
            return $this->redirect(['location-account']);
        }

    }

    public function actionDeleteMemberAccount()
    {
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($request['id']) {
            $model = MemberAccount::find()->where(['id' => trim($request['id'])])->One();
            $model->delete();
            return $this->redirect(['location-account']);
        }
    }

    // public function actionLocationGroup()
    // {
    //     $model = AreaGroup::find()->all();
    //     $modelRows = AreaGroup::find()->count();
    //     $modelMemberAccount = MemberAccount::find()->all();

    //     return $this->render('location-group', [
    //         'model' => $model,
    //         'modelRows' => $modelRows,
    //         'modelMemberAccount' => $modelMemberAccount,
    //     ]);
    // }

    public function actionInsertAreaGroup()
    {

        $request = Yii::$app->request->post();
        // \app\components\Helpers::Debig($request);

        if (!empty($request)) {
            // \app\components\Helpers::Debig($request);

            $transaction = Yii::$app->db->beginTransaction();
            try {

                $modelSave = new AreaGroup();
                $modelSave->group_name = $request['AreaGroup']['group_name'];
                $modelSave->keeper = $request['AreaGroup']['keeper'];
                $modelSave->save(false);

                foreach ($request['AreaList']['area_id'] as $key => $e) {
                    $modelListSave = new AreaList();

                    $modelListSave->group_id = $modelSave->getPrimaryKey();
                    $modelListSave->area_id = $e;
                    $modelListSave->save(false);
                }

                $transaction->commit();
                return $this->redirect(Yii::$app->request->baseUrl . '/area/location-group');

            } catch (\Exception $th) {
                $transaction->rollBack();
                \app\components\Helpers::Debig($th);
            }

        }

    }

    public function actionUpdateAreaGroup()
    {

        $request = Yii::$app->request->post();
        // \app\components\Helpers::Debig($request);

        if (!empty($request)) {
            // \app\components\Helpers::Debig($request);

            $transaction = Yii::$app->db->beginTransaction();
            try {

                $modelSave = AreaGroup::find()->where(['id' => trim($request['id'])])->One();
                $modelSave->group_name = $request['AreaGroup']['group_name'];
                $modelSave->keeper = $request['AreaGroup']['keeper'];
                $modelSave->save(false);

                $transaction->commit();
                return $this->redirect(Yii::$app->request->baseUrl . '/area/location-group-detail?id=' . $request['id']);

            } catch (\Exception $th) {
                $transaction->rollBack();
                \app\components\Helpers::Debig($th);
            }

        }

    }

    public function actionDeleteAreaGroup()
    {
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        // \app\components\Helpers::Debig($request);

        if ($request['id']) {
            $transaction = Yii::$app->db->beginTransaction();

            try {
                //code...
                $model = AreaGroup::find()->where(['id' => trim($request['id'])])->One();
                $model->delete();
                Yii::$app->db->createCommand()->delete('area_list', ['group_id' => trim($request['id'])])->execute();

                $transaction->commit();

                return [
                    'code' => 200,
                    'message' => 'ลบข้อมูลเรียบร้อย',
                ];
            } catch (\Exception $th) {
                //throw $th;
                $transaction->rollBack();

                return [
                    'code' => 500,
                    'message' => $th,
                ];
            }

        }
    }

    // public function actionLocationGroupDetail($id)
    // {
    //     $model = AreaGroup::find()->where(['id' => $id])->One();
    //     $ListRows = AreaList::find()->where(['group_id' => $id])->count();
    //     // \app\components\Helpers::Debig($model->areaList[0]->area);

    //     return $this->render('location-group-detail', [
    //         'model' => $model,
    //         'ListRows' => $ListRows,
    //     ]);
    // }

    public function actionGroupListDel($id, $link)
    {
        // \app\components\Helpers::Debig($id);
        try {
            //code...
            Yii::$app->db->createCommand()->delete('area_list', ['area_id' => trim($id)])->execute();

        } catch (\Throwable $th) {
            //throw $th;
        }

        return $this->redirect(Yii::$app->request->baseUrl . '/area/location-group-detail?id=' . $link);
    }

    public function actionAreaDt()
    {
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        // \app\components\Helpers::Debig($request);

        $start = $request['start'];
        // $start = !empty($request['start'])?$request['start']:1;
        $length = $request['length'];
        $search = !empty($request['search']['value']) ? $request['search']['value'] : null;
        $name = null;
        $status = null;
        $type = null;
        $offset = 0;
        if ($start > 1) {
            $start = $start / 10;
            $offset = $start * $length;
        }
        $model = Area::find()
            ->where(['isDelete' => 'N'])
            ->limit($length)
            ->offset($offset)
        ;
        $modelRows = Area::find()
            ->where(['isDelete' => 'N']);

        if ($search) {
            $search_list = explode("&", $search);
            if (!empty($search_list[0])) {
                $local_name = $search_list[0];
                $model->andFilterWhere(['like', 'local_name', $local_name]);
                $modelRows->andFilterWhere(['like', 'local_name', $local_name]);
            }
            if (!empty($search_list[1])) {
                $account = trim($search_list[1]);
                $model->andFilterWhere(['like', 'account', $account]);
                $modelRows->andFilterWhere(['like', 'account', $account]);
            }
            if (!empty($search_list[2])) {
                $channel = $search_list[2];
                $model->andFilterWhere(['like', 'channel', $channel]);
                $modelRows->andFilterWhere(['like', 'channel', $channel]);
            }
            if (!empty($search_list[3])) {
                $area = $search_list[3];
                $model->andFilterWhere(['like', 'area', $area]);
                $modelRows->andFilterWhere(['like', 'area', $area]);
            }

            if (!empty($search_list[4])) {
                $zone = $search_list[4];
                $model->andFilterWhere(['like', 'zone', $zone]);
                $modelRows->andFilterWhere(['like', 'zone', $zone]);
            }
        }

        $modelJson = [];
        $modelJson['data'] = [];
        $modelJson['draw'] = $request['draw'];
        $modelJson['recordsTotal'] = $modelRows->count();
        $modelJson['recordsFiltered'] = $modelRows->count();
        // \app\components\Helpers::Debig($model);

        foreach ($model->all() as $key => $item) {
            if ($item->type == 'web') {
                $img = 'https://att.thebestpromote.com/web/' . $item->img;
            } else if ($item->type == 'mobile') {
                $img = 'https://api.thebestpromote.com/web/' . $item->img;
            } else {
                $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
            }

            if(empty($item->img)){
                $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
            }

            $modelJson['data'][] = [
                "img" => '<img class="thumb mr-2 lazy" src="' . $img . '" align="left" >',
                "account" => $item->account,
                "brach_no" => $item->brach_no,
                "local_name" => $item->local_name,
                "latitude" => $item->latitude,
                "longitude" => $item->longitude,
                "distance" => $item->distance,
                "area" => $item->area,
                "zone" => $item->zone,
                "channel" => $item->channel,
                "created_by" => $item->created_by,
                "id" => $item->id,
                // 'test' => !empty($search_list) ? $search_list : "",

            ];

        }

        return $modelJson;
    }

}
