<?php

namespace app\controllers;

use app\components\Helpers;
use app\components\Utilities;
use app\models\Agency;
use app\models\Department;
use app\models\MemberSubtype;
use app\models\MemberType;
use app\models\PaySlip;
use app\models\PaySlipTemp;
use app\models\Position;
use Yii;
use app\models\Company;
use app\models\CompanySearch;
use yii\helpers\BaseFileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\CompanyLeave;
use yii\web\UploadedFile;
use yii\filters\AccessControl;


/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error', 'login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [

                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],

            ],

        ];
    }

    public function beforeAction($action)
    {
        $auth = Utilities::setAuthController();
        if($auth['company'] == 'N')
            throw new NotFoundHttpException('page not found');

        return parent::beforeAction($action);
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {

        $max = (new \yii\db\Query())
            ->from('company')
            ->max('id');


        if($max){
            $model   = $this->findModel($max);
        }else{
            $model = new Company();
        }
        $modelHolidayMaster = CompanyLeave::find()->orderBy('days_leave')->all();
        $path = Yii::$app->params['path_company'];

         $request = Yii::$app->request->post();

         if($model->load($request) ) {
            $file = UploadedFile::getInstance($model, 'img_url');
            if ($file) {
                $basePath = $path;
                BaseFileHelper::createDirectory($basePath);
                $realFileName = md5($file->tempName . time()) . '.' . $file->extension;
                $fullPath = $basePath . $realFileName;
                $file->saveAs($fullPath);
            }
            $model->img_url = $fullPath;
            $model->save();
             $com_id = $model->id;
            $holidaysModel = $request['holidaysModel'];
            $arrHoliday = explode(',',$holidaysModel);
            for($i=0;$i<count($arrHoliday);$i++)
            {
                $arrValueDate = explode('|x|',$arrHoliday[$i]);
                $modelHoliday = CompanyLeave::find()->where(['company_id'=>$com_id])->andWhere(['days_leave'=>Helpers::convertDateSearch($arrValueDate[0])])->One();
                if(empty($modelHoliday))
                {

                    $modelHoliday = new CompanyLeave();
                }
                if(!empty($arrValueDate[0]) && !empty($arrValueDate[1]))
                {
                    $modelHoliday->company_id = $com_id;
                    $modelHoliday->days_leave = Helpers::convertDateSearch($arrValueDate[0]);
                    $modelHoliday->subject = $arrValueDate[1];
                    $modelHoliday->remark = !empty($arrValueDate[2])?$arrValueDate[2]:null;
                    $modelHoliday->created_date = date('Y-m-d H:i:s');
                    $modelHoliday->save();
                }


            }


        }
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'model' => $model,
            'modelHoliday' => $modelHolidayMaster
        ]);
    }

    public function actionSetHoliday(){
        
        $request = Yii::$app->request->post();
        $max = (new \yii\db\Query())
            ->from('company')
            ->max('id');
        $model = CompanyLeave::find()->Where(['days_leave'=>Helpers::convertDateSearch($request['holiday_date'])])->One();
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if(!empty($model)){
             return [
                    'code' => 401,
                    'message' => 'วันหยุดนี้มีในระบบแล้ว',
                    'id' => null
                ];
        }else{
        

                    $modelHoliday = new CompanyLeave();
                    $modelHoliday->company_id = $max;
                    $modelHoliday->days_leave = Helpers::convertDateSearch($request['holiday_date']);
                    $modelHoliday->subject = $request['holidays'];
                    $modelHoliday->remark = !empty($request['holidays_remark'])?$request['holidays_remark']:null;
                    $modelHoliday->created_date = date('Y-m-d H:i:s');
                    $modelHoliday->save();
                    return [
                        'code' => 200,
                        'message' => 'บันทึกเรียบร้อย',
                        'id' => $modelHoliday->id
                    ];
        }
    }

    public function actionDepartment(){
        $model = Department::find()->OrderBy('department_name')->all();
        $modelRows   = Department::find()->count();
        return $this->render('_department', [
            'model' => $model,
            'modelRows' => $modelRows
        ]);
    }

    public function actionGetDep(){
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($request['id']){
            $model = Department::find()->where(['id'=>trim($request['id'])])->One();
           
            return [
                'code' => 200,
                'department_name' => $model->department_name,
                'detail' => $model->detail ,
                'remark' => $model->remark
            ];
        }
    }

    public function actionDelDep(){
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($request['id']){
            $model = Department::find()->where(['id'=>trim($request['id'])])->One();
            $model->delete();
            return [
                'code' => 200,
                'message' => 'ลบข้อมูลเรียบร้อย'
            ];
        }
    }

    public function actionDelAgency(){
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($request['id']){
            $model = Agency::find()->where(['id'=>trim($request['id'])])->One();
            $model->delete();
            return [
                'code' => 200,
                'message' => 'ลบข้อมูลเรียบร้อย'
            ];
        }
    }

    public function actionGetAgency(){
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($request['id']){
            $model = Agency::find()->where(['id'=>trim($request['id'])])->One();
           
            return [
                'code' => 200,
                'agent_name' => $model->agent_name,
                'detail' => $model->detail,
                'remark' => $model->remark
            ];
        }
    }

    public function actionEditHoliday(){
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($request['id']){

            $model = CompanyLeave::find()->where(['id'=>trim($request['id'])])->One();

            $model->days_leave =$request['holiday_date'] ;
            $model->subject = Helpers::checkEmpty($request['holidays']);
            $model->remark = Helpers::checkEmpty($request['holidays_remark']);
            $model->created_date = date('Y-m-d H:i:s');
            $model->save();
            return [
                'code' => 200,
                'message' => 'แก้ไขข้อมูลเรียบร้อย'
            ];
        }
    }

    public function actionDelHoliday(){
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($request['id']){
            $model = CompanyLeave::find()->where(['id'=>trim($request['id'])])->One();
            $model->delete();
            return [
                'code' => 200,
                'message' => 'ลบข้อมูลเรียบร้อย'
            ];
        }
    }

    public function actionSetDep(){
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($request['department']){
            $modelDup = Department::find()->where(['department_name'=>trim($request['department'])])->One();
            if(!empty($modelDup) && empty($request['el'])){
                return [
                    'code' => 200,
                    'message' => 'แผนกมีในระบบแล้ว'
                ];
            }
            
            $model = new Department();
            if(!empty($request['el'])){
                    $model =  Department::find()->where(['id'=>$request['el']])->One();
            }
            
            $model->department_name = $request['department'];
            $model->detail = empty($request['department_detail'])?null:$request['department_detail'];
            $model->remark = empty($request['department_remark'])?null:$request['department_remark'];
            $model->save();
            return [
                'code' => 200,
                'message' => 'บันทึกแผนกเรียบร้อย'
            ];
        }
    }

    public function actionPosition(){
        $model = Position::find()->all();
        $modelRows   = Position::find()->count();
        return $this->render('_position', [
            'model' => $model,
            'modelRows' => $modelRows
        ]);
    }

    public function actionPositionAdd($id = null){
        $model = new Position();

        if($id){
            $model =  Position::find()->where(['id'=>$id])->One();
            if(empty($model)){
                throw new NotFoundHttpException('ไม่พบตำแหน่งนี้');
            }

        }


        $request = Yii::$app->request->post();

       if(!empty($request)){

           if($request['position_name'])
           {

                $modelDup = Position::find()->where(['position_name'=>trim($request['position_name'])])->One();
                if(!empty($modelDup) && empty($id)){
                    throw new NotFoundHttpException('ตำแหน่งนี้มีในระบบแล้ว');
                }
                $modelSave = new Position();
               if($id){
                   $modelSave =  Position::find()->where(['id'=>$id])->One();
               }
                $modelSave->position_name  = $request['position_name'];
                $modelSave->remark         = empty($request['remark'])?null:$request['remark'];
                $modelSave->life_cycle     = empty($request['life_cycle'])?null:$request['life_cycle'];
                $modelSave->auth_member_role     = empty($request['auth_member_role'])?null:$request['auth_member_role'];
                $modelSave->auth_backend_role     = empty($request['auth_backend_role'])?null:$request['auth_backend_role'];
                $modelSave->auth_company   = empty($request['auth_company'])?null:$request['auth_company'];
                $modelSave->auth_member    = empty($request['auth_member'])?null:$request['auth_member'];
                $modelSave->auth_add_member = empty($request['auth_add_member'])?null:$request['auth_add_member'];
                $modelSave->auth_import_member = empty($request['auth_import_member'])?null:$request['auth_import_member'];
                $modelSave->auth_group_member = empty($request['auth_group_member'])?null:$request['auth_group_member'];
                $modelSave->auth_report_ba = empty($request['auth_report_ba'])?null:$request['auth_report_ba'];
                $modelSave->auth_approve_order = empty($request['auth_approve_order'])?null:$request['auth_approve_order'];
                $modelSave->auth_record_time = empty($request['auth_record_time'])?null:$request['auth_record_time'];
                $modelSave->auth_record_leave = empty($request['auth_record_leave'])?null:$request['auth_record_leave'];
                $modelSave->auth_update_time = empty($request['auth_update_time'])?null:$request['auth_update_time'];
                $modelSave->auth_holidays = empty($request['auth_holidays'])?null:$request['auth_holidays'];
                $modelSave->auth_location = empty($request['auth_location'])?null:$request['auth_location'];
                $modelSave->auth_vacancy = empty($request['auth_vacancy'])?null:$request['auth_vacancy'];

                $modelSave->auth_news = empty($request['auth_news'])?null:$request['auth_news'];
                $modelSave->auth_export = empty($request['auth_export'])?null:$request['auth_export'];
                $modelSave->auth_record_ot = empty($request['auth_record_ot'])?null:$request['auth_record_ot'];
                $modelSave->created_date = date('Y-m-d H:i:s');

                if($modelSave->save(false)){
                    $model =  Position::find()->where(['id'=>$id])->One();
                     return $this->redirect('position');
                }
           }

       }
        return $this->render('_position_add', [
            'model' => $model,
        ]);
    }


    public function actionDelPosition(){
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($request['id']){
            $model = Position::find()->where(['id'=>trim($request['id'])])->One();
            $model->delete();
            return [
                'code' => 200,
                'message' => 'ลบข้อมูลเรียบร้อย'
            ];
        }
    }

    public function actionDelSlip(){
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($request['id']){
            $model = PaySlipTemp::find()->where(['id'=>trim($request['id'])])->One();
            $model->delete();
            return [
                'code' => 200,
                'message' => 'ลบข้อมูลเรียบร้อย'
            ];
        }
    }

    public function actionType(){
        $model = MemberType::find()->orderBy('created_date')->all();
        $modelSubType = MemberSubtype::find()->orderBy('created_date')->all();
        $modelRows   = MemberType::find()->count();
        $modelSubRows   = MemberSubtype::find()->count();
        return $this->render('_company_type', [
            'model' => $model,
            'modelSubType' => $modelSubType,
            'modelRows' => $modelRows,
            'modelSubRows' => $modelSubRows
        ]);
    }

    public function actionDelType(){
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($request['id']){
            $model = MemberType::find()->where(['id'=>trim($request['id'])])->One();
            $model->delete();
            return [
                'code' => 200,
                'message' => 'ลบข้อมูลเรียบร้อย'
            ];
        }
    }

    public function actionAddType($id = null){
        if($id){
             $model =  MemberType::find()->where(['id'=>$id])->One();
        }else{
             $model = new MemberType();
        }
       
        $request = Yii::$app->request->post();

        if(!empty($request)){

            $model->type_name = empty($request['type_name'])?null:$request['type_name'];
            $model->remark = empty($request['remark'])?null:$request['remark'];
            $model->time_attendance = empty($request['time_attendance'])?"N":$request['time_attendance'];
            $model->attendance_type = empty($request['attendance_type'])?"N":$request['attendance_type'];
            $model->ba = empty($request['ba'])?"N":$request['ba'];
            $model->leave_codition = empty($request['leave_codition'])?"N":$request['leave_codition'];
            $model->leave_type = empty($request['leave_type'])?"N":$request['leave_type'];
            $model->leave_year = empty($request['leave_year'])?"N":$request['leave_year'];
            $model->monday = empty($request['monday'])?"N":$request['monday'];
            $model->thesday = empty($request['thesday'])?"N":$request['thesday'];
            $model->wednesday = empty($request['wednesday'])?"N":$request['wednesday'];
            $model->thursday = empty($request['thursday'])?"N":$request['thursday'];
            $model->friday = empty($request['friday'])?"N":$request['friday'];
            $model->saturday = empty($request['saturday'])?"N":$request['saturday'];
            $model->sunday = empty($request['sunday'])?"N":$request['sunday'];
            $model->substitution_holiday = empty($request['substitution_holiday'])?"N":$request['substitution_holiday'];
            $model->location_type = empty($request['location_type'])?"N":$request['location_type'];
            $model->created_date = date('Y-m-d H:i:s');
            $model->save(false);
            return $this->redirect('type');
           
        }
        return $this->render('_company_add_type', [
            'model' => $model,
        ]);
    }

    public function actionAddSubtype($id = null)
    {
        $model = new MemberSubtype();

        if($id){
            $model =  MemberSubtype::find()->where(['id'=>$id])->One();
        }

        $request = Yii::$app->request->post();
            // Helpers::Debig($request);

        if(!empty($request) && !empty($request['type_id']) & !empty($request['subtype_name']) ){


            $model->subtype_name = empty($request['subtype_name'])?null:$request['subtype_name'];
            $model->type_id = empty($request['type_id'])?null:$request['type_id'];
            $model->remark = empty($request['remark'])?null:$request['remark'];
            $model->time_attendance = empty($request['time_attendance'])?"N":$request['time_attendance'];
            $model->attendance_type = empty($request['attendance_type'])?"N":$request['attendance_type'];
            $model->ba = empty($request['ba'])?"N":$request['ba'];
            $model->leave_codition = empty($request['leave_codition'])?"N":$request['leave_codition'];
            $model->leave_type = empty($request['leave_type'])?"N":$request['leave_type'];
            $model->leave_year = empty($request['leave_year'])?"N":$request['leave_year'];
            $model->monday = empty($request['monday'])?"N":$request['monday'];
            $model->thesday = empty($request['thesday'])?"N":$request['thesday'];
            $model->wednesday = empty($request['wednesday'])?"N":$request['wednesday'];
            $model->thursday = empty($request['thursday'])?"N":$request['thursday'];
            $model->friday = empty($request['friday'])?"N":$request['friday'];
            $model->saturday = empty($request['saturday'])?"N":$request['saturday'];
            $model->sunday = empty($request['sunday'])?"N":$request['sunday'];
            $model->substitution_holiday = empty($request['substitution_holiday'])?"N":$request['substitution_holiday'];
            $model->location_type = empty($request['location_type'])?"N":$request['location_type'];
            $model->isLeave = empty($request['isLeave'])?"Y":$request['isLeave'];
            $model->created_date = date('Y-m-d H:i:s');
            $model->save(false);
            //Helpers::Debig($request);
             return $this->redirect('type');
        }
        return $this->render('_company_add_subtype', [
            'model' => $model,
        ]);
    }

    public function actionAgency(){
        $model = Agency::find()->orderBy('agent_name')->all();
        $modelRows = Agency::find()->count();
        return $this->render('_agency', [
            'model' => $model,
            'modelRows' => $modelRows
        ]);
    }

    public function actionSetAgency(){
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if(!empty($request['agency'])){
            $modelDup = Agency::find()->where(['agent_name'=>trim($request['agency'])])->One();
            if(!empty($modelDup) && empty($request['id'])){
                return [
                    'code' => 200,
                    'message' => 'ชื่อเอเจนซี่มีในระบบแล้ว'
                ];
            }
            $model = new Agency();
            if(!empty($request['id'])){
                $model = Agency::find()->where(['id'=>trim($request['id'])])->One();
            }
            $model->agent_name = $request['agency'];
            $model->detail = empty($request['detail'])?null:$request['detail'];
            $model->remark = empty($request['remark'])?null:$request['remark'];
            $model->save();
            return [
                'code' => 200,
                'message' => 'บันทึกเรียบร้อย'
            ];
        }
    }

    public function actionSlip(){
        $model = PaySlipTemp::find()->orderBy('name')->all();
        $modelRows   = PaySlipTemp::find()->count();
        return $this->render('_slip', [
            'model' => $model,
            'modelRows' => $modelRows
        ]);
    }

    public function actionImportSlip(){
        $model = new PaySlip();
        $path = Yii::$app->params['path_slip'];
     
        if($model->load(Yii::$app->request->post())) {

            $file = UploadedFile::getInstance($model, 'file');

            if ($file) {

                $basePath = $path;
                BaseFileHelper::createDirectory($basePath);
                $realFileName = md5($file->tempName . time()) . '.' . $file->extension;
                $fullPath = $basePath . $realFileName;
                if($file->saveAs($fullPath)) {
                    $objPHPExcel = \PHPExcel_IOFactory::load($fullPath);
                    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    foreach($sheetData as $key => $value)
                    {
                        if($key>1 && $value['A'] && $value['B']){
                            $modelSave = new PaySlipTemp();
                            $modelSave->name = Helpers::checkEmpty($value['A']);
                            $modelSave->member_no = Helpers::checkEmpty($value['B']);
                            $modelSave->department = Helpers::checkEmpty($value['C']);
                            $modelSave->bank_no = Helpers::checkEmpty($value['D']);
                            $modelSave->salary = Helpers::checkEmpty($value['E']);
                            $modelSave->diligent_allowance = Helpers::checkEmpty($value['F']);
                            $modelSave->phone_allowance = Helpers::checkEmpty($value['G']);
                            $modelSave->other_allowance = Helpers::checkEmpty($value['H']);
                            $modelSave->total_salary = Helpers::checkEmpty($value['I']);
                            $modelSave->social_security = Helpers::checkEmpty($value['J']);
                            $modelSave->cumulative_social_security = Helpers::checkEmpty($value['K']);
                            $modelSave->deduction = Helpers::checkEmpty($value['L']);
                            $modelSave->net_income = Helpers::checkEmpty($value['M']);
                            $modelSave->net_total = Helpers::checkEmpty($value['N']);
                            $modelSave->tax_income = Helpers::checkEmpty($value['O']);
                            $modelSave->funds_income = Helpers::checkEmpty($value['P']);
                            $modelSave->total_days = Helpers::checkEmpty($value['Q']);
                            $modelSave->pay_date = Helpers::convertDateSearch($value['R']);
                            $modelSave->month_expend = Helpers::convertDateSearch($value['S']);
                            $modelSave->created_date = date('Y-m-d H:i:s');
                            $modelSave->file_name = $realFileName;  
                            $modelSave->save(false);    
                        }
                    }
                }


            }

        }
        
        return $this->redirect('slip');
    }

    public function actionAddSlip(){

        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if(!empty($request['id'])){

           $model = PaySlipTemp::find()->where(['id'=>trim($request['id'])])->One();;

            $modelSave = new PaySlip();
            $modelSave->name = Helpers::checkEmpty($model->name);
            $modelSave->member_no = Helpers::checkEmpty($model->member_no);
            $modelSave->department = Helpers::checkEmpty($model->department);
            $modelSave->bank_no = Helpers::checkEmpty($model->bank_no);
            $modelSave->salary = Helpers::checkEmpty($model->salary);
            $modelSave->diligent_allowance = Helpers::checkEmpty($model->diligent_allowance);
            $modelSave->phone_allowance = Helpers::checkEmpty($model->phone_allowance);
            $modelSave->other_allowance = Helpers::checkEmpty($model->other_allowance);
            $modelSave->total_salary = Helpers::checkEmpty($model->total_salary);
            $modelSave->social_security = Helpers::checkEmpty($model->social_security);
            $modelSave->deduction = Helpers::checkEmpty($model->deduction);
            $modelSave->net_income = Helpers::checkEmpty($model->net_income);
            $modelSave->net_total = Helpers::checkEmpty($model->net_total);
            $modelSave->tax_income = Helpers::checkEmpty($model->tax_income);
            $modelSave->funds_income = Helpers::checkEmpty($model->funds_income);
            $modelSave->total_days = Helpers::checkEmpty($model->total_days);
            $modelSave->cumulative_social_security = Helpers::checkEmpty($model->cumulative_social_security);
            $modelSave->pay_date = $model->pay_date;
            $modelSave->month_expend = $model->month_expend;
            $modelSave->created_date = date('Y-m-d H:i:s');
            $modelSave->file_name = $model->file_name;
            $modelSave->save(false);
            $model->delete();
            return [
                'code' => 200,
                'message' => 'บันทึกสลิปเรียบร้อย'
            ];

        }
        
    }

    public function actionAddSlipall(){

           $model = PaySlipTemp::find()->all();

           foreach ($model as $key => $item) 
           {
                $modelSave = new PaySlip();
                $modelSave->name = Helpers::checkEmpty($item->name);
                $modelSave->member_no = Helpers::checkEmpty($item->member_no);
                $modelSave->department = Helpers::checkEmpty($item->department);
                $modelSave->bank_no = Helpers::checkEmpty($item->bank_no);
                $modelSave->salary = Helpers::checkEmpty($item->salary);
                $modelSave->diligent_allowance = Helpers::checkEmpty($item->diligent_allowance);
                $modelSave->phone_allowance = Helpers::checkEmpty($item->phone_allowance);
                $modelSave->other_allowance = Helpers::checkEmpty($item->other_allowance);
                $modelSave->total_salary = Helpers::checkEmpty($item->total_salary);
                $modelSave->social_security = Helpers::checkEmpty($item->social_security);
                $modelSave->deduction = Helpers::checkEmpty($item->deduction);
                $modelSave->net_income = Helpers::checkEmpty($item->net_income);
                $modelSave->net_total = Helpers::checkEmpty($item->net_total);
                $modelSave->tax_income = Helpers::checkEmpty($item->tax_income);
                $modelSave->funds_income = Helpers::checkEmpty($item->funds_income);
                $modelSave->total_days = Helpers::checkEmpty($item->total_days);
                $modelSave->pay_date = $item->pay_date;
                $modelSave->month_expend = $item->month_expend;
                $modelSave->cumulative_social_security = Helpers::checkEmpty($item->cumulative_social_security);
                $modelSave->created_date = date('Y-m-d H:i:s');
                $modelSave->file_name = $item->file_name;
                $modelSave->save(false);
            }

            PaySlipTemp::DeleteAll();

             \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'code' => 200,
                'message' => 'บันทึกทั้งหมดเรียบร้อย'
            ];

        
        
    }


    public function actionGetMembertype(){
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
         $request = Yii::$app->request->post();
        if(!empty($request['id'])){
            $model = MemberType::find()->where(['id'=>trim($request['id'])])->One();
           
            return [
                'time_attendance' => $model->time_attendance,
                'attendance_type' => $model->attendance_type,
                'ba' => $model->ba,
                'leave_codition' => $model->leave_codition,
                'leave_type' => $model->leave_type,
                'leave_year' => $model->leave_year,
                'monday' => $model->monday,
                'thesday' => $model->thesday,
                'wednesday' => $model->wednesday,
                'thursday' => $model->thursday,
                'friday' => $model->friday,
                'saturday' => $model->saturday,
                'sunday' => $model->sunday,
                'substitution_holiday' => $model->substitution_holiday,
                'location_type' => $model->location_type,
                'code' => 200,
                'message' => 'success'
            ];
        }

        return [
                'code' => 400,
                'message' => 'ไม่พบข้อมูล'
        ];
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Company();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

  protected function findIDCompany(){

  }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
