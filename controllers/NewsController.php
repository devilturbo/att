<?php

namespace app\controllers;

use app\components\Utilities;
use app\models\News;
use app\models\Notification;
use app\models\NotificationHasMember;
use DateTime;
use DateTimeZone;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\BaseFileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [

                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['GET'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $auth = Utilities::setAuthController();
        if ($auth['news'] == 'N') {
            throw new NotFoundHttpException('page not found');
        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new NewsSearch();
        $data = News::find()->orderBy(['news_id' => SORT_DESC])->all();

        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'data' => $data,
        ]);
    }

    public function actionApprove()
    {
        // $searchModel = new NewsSearch();
        // $data = News::find()->all();

        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('user_ot', [
            // 'data' => $data,
        ]);
    }

    public function actionNewsAdd($id = null)
    {

        if ($id) {
            $modelNews = News::find()->where(['news_id' => $id])->One();
            // \app\components\Helpers::Debig($model);
            return $this->render('news-add', [
                'modelNews' => $modelNews,
            ]);
        } else {
            $modelNews = new News();
            return $this->render('news-add', [
                'modelNews' => $modelNews,
            ]);
        }
    }

    public function actionInsertNews()
    {
        // date_default_timezone_set("Asia/Bangkok");

        $request = Yii::$app->request->post();

        if (!empty($request)) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $transaction = Yii::$app->db->beginTransaction();

            try {
                //code...

                if (!empty($request['id'])) {

                    $model = News::find()->where(['news_id' => $request['id']])->One();
                    $model->news_id = $request['id'];

                } else {
                    $model = new News();
                    $today = (new DateTime("now", new DateTimeZone('Asia/Bangkok')))->format('Y-m-d H:i:s');
                    $model->createDate = $today;
                }

                $model->load($request);
                $file = UploadedFile::getInstance($model, 'topic_img');
                $path = Yii::$app->params['path_news'];

                if ($file) {

                    $basePath = $path;
                    BaseFileHelper::createDirectory($basePath);
                    $realFileName = md5($file->tempName . time()) . '.' . $file->extension;
                    $fullPath = $basePath . $realFileName;
                    $file->saveAs($fullPath);
                    $model->topic_img = $fullPath;

                }
                // check news is send today?
                $check_today = Yii::$app->Utilities->isToday(Yii::$app->Utilities->formatDate('Y-m-d', $request['News']['newsDate']));

                // \app\components\Helpers::Debig($check_today);
                $model->topic = empty($request['News']['topic']) ? "ไม่มีหัวข้อ" : $request['News']['topic'];
                $model->details = ($request["News"]['details']);
                $model->news_date = empty($request['News']['newsDate']) ? date("Y-m-d") : Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $request['News']['newsDate']);
                $model->position_id = empty($request['News']['position']) ? "9999" : $request['News']['position'];
                $model->member_type_id = empty($request['News']['type']) ? "9999" : $request['News']['type'];
                $model->member_subtype_id = empty($request['News']['subType']) ? "9999" : $request['News']['subType'];
                $model->member_area_id = empty($request['News']['memberArea']) ? "9999" : $request['News']['memberArea'];
                $model->member_zone_id = empty($request['News']['memberZone']) ? "9999" : $request['News']['memberZone'];
                $model->member_chanel_id = empty($request['News']['memberChannel']) ? "9999" : $request['News']['memberChannel'];
                $model->member_account_id = empty($request['News']['memberAccount']) ? "9999" : $request['News']['memberAccount'];
                $model->note = empty($request['News']['note']) ? "" : $request['News']['note'];
                $model->status = ($check_today) ? "Y" : "N";

                $model->save(false);

                if ($check_today && empty($request['id'])) {

                    $modelNoti = NotificationHasMember::find()
                        ->where(['module_id' => $model->getPrimaryKey()])
                        ->andWhere(['=', 'module', 'news'])
                        ->One();

                    if (!$modelNoti) {
                        $modelNoti = new NotificationHasMember();
                        // \app\components\Helpers::Debig('not found');

                    }
                    // \app\components\Helpers::Debig($modelNoti);

                    $modelNoti->detail = empty($request['News']['topic']) ? "ไม่มีหัวข้อ" : $request['News']['topic'];
                    $modelNoti->module_id = $model->getPrimaryKey();
                    $modelNoti->module = "news";
                    $modelNoti->member_id = "";
                    $modelNoti->position_id = empty($request['News']['position']) ? "9999" : $request['News']['position'];
                    $modelNoti->member_type_id = empty($request['News']['type']) ? "9999" : $request['News']['type'];
                    $modelNoti->member_subtype_id = empty($request['News']['subType']) ? "9999" : $request['News']['subType'];
                    $modelNoti->member_area_id = empty($request['News']['memberArea']) ? "9999" : $request['News']['memberArea'];
                    $modelNoti->member_zone_id = empty($request['News']['memberZone']) ? "9999" : $request['News']['memberZone'];
                    $modelNoti->member_chanel_id = empty($request['News']['memberChannel']) ? "9999" : $request['News']['memberChannel'];
                    $modelNoti->member_account_id = empty($request['News']['memberAccount']) ? "9999" : $request['News']['memberAccount'];
                    $modelNoti->create_date = date('Y-m-d H:i:s');
                    $modelNoti->save(false);

                    $transaction->commit();

                    $ch = curl_init('https://api.thebestpromote.com/web/api/notification');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($ch);
                    if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
                        // \app\components\Helpers::Debig(curl_getinfo($ch, CURLINFO_HTTP_CODE));
                        // \app\components\Helpers::Debig('ok');
                    }
                    curl_close($ch);
                } else {
                    $transaction->commit();

                }


                return $this->redirect(Yii::$app->request->baseUrl . '/news');
            } catch (\Exception $th) {
                //throw $th;
                $transaction->rollBack();
                \app\components\Helpers::Debig($th);

                // $modelJson = [
                //     'code' => 400,
                //     'status' => false,
                //     'message' => "Fail",
                // ];
            }


        }

    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionGetlist($list)
    {

        // return $list;
        $list = trim($list);
        $arr = "";
        if ($list == "ตำแหน่ง") {
            $arr = Yii::$app->Utilities->getListPositionNews();
        } else if ($list == "ประเภท") {
            $arr = Yii::$app->Utilities->getListTypeNews();
        } else if ($list == "ประเภทย่อย") {
            $arr = Yii::$app->Utilities->getListSubTypeNews();
        } else if ($list == "พื้นที่") {
            $arr = Yii::$app->Utilities->getMemberAreaNews();
        } else if ($list == "เขตพื้นที่") {
            $arr = Yii::$app->Utilities->getMemberZoneNews();
        } else if ($list == "ช่องทาง") {
            $arr = Yii::$app->Utilities->getMemberChannelNews();
        } else if ($list == "Account") {
            $arr = Yii::$app->Utilities->getMemberAccountNews();
        }

        return json_encode($arr);

        // i love you
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->news_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->news_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // $request = Yii::$app->request->post();

        try {
            //code...
            // $this->findModel($request['id'])->delete();
            $modelNotiHas = NotificationHasMember::find()->where(['module_id' => $id])->all();

            foreach ($modelNotiHas as $key => $e) {
                $modelNoti = Notification::find()->where(['notification_id' => $e->id])->all();
                foreach ($modelNoti as $k => $v) {

                    Yii::$app->db->createCommand()
                        ->delete('notification', ['notification_id' => $v->notification_id])
                        ->execute();

            
                }

                Yii::$app->db->createCommand()
                    ->delete('notification_has_member', 'module_id = ' . $e->module_id)
                    ->execute();
            }

            $this->findModel($id)->delete();

            return "Success";

        } catch (\Exception $th) {
            //throw $th;
            return $th;
        }

    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
