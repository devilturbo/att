<?php

namespace app\controllers;

use app\components\Helpers;
use app\components\Utilities;
use app\models\Area;
use app\models\Company;
use app\models\LocationWork;
use app\models\Member;
use app\models\News;
use app\models\NotificationHasMember;
use app\models\PaySlip;
use DateTime;
use DateTimeZone;
use Yii;
use yii\db\Exception;
use yii\rest\ActiveController;
use yii\web\Response;

class ApiController extends ActiveController
{

    public $enableCsrfValidation = false;

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['index'] = ['POST', 'GET'];
        return $verbs;
    }

    public $modelClass = 'app\models\Member';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actionProfile($id = null) //create

    {

        if (empty($id)) {
            $modelJson = [
                'code' => 400,
                'message' => 'bad request',
                'access_token' => false,
            ];
        } else {
            $model = Member::find()->where(['id' => $id])->One();
            $modelCompany = Company::find()->One();

            if (empty($model)) {
                $modelJson = [
                    'code' => 401,
                    'message' => 'authorized false',
                    'access_token' => false,
                ];

            } else {
                $hoilday_leave = $modelCompany->hoilday_leave;
                $errand_leave = $modelCompany->errand_leave;
                if (!empty($model->start_work_date)) {

                    $arrNormalHoilday = Utilities::getNormalHoliday($model->start_work_date);
                    $hoilday_leave = $arrNormalHoilday['hoilday_leave'];
                    $errand_leave = $arrNormalHoilday['errand_leave'];

                }
                $modelJson = [

                    'img' => $model->img,
                    'name' => Helpers::checkEmpty($model->fullname),
                    'member_no' => Helpers::checkEmpty($model->member_no),
                    'position' => Helpers::checkEmpty($model->position_name),
                    'type' => Helpers::checkEmpty($model->type_name),
                    'subtype' => Helpers::checkEmpty($model->subtype_name),
                    'email' => Helpers::checkEmpty($model->email),
                    'phone_number' => Helpers::checkEmpty($model->phone_number),
                    'line_id' => Helpers::checkEmpty($model->line_id),
                    'observer' => Helpers::checkEmpty($model->memberundder_name),
                    'time_attendance' => Helpers::checkEmpty($model->time_att),
                    'time_work' => '8 ชั่วโมง',
                    'time_day_off' => Helpers::checkEmpty($model->txtnormal_holiday),
                    'errand_leave' => $errand_leave,
                    'sick_leave' => Helpers::checkEmpty($modelCompany->sick_leave),
                    'errand_cut_leave' => Helpers::checkEmpty($modelCompany->errand_cut_leave),
                    'sick_cut_leave' => Helpers::checkEmpty($modelCompany->sick_cut_leave),
                    'hoilday_leave' => $hoilday_leave,
                    'maternity_leave' => Helpers::checkEmpty($modelCompany->maternity_leave),
                    'ordained_leave' => Helpers::checkEmpty($modelCompany->ordained_leave),
                    'errand_leave_total' => Helpers::checkEmpty($modelCompany->errand_leave),
                    'sick_leave_total' => Helpers::checkEmpty($modelCompany->sick_leave),
                    'errand_cut_leave_total' => Helpers::checkEmpty($modelCompany->errand_cut_leave),
                    'sick_cut_leave_total' => Helpers::checkEmpty($modelCompany->sick_cut_leave),
                    'hoilday_leave_total' => $hoilday_leave,
                    'maternity_leave_total' => Helpers::checkEmpty($modelCompany->maternity_leave),
                    'ordained_leave_total' => Helpers::checkEmpty($modelCompany->ordained_leave),
                    'status' => $model->status,
                    'code' => 200,
                    'message' => 'login pass',
                    'access_token' => true,
                ];
            }
        }

        return $modelJson;
    }

    public function actionLogin()
    {
        try {
            $request = !empty(Yii::$app->request->post()) ? Yii::$app->request->post() : Yii::$app->request->get();
            if (empty($request) || empty($request['username']) || empty($request['password']) || empty($request['type'])) {

                $modelJson = [
                    'member_id' => null,
                    'code' => 400,
                    'message' => 'bad request',
                    'data' => $request,
                    'access_token' => false,
                ];

                return $modelJson;
            }
            $username = trim($request['username']);
            $password = trim($request['password']);
            $model = Member::find()->where(['email' => $username])->andWhere(['phone_number' => $password])->One();
            if (empty($model)) {
                $modelJson = [
                    'member_id' => null,
                    'code' => 200,
                    'message' => 'Username หรือ Password ไม่ถูกต้อง',
                    'access_token' => false,
                ];
            } else {
                if ($model->status == 'N') {
                    $modelJson = [
                        'member_id' => $model->id,
                        'code' => 200,
                        'message' => 'Username ถูกระงับการใช้งาน',
                        'access_token' => false,
                    ];

                } else {

                    if ($request['type'] == 2) {
                        if ($model->isBa == 'Y') {
                            $modelJson = [
                                'member_id' => $model->id,
                                'code' => 200,
                                'message' => 'Success',
                                'access_token' => true,
                            ];
                        } else {
                            $modelJson = [
                                'member_id' => $model->id,
                                'code' => 200,
                                'message' => 'Username ไม่สามารถใช้งานระบบนี้ได้',
                                'access_token' => false,
                            ];
                        }

                    } else {

                        $modelJson = [
                            'member_id' => $model->id,
                            'code' => 200,
                            'message' => 'Success',
                            'access_token' => true,
                        ];
                    }

                }

            }
            return $modelJson;

        } catch (Exception $exception) {
            $modelJson = [
                'member_id' => null,
                'code' => 500,
                'message' => $exception,
                'access_token' => false,
            ];
            return $modelJson;
        }

    }

    public function actionLocation($id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (empty($id)) {
            $modelJson = [
                'code' => 400,
                'status' => false,
                'message' => 'bad request',
            ];
        } else {
            try {
                $modelJson = [];
                $model = LocationWork::find()->where(['member_id' => $id])->all();
                foreach ($model as $key => $item) {
                    if ($item->location_name) {
                        $modelJson['result'][] = [
                            'name' => $item->location_name,
                            'id' => $item->id,
                        ];
                    }

                }
                $modelJson['code'] = 200;
                $modelJson['message'] = 'Success';
                $modelJson['status'] = true;

            } catch (Exception $e) {
                $modelJson = [
                    'code' => 400,
                    'status' => false,
                    'message' => 'bad request',
                ];
                return ($modelJson);
            }
        }
        return ($modelJson);

    }

    public function actionPaySlip($id = null)
    {
        if (empty($id)) {
            $modelJson = [
                'code' => 400,
                'status' => false,
                'message' => 'bad request',
            ];
        } else {
            try {
                $member = Member::find()->where(['id' => $id])->One();
                $modelSalary = PaySlip::find()->where(['member_no' => $member->member_no])->orderBy(['pay_date' => SORT_DESC, 'updated_date' => SORT_DESC])->One();
                if (empty($modelSalary)) {
                    $modelSalary = new PaySlip();
                }
                $modelJson = array(
                    'result' => array(
                        'month_cycle' => Helpers::checkEmpty($modelSalary->month_expend),
                        'payment_date' => Helpers::checkEmpty($modelSalary->pay_date),
                        'member_no' => Helpers::checkEmpty($member->member_no),
                        'full_name' => Helpers::checkEmpty($member->fullname),
                        'department' => Helpers::checkEmpty($member->department_name),
                        'account_no' => Helpers::checkEmpty($modelSalary->bank_no),
                        'salary' => Helpers::checkEmpty(number_format($modelSalary->salary, 2)),
                        'incentive_money' => Helpers::checkEmpty(number_format($modelSalary->diligent_allowance, 2)),
                        'telephone_bill' => Helpers::checkEmpty(number_format($modelSalary->phone_allowance, 2)),
                        'other_income' => Helpers::checkEmpty(number_format($modelSalary->other_allowance, 2)),
                        'total_income' => Helpers::checkEmpty(number_format($modelSalary->total_salary, 2)),
                        'social_security' => Helpers::checkEmpty(number_format($modelSalary->social_security, 2)),
                        'total_debit' => Helpers::checkEmpty(number_format($modelSalary->deduction, 2)),
                        'net_income' => Helpers::checkEmpty(number_format($modelSalary->net_income, 2)),
                        'cumulative_income' => Helpers::checkEmpty(number_format($modelSalary->tax_income, 2)),
                        'cumulative_tax' => Helpers::checkEmpty(number_format($modelSalary->funds_income, 2)),
                        'cumulative_social_security' => Helpers::checkEmpty(number_format($modelSalary->cumulative_social_security, 2)),
                        'cumulative_funds' => Helpers::checkEmpty(number_format($modelSalary->funds_income, 2)),
                        'number_of_day' => Helpers::checkEmpty($modelSalary->total_days),
                        'rate_per_day' => Helpers::checkEmpty(number_format(($modelSalary->salary / 30), 2)),

                    ),
                    'code' => 200,
                    'status' => true,
                    'message' => 'Success',
                );
            } catch (Exception $e) {
                $modelJson = [
                    'code' => 400,
                    'status' => false,
                    'message' => 'bad request',
                ];
                return ($modelJson);
            }
        }
        return ($modelJson);
    }

    public function actionGetNews($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        // echo ($id);
        // die;
        if (empty($id)) {
            $modelJson = [
                'code' => 400,
                'status' => false,
                'message' => 'bad request',
            ];
        } else {
            try {
                $newsModel = News::find()->where(['news_id' => $id])->One();

                // $model->topic = empty($request['News']['topic']) ? "ไม่มีหัวข้อ" : $request['News']['topic'];
                //     // $model->topic_img = empty($request['News']['img']) ? "" : $request['News']['img'];
                //     $model->details = ($request["News"]['details']);
                //     $model->news_date = empty($request['News']['newsDate']) ? date("Y-m-d") : Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $request['News']['newsDate']);
                //     $model->position_id = empty($request['News']['position']) ? "9999" : $request['News']['position'];
                //     $model->member_type_id = empty($request['News']['type']) ? "9999" : $request['News']['type'];
                //     $model->member_subtype_id = empty($request['News']['subType']) ? "9999" : $request['News']['subType'];
                //     $model->member_area_id = empty($request['News']['memberArea']) ? "9999" : $request['News']['memberArea'];
                //     $model->member_zone_id = empty($request['News']['memberZone']) ? "9999" : $request['News']['memberZone'];
                //     $model->member_chanel_id = empty($request['News']['memberChannel']) ? "9999" : $request['News']['memberChannel'];
                //     $model->member_account_id = empty($request['News']['memberAccount']) ? "9999" : $request['News']['memberAccount'];
                //     $model->note = empty($request['News']['note']) ? "" : $request['News']['note'];
                //     $model->status = "N";
                $modelJson = array(
                    'result' => array(
                        'topic' => Helpers::checkEmpty($newsModel->topic),
                        'topic_img' => Helpers::checkEmpty($newsModel->topic_img),
                        'details' => Helpers::checkEmpty($newsModel->details),
                        'news_date' => Helpers::checkEmpty($newsModel->news_date),
                        'note' => Helpers::checkEmpty($newsModel->topic_img),

                    ),
                    'code' => 200,
                    'status' => true,
                    'message' => 'Success',
                );
            } catch (Exception $e) {
                $modelJson = [
                    'code' => 400,
                    'status' => false,
                    'message' => $e,
                ];
                return ($modelJson);
            }
        }
        return ($modelJson);
    }
    public function actionGetAllNews()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        try {
            $modelJson = [];
            // $newsModel = News::find()->where(['news_id' => $id])->One();

            $model = News::find()->all();
            foreach ($model as $key => $item) {
                $modelJson['result'][] = [
                    // 'name' => $item->location_name,
                    // 'id' => $item->id,
                    'id' => Helpers::checkEmpty($item->news_id),
                    'topic' => Helpers::checkEmpty($item->topic),
                    'topic_img' => Helpers::checkEmpty($item->topic_img),
                    // 'details' => Helpers::checkEmpty($item->details),
                    'news_date' => Helpers::checkEmpty($item->news_date),
                    'note' => Helpers::checkEmpty($item->note),

                ];

            }
            $modelJson['code'] = 200;
            $modelJson['message'] = 'Success';
            $modelJson['status'] = true;

        } catch (Exception $e) {
            $modelJson = [
                'code' => 400,
                'status' => false,
                'message' => $e,
            ];
            return ($modelJson);
        }

        return ($modelJson);

    }

    public function actionCheck_citizen($id)
    {
        // return "OK";
        Yii::$app->response->format = Response::FORMAT_JSON;

        $modelDup = Member::find()->where(['citizen_id' => $id])->count();
        if ($modelDup == 0) {
            $modelJson = [
                'code' => 200,
                'status' => true,
                'message' => 'Success',
            ];

        } else {
            $modelJson = [
                'code' => 400,
                'status' => false,
                'message' => "Fail",
            ];
        }

        return $modelJson;

    }

    public function actionCheck_branch_no($id)
    {
        // return "OK";
        Yii::$app->response->format = Response::FORMAT_JSON;

        $modelDup = Area::find()->where(['brach_no' => $id,'isDelete' => 'N'])->count();
        if ($modelDup == 0) {
            $modelJson = [
                'code' => 200,
                'status' => true,
                'message' => 'Success',
            ];

        } else {
            $modelJson = [
                'code' => 400,
                'status' => false,
                'message' => "Fail",
            ];
        }

        return $modelJson;

    }

    public function actionCheckDailyNews()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        // $today = Date("Y-m-d H:i:s");
        $today = (new DateTime("now", new DateTimeZone('Asia/Bangkok')))->format('Y-m-d H:i:s');

        $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $today);
        $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $today);
        // \app\components\Helpers::Debig(($today));

        // $countLeave = MemberLeave::find()->where(['isDelete' => 'N'])->andWhere(['between', 'created_date', $from, $to])->andWhere(['<>', 'isActive', "N"])->groupBy(['id'])->count();

        $model = News::find()
            ->select('*')
            ->where(['status' => 'N'])
            ->andWhere(['between', 'news_date', $from, $to])
            ->createCommand()
            ->queryAll();
        // \app\components\Helpers::Debig($model);

        if ($model) {

            $transaction = Yii::$app->db->beginTransaction();

            try {
                $count = 0;
                $modelNoti = new NotificationHasMember();
                foreach ($model as $key => $item) {
                    // \app\components\Helpers::Debig(( $item));

                    $count++;
                    $modelNoti->detail = $item['topic'];
                    $modelNoti->module_id = $item['news_id'];
                    $modelNoti->module = "news";
                    $modelNoti->position_id = $item['position_id'];
                    $modelNoti->member_type_id = $item['member_type_id'];
                    $modelNoti->member_subtype_id = $item['member_subtype_id'];
                    $modelNoti->member_area_id = $item['member_area_id'];
                    $modelNoti->member_zone_id = $item['member_zone_id'];
                    $modelNoti->member_chanel_id = $item['member_chanel_id'];
                    $modelNoti->member_account_id = $item['member_account_id'];
                    $modelNoti->create_date = $today;
                    $modelNoti->save(false);

                    Yii::$app->db->createCommand()->update('news', ['status' => 'Y'], 'news_id = ' . $item['news_id'])->execute();

                    // \app\components\Helpers::Debig($item);

                }

                $ch = curl_init('https://api.thebestpromote.com/web/api/notification');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
                curl_close($ch);

                $transaction->commit();

                $modelJson['code'] = 200;
                $modelJson['message'] = 'Success';
                $modelJson['status'] = true;
                $modelJson['count'] = $count;
            } catch (\Exception $th) {
                //throw $th;
                $transaction->rollBack();
                \app\components\Helpers::Debig(($th));

                $modelJson = [
                    'code' => 400,
                    'status' => false,
                    'message' => $th,
                ];
            }

        } else {
            $modelJson = [
                'code' => 200,
                'status' => false,
                'message' => "no news today",
                'count' => 0,
            ];
        }

        // \app\components\Helpers::Debig(( count($countLeave)));

        return $modelJson;

    }

    public function actionGetAccount()
    {
        $arr = Utilities::getMemberAccountNews();

        return $modelJson = [
            'code' => 200,
            'status' => 'Success',
            'data' => $arr,
        ];
    }

    public function actionGetMemberWork($id)
    {
        $arr = Utilities::getMemberLocationWork($id);

        return $modelJson = [
            'code' => 200,
            'status' => 'Success',
            'result' => $arr,
        ];
    }
}
