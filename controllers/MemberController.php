<?php

namespace app\controllers;

use app\components\Helpers;
use app\components\Utilities;
use app\models\Agency;
use app\models\Area;
use app\models\Company;
use app\models\Department;
use app\models\Gender;
use app\models\LocationWork;
use app\models\Member;
use app\models\MemberArea;
use app\models\MemberChangeTime;
use app\models\MemberChannel;
use app\models\MemberLeave;
use app\models\MemberOt;
use app\models\MemberPunchClock;
use app\models\MemberSubtype;
use app\models\MemberTempImport;
use app\models\MemberType;
use app\models\MemberZone;
use app\models\Position;
use app\models\Title;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\BaseFileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * MemberController implements the CRUD actions for Member model.
 */
class MemberController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [

                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $auth = Utilities::setAuthController();
        if($auth['member'] == 'N' && $action->id == 'index')
            throw new NotFoundHttpException('page not found');
        if($auth['member_add'] == 'N' && $action->id == 'profile')
            throw new NotFoundHttpException('page not found');
        if($auth['member_import'] == 'N' && $action->id == 'import')
            throw new NotFoundHttpException('page not found');

        return parent::beforeAction($action);
    }

    /**
     * Lists all Member models.
     * @return mixed
     */
    public function actionIndex()
    {

        $model = Member::find()->orderBy('name_th')->limit(15)->all();
        $modelRows = Member::find()->count();
        return $this->render('index', [
            'model' => $model,
            'modelRows' => $modelRows,
        ]);
    }

    public function actionMemberDt()
    {
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $start = $request['start'];
        $length = $request['length'];
        $search = !empty($request['search']['value']) ? $request['search']['value'] : null;
        $name = null;
        $status = null;
        $postion_role = null;
        $member_under_role = null;
        $type = null;
        $subtype = null;
        $position = null;
        $offset = 0;
        if ($start > 1) {
            $start = $start / 10;
            $offset = $start * $length;
        }
        if ($search) {
            $search_list = explode("&", $search);
            if (!empty($search_list[0])) {
                $name = $search_list[0];
            }

            if (!empty($search_list[1]));
            $type = trim($search_list[1]);
            if (!empty($search_list[2]));
            $status = $search_list[2];
            if (!empty($search_list[3]));
            $subtype = $search_list[3];
            if (!empty($search_list[4]));
            $position = $search_list[4];
        }


        if(Yii::$app->user->identity->auth_member_role == 1){
            if(Yii::$app->user->identity->status != 'X')
                $position = Yii::$app->user->identity->position_id;
            if(Yii::$app->user->identity->id)
                $member_under_role = Yii::$app->user->identity->id;

        }

        $model = Member::find()->where(['!=','status','X'])
            ->andFilterWhere(['status' => $status])
            ->andFilterWhere((['member_type_id' => $type]))
            ->andFilterWhere(['member_subtype_id' => $subtype])
            ->andFilterWhere(['<>','id' , $member_under_role])
            ->andFilterWhere(['position_id' => $position])
            ->orFilterWhere(['member_under_id' => $member_under_role])
            ->andFilterWhere(['like', 'name_th', $name])
            ->orFilterWhere(['like', 'surname_th', $name])
            ->limit($length)
            ->offset($offset)
            ->orderBy('name_th')
            ->all();
        $modelRows = Member::find()->where(['!=','status','X'])
            ->andFilterWhere(['status' => $status])
            ->andFilterWhere((['member_type_id' => $type]))
            ->andFilterWhere(['member_subtype_id' => $subtype])
            ->andFilterWhere(['<>','id' , $member_under_role])
            ->andFilterWhere(['position_id' => $position])
            ->orFilterWhere(['member_under_id' => $member_under_role])
            ->andFilterWhere(['like', 'name_th', $name])
            ->orFilterWhere(['like', 'surname_th', $name])

            ->count();
        $modelJson = [];
        $modelJson['data'] = [];
        $modelJson['draw'] = $request['draw'];
        $modelJson['recordsTotal'] = $modelRows;
        $modelJson['recordsFiltered'] = $modelRows;
        foreach ($model as $key => $item) {
            $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
            if (!empty($item->img)) {
                $img = Yii::$app->request->baseUrl . '/' . $item->img;
            }
            $modelJson['data'][] = [
                '<img class="thumb mr-2 lazy" src="' . $img . '" align="left" >',
                $item->member_no,
                $item->citizen_id,
                $item->title_th,
                $item->name_th . ' ' . $item->surname_th,
                $item->gender_name,
                $item->phone_number,
                $item->email,
                $item->agency_name,
                $item->department_name,
                $item->position_name,
                $item->type_name,
                $item->subtype_name,
                $item->memberundder_name,
                $item->area_name,
                $item->zone_name,
                $item->channel_name,
                Helpers::convertDate($item->start_work_date),
                $item->end_date,
                Utilities::timespan($item->start_work_date),
                $item->status_name,
                Yii::$app->request->baseUrl . '/member/profile-detail?id=' . $item->id,
            ];
        }

        return $modelJson;
    }

    /**
     * Displays a single Member model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Member model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionProfile($id = null)
    {

        if ($id) {
            $model = Member::find()->where(['id' => $id])->One();
            $modelLocationWork = LocationWork::find()->where(['member_id' => $id])->all();
            // \app\components\Helpers::Debig($modelLocationWork);
        } else {
            $model = new Member();
            $modelLocationWork = new LocationWork();
        }

        $request = Yii::$app->request->post();
            // \app\components\Helpers::Debig($request);

        $modelArea = Area::find()->where(['isDelete' => 'N'])->orderBy([new \yii\db\Expression('account = "7-11" DESC , account ASC')])->all();
        if (!empty($request)) {

            $model->load($request);

            $file = UploadedFile::getInstance($model, 'img');
            $path = Yii::$app->params['path_member'];

            if ($file) {
                $basePath = $path;
                BaseFileHelper::createDirectory($basePath);
                $realFileName = md5($file->tempName . time()) . '.' . $file->extension;
                $fullPath = $basePath . $realFileName;
                $file->saveAs($fullPath);
                Utilities::setImage($fullPath,$basePath);
                $model->img = $fullPath;

            }
            if ($model->status == 'N') {
                $model->inActive_date = date('Y-m-d H:i:s');
            }

            if (!$id) {
                $model->member_no = Utilities::getMemberNo();
            }

            $model->save(false);
            $idMember = $model->id;
            if (!empty($request['location'])) {
                LocationWork::deleteAll(['member_id' => $idMember]);
                foreach ($request['location'] as $key => $item) {
                    $modelLocationSave = new LocationWork();
                    $modelLocationSave->member_id = $idMember;
                    $modelLocationSave->area_id = trim($item);
                    $modelLocationSave->save(false);
                }

            }

            return $this->redirect('index');

        }

        $modelRows = Member::find()->count();
        return $this->render('_profile', [
            'model' => $model,
            'modelArea' => $modelArea,
            'modelLocationWork' => $modelLocationWork,
        ]);
    }

    public function actionImport()
    {


        $path = Yii::$app->params['path_member'];
        $model = new MemberTempImport();
        if ($model->load(Yii::$app->request->post())) {

            $file = UploadedFile::getInstance($model, 'file');

            if ($file) {
                $basePath = $path;
                BaseFileHelper::createDirectory($basePath);
                $realFileName = md5($file->tempName . time()) . '.' . $file->extension;
                $fullPath = $basePath . $realFileName;
                if ($file->saveAs($fullPath)) {
                    $objPHPExcel = \PHPExcel_IOFactory::load($fullPath);
                    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    foreach ($sheetData as $key => $value) {
                        try {
                            if ($key > 1  && !empty($value['B']) && !empty($value['C']) && !empty($value['D']) && !empty($value['G']) && !empty($value['F'])) {

                                $modelSave = new MemberTempImport();
                                $modelSave->citizen_id = Helpers::checkEmpty($value['A']);
                                $modelSave->title = Helpers::checkEmpty($value['B']);
                                $modelSave->name_th = Helpers::checkEmpty($value['C']);
                                $modelSave->surname_th = Helpers::checkEmpty($value['D']);
                                $modelSave->gender = Helpers::checkEmpty($value['E']);
                                $modelSave->phone_number = Helpers::checkEmpty($value['F']);
                                $modelSave->email = Helpers::checkEmpty($value['G']);
                                $modelSave->agency = Helpers::checkEmpty($value['H']);
                                $modelSave->department = Helpers::checkEmpty($value['I']);
                                $modelSave->position = Helpers::checkEmpty($value['J']);
                                $modelSave->mebertype = Helpers::checkEmpty($value['K']);
                                $modelSave->merber_subtype = Helpers::checkEmpty($value['L']);
                                $modelSave->contact = Helpers::checkEmpty($value['M']);
                                $modelSave->under = Helpers::checkEmpty($value['N']);
                                $modelSave->area = Helpers::checkEmpty($value['O']);
                                $modelSave->distinct_area = Helpers::checkEmpty($value['P']);
                                $modelSave->chanel = Helpers::checkEmpty($value['Q']);
                                $modelSave->work_station = Helpers::checkEmpty($value['R']);
                                $modelSave->branch_station = Helpers::checkEmpty($value['S']);
                                $modelSave->start_date = Helpers::convertDateSearch($value['T']);
                                $modelSave->address = Helpers::checkEmpty($value['U']);
                                $modelSave->bookbank_no = Helpers::checkEmpty($value['V']);
                                $modelSave->save(false);

                            }
                        }catch (\Exception $e){
                            throw new NotFoundHttpException('ไม่สามารถ import ข้อมูล พนักงานได้');
                        }
                    }
                }
            }

        }
        $modelImport = MemberTempImport::find()->orderBy('name_th')->all();
        $modelRows = MemberTempImport::find()->count();

        return $this->render('_member_import', [
            'model' => $modelImport,
            'rows' => $modelRows,
        ]);
    }

    public function actionProfileDetail($id)
    {
        if (empty($id)) {
            throw new NotFoundHttpException('400 bad request');
        }

        $model = Member::find()->where(['id' => $id])->One();
        $modelCompany = Company::find()->One();
        $modelArea = LocationWork::find()->where(['member_id' => $id])->all();
        $data = MemberLeave::find()->where(['isDelete' => 'N', 'member_id' => $id])->andWhere(['=', 'isActive', "Y"])->orderBy(['created_date' => SORT_DESC])->all();

        return $this->render('_profile_detail', [
            'model' => $model,
            'data' => $data,
            'modelCompany' => $modelCompany,
            'modelArea' => $modelArea,
        ]);
    }

    public function actionProfileLocation($id)
    {
        if (empty($id)) {
            throw new NotFoundHttpException('400 bad request');
        }
        $model = Member::find()->where(['id' => $id])->One();
        $modelArea = LocationWork::find()->where(['member_id' => $id])->all();
        $modelAreaRows = LocationWork::find()->where(['member_id' => $id])->count();

        return $this->render('_profile_location', [
            'model' => $model,
            'modelArea' => $modelArea,
            'modelAreaRows' => $modelAreaRows,
        ]);
    }

    public function BuildMember($model)
    {
        $modelMemberSave = Member::find()->where(['like','name_th' , trim($model->name_th)])->andWhere(['like','surname_th' , trim($model->surname_th)])->One();
        if (empty($modelMemberSave)) {
            $modelMemberSave = new Member();
        }

        $title = null;
        $gender = null;
        $agency = null;
        $department = null;
        $position = null;
        $memberType = null;
        $memberSubType = null;
        $area = null;
        $channel = null;
        $zone = null;
        $member_under = null;
        if (!empty($model->title)) {
            $title = Title::find()->where(['like', 'title_th', $model->title])->One();
        }

        if (!empty($model->gender)) {
            $gender = Gender::find()->where(['like', 'gender_th', $model->gender])->One();
        }

        if (!empty($model->agency)) {
            $agency = Agency::find()->where(['like', 'agent_name', $model->agency])->One();
        }

        if (!empty($model->department)) {
            $department = Department::find()->where(['like', 'department_name', $model->department])->One();
        }

        if (!empty($model->position)) {
            $position = Position::find()->where(['like', 'position_name', $model->position])->One();
        }

        if (!empty($model->mebertype)) {
            $memberType = MemberType::find()->where(['like', 'type_name', $model->mebertype])->One();
        }

        if (!empty($model->merber_subtype)) {
            $memberSubType = MemberSubtype::find()->where(['like', 'subtype_name', $model->merber_subtype])->One();
        }

        if (!empty($model->area)) {
            $area = MemberArea::find()->where(['like', 'area', trim($model->area)])->One();
        }

        if (!empty($model->distinct_area)) {
            $zone = MemberZone::find()->where(['like', 'zone', trim($model->distinct_area)])->One();
        }

        if (!empty($model->chanel)) {
            $channel = MemberChannel::find()->where(['like', 'channel', trim($model->chanel)])->One();
        }

        if (!empty($model->under)) {
            $member_under = Member::find()->where(['like', 'name_th', trim($model->under)])->orWhere(['like', 'surname_th', $model->under])->One();
        }

        $modelMemberSave->citizen_id = Helpers::checkEmpty($model->citizen_id);
        $modelMemberSave->title_th_id = Helpers::checkEmptyModel($title);
        $modelMemberSave->gender_id = Helpers::checkEmptyModel($gender);
        $modelMemberSave->area_id = Helpers::checkEmptyModel($area);
        $modelMemberSave->channel_id = Helpers::checkEmptyModel($channel);
        $modelMemberSave->zone_id = Helpers::checkEmptyModel($zone);
        $modelMemberSave->position_id = Helpers::checkEmptyModel($position);
        $modelMemberSave->department_id = Helpers::checkEmptyModel($department);
        $modelMemberSave->agency_id = Helpers::checkEmptyModel($agency);
        $modelMemberSave->member_under_id = Helpers::checkEmptyModel($member_under);
        $modelMemberSave->member_type_id = Helpers::checkEmptyModel($memberType);
        $modelMemberSave->member_subtype_id = Helpers::checkEmptyModel($memberSubType);
        $modelMemberSave->name_th = Helpers::checkEmpty($model->name_th);
        $modelMemberSave->surname_th = Helpers::checkEmpty($model->surname_th);
        $modelMemberSave->name_th = Helpers::checkEmpty($model->name_th);
        $modelMemberSave->surname_th = Helpers::checkEmpty($model->surname_th);
        $modelMemberSave->phone_number = Helpers::checkEmpty($model->phone_number);
        $modelMemberSave->address = Helpers::checkEmpty($model->address);
        $modelMemberSave->bookbank_no = Helpers::checkEmpty($model->bookbank_no);
        $modelMemberSave->email = Helpers::checkEmpty($model->email);
        $modelMemberSave->status = 'Y';
        $modelMemberSave->member_no = Utilities::getMemberNo();
        $modelMemberSave->save(false);
        $model->delete();
        return $modelMemberSave->id;

    }

    public function actionSetMember()
    {
        $request = Yii::$app->request->post();
        $id = null;
        if (!empty($request) && !empty($request['ele']) && !empty($request['id'])) {
            $model = MemberTempImport::find()->where(['id' => $request['id']])->One();
            if ($request['ele'] == 'del') {
                $model->delete();
            } else {
                $id = $this->BuildMember($model);

            }
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        }

        return [
            'code' => 200,
            'message' => 'บันทึกเรียบร้อย',
            'id' => $id,
        ];

    }

    public function actionDelMemberall(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        MemberTempImport::deleteAll();
        return [
            'code' => 200,
            'message' => 'ลบข้อมูลเรียบร้อย',
        ];
    }

    public function actionSetMemberall()
    {
        $modelAll = MemberTempImport::find()->all();
        foreach ($modelAll as $key => $value) {
            $model = MemberTempImport::find()->where(['id' => $value->id])->One();
            $this->BuildMember($model);
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return [
            'code' => 200,
            'message' => 'บันทึกเรียบร้อย',
        ];

    }

    /**
     * Updates an existing Member model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Member model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Member model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Member the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Member::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionLeave()
    {
        // $searchModel = new MemberLeaveSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = MemberLeave::find()->where(['isDelete' => 'N'])->andWhere(['<>', 'isActive', "W"])->limit(1)->orderBy(['created_date' => SORT_DESC])->all();
        // $dataCount = MemberLeave::find()->where(['isActive' => 'W', 'isDelete' => 'N'])->count();

        return $this->render('member-leave', [
            'data' => $data,
            // 'dataCount' => $dataCount,
            // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLeaveDt()
    {
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $start = $request['start'];
        $length = $request['length'];
        $search = !empty($request['search']['value']) ? $request['search']['value'] : null;
        $name = null;
        $gender = null;
        $department = null;
        $position = null;
        $type = null;
        $status = null;
        $check_date = null;

        $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', '02/13/2000');
        $to = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', '02/13/2050');

        $offset = 0;
        if ($start > 1) {
            $start = $start / 10;
            $offset = $start * $length;
        }
        if ($search) {
            $search_list = explode("&", $search);
            if (!empty($search_list[0])) {
                $name = trim($search_list[0]);
            }
            if (!empty($search_list[1]));
            $gender = trim($search_list[1]);
            if (!empty($search_list[2]));
            $department = $search_list[2];
            if (!empty($search_list[3]));
            $position = $search_list[3];
            if (!empty($search_list[4]));
            $type = $search_list[4];
            if (!empty($search_list[5])) {
                if ($search_list[5] == "accept") {
                    $status = "Y";
                } else if ($search_list[5] == "wait") {
                    $status = "W";
                } else if ($search_list[5] == "decline") {
                    $status = "N";
                }
            }

            if (!empty($search_list[6])) {
                // $check_date = $search_list[6];
                $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $search_list[6]);
                $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $search_list[6]);
            }
        }

        $model = MemberLeave::find()
            ->leftJoin('member', 'member.id = member_leave.member_id')
            ->andFilterWhere(['like', 'member.name_th', $name])
            ->orFilterWhere(['like', 'member.surname_th', $name])
            ->andFilterWhere(['isActive' => $status])
            ->andFilterWhere((['member.gender_id' => $gender]))
            ->andFilterWhere(['member.department_id' => $department])
            ->andFilterWhere(['member.position_id' => $position])
            ->andFilterWhere(['member.member_type_id' => $type])
            ->orderBy(['created_date' => SORT_DESC])
            ->limit($length)
            ->offset($offset);

        $modelRows = MemberLeave::find()
            ->leftJoin('member', 'member.id = member_leave.member_id')
            ->andFilterWhere(['like', 'member.name_th', $name])
            ->orFilterWhere(['like', 'member.surname_th', $name])
            ->andFilterWhere(['isActive' => $status])
            ->andFilterWhere((['member.gender_id' => $gender]))
            ->andFilterWhere(['member.department_id' => $department])
            ->andFilterWhere(['member.position_id' => $position])
            ->andFilterWhere(['member.member_type_id' => $type])
            ->andWhere(['between', 'created_date', $from, $to])
            ->limit($length)
            ->offset($offset);

        if (!empty($search_list[6])) {
            $model->andWhere(['between', 'created_date', $from, $to]);
            $modelRows->andWhere(['between', 'created_date', $from, $to]);
        }

        $count = $modelRows->count();
        $modelJson = [];
        $modelJson['data'] = [];
        $modelJson['draw'] = $request['draw'];
        $modelJson['recordsTotal'] = $count;
        $modelJson['recordsFiltered'] = $count;

        foreach ($model->all() as $key => $item) {
            $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
            if (!empty($item->img)) {
                $img = Yii::$app->request->baseUrl . '/' . $item->img;
            }

            $img_2 = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
            if (!empty($item->img_url)) {
                $img_2 = 'https://api.thebestpromote.com/web/' . $item->img_url;
            }

            $displayActive = "";
            if ($item->isActive == "W") {
                $displayActive = 'รออนุมัติ';
            } else if ($item->isActive == "Y") {
                $displayActive = "อนุมัติแล้ว";
            } else if ($item->isActive == "N") {
                $displayActive = "ไม่อนุมัติ";
            }

            $modelJson['data'][] = [
                '<img class="thumb mr-2 lazy" src="' . $img . '" align="left" >',
                $item->member_no,
                $item->name_th . ' ' . $item->surname_th,
                $item->gender_name,
                $item->department_name,
                $item->position_name,
                $item->type_name,
                $item->subtype_name,
                $item->memberundder_name,
                $item->leave_type,
                Yii::$app->Utilities->formatDate('d/m/Y', $item->start_date),
                Yii::$app->Utilities->formatDate('d/m/Y', $item->end_date),
                $item->remark,
                $displayActive,
                '<span class="img-thumbmail"><a href="' . $img_2 . '" target="blank"><img src="' . $img_2 . '"align="left"></a></span>',
                // empty($search_list) ? "" : $search_list,
            ];

        }

        return $modelJson;
    }

    public function actionProfileLeave($id,$date=null)
    {

        if (empty($id)) {
            throw new NotFoundHttpException('400 bad request');
        }


        if ($date == null) {
            $date = date('Y-m-d H:i:s');
        }

        $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $date);
        $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $date);


        $model = Member::find()->where(['id' => $id])->One();
        $modelCompany = Company::find()->One();
        $dataAll = MemberLeave::find()
        ->where(['isDelete' => 'N', 'member_id' => $id])
        ->andWhere(['=', 'isActive', "Y"])
        // ->andWhere(['between', 'created_date', $from, $to])
        ->orderBy(['created_date' => SORT_DESC])
        ->all();

        $data = MemberLeave::find()
        ->where(['isDelete' => 'N', 'member_id' => $id])
        ->andWhere(['<>', 'isActive', "W"])
        ->andWhere(['between', 'created_date', $from, $to])
        ->orderBy(['created_date' => SORT_DESC])
        ->all();

        $dataCount = MemberLeave::find()
        ->where(['isDelete' => 'N', 'member_id' => $id])
        ->andWhere(['<>', 'isActive', "W"])
        ->andWhere(['between', 'created_date', $from, $to])
        ->count();

        // \app\components\Helpers::Debig(($dataCount));

        return $this->render('_profile_leave', [
            'data' => $data,
            'dataCount' => $dataCount,
            'dataAll'=>$dataAll,
            'model' => $model,
            'modelCompany' => $modelCompany,
        ]);
    }

    public function actionChangeTime()
    {
        $data = MemberChangeTime::find()->where(['isDelete' => 'N'])->andWhere(['<>', 'isActive', "W"])->limit(1)->orderBy(['created_date' => SORT_DESC])->all();

        return $this->render('member-change-time', [
            'data' => $data,
        ]);
    }

    public function actionChangeDt()
    {
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $start = $request['start'];
        $length = $request['length'];
        $search = !empty($request['search']['value']) ? $request['search']['value'] : null;
        $name = null;
        $gender = null;
        $department = null;
        $position = null;
        $type = null;
        $status = null;
        $check_date = null;
        $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', '02/13/2000');
        $to = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', '02/13/2080');

        $offset = 0;
        if ($start > 1) {
            $start = $start / 10;
            $offset = $start * $length;
        }
        if ($search) {
            $search_list = explode("&", $search);
            if (!empty($search_list[0])) {
                $name = trim($search_list[0]);
            }
            if (!empty($search_list[1]));
            $gender = trim($search_list[1]);
            if (!empty($search_list[2]));
            $department = $search_list[2];
            if (!empty($search_list[3]));
            $position = $search_list[3];
            if (!empty($search_list[4]));
            $type = $search_list[4];
            if (!empty($search_list[5])) {
                if ($search_list[5] == "accept") {
                    $status = "Y";
                } else if ($search_list[5] == "wait") {
                    $status = "W";
                } else if ($search_list[5] == "decline") {
                    $status = "N";
                }
            }

            if (!empty($search_list[6])) {
                // $check_date = $search_list[6];
                $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $search_list[6]);
                $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $search_list[6]);
            }
        }

        $model = MemberChangeTime::find()
            ->leftJoin('member', 'member.id = member_change_time.member_id')
            ->andFilterWhere(['like', 'member.name_th', $name])
            ->orFilterWhere(['like', 'member.surname_th', $name])
            ->andFilterWhere(['isActive' => $status])
            ->andFilterWhere((['member.gender_id' => $gender]))
            ->andFilterWhere(['member.department_id' => $department])
            ->andFilterWhere(['member.position_id' => $position])
            ->andFilterWhere(['member.member_type_id' => $type])
            ->orderBy(['created_date' => SORT_DESC])
        // ->andWhere(['between', 'created_date', $from, $to])
            ->limit($length)
            ->offset($offset);

        $modelRows = MemberChangeTime::find()
            ->leftJoin('member', 'member.id = member_change_time.member_id')
            ->andFilterWhere(['like', 'member.name_th', $name])
            ->orFilterWhere(['like', 'member.surname_th', $name])
            ->andFilterWhere(['isActive' => $status])
            ->andFilterWhere((['member.gender_id' => $gender]))
            ->andFilterWhere(['member.department_id' => $department])
            ->andFilterWhere(['member.position_id' => $position])
            ->andFilterWhere(['member.member_type_id' => $type])
            ->limit($length)
            ->offset($offset);

        if (!empty($search_list[6])) {
            $model->andWhere(['between', 'created_date', $from, $to]);
            $modelRows->andWhere(['between', 'created_date', $from, $to]);
        }

        $count = $modelRows->count();
        $modelJson = [];
        $modelJson['data'] = [];
        $modelJson['draw'] = $request['draw'];
        $modelJson['recordsTotal'] = $count;
        $modelJson['recordsFiltered'] = $count;

        foreach ($model->all() as $key => $item) {
            $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
            if (!empty($item->img)) {
                $img = Yii::$app->request->baseUrl . '/' . $item->img;
            }

            $img_2 = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
            if (!empty($item->img_url)) {
                $img_2 = 'https://api.thebestpromote.com/web/' . $item->img_url;
            }

            $displayActive = "";
            if ($item->isActive == "W") {
                $displayActive = 'รออนุมัติ';
            } else if ($item->isActive == "Y") {
                $displayActive = "อนุมัติแล้ว";
            } else if ($item->isActive == "N") {
                $displayActive = "ไม่อนุมัติ";
            }

            $modelJson['data'][] = [
                '<img class="thumb mr-2 lazy" src="' . $img . '" align="left" >',
                $item->member_no,
                $item->name_th . ' ' . $item->surname_th,
                $item->gender_name,
                $item->department_name,
                $item->position_name,
                $item->type_name,
                $item->subtype_name,
                $item->memberundder_name,
                // $item->leave_type,
                'ปรับปรุงเวลา',
                Yii::$app->Utilities->formatDate('d/m/Y - H:i', $item->start_date . " " . $item->start_time),
                Yii::$app->Utilities->formatDate('d/m/Y - H:i', $item->end_date . " " . $item->end_time),
                $item->member->member_location_name,
                $item->remark,
                $displayActive,
                '<span class="img-thumbmail"><a href="' . $img_2 . '" target="blank"><img src="' . $img_2 . '"align="left"></a></span>',
                // empty($search_list) ? "" : $search_list,
            ];

        }

        return $modelJson;
    }

    public function actionProfileChangeTime($id , $date = null)
    {

        if (empty($id)) {
            throw new NotFoundHttpException('400 bad request');
        }

        if ($date == null) {
            $date = date('Y-m-d H:i:s');
        }

        $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $date);
        $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $date);



        $model = Member::find()->where(['id' => $id])->One();
        $modelCompany = Company::find()->One();
        $data = MemberChangeTime::find()
        ->where(['isDelete' => 'N', 'member_id' => $id])
        ->andWhere(['<>', 'isActive', "W"])
        ->orderBy(['created_date' => SORT_DESC])
        ->andWhere(['between', 'created_date', $from, $to])
        ->all();

        $dataCount = MemberChangeTime::find()
        ->where(['isDelete' => 'N', 'member_id' => $id])
        ->andWhere(['<>', 'isActive', "W"])
        ->andWhere(['between', 'created_date', $from, $to])
        ->count();

        // \app\components\Helpers::Debig(($dataCount));

        return $this->render('_profile_change_time', [
            'data' => $data,
            'dataCount' => $dataCount,
            'model' => $model,
            'modelCompany' => $modelCompany,
        ]);
    }

    public function actionOt()
    {
        $data = MemberOt::find()->where(['isDelete' => 'N'])->andWhere(['<>', 'isActive', "W"])->limit(1)->orderBy(['created_date' => SORT_DESC])->all();

        return $this->render('member-ot', [
            'data' => $data,

        ]);
    }

    public function actionOtDt()
    {
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $start = $request['start'];
        $length = $request['length'];
        $search = !empty($request['search']['value']) ? $request['search']['value'] : null;
        $name = null;
        $gender = null;
        $department = null;
        $position = null;
        $type = null;
        $status = null;
        $check_date = null;
        $offset = 0;

        if ($start > 1) {
            $start = $start / 10;
            $offset = $start * $length;
        }
        if ($search) {
            $search_list = explode("&", $search);
            if (!empty($search_list[0])) {
                $name = trim($search_list[0]);
            }
            if (!empty($search_list[1]));
            $gender = trim($search_list[1]);
            if (!empty($search_list[2]));
            $department = $search_list[2];
            if (!empty($search_list[3]));
            $position = $search_list[3];
            if (!empty($search_list[4]));
            $type = $search_list[4];
            if (!empty($search_list[5])) {
                if ($search_list[5] == "accept") {
                    $status = "Y";
                } else if ($search_list[5] == "wait") {
                    $status = "W";
                } else if ($search_list[5] == "decline") {
                    $status = "N";
                }
            }

            if (!empty($search_list[6])) {
                // $check_date = $search_list[6];
                $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $search_list[6]);
                $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $search_list[6]);
            }
        }

        $model = MemberOt::find()
            ->leftJoin('member', 'member.id = member_ot.member_id')
            ->andFilterWhere(['like', 'member.name_th', $name])
            ->orFilterWhere(['like', 'member.surname_th', $name])
            ->andFilterWhere(['isActive' => $status])
            ->andFilterWhere((['member.gender_id' => $gender]))
            ->andFilterWhere(['member.department_id' => $department])
            ->andFilterWhere(['member.position_id' => $position])
            ->andFilterWhere(['member.member_type_id' => $type])
            ->orderBy(['created_date' => SORT_DESC])
        // ->andWhere(['between', 'created_date', $from, $to])
            ->limit($length)
            ->offset($offset);
        // ->all();

        $modelRows = MemberOt::find()
            ->leftJoin('member', 'member.id = member_ot.member_id')
            ->andFilterWhere(['like', 'member.name_th', $name])
            ->orFilterWhere(['like', 'member.surname_th', $name])
            ->andFilterWhere(['isActive' => $status])
            ->andFilterWhere((['member.gender_id' => $gender]))
            ->andFilterWhere(['member.department_id' => $department])
            ->andFilterWhere(['member.position_id' => $position])
            ->andFilterWhere(['member.member_type_id' => $type])
        // ->andWhere(['between', 'created_date', $from, $to])
            ->limit($length)
            ->offset($offset);
        // ->count();

        if (!empty($search_list[6])) {
            $model->andWhere(['between', 'created_date', $from, $to]);
            $modelRows->andWhere(['between', 'created_date', $from, $to]);
        }

        $modelRows = $modelRows->count();
        $modelJson = [];
        $modelJson['data'] = [];
        $modelJson['draw'] = $request['draw'];
        $modelJson['recordsTotal'] = $modelRows;
        $modelJson['recordsFiltered'] = $modelRows;

        foreach ($model->all() as $key => $item) {
            $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
            if (!empty($item->img)) {
                $img = Yii::$app->request->baseUrl . '/' . $item->img;
            }

            $img_2 = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
            if (!empty($item->img_url)) {
                $img_2 = 'https://api.thebestpromote.com/web/' . $item->img_url;
            }

            $displayActive = "";
            if ($item->isActive == "W") {
                $displayActive = 'รออนุมัติ';
            } else if ($item->isActive == "Y") {
                $displayActive = "อนุมัติแล้ว";
            } else if ($item->isActive == "N") {
                $displayActive = "ไม่อนุมัติ";
            }

            $modelJson['data'][] = [
                '<img class="thumb mr-2 lazy" src="' . $img . '" align="left" >',
                $item->member_no,
                $item->name_th . ' ' . $item->surname_th,
                $item->gender_name,
                $item->department_name,
                $item->position_name,
                $item->type_name,
                $item->subtype_name,
                $item->memberundder_name,
                // $item->leave_type,
                'ทำงานล่วงเวลา',
                Yii::$app->Utilities->formatDate('d/m/Y - H:i', $item->ot_date . " " . $item->start_time),
                Yii::$app->Utilities->formatDate('d/m/Y - H:i', $item->ot_date . " " . $item->end_time),
                $item->member->member_location_name,
                $item->remark,
                $displayActive,
                '<span class="img-thumbmail"><a href="' . $img_2 . '" target="blank"><img src="' . $img_2 . '"align="left"></a></span>',
                // empty($search_list) ? "" : $search_list,
            ];
         
        }

        return $modelJson;
    }

    public function actionProfileOt($id ,$date = null)
    {

        if (empty($id)) {
            throw new NotFoundHttpException('400 bad request');
        }

        $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $date);
        $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $date);




        $model = Member::find()->where(['id' => $id])->One();
        $modelCompany = Company::find()->One();
        $data = MemberOt::find()
        ->where(['isDelete' => 'N', 'member_id' => $id])
        ->andWhere(['<>', 'isActive', "W"])
        ->andWhere(['between', 'created_date', $from, $to])
        ->orderBy(['created_date' => SORT_DESC])
        ->all();
        $dataCount = MemberOt::find()
        ->where(['isDelete' => 'N', 'member_id' => $id])
        ->andWhere(['between', 'created_date', $from, $to])
        ->andWhere(['<>', 'isActive', "W"])->count();

        // \app\components\Helpers::Debig(($dataCount));

        return $this->render('_profile_ot', [
            'data' => $data,
            'dataCount' => $dataCount,
            'model' => $model,
            'modelCompany' => $modelCompany,
        ]);
    }

    public function actionProfileWorkday($id, $date = null)
    {

        if (empty($id)) {
            throw new NotFoundHttpException('400 bad request');
        }

        $model = Member::find()->where(['id' => $id])->One();

        if ($date == null) {
            $date = date('Y-m-d H:i:s');
        }

        $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $date);
        $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $date);

        $data = MemberPunchClock::find()
            ->where(['member_id' => $id])
            ->andWhere(['between', 'punch_datetime', $from, $to])
            ->orderBy(['area_id' => SORT_DESC])
            ->all();

        // \app\components\Helpers::Debig(($modelArea));

        return $this->render('_profile_workday', [
            'model' => $model,
            'data' => $data,
            // 'modelAreaRows' => $modelAreaRows,
        ]);

    }

    public function actionWorkday($date = null)
    {
        date_default_timezone_set("Asia/Bangkok");

        if ($date == null) {
            $date = date('Y-m-d H:i:s');
        }

        // $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $date);
        // $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $date);

        $data = MemberPunchClock::find()
        // ->where(['isDelete' => 'N'])
            // ->andWhere(['between', 'punch_datetime', $from, $to])
            ->limit(0)
        // ->orderBy(['area_id' => SORT_DESC])
            ->all();

        return $this->render('member-workday', [
            'data' => $data,

        ]);
    }

    public function actionWorkdayDt()
    {
        $request = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $start = $request['start'];
        $length = $request['length'];
        $search = !empty($request['search']['value']) ? $request['search']['value'] : null;
        $name = null;
        $department = null;
        $position = null;
        $type = null;
        $subtype = null;
        $area = null;
        $zone = null;
        $channel = null;
        $account = null;
        $check_date = null;
        $offset = 0;
        // let strSearch = global_filter +'&'+departmentSearch+'&'+positionSearch+'&'+typeSearch +'&'+subtypeSearch +'&'+date +'&'+areaSearch +'&'+zoneSearch +'&'+channelSearch +'&'+accountSearch

        if ($start > 1) {
            $start = $start / 10;
            $offset = $start * $length;
        }
        if ($search) {
            $search_list = explode("&", $search);
            if (!empty($search_list[0])) {
                $name = trim($search_list[0]);
            }
            if (!empty($search_list[1]));
            $department = trim($search_list[1]);
            if (!empty($search_list[2]));
            $position = $search_list[2];
            if (!empty($search_list[3]));
            $type = $search_list[3];
            if (!empty($search_list[4]));
            $subtype = $search_list[4];
            if (!empty($search_list[5])) {
                // $check_date = $search_list[6];
                $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', date('Y-m-d H:i:s'));
                $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', date('Y-m-d H:i:s'));
            }

            if (!empty($search_list[6]));
            $area = $search_list[6];
            if (!empty($search_list[7]));
            $zone = $search_list[7];
            if (!empty($search_list[8]));
            $channel = $search_list[8];
            if (!empty($search_list[9]));
            $account = $search_list[9];

            if (!empty($search_list[10]) && !empty($search_list[11]) ) {
                // $check_date = $search_list[6];
                $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $search_list[10]);
                $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', $search_list[11]);
            }

        }

        $model = MemberPunchClock::find()
            ->leftJoin('member', 'member.id = member_punch_clock.member_id')
            ->andFilterWhere(['like', 'member.name_th', $name])
            ->orFilterWhere(['like', 'member.surname_th', $name])
            ->andFilterWhere(['member.department_id' => $department])
            ->andFilterWhere(['member.position_id' => $position])
            ->andFilterWhere(['member.member_type_id' => $type])
            ->andFilterWhere(['member.member_subtype_id' => $subtype])
            ->andFilterWhere(['member.area_id' => $area])
            ->andFilterWhere(['member.zone_id' => $zone])
            ->andFilterWhere(['member.channel_id' => $channel])
            ->andFilterWhere(['member.account_id' => $account])
            ->orderBy(['punch_datetime' => SORT_DESC])
        // ->andWhere(['between', 'created_date', $from, $to])
            ->limit($length)
            ->offset($offset);
        // ->all();

        $modelRows = MemberPunchClock::find()
            ->leftJoin('member', 'member.id = member_punch_clock.member_id')
            ->andFilterWhere(['like', 'member.name_th', $name])
            ->orFilterWhere(['like', 'member.surname_th', $name])
            ->andFilterWhere(['member.department_id' => $department])
            ->andFilterWhere(['member.position_id' => $position])
            ->andFilterWhere(['member.member_type_id' => $type])
            ->andFilterWhere(['member.member_subtype_id' => $subtype])
            ->andFilterWhere(['member.area_id' => $area])
            ->andFilterWhere(['member.zone_id' => $zone])
            ->andFilterWhere(['member.channel_id' => $channel])
            ->andFilterWhere(['member.account_id' => $account])
            ->limit($length)
            ->offset($offset);
        // ->count();

        if (!empty($search_list[10]) && !empty($search_list[11]) ) {
            $model->andWhere(['between', 'punch_datetime', $from, $to]);
            $modelRows->andWhere(['between', 'punch_datetime', $from, $to]);
        }else{
            $from = Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', date('Y-m-d H:i:s'));
            $to = Yii::$app->Utilities->formatDate('Y-m-d 23:59:59', date('Y-m-d H:i:s'));
            
            $model->andWhere(['between', 'punch_datetime', $from, $to]);
            $modelRows->andWhere(['between', 'punch_datetime', $from, $to]);

        }

        $modelRows = $modelRows->count();
        $modelJson = [];
        $modelJson['data'] = [];
        $modelJson['draw'] = $request['draw'];
        $modelJson['recordsTotal'] = $modelRows;
        $modelJson['recordsFiltered'] = $modelRows;

        foreach ($model->all() as $key => $item) {

            $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
            if (!empty($item->img)) {
                // $img = Yii::$app->request->baseUrl . '/' . $e->img;
                $img = 'https://api.thebestpromote.com/web/' . $item->img;
            }
            $location = $item->area->account . "/" . $item->area->brach_no . "/" . $item->area->local_name;

            $modelJson['data'][] = [
                '<img class="thumb mr-2 lazy" src="' . $img . '" align="left" >',
                $item->member->member_no,
                $item->member->name_th . ' ' . $item->member->surname_th,
                Yii::$app->Utilities->formatDate('H:i', $item->punch_datetime),
                $item->punch_type == "in" ? "บันทึกเข้า" : "บันทึกออก",
                $location,
                $item->member->department_name,
                $item->member->position_name,
                $item->member->type_name,
                $item->member->subtype_name,
                Yii::$app->Utilities->formatDate('d/m/yy', $item->punch_datetime),
                $item->member->memberundder_name,
                $item->member->area_name,
                $item->member->zone_name,
                $item->member->channel_name,
                $item->member->account_name,
                // $item->leave_type,
                // 'ทำงานล่วงเวลา',
                // Yii::$app->Utilities->formatDate('d/m/Y - H:i', $item->ot_date . " " . $item->start_time),
                // Yii::$app->Utilities->formatDate('d/m/Y - H:i', $item->ot_date . " " . $item->end_time),
                // $item->member->member_location_name,
                // $item->remark,
                // $displayActive,
                '<a href="' . Yii::$app->request->baseUrl . '/member/profile-workday?id=' . $item->member->id . '"><button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button></a>',
                empty($search_list) ? "" : $search_list,
            ];
            //     <td class="employee-col">
            //     <img class="thumb mr-2 lazy" src="<?=$img" align="left">
            // </td>
            // <td><?=$e->member->member_no</td>
            // <td><?=$e->member->fullname</td>
            // <td><?=Yii::$app->Utilities->formatDate('H:i', $e->punch_datetime)</td>
            // <td><?=$e->punch_type== "in" ? "บันทึกเข้า" : "บันทึกออก"</td>
            // <td><?=$location</td>
            // <td><?=$e->member->department_name</td>
            // <td><?=$e->member->position_name</td>
            // <td><?=$e->member->type_name</td>
            // <td><?=$e->member->subtype_name</td>
            // <td><?=Yii::$app->Utilities->formatDate('d/m/yy', $e->punch_datetime)</td>
            // <td><?=$e->member->memberundder_name</td>
            // <td><?=$e->member->area_name</td>
            // <td><?=$e->member->zone_name</td>
            // <td><?=$e->member->channel_name</td>
            // <td><?=$e->member->account_name</td>
            // <td>
            //     <a href="<?=Yii::$app->request->baseUrl/member/profile-workday?id=<?=$e->member->id">
            //         <button class="btn btn-outline-primary btn-sm mb-1">ประวัติบันทึกเวลาพนักงาน</button>
            //     </a>
            // </td>
        }

        return $modelJson;
    }

    public function actionGetlist($list)
    {

        // return $list;
        $list = trim($list);
        $arr = [];
        if ($list == "ตำแหน่ง") {
            $arr = Yii::$app->Utilities->getListPositionNews();
        } else if ($list == "ประเภท") {
            $arr = Yii::$app->Utilities->getListTypeNews();
        } else if ($list == "ประเภทย่อย") {
            $arr = Yii::$app->Utilities->getListSubTypeNews();
        } else if ($list == "พื้นที่") {
            $arr = Yii::$app->Utilities->getMemberAreaNews();
        } else if ($list == "เขตพื้นที่") {
            $arr = Yii::$app->Utilities->getMemberZoneNews();
        } else if ($list == "ช่องทาง") {
            $arr = Yii::$app->Utilities->getMemberChannelNews();
        } else if ($list == "Account") {
            $arr = Yii::$app->Utilities->getMemberAccountNews();
        } else if ($list == "เพศ") {
            $arr = Yii::$app->Utilities->getListGenderPure();
        } else if ($list == "แผนก") {
            $arr = Yii::$app->Utilities->getListDepartmentPure();
        } else if ($list == "ห้างร้าน") {
            $arr = Yii::$app->Utilities->getListMemberAccountPure();
        }

        // \app\components\Helpers::Debig(($arr));

        return json_encode($arr);

        // i love you
    }

}
