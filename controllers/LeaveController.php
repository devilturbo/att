<?php

namespace app\controllers;

use app\components\Utilities;
use app\models\Member;
use app\models\MemberLeave;
use Yii;
use yii\db\Exception;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class LeaveController extends ActiveController
{

    public $enableCsrfValidation = false;

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['index'] = ['POST', 'GET'];
        return $verbs;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [

                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],

            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public $modelClass = 'app\models\Member';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actionCreateLeave()
    {

        // \app\components\Helpers::Debig('testsetsts');


        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request->post();

        $model = new MemberLeave();

        if (!empty($request)) {

            try {
                //code...

                $model->load($request);
                $file = UploadedFile::getInstance($model, 'img_url');
                $path = Yii::$app->params['path_member_leave'];

                if ($file) {

                    $basePath = $path;
                    BaseFileHelper::createDirectory($basePath);
                    $realFileName = md5($file->tempName . time()) . '.' . $file->extension;
                    $fullPath = $basePath . $realFileName;
                    $file->saveAs($fullPath);
                    $model->img_url = $fullPath;

                }
                // 'id' => 'ID',
                // 'type_id' => 'Type ID',
                // 'member_id' => 'Member ID',
                // 'start_date' => 'Start Date',
                // 'end_date' => 'End Date',
                // 'img_url' => 'Img Url',
                // 'remark' => 'Remark',
                // 'isActive' => 'Is Active',
                // 'isDelete' => 'Is Delete',
                // 'created_date' => 'Created Date',
                // 'updated_date' => 'Updated Date',
                $model->type_id = $request['type_id'];
                $model->member_id = $request['member_id'];
                $model->start_date = $request['start_date'];
                $model->end_date = $request['end_date'];
                $model->remark = $request['remark'];
                $model->isActive = "N";
                $model->isDelete = "N";
                $model->created_date = empty($request['created_date']) ? date("Y-m-d H:i:s") : Yii::$app->Utilities->formatDate('Y-m-d 00:00:00', $request['created_date']);
                $model->save(false);

                $modelJson = [
                    'code' => 200,
                    'status' => true,
                    'message' => 'Success',
                ];
                return ($modelJson);

            } catch (\Exception $th) {
                //throw $th;
                $modelJson = [
                    'code' => 400,
                    'status' => false,
                    'message' => $th,
                ];
                return ($modelJson);

            }

        } else {
            $modelJson = [
                'code' => 400,
                'status' => false,
                'message' => 'no request ',
            ];
            return ($modelJson);
        }

    }

    public function actionGet_member_leave()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request->get();
        if ($request['id']) {
            try {
                $modelJson = [];
                // $model = LocationWork::find()->where(['member_id'=>$id])->all();
                $model = MemberLeave::find()->where(['isActive' => 'N', 'isDelete' => 'N', 'member_id' => $request['id']])->all();
                // $dataCount = MemberLeave::find()->where(['isActive' => 'N', 'isDelete' => 'N', 'member_id' => $request['id']])->count();

                foreach ($model as $key => $item) {

                    $modelMember = Member::find()->where(['id' => $request['id']])->One();

                    if ($item->isActive == "N") {
                        $displayActive = 'ไม่อนุมัติ';
                    } else if ($item->isActive == "Y") {
                        $displayActive = "อนุมัติ";
                    }

                    $img = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';
                    if (!empty($item->img)) {

                        $img = Yii::$app->request->baseUrl . '/' . $modelMember->img;
                    }

                    $leaveImg = Yii::$app->request->baseUrl . '/images/user/thumb/thumb.jpg';

                    if (!empty($item->img_url)) {

                        $leaveImg = Yii::$app->request->baseUrl . '/' . $item->img_url;
                    }

                    $modelJson['result'][] = [
                        'id' => $item->id,
                        'user_img' => $img,
                        'member_no' => $modelMember->member_no,
                        'name_th' => $modelMember->name_th,
                        'surname_th' => $modelMember->surname_th,
                        'full_name_th' => $modelMember->name_th . ' ' . $modelMember->surname_th,
                        'gender_name' => $modelMember->gender_name,
                        'department_name' => $modelMember->department_name,
                        'position_name' => $modelMember->position_name,
                        'type_name' => $modelMember->type_name,
                        'subtype_name' => $modelMember->subtype_name,
                        'memberundder_name' => $modelMember->memberundder_name,
                        'start_date' => Yii::$app->Utilities->formatDate('d/m/Y', $item->start_date),
                        'end_date' => Yii::$app->Utilities->formatDate('d/m/Y', $item->end_date),
                        'remark' => $item->remark,
                        'leaveImg' => $leaveImg,
                        'isActive' => $item->isActive,
                        'isDelete' => $item->isDelete,
                        // 'dataCount' => $dataCount,
                    ];

                }
                $modelJson['code'] = 200;
                $modelJson['message'] = 'Success';
                $modelJson['status'] = true;

            } catch (Exception $e) {
                $modelJson = [
                    'code' => 400,
                    'status' => false,
                    'message' => 'bad request ' . $e,
                ];
                return ($modelJson);
            }

        } else {
            $modelJson = [
                'code' => 400,
                'status' => false,
                'message' => 'request id',
            ];
            return ($modelJson);
        }

        return ($modelJson);

    }

}