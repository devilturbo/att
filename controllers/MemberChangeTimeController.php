<?php

namespace app\controllers;

use app\components\Utilities;
use app\models\MemberChangeTime;
use app\models\NotificationHasMember;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * MemberChangeTimeController implements the CRUD actions for MemberChangeTime model.
 */
class MemberChangeTimeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [

                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],

            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $auth = Utilities::setAuthController();
        if($auth['history_update'] == 'N' || $auth['approve'] == 'N')
            throw new NotFoundHttpException('page not found');

        return parent::beforeAction($action);
    }
    /**
     * Lists all MemberChangeTime models.
     * @return mixed
     */
    public function actionIndex()
    {
        $data = MemberChangeTime::find()->where(['isActive' => 'W', 'isDelete' => 'N'])->orderBy(['created_date' => SORT_DESC])->all();
        $dataCount = MemberChangeTime::find()->where(['isActive' => 'W', 'isDelete' => 'N'])->count();

        return $this->render('index', [
            'data' => $data,
            'dataCount' => $dataCount,
        ]);
    }

    /**
     * Displays a single MemberChangeTime model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MemberChangeTime model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MemberChangeTime();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MemberChangeTime model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MemberChangeTime model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MemberChangeTime model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MemberChangeTime the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MemberChangeTime::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetlist($list)
    {

        // return $list;
        $list = trim($list);
        $arr = [];
        if ($list == "ตำแหน่ง") {
            $arr = Yii::$app->Utilities->getListPositionNews();
        } else if ($list == "ประเภท") {
            $arr = Yii::$app->Utilities->getListTypeNews();
        } else if ($list == "ประเภทย่อย") {
            $arr = Yii::$app->Utilities->getListSubTypeNews();
        } else if ($list == "พื้นที่") {
            $arr = Yii::$app->Utilities->getMemberAreaNews();
        } else if ($list == "เขตพื้นที่") {
            $arr = Yii::$app->Utilities->getMemberZoneNews();
        } else if ($list == "ช่องทาง") {
            $arr = Yii::$app->Utilities->getMemberChannelNews();
        } else if ($list == "Account") {
            $arr = Yii::$app->Utilities->getMemberAccountNews();
        } else if ($list == "เพศ") {
            $arr = Yii::$app->Utilities->getListGenderPure();
        } else if ($list == "แผนก") {
            $arr = Yii::$app->Utilities->getListDepartmentPure();
        }

        // \app\components\Helpers::Debig(($arr));

        return json_encode($arr);

        // i love you
    }

    public function actionAccept($id)
    {
        $request = Yii::$app->request->queryParams;

        $transaction = Yii::$app->db->beginTransaction();

        try {
            //code...
            Yii::$app->db->createCommand()->update('member_change_time', ['isActive' => 'Y'], 'id = ' . $request['id'])->execute();

            $model = MemberChangeTime::find()->where(['id' => $request['id']])->One();
            $modelNoti = new NotificationHasMember();

            $modelNoti->detail = 'approve';
            $modelNoti->module_id = $model->id;
            $modelNoti->member_id = $model->member_id;
            $modelNoti->module = "member_change_time";
            $modelNoti->position_id = $model->member->position_id;
            $modelNoti->member_type_id = $model->member->member_type_id;
            $modelNoti->member_subtype_id = $model->member->member_subtype_id;
            $modelNoti->member_area_id = $model->member->area_id;
            $modelNoti->member_zone_id = $model->member->zone_id;
            $modelNoti->member_chanel_id = $model->member->channel_id;
            $modelNoti->member_account_id = $model->member->account_id;
            $modelNoti->create_date = date('Y-m-d H:i:s');
            $modelNoti->save(false);
            // \app\components\Helpers::Debig($model);

            $transaction->commit();

// send to third party notification
            $ch = curl_init('https://api.thebestpromote.com/web/api/notification?id=' . $model->member_id);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);

            return "Success";

        } catch (\Exception $th) {
            //throw $th;
            $transaction->rollBack();
            // \app\components\Helpers::Debig($th);
            return $th;
            // echo $th;
        }

    }

    public function actionDecline($id)
    {
        $request = Yii::$app->request->queryParams;

        $transaction = Yii::$app->db->beginTransaction();

        try {
            //code...
            Yii::$app->db->createCommand()->update('member_change_time', ['isActive' => 'N'], 'id = ' . $request['id'])->execute();

            $model = MemberChangeTime::find()->where(['id' => $request['id']])->One();
            $modelNoti = new NotificationHasMember();

            $modelNoti->detail = 'decline';
            $modelNoti->module_id = $model->id;
            $modelNoti->member_id = $model->member_id;
            $modelNoti->module = "member_change_time";
            $modelNoti->position_id = $model->member->position_id;
            $modelNoti->member_type_id = $model->member->member_type_id;
            $modelNoti->member_subtype_id = $model->member->member_subtype_id;
            $modelNoti->member_area_id = $model->member->area_id;
            $modelNoti->member_zone_id = $model->member->zone_id;
            $modelNoti->member_chanel_id = $model->member->channel_id;
            $modelNoti->member_account_id = $model->member->account_id;
            $modelNoti->create_date = date('Y-m-d H:i:s');
            $modelNoti->save(false);
            // \app\components\Helpers::Debig($model);

            $transaction->commit();

// send to third party notification
            $ch = curl_init('https://api.thebestpromote.com/web/api/notification?id=' . $model->member_id);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);

            return "Success";

        } catch (\Exception $th) {
            //throw $th;
            $transaction->rollBack();
            // \app\components\Helpers::Debig($th);
            return $th;
            // echo $th;
        }

      
    }


    public function actionGetdata()
    {
        date_default_timezone_set("Asia/Bangkok");
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $query = MemberChangeTime::find()
            ->orderBy(['created_date' => SORT_DESC])
            ->all();

        // \app\components\Helpers::Debig($query[1]->remark);
        $count = 0;
        $output = [];
        $data = [];
        $tmp = array();
        foreach ($query as $key => $e) {
            // \app\components\Helpers::Debig($e);
            $displayActive = "";
            if ($e->isActive == "W") {
                $displayActive = 'รออนุมัติ';
            } else if ($e->isActive == "Y") {
                $displayActive = "อนุมัติแล้ว";
            } else if ($e->isActive == "N") {
                $displayActive = "ไม่อนุมัติ";
            }

            $tmp['member_no'] = $e->member_no;
            $tmp['fullname'] = $e->name_th . ' ' . $e->surname_th;
            $tmp['gender_name'] = $e->gender_name;
            $tmp['department_name'] = $e->department_name;
            $tmp['position_name'] = $e->position_name;
            $tmp['type_name'] = $e->type_name;
            $tmp['subtype_name'] = $e->subtype_name;
            $tmp['memberundder_name'] = $e->memberundder_name;
            $tmp['detail'] = "ปรับปรุงเวลา";
            $tmp['start_date'] = Yii::$app->Utilities->formatDate('d/m/Y - H:i', $e->start_date . " " . $e->start_time);
            $tmp['end_date'] = Yii::$app->Utilities->formatDate('d/m/Y - H:i', $e->end_date . " " . $e->end_time);
            $tmp['member_location_name'] =  $e->member->member_location_name;
            $tmp['remark'] = $e->remark;
            $tmp['displayActive'] = $displayActive;

            array_push($data, $tmp);
            $tmp = array();

        }

        // \app\components\Helpers::Debig($data);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $modelJson = array(
            'result' => $data,
            'code' => 200,
            'status' => true,
            'message' => 'Success',
        );

    }

}
