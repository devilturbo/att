<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use app\components\Helpers;
use app\models\Member;
use app\models\LoginForm;
use app\models\ContactForm;

class LoginController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                return $this->goBack();
            }
        }
        if(!empty(Yii::$app->user->identity->email)){
            $this->redirect('site');
        }
        return $this->render('index', [
            'model' => $model
        ]);
    }

    public function actionUserAuthen(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        try {
            $request = !empty(Yii::$app->request->post()) ? Yii::$app->request->post() : Yii::$app->request->get();
            // echo '<pre>';print_r($request);exit;
            if (empty($request) || empty($request['username']) || empty($request['password']) || empty($request['type'])) {

                $modelJson = [
                    'member_id' => null,
                    'code' => 400,
                    'message' => 'bad request',
                    'data' => $request,
                    'access_token' => false
                ];

                return $modelJson;
            }
            $username = trim($request['username']);
            $password = trim($request['password']);
            $isActive = 0;
            // Use : atadmin@gmail.com
            // Pass :  Usx434psdf


                $model = Member::find()->where(['email'=>$username])->andWhere(['phone_number'=>$password])->One();
                if (!empty($model)) {
                    if($model->position->auth_backend_role == 1)
                    {
                        $isActive = 1;
                    }else{
                        $modelJson = [
                            'member_id' => null,
                            'code' => 200,
                            'message' => 'Username ไม่สามารถเข้าใช้งาน Back-office ได้',
                            'access_token' => false
                        ];
                        return $modelJson;
                    }
                }


            if ($isActive == 1) {
                $modelJson = [
                    'member_id' => null,
                    'code' => 200,
                    'message' => 'Success',
                    'access_token' => true
                ];
            } else {
                $modelJson = [
                    'member_id' => null,
                    'code' => 200,
                    'message' => 'Username หรือ Password ไม่ถูกต้อง',
                    'access_token' => false
                ];
            }
            return $modelJson;

        } catch (Exception $exception) {
            $modelJson = [
                'member_id' => null,
                'code' => 500,
                'message' => $exception,
                'access_token' => false
            ];
            return $modelJson;
        }
    }
}