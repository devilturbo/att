<?php
namespace app\components;

use Yii;
use yii\base\Component;

class Helpers extends Component
{
    public static function GetErrorModel($model)
    {
        $arrError = [];
        foreach ($model->getErrors() as $key => $value) {
            $arrError[] = $value[0];
        }

        return implode(',', $arrError);
    }

    public static function getYear(){
        $data = [];
        $year=date('Y');

        for($i=0;$i<30;$i++)
        {
            if($i==0){
                $data[$year-$i] = $year-$i;
            }else{
                $data[$year-$i] = $year-$i;
            }
        }

        return $data;
    }

    public static function thaiMonth($show = 'short')
    {
        $month = [
            'short' => [1=>"ม.ค.", 2=>"ก.พ.", 3=>"มี.ค.", 4=>"เม.ย.", 5=>"พ.ค.", 6=>"มิ.ย.", 7=>"ก.ค.", 8=>"ส.ค.", 9=>"ก.ย.", 10=>"ต.ค.", 11=>"พ.ย.", 12=>"ธ.ค."],
            'long' => [1=>"มกราคม", 2=>"กุมภาพันธ์", 3=>"มีนาคม", 4=>"เมษายน", 5=>"พฤษภาคม", 6=>"มิถุนายน", 7=>"กรกฎาคม", 8=>"สิงหาคม", 9=>"กันยายน", 10=>"ตุลาคม", 11=>"พฤศจิกายน", 12=>"ธันวาคม"]
        ];

        return $month[$show];
    }

    public static function convertDateSearch($str)
    {
        $res = NULL;
        $str = trim($str);
        if(!empty($str))
        {
            $explodDate  = explode("/",$str);
            $Year        = $explodDate[2];
            $date        = $Year.'-'.$explodDate[1].'-'.$explodDate[0];
            $old_date_timestamp = strtotime($date);
            $res  = date('Y-m-d', $old_date_timestamp);

        }
        return $res;
    }

    public static function convertDateSearchStr($str)
    {
        $res = NULL;
        $str = trim($str);
        if(!empty($str))
        {
            $explodDate  = explode("-",$str);
            $Year        = $explodDate[2];
            $date        = $Year.'-'.$explodDate[1].'-'.$explodDate[0];
            $old_date_timestamp = strtotime($date);
            $res  = date('Ymd', $old_date_timestamp);

        }
        return $res;
    }

    public static function importType(){
        $data = [
            Yii::$app->request->baseUrl.'/report/statisticopd/statistic' => 'statistic',
            Yii::$app->request->baseUrl.'/report/statisticopd/pmpa' => 'pmpa'
        ];

        return $data;
    }

    public static function importTypeIPD(){
        $data = [
            Yii::$app->request->baseUrl.'/ipdreport/statisticipd/statisticipd' => 'Statistics-IPD',
            Yii::$app->request->baseUrl.'/ipdreport/statisticipd/pmpa' => 'pmpa-in-out',

        ];

        return $data;
    }

    public static function convertDate($str)
    {
        $res = NULL;
        $str = trim($str);
        if(!empty($str))
        {

            $res  = date('d/m/Y', strtotime($str));

        }
        return $res;
    }

    public function covertAge($str)
    {
        $res = null;

        if(!empty($str))
        {
            $birthDate = $str;

            $birthDate = explode("-", $birthDate);

            $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[2], $birthDate[1], $birthDate[0]))) > date("md")? ((date("Y") - $birthDate[0]) - 1) : (date("Y") - $birthDate[0]));

            $res = $age;

        }

        return $res;

    }

    public function convertICD($str)
    {
        $res = trim($str);
        $resArr = [
            'group' => NULL,
            'first' => NULL,
            'second' => NULL
        ];
        if($res == 9999)
            return $resArr;

        if($res)
        {

            $strLen = strlen($res);

            if($strLen > 3)
            {

                if(strpos($res,'.') > 0)
                {

                    $res_explode = explode(".",$res);
                    $resArr['group'] = $res_explode[0];
                    $strlen_dot = strlen($res_explode[1]);
                    if($strlen_dot > 1){
                        $resArr['first'] = substr($res_explode[1],0,1);
                        $resArr['second'] = substr($res_explode[1],1);

                    }else{
                        $resArr['first'] = $res_explode[1];

                    }
                }

            }else{

                $resArr['group'] = $res;
            }

            return $resArr;


        }
        else
        {
            return $resArr;
        }

    }

    public static function checkEmpty($str)
    {
        $res = NULL;
        $str = trim($str);
        if(!empty($str))
        {
            $res = $str;
        }
        return $res;
    }

    public static function checkEmptyModel($str)
    {
        $res = NULL;
        
        if(!empty($str))
        {
            $res = $str->id;
        }
        return $res;
    }



    public static function Debig($e, $exit = true)
    {
        echo "<pre style='background: #202020; color: #fff'>";
        print_r($e);
        echo "<pre>";
        if ($exit) {
            die;
        }
    }

}