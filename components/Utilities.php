<?php
namespace app\components;

use app\models\Agency;
use app\models\Area;
use app\models\Company;
use app\models\Department;
use app\models\Gender;
use app\models\Member;
use app\models\MemberAccount;
use app\models\MemberArea;
use app\models\MemberChangeTime;
use app\models\MemberChannel;
use app\models\MemberLeave;
use app\models\MemberOt;
use app\models\MemberSubtype;
use app\models\MemberType;
use app\models\MemberZone;
use app\models\Position;
use app\models\Title;
use app\models\LocationWork ;
use app\models\AreaGroup ;
use Imagine\Image\Box;
use Yii;

use DateTime;
use DateTimeZone;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

class Utilities extends Component
{
    public function getListPosition()
    {
        $model = Position::find()->orderBy('position_name')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'position_name');
        $arrayData = ['' => 'เลือก'] + $arrData;

        return $arrayData;
    }

    public static function setImage($openPath,$savePath){

        $imagine = Image::getImagine();
        $imagine = $imagine->open($openPath);
        $sizes = getimagesize ( $openPath );
        $width = 500;
        $height = round($sizes[1]*$width/$sizes[0]);
        $imagine = $imagine->resize(new Box($width, $height))->save($openPath, ['quality' => 70]);
        return true;
    }

    public static function setMenu(){
        $auth = \Yii::$app->user->identity;

        $menuArr = [
            'company' => $auth->auth_company == 3?'none':'',
            'company_holidays' => $auth->auth_holidays == 3?'none':'',
            'member' =>  $auth->auth_member == 3?'none':'',
            'member_add' =>  $auth->auth_add_member == 3?'none':'',
            'member_import' =>  $auth->auth_import_member == 3?'none':'',
            'approve' =>  $auth->auth_approve_order == 3?'none':'',
            'area' =>  $auth->auth_location == 3?'none':'',
            'history_add' => $auth->auth_record_time == 3?'none':'',
            'history_leave' => $auth->auth_record_leave == 3?'none':'',
            'history_update' => $auth->auth_update_time == 3?'none':'',
            'history_ot' => $auth->auth_record_ot == 3?'none':'',
            'news' => $auth->auth_news == 3?'none':'',
            'export'  => $auth->auth_export == 3?'none':'',
        ];

        return $menuArr;
    }

    public static function setAuthController(){
        $auth = \Yii::$app->user->identity;

        $menuArr = [
            'company' => $auth->auth_company == 3?'N':'Y',
            'company_holidays' => $auth->auth_holidays == 3?'N':'Y',
            'member' =>  $auth->auth_member == 3?'N':'Y',
            'member_add' =>  $auth->auth_add_member == 3?'N':'Y',
            'member_import' =>  $auth->auth_import_member == 3?'N':'Y',
            'approve' =>  $auth->auth_approve_order == 3?'N':'Y',
            'area' =>  $auth->auth_location == 3?'N':'Y',
            'history_add' => $auth->auth_record_time == 3?'N':'Y',
            'history_leave' => $auth->auth_record_leave == 3?'N':'Y',
            'history_update' => $auth->auth_update_time == 3?'N':'Y',
            'history_ot' => $auth->auth_record_ot == 3?'N':'Y',
            'news' => $auth->auth_news == 3?'N':'Y',
            'export'  => $auth->auth_export == 3?'N':'Y',
        ];

        return $menuArr;
    }

    public static function getListMember(){
        $auth = Member::find()->where(['status'=>'Y'])->all();
        foreach ($auth as $key => $item)
        {
            $a[$item->id] = [

                    'id' => $item->id,
                    'username' => $item->email,
                    'password' => $item->phone_number,
                    'authKey' => 'test100key',
                    'accessToken' => '100-token'

            ];
        }

        return $a;
    }

    public function getListPositionNews()
    {
        $model = Position::find()->orderBy('position_name')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'position_name');
        $arrayData = $arrData;

        return $arrayData;
    }

    public function getListDepartment()
    {
        $model = Department::find()->orderBy('department_name')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'department_name');
        $arrayData = ['' => 'เลือก'] + $arrData;

        return $arrayData;
    }

    public function getListDepartmentPure()
    {
        $model = Department::find()->orderBy('department_name')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'department_name');

        return $arrData;
    }

    public static function getListMemberAccountPure()
    {
        $model = MemberAccount::find()->orderBy('account')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'account');
        return $arrData;
    }

    public static function getRowsPosition($name)
    {

        $count = 0;

        $modelSelect = Position::find()->where(['like', 'position_name', $name])->One();
        if (!empty($modelSelect)) {
            $model = Member::find()->where(['position_id' => $modelSelect->id])->Count();
            $count = $model;
        }

        return $count;
    }

    public static function getRowsType($name)
    {

        $count = 0;

        $modelSelect = MemberType::find()->where(['like', 'type_name', $name])->One();
        if (!empty($modelSelect)) {
            $model = Member::find()->where(['member_type_id' => $modelSelect->id])->Count();
            $count = $model;
        }

        return $count;
    }

    public static function getRowsSubType($name)
    {

        $count = 0;

        $modelSelect = MemberSubtype::find()->where(['like', 'subtype_name', $name])->One();
        if (!empty($modelSelect)) {
            $model = Member::find()->where(['member_subtype_id' => $modelSelect->id])->Count();
            $count = $model;
        }

        return $count;
    }

    public static function getMemberById($id)
    {

        // \app\components\Helpers::Debig(( $id));
        $modelMember = Member::find()->where(['like', 'id', $id])->One();

        return $modelMember;
    }

    public function getListTitleTH()
    {
        $model = Title::find()->orderBy('id')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'title_th');
        $arrayData = ['' => 'เลือก'] + $arrData;

        return $arrayData;
    }

    public function getListRelation()
    {
        $arrData = [
            1 => 'บิดามารดา',
            2 => 'ญาติพี่น้อง',
            3 => 'คู่สมรส',
            4 => 'เพื่อน',
        ];
        $arrayData = ['' => 'เลือก'] + $arrData;
        return $arrayData;
    }

    public function getListStatus()
    {
        $arrData = [
            'Y' => 'ใช้งาน',
            'N' => 'ไม่ใช้งาน',

        ];
        $arrayData = $arrData;
        return $arrayData;
    }

    public function getListGender()
    {
        $model = Gender::find()->orderBy('id')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'gender_th');
        $arrayData = ['' => 'เลือก'] + $arrData;
        return $arrayData;
    }

    public function getListGenderPure()
    {
        $model = Gender::find()->orderBy('id')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'gender_th');
        return $arrData;
    }

    public function getListTitleEN()
    {
        $model = Title::find()->orderBy('id')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'title_en');
        $arrayData = ['' => 'เลือก'] + $arrData;

        return $arrayData;
    }

    public function getListMemberNo()
    {
        $arrData = [];
        $model = Member::find()->orderBy('id')->asArray()->all();
        foreach ($model as $key => $item) {
            // \app\components\Helpers::Debig(( $item));
            $arrData[$item['member_no']] = $item['member_no'] . ' ' . $item['name_th']  . ' ' . $item['surname_th'] ;
        }

        $arrayData = ['' => 'เลือก'] + $arrData;

        return $arrayData;
    }

    public static function getMemberNo()
    {

        $model = Member::find()->select('max(member_no) as member_no')->One();
        $year = date('Y') + 543;
        $year = substr($year, -2);
        $res = substr_replace("0000000", ($model->member_no + 1), (7 - strlen(($model->member_no + 1))));
        $res = $year . substr($res, 2);

        return $res;

    }

    public static function getListType()
    {
        $model = MemberType::find()->orderBy('id')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'type_name');
        $arrayData = ['' => 'เลือก'] + $arrData;
        return $arrayData;
    }

    public static function getListTypeNews()
    {
        $model = MemberType::find()->orderBy('id')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'type_name');
        $arrayData = $arrData;
        return $arrayData;
    }

    public static function getListAgency()
    {
        $model = Agency::find()->orderBy('agent_name')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'agent_name');
        $arrayData = ['' => 'เลือก'] + $arrData;
        return $arrayData;
    }

    public static function getListSubType()
    {
        $model = MemberSubtype::find()->orderBy('id')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'subtype_name');
        $arrayData = ['' => 'เลือก'] + $arrData;
        return $arrayData;
    }

    public static function getListSubTypeNews()
    {
        $model = MemberSubtype::find()->orderBy('id')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'subtype_name');
        $arrayData = $arrData;
        return $arrayData;
    }

    public static function getListSup()
    {
        $arrData = [];
        $modelPosition = Position::find()->select('id')->where(['auth_member_role'=>1]);
        $model = Member::find()->where(['in', 'position_id', $modelPosition])->orderBy('name_th')->all();
        foreach ($model as $key => $item) {
            $arrData[$item->id] = $item->title_th . ' ' . $item->name_th . ' ' . $item->surname_th;
        }

        $arrayData = ['' => 'เลือก'] + $arrData;

        return $arrayData;
    }

    public static function getMemberZone()
    {
        $model = MemberZone::find()->orderBy('zone')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'zone');
        $arrayData = ['' => 'เลือก'] + $arrData;
        return $arrayData;
    }

    public static function getMemberZoneNews($select = "id")
    {
        $model = MemberZone::find()->orderBy('zone')->asArray()->all();
        $arrData = ArrayHelper::map($model, $select, 'zone');
        $arrayData = $arrData;
        return $arrayData;
    }

    public static function getMemberAccount()
    {
        $model = MemberAccount::find()->orderBy('account')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'account');
        $arrayData = ['' => 'เลือก'] + $arrData;
        return $arrayData;
    }

    public static function getMemberAccountNews($select ="id")
    {
        $model = MemberAccount::find()->orderBy('account')->asArray()->all();
        $arrData = ArrayHelper::map($model, $select, 'account');
        $arrayData = $arrData;
        return $arrayData;
    }

    public static function getMemberArea()
    {

        $model = MemberArea::find()->orderBy('area')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'area');
        $arrayData = ['' => 'เลือก'] + $arrData;
        return $arrayData;
    }

    public static function getMemberAreaNews($select = "id")
    {

        $model = MemberArea::find()->orderBy('area')->asArray()->all();
        $arrData = ArrayHelper::map($model, $select, 'area');
        $arrayData = $arrData;
        return $arrayData;
    }

    public static function getMemberLocationWork($id)
    {

        $model = LocationWork::find()->orderBy('id')->where(['member_id' => $id])->all();
        // \app\components\Helpers::Debig(( $model));
        $arrData =[];
        foreach ($model as $key => $e) {
            $arrData[$e->area_id] = $e->location_name;
        }

        return $arrData;
    }

    public static function getMemberLeaveCount()
    {

        $dataCount = MemberLeave::find()->where(['isActive' => 'W', 'isDelete' => 'N'])->count();

        return $dataCount;
    }

    public static function getAreaByBranchNo($brach_no)
    {

        $model = Area::find()->where(['brach_no' => $brach_no,'isDelete' => 'N'])->One();

        return $model;
    }


    public static function getAreaGroup($member_no)
    {

        $model = AreaGroup::find()->where(['keeper' => $member_no,'isDelete' => "N"])->all();

        return $model;
    }

    public static function getMemberLeaveCountById($id)
    {

        // \app\components\Helpers::Debig(( $id));
        $data = MemberLeave::find()->where(['member_id' => $id, 'isActive' => "Y"])->all();
        $countLeave= 0;

        foreach ($data as $key => $e) {
            // \app\components\Helpers::Debig(($e));
            $start = strtotime($e->start_date);
            $end = strtotime($e->end_date);
            $datediff = $end - $start;
            $day_ =  round($datediff / (60 * 60 * 24));
            $countLeave += $day_;
            // \app\components\Helpers::Debig(($day_));

            // switch ($e->leave_type_id) {
            //     case 1:
            //         // ลาพักร้อน
            //         $holidayLeft -= $day_;
            //         break;
            //     case 2:
            //         // ลากิจไม่คิดเงิน
            //         $errandLeft -= $day_;
            //         break;
            //     case 3:
            //         // ลากิจคิดเงิน
            //         $errandCutLeft -= $day_;
            //         break;
            //     case 4:
            //         // ลาป่วยไม่คิดเงิน
            //         $sickLeft -= $day_;
            //         break;
            //     case 5:
            //         // ลาป่วยคิดเงิน
            //         $sickCutLeft -= $day_;
            //         break;
            //     case 6:
            //         // ลาคลอด
            //         $maternityLeft -= $day_;
            //         break;
            //     case 7:
            //         // ลาบวช
            //         $ordainedLeft -= $day_;
            //         break;

            // }

        }


        return $countLeave;
    }

    public static function getMemberLeaveToday()
    {

        date_default_timezone_set("Asia/Bangkok");

        $today_old = Date("Y-m-d");
        // $today = (new DateTime("now", new DateTimeZone('Asia/Bangkok')))->format('Y-m-d H:i:s');

        $from = self::formatDate('Y-m-d 00:00:00',  date('Y-m-d H:i:s'));
        $to = self::formatDate('Y-m-d 23:59:59',  date('Y-m-d H:i:s'));
        // \app\components\Helpers::Debig( $today_old,false);
        // \app\components\Helpers::Debig(  $from);

        // $countLeave = MemberLeave::find()->where(['isDelete' => 'N'])->andWhere(['between', 'created_date', $from, $to])->andWhere(['<>', 'isActive', "N"])->groupBy(['id'])->count();
        $member_under_role = null;
        $position =null;
        // \app\components\Helpers::Debig(Yii::$app->user->identity->position_id);
// ทำไมต้องเป็น 0 ????
        if(Yii::$app->user->identity->auth_member_role == 1){
            if(Yii::$app->user->identity->status != 'X'){
                $position = Yii::$app->user->identity->position_id;
            }
            if(Yii::$app->user->identity->id){
                $member_under_role = Yii::$app->user->identity->id;
            }
        }

        if(Yii::$app->user->identity->position_id == 8){
            $member_under_role = null;
            $position =null;
        }

        $countLeave = MemberLeave::find()
            ->leftJoin('member', 'member.id = member_leave.member_id')
            ->select('count(*) as counters, member_id')
            ->where(['isDelete' => 'N'])
            ->andWhere(['between', 'created_date', $from, $to])
            ->andWhere(['!=','member.status','X'])
            ->andFilterWhere(['<>','member.id' , $member_under_role])
            // ->andFilterWhere(['member.position_id' => $position])
            // ->orFilterWhere(['member.member_under_id' => $member_under_role])
            ->andFilterWhere(['member.member_under_id' => $member_under_role])
            ->groupBy('member_id')
            ->createCommand()
            ->queryAll();

        // \app\components\Helpers::Debig(( count($countLeave)));

        return count($countLeave);
    }

    public static function getMemberDayOffToday()
    {

        date_default_timezone_set("Asia/Bangkok");

        $today = date('w');
        // $day_off = 0;
        $member_under_role = null;
        $position =null;

        if(Yii::$app->user->identity->auth_member_role == 1){
            if(Yii::$app->user->identity->status != 'X')
                $position = Yii::$app->user->identity->position_id;
            if(Yii::$app->user->identity->id)
                $member_under_role = Yii::$app->user->identity->id;
        }

        if(Yii::$app->user->identity->position_id == 8){
            $member_under_role = null;
            $position =null;
        }

        $modelMember = Member::find()
            ->where(['!=','status','X'])
            ->andFilterWhere(['<>','id' , $member_under_role])
            // ->andFilterWhere(['position_id' => $position])
            // ->orFilterWhere(['member_under_id' => $member_under_role])
            ->andFilterWhere(['member_under_id' => $member_under_role])
            ->all();
        // \app\components\Helpers::Debig(( count($modelMember)));

        $countDayOff = 0;
        foreach ($modelMember as $key => $e) {

            if (!empty($e->membersubtype)) {

                // $member_day_off_today;
                $member_day_monday = $e->membersubtype->monday;
                $member_day_thesday = $e->membersubtype->thesday;
                $member_day_wednesday = $e->membersubtype->wednesday;
                $member_day_thursday = $e->membersubtype->thursday;
                $member_day_friday = $e->membersubtype->friday;
                $member_day_saturday = $e->membersubtype->saturday;
                $member_day_sunday = $e->membersubtype->sunday;
                if ($member_day_monday == "Y") {
                    if ($today == 1) {
                        $countDayOff++;
                    }
                }
                if ($member_day_thesday == "Y") {
                    if ($today == 2) {
                        $countDayOff++;
                    }
                }
                if ($member_day_wednesday == "Y") {
                    if ($today == 3) {
                        $countDayOff++;
                    }
                }
                if ($member_day_thursday == "Y") {
                    if ($today == 4) {
                        $countDayOff++;
                    }
                }
                if ($member_day_friday == "Y") {
                    if ($today == 5) {
                        $countDayOff++;
                    }
                }
                if ($member_day_saturday == "Y") {
                    if ($today == 6) {
                        $countDayOff++;
                    }
                }
                if ($member_day_sunday == "Y") {
                    if ($today == 0) {
                        $countDayOff++;
                    }
                }

            }
            // \app\components\Helpers::Debig(( $member_day_monday));

        }
        // \app\components\Helpers::Debig(($modelMember));

        return $countDayOff;
    }

    public static function getMemberOtCount()
    {

        $dataCount = MemberOt::find()->where(['isActive' => 'W', 'isDelete' => 'N'])->count();

        return $dataCount;
    }

    public static function getMemberChangeTimeCount()
    {

        $dataCount = MemberChangeTime::find()->where(['isActive' => 'W', 'isDelete' => 'N'])->count();

        return $dataCount;
    }

    public static function getAreaByAccount($account, $modelLocationWork)
    {
        $model = Area::find()->where(['like', 'account', $account])->andWhere(['=', 'isDelete', "N"])->orderBy('local_name')->all();
        $html = '';
        foreach ($model as $key => $item) {
            $selected = '';
            if (!empty($modelLocationWork)) {
                foreach ($modelLocationWork as $keys => $values) {
                    if (!empty($values->area_id)) {

                        if ($values->area_id == trim($item->brach_no)) {
                            $selected = 'selected';
                        }
                    }

                }
            }
            $html .= "<option " . $selected . " value='" . $item->brach_no . "'>" . $item->area_name . "</option>";
        }

        return $html;
    }

    public static function getMemberChannel()
    {
        $model = MemberChannel::find()->orderBy('channel')->asArray()->all();
        $arrData = ArrayHelper::map($model, 'id', 'channel');
        $arrayData = ['' => 'เลือก'] + $arrData;
        return $arrayData;
    }

    public static function getMemberChannelNews($select = "id")
    {
        $model = MemberChannel::find()->orderBy('channel')->asArray()->all();
        $arrData = ArrayHelper::map($model, $select, 'channel');
        $arrayData = $arrData;
        return $arrayData;
    }

    public static function timespan($seconds = 1, $time = '')
    {
        // $model = Company::find()->One();
        // $seconds = strtotime($seconds . "+".$model->probation." days");
        $seconds = strtotime($seconds);
        if (!is_numeric($seconds)) {
            $seconds = 1;
        }

        if (!is_numeric($time)) {
            $time = time();
        }

        if ($time <= $seconds) {
            $seconds = 1;
        } else {
            $seconds = $time - $seconds;
        }

        $str = '';
        $years = floor($seconds / 31536000);

        if ($years > 0) {
            $str .= $years . ' ปี, ';
        }

        $seconds -= $years * 31536000;
        $months = floor($seconds / 2628000);

        if ($years > 0 or $months > 0) {
            if ($months > 0) {
                $str .= $months . ' เดือน, ';
            }

            $seconds -= $months * 2628000;
        }

        $weeks = floor($seconds / 604800);

        // if ($years > 0 OR $months > 0 OR $weeks > 0)
        // {
        //     if ($weeks > 0)
        //     {
        //         $str .= $weeks.' สัปดาห์, ';
        //     }

        //     $seconds -= $weeks * 604800;
        // }

        $days = floor($seconds / 86400);

        if ($months > 0 or $weeks > 0 or $days > 0) {
            if ($days > 0) {
                $str .= $days . ' วัน, ';
            }

            $seconds -= $days * 86400;
        }

        $hours = floor($seconds / 3600);
        if ($str == null) {
            $str = '1 วัน ,';
        }

        return substr(trim($str), 0, -1);
    }

    public static function formatDate($format, $date, $replace = '-')
    {
        if ($replace != '-') {
            $date = str_replace($replace, "-", $date);
        }
        return date($format, strtotime($date));
    }

    public static function isToday($current_date)
    {
        $today = (new DateTime("now", new DateTimeZone('Asia/Bangkok')))->format('Y-m-d');

        // \app\components\Helpers::Debig(($today),false);
        // \app\components\Helpers::Debig(($current_date));

        if ($today == $current_date) {
            // \app\components\Helpers::Debig("today");

            return true;
        } else {
            // \app\components\Helpers::Debig("no");

            return false;
        }
    }

    public static function getNormalHoliday($date,$start_work,$leave_type)
    {

        $model = Company::find()->One();
        $arrStartWork = explode('/',$start_work);
        $arrLeave = [];
        $probationStatus = 0;
        $now =  time(); // or your date as well
        $your_date = strtotime($date);
        $datediff = $now - $your_date;
        $datediff = round($datediff / (60 * 60 * 24));
        $monthProbation = abs($arrStartWork[1]);
        $errand_leave = 0;
        $hoilday_leave = 0;
        $probationDays = time() - strtotime($date . "+" . $model->probation . " days");
        if($leave_type == 'Y'){
            if($datediff < 366){
                $datediff = 366;
            }

            $monthProbation = 1;


            $probationStatus = 1;
        }

        // find in start date work
        if ($datediff < 366) {
            if ($probationDays > 0) {
                $probationStatus = 1;
                if ($monthProbation == 1 || $monthProbation == 2) {
                    $errand_leave = 6;
                } elseif ($monthProbation == 3 || $monthProbation == 4) {
                    $errand_leave = 5;
                } elseif ($monthProbation == 5 || $monthProbation == 6) {
                    $errand_leave = 4;
                } elseif ($monthProbation == 7 || $monthProbation == 8) {
                    $errand_leave = 3;
                } elseif ($monthProbation == 9 || $monthProbation == 10) {
                    $errand_leave = 2;
                } else {
                    $errand_leave = 1;
                }
            }

        } else {
            // 1 ปี
            $probationStatus = 1;
            $errand_leave = $model->errand_leave;

            if ($monthProbation == 1 || $monthProbation == 2) {
                $hoilday_leave = 6;
            } elseif ($monthProbation == 3 || $monthProbation == 4) {
                $hoilday_leave = 5;
            } elseif ($monthProbation == 5 || $monthProbation == 6) {
                $hoilday_leave = 4;
            } elseif ($monthProbation == 7 || $monthProbation == 8) {
                $hoilday_leave = 3;
            } elseif ($monthProbation == 9 || $monthProbation == 10) {
                $hoilday_leave = 2;
            } else {
                $hoilday_leave = 1;
            }
            if($datediff >= 731){
                $hoilday_leave = 6;
            }

            if ($datediff >= 1095) {
                if ($monthProbation <= 3) {
                    $hoilday_leave = 10;
                } elseif ($monthProbation > 3 && $monthProbation <= 6) {
                    $hoilday_leave = 9;
                } elseif ($monthProbation > 6 && $monthProbation <= 9) {
                    $hoilday_leave = 8;
                } else {
                    $hoilday_leave = 7;
                }
            }

            if($datediff >= 1462){
                $hoilday_leave = 10;
            }

            if ($datediff >= 1825) {
                if ($monthProbation <= 6) {
                    $hoilday_leave = 12;
                } else {
                    $hoilday_leave = 11;
                }
            }

            if($datediff >= 1830){
                $hoilday_leave = 12;
            }

        }

        $arrLeave = [
            'errand_leave' => $errand_leave,
            'hoilday_leave' => $hoilday_leave,
            'date_work_total' => $datediff,
            'probationStatus' => $probationStatus,
            'monthProbation' => $monthProbation,
            'total' => $errand_leave + $hoilday_leave,
            'start_work' => $start_work,
            'leave_type' => $leave_type
        ];



        return $arrLeave;

    }

}
