<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'path_slip' => 'shared/slip/',
    'path_member' => 'shared/member/',
    'path_area' => 'shared/location/',
    'path_company' => 'shared/company/',
    'path_news' => 'shared/news/',
    'path_area' => 'shared/area/',

];
